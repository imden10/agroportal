<?php


namespace App\Traits;

use App\Models\BannerStatisticViews;
use App\Models\BannerStatisticClicks;
use App\Models\BannerStatisticViewsUnique;
use App\Models\BannerStatisticViewsAll;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

trait Statistics
{
    private $lastErrorTime;
    private $lastVisitTime;
    private $dbLimit = 1000;
    private $getViewsByPeriodData = [];
    private $getViewsUniqueByPeriodData = [];
    private $getClickByPeriodData = [];
    private $getClickUniqueByPeriodData = [];

    private function isBot(&$botname = '',$user_agent = null)
    {
        /* Эта функция будет проверять, является ли посетитель роботом поисковой системы */
        $bots = cache()->rememberForever(
            'bots',
            function () {
                return \App\Models\BotExceptions::query()->pluck('name')->toArray();
            });
        // $bots = \App\Models\BotExceptions::query()->pluck('name')->toArray();

        $userAgent = $user_agent ?? null;

        if (!$userAgent)
            return true;

        foreach ($bots as $bot)
            if (stripos($userAgent, $bot) !== false) {
                $botname = $bot;
                return true;
            }
        return false;
    }

    public function postponeSetViews($data)
    {
        $str = json_encode($data) . PHP_EOL;
        file_put_contents(public_path() . '/banner_views.txt', $str, FILE_APPEND | LOCK_EX);
    }

    public function setViews($item_id, $ip, $date, $user_agent): void
    {
        $bot = '';
        if ($this->isBot($bot,$user_agent))
            return;

        BannerStatisticViews::updateOrCreate(
            [
                'ip'      => $ip,
                'date'    => $date,
                'item_id' => $item_id,
            ],
            [
                'ip'              => $ip,
                'count'           => DB::raw('count+1'),
                'date'            => $date,
                'http_user_agent' => $user_agent ?? null,
                'item_id'         => $item_id,
            ]
        );
    }

    public function setViewsMany($item_ids, $ip): void
    {
        if ($this->isBot())
            return;

        $date = Carbon::today()->format('Y-m-d');

        foreach ($item_ids as $item_id){
            BannerStatisticViews::updateOrCreate(
                [
                    'ip'      => $ip,
                    'date'    => $date,
                    'item_id' => $item_id,
                ],
                [
                    'ip'              => $ip,
                    'count'           => DB::raw('count+1'),
                    'date'            => $date,
                    'http_user_agent' => $_SERVER['HTTP_USER_AGENT'] ?? null,
                    'item_id'         => $item_id,
                ]
            );
        }
    }

    /**
     * Общее количество показов
     *
     * @return int
     */
    public function getViews($itemId): int
    {
        $count = 0;

        $visits = BannerStatisticViews::query()->where('item_id',$itemId)->get();

        foreach ($visits as $visit) {
            $count += $visit->count;
        }

        return $count;
    }

    /**
     * Общее количество показов за период
     *
     * @param $from
     * @param $to
     * @return array
     */
    public function getViewsByPeriod($itemId,$from, $to): array
    {
        $data = [];

        try {
            BannerStatisticViews::query()
                ->where('date', '>=', $from)
                ->where('date', '<=', $to)
                ->where('item_id',$itemId)
                ->chunk($this->dbLimit, function ($models) {
                    foreach ($models as $visit) {
                        if (!isset($this->getViewsByPeriodData[$visit->date])) {
                            $this->getViewsByPeriodData[$visit->date] = 0;
                        }

                        $this->getViewsByPeriodData[$visit->date] += $visit->count;
                    }
                });
        } catch (QueryException $e){
            dd($e->getMessage());
        }

        return $this->getViewsByPeriodData;
    }

    public function getViewsByPeriod2($itemId,$from, $to): array
    {
        $data = BannerStatisticViewsAll::query()
            ->where('date', '>=', $from)
            ->where('date', '<=', $to)
            ->where('item_id',$itemId)
            ->pluck('count','date')
            ->toArray();

        return $data;
    }

    public function getClickByPeriod($itemId,$from, $to): array
    {
        $data = [];

        $visits = BannerStatisticClicks::query()
            ->where('date', '>=', $from)
            ->where('date', '<=', $to)
            ->where('item_id',$itemId)
            ->chunk($this->dbLimit, function ($models) {
                foreach ($models as $visit) {
                    if (!isset($this->getClickByPeriodData[$visit->date])) {
                        $this->getClickByPeriodData[$visit->date] = 0;
                    }

                    $this->getClickByPeriodData[$visit->date] += $visit->count;
                }
            });

        return $this->getClickByPeriodData;
    }

    /**
     * Общее количество уникльных показов
     *
     * @return int
     */
    public function getViewsUnique($itemId): int
    {
        return BannerStatisticViews::query()
            ->where('item_id',$itemId)
            ->get()
            ->groupBy('ip')
            ->count();
    }

    /**
     * Количество уникльных показов за период
     *
     * @return int
     */
    public function getViewsUniqueByPeriod($itemId,$from, $to): array
    {
        $data = [];

        BannerStatisticViews::query()
            ->where('date', '>=', $from)
            ->where('date', '<=', $to)
            ->where('item_id',$itemId)
            ->chunk($this->dbLimit, function ($models, $i) {
                foreach ($models as $key => $visit) {
                    if(! isset($this->getViewsUniqueByPeriodData[$visit->date])){
                        $this->getViewsUniqueByPeriodData[$visit->date] = [[
                            'date' => $visit->date,
                            'ip' => $visit->ip,
                        ]];
                    } else {
                        $this->getViewsUniqueByPeriodData[$visit->date][] = [
                            'date' => $visit->date,
                            'ip' => $visit->ip,
                        ];
                    }
                }
            });

        foreach ($this->getViewsUniqueByPeriodData as $date) {
            $ips = [];
            foreach ($date as $item) {
                if (!isset($data[$item['date']])) {
                    $data[$item['date']] = 0;
                }

                if (!in_array($item['ip'], $ips)) {
                    $data[$item['date']]++;
                    $ips[] = $item['ip'];
                }
            }
        }

        return $data;
    }

    /**
     * Количество уникльных показов за период
     *
     * @return int
     */
    public function getViewsUniqueByPeriod2($itemId,$from,$to): array
    {
        $data = BannerStatisticViewsUnique::query()
            ->where('date', '>=', $from)
            ->where('date', '<=', $to)
            ->where('item_id',$itemId)
            ->pluck('count','date')
            ->toArray();

        return $data;
    }

    public function getClickUniqueByPeriod($itemId,$from, $to): array
    {
        $data = [];

        BannerStatisticClicks::query()
            ->where('date', '>=', $from)
            ->where('date', '<=', $to)
            ->where('item_id',$itemId)
            ->chunk($this->dbLimit, function ($models, $i) {
                foreach ($models as $key => $visit) {
                    if(! isset($this->getClickUniqueByPeriodData[$visit->date])){
                        $this->getClickUniqueByPeriodData[$visit->date] = [[
                            'date' => $visit->date,
                            'ip' => $visit->ip,
                        ]];
                    } else {
                        $this->getClickUniqueByPeriodData[$visit->date][] = [
                            'date' => $visit->date,
                            'ip' => $visit->ip,
                        ];
                    }
                }
            });


        foreach ($this->getClickUniqueByPeriodData as $date) {
            $ips = [];
            foreach ($date as $item) {
                if (!isset($data[$item['date']])) {
                    $data[$item['date']] = 0;
                }

                if (!in_array($item['ip'], $ips)) {
                    $data[$item['date']]++;
                    $ips[] = $item['ip'];
                }
            }
        }

        return $data;
    }

    /**
     * Количество показов за сегодня
     *
     * @return int
     */
    public function getViewsToDay(): int
    {
        $count = 0;

        $visits = BannerStatisticViews::query()
            ->where('date', Carbon::today()->format('Y-m-d'))
            ->get();

        foreach ($visits as $visit) {
            $count += $visit->count;
        }

        return $count;
    }

    /**
     * Количество уникальных показов за сегодня
     *
     * @return int
     */
    public function getViewsUniqueToDay(): int
    {
        return BannerStatisticViews::query()
            ->where('date', Carbon::today()->format('Y-m-d'))
            ->get()
            ->groupBy('ip')
            ->count();
    }

    public function setClick($item_id, $ip, $user_agent = null): void
    {
        $bot = '';
        if ($this->isBot($bot,$user_agent))
            return;

        $date = Carbon::today()->format('Y-m-d');

        BannerStatisticClicks::updateOrCreate(
            [
                'ip'      => $ip,
                'date'    => $date,
                'item_id' => $item_id,
            ],
            [
                'ip'              => $ip,
                'count'           => DB::raw('count+1'),
                'date'            => $date,
                'http_user_agent' => $_SERVER['HTTP_USER_AGENT'] ?? null,
                'item_id'         => $item_id,
            ]
        );
    }
}
