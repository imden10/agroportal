<?php

return [

    /*
    |------------------------------------------------------------------
    | View layout component
    |------------------------------------------------------------------
    */
    //    'view-layout' => 'admin::layout',
    'view-layout'  => 'admin::layout',

    /*
    |------------------------------------------------------------------
    | View layout title
    |------------------------------------------------------------------
    */
    'layout-title' => 'widgets::strings.layout.title',


    'widgets'           => [
        'telegram'            => \App\Modules\Widgets\Collections\Telegram\TelegramWidget::class,
        'steps'               => \App\Modules\Widgets\Collections\Steps\StepsWidget::class,
        'steps-socials'       => \App\Modules\Widgets\Collections\StepsSocials\StepsSocialsWidget::class,
        'press-kit'           => \App\Modules\Widgets\Collections\PressKit\PressKitWidget::class,
        'members'             => \App\Modules\Widgets\Collections\Members\MembersWidget::class,
        'ticker'              => \App\Modules\Widgets\Collections\Ticker\TickerWidget::class,
        'first-screen'        => \App\Modules\Widgets\Collections\FirstScreen\FirstScreenWidget::class,
        //        'first-screen-2' => \App\Modules\Widgets\Collections\FirstScreen2\FirstScreen2Widget::class,
        'footer-landing'      => \App\Modules\Widgets\Collections\FooterLanding\FooterLandingWidget::class,
        'incredible-villages' => \App\Modules\Widgets\Collections\IncredibleVillages\IncredibleVillagesWidget::class,
    ],

    /*
    |------------------------------------------------------------------
    | Permissions for manipulation widgets
    |------------------------------------------------------------------
    */
    'permissions'       => [
        'widgets.view'   => function ($user) {
            return true;
            //return $user->isSuperUser() || $user->hasPermission('update_setting');
        },
        'widgets.create' => function ($user) {
            return true;
            //return $user->isSuperUser() || $user->hasPermission('update_setting');
        },
        'widgets.update' => function ($user) {
            return true;
            //return $user->isSuperUser() || $user->hasPermission('update_setting');
        },
        'widgets.delete' => function ($user) {
            return true;
            //return $user->isSuperUser() || $user->hasPermission('update_setting');
        },
    ],

    /*
    |------------------------------------------------------------------
    | Middleware for settings
    |------------------------------------------------------------------
    */
    'middleware'        => ['web', 'auth', 'verified'],

    /*
    |------------------------------------------------------------------
    | Uri Route prefix
    |------------------------------------------------------------------
    */
    'uri_prefix'        => 'admin',

    /*
    |------------------------------------------------------------------
    | Route name prefix
    |------------------------------------------------------------------
    */
    'route_name_prefix' => 'admin.',

    /*
    |------------------------------------------------------------------
    | Request lang key
    |------------------------------------------------------------------
    */
    'request_lang_key'  => 'lang',

];
