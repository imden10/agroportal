<?php

namespace App\Modules\Widgets\Collections\FooterLanding;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class FooterLandingWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Футтер (Лендинг)';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.footer-landing.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type'  => 'text',
                'name'  => 'subscribe_title',
                'label' => 'Заголовок формы',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'subscribe_input_placeholder',
                'label' => 'Подсказка на инпуте',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'copyright',
                'label' => 'Копирайт',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
        ];
    }
}
