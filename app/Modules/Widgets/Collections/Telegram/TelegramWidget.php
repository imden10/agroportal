<?php

namespace App\Modules\Widgets\Collections\Telegram;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class TelegramWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Больше новостей на Телеграм канале';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.telegram.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type' => 'text',
                'name' => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'subtitle',
                'label' => 'Подзаголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'text',
                'name' => 'link',
                'label' => 'Ссылка',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ]
        ];
    }
}
