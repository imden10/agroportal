<?php

namespace App\Modules\Widgets\Collections\PressKit;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class PressKitWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'PressKit (страница реклама)';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.press-kit.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type' => 'text',
                'name' => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'step-list-socials',
                'name' => 'list',
                'label' => 'Шаги',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }
}
