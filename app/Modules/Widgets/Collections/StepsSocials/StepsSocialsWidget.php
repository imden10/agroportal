<?php

namespace App\Modules\Widgets\Collections\StepsSocials;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class StepsSocialsWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Читайте нас на зручних платформах (страница реклама)';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.steps-socials.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type' => 'step-list-socials',
                'name' => 'list',
                'label' => 'Шаги',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }
}
