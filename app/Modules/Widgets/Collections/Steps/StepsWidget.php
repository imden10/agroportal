<?php

namespace App\Modules\Widgets\Collections\Steps;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class StepsWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Чому обирають нас (страница реклама)';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.steps.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type' => 'text',
                'name' => 'title',
                'label' => 'Текст внизу',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type' => 'step-list',
                'name' => 'list',
                'label' => 'Шаги',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }
}
