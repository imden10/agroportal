<?php

namespace App\Modules\Widgets\Collections\FirstScreen;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class FirstScreenWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Первый экран с фоном (Лендинг)';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.first-screen.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type'  => 'text',
                'name'  => 'menu_title',
                'label' => 'Надпись на меню',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'title',
                'label' => 'Заголовок',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'editor',
                'name'  => 'text',
                'label' => 'Текст',
                'class' => '',
                'rules' => 'nullable|string|max:3000',
                'value' => '',
            ],
            [
                'type'  => 'image',
                'name'  => 'image',
                'label' => 'Фон',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'video',
                'name'  => 'video',
                'label' => 'Видео',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            ['separator' => 'Кнопка 1'],
            [
                'type'  => 'text',
                'name'  => 'btn_title',
                'label' => 'Надпись',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'btn_link',
                'label' => 'Произвольная ссылка',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'select',
                'name'  => 'btn_type',
                'label' => 'Внешний вид',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
                'list'  => function () {
                    return [
                        'large'  => 'Большая',
                        'middle' => 'Средняя',
                        'small'  => 'Маленькая'
                    ];
                }
            ],
            ['separator' => 'Кнопка 2'],
            [
                'type'  => 'text',
                'name'  => 'btn2_title',
                'label' => 'Надпись',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'text',
                'name'  => 'btn2_link',
                'label' => 'Произвольная ссылка',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
            ],
            [
                'type'  => 'select',
                'name'  => 'btn2_type',
                'label' => 'Внешний вид',
                'class' => '',
                'rules' => 'nullable|string|max:255',
                'value' => '',
                'list'  => function () {
                    return [
                        'large'  => 'Большая',
                        'middle' => 'Средняя',
                        'small'  => 'Маленькая'
                    ];
                }
            ],
        ];
    }
}
