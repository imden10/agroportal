<?php

namespace App\Modules\Widgets\Collections\Members;

use App\Modules\Widgets\Contracts\Widget as WidgetInterface;

class MembersWidget implements WidgetInterface
{
    /**
     * @var string
     */
    public static string $name = 'Members (страница про нас)';

    /**
     * @var array
     */
    public array $data;

    /**
     * Widget constructor.
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    public function execute()
    {
        return view('widgets::collections.members.index', [
            'data' => $this->data,
        ]);
    }

    /**
     * @return array
     */
    public function fields(): array
    {
        return [
            [
                'type' => 'members-list',
                'name' => 'list',
                'label' => 'Список',
                'class' => '',
                'rules' => 'nullable|array',
                'value' => [],
            ],
        ];
    }

    public function adapter($data, $lang)
    {
        $list = [];

        foreach ($data['list'] as $item){
            $list[] = $item;
        }

        $data['list'] = $list;

        return $data;
    }
}
