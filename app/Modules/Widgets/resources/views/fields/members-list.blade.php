<?php
$bloggers = \App\Models\Bloggers::query()->active()->get();
?>

<div class="form-group">
    <label for="widget{{ studly_case($field['name']) }}">{{ $field['label'] }}</label>

    <div class="input-group mb-3">
        <div style="display: none;">
            <div data-item-id="#dynamicListPlaceholder" class="item-template item-group border border-grey-light">
                <div class="row align-items-center" style="padding: 10px">
                    <div class="col-md-3">
                        <label>Изображение</label>
                        {{ media_preview_box($field['name'] . '[#dynamicListPlaceholder][image]') }}
                        {{ media_preview_box($field['name'] . '[#dynamicListPlaceholder][image_mob]') }}
                    </div>

                    <div class="col-md-9">
                        <input type="text" name="{{ $field['name'] }}[#dynamicListPlaceholder][name]" placeholder="Имя"
                               class="form-control mb-1" disabled>
                        <input type="text" name="{{ $field['name'] }}[#dynamicListPlaceholder][position]"
                               placeholder="Должность" class="form-control mb-1" disabled>
                        <input type="text" name="{{ $field['name'] }}[#dynamicListPlaceholder][email]"
                               placeholder="E-mail" class="form-control mb-1" disabled>
                        <textarea name="{{ $field['name'] }}[#dynamicListPlaceholder][text]" placeholder="Текст"
                                  class="summernote form-control" disabled></textarea>
                        <h5>Соц. сети</h5>
                        <input type="text" name="{{ $field['name'] }}[#dynamicListPlaceholder][soc_facebook]"
                               placeholder="Facebook" class="form-control mb-1" disabled>
                        <input type="text" name="{{ $field['name'] }}[#dynamicListPlaceholder][soc_instagram]"
                               placeholder="Instagram" class="form-control mb-1" disabled>
                        <input type="text" name="{{ $field['name'] }}[#dynamicListPlaceholder][soc_telegram]"
                               placeholder="Telegram" class="form-control mb-1" disabled>


                    </div>
                </div>

                <div class="row" style="padding: 10px">
                    <div class="col-md-12">
                        <button type="button" class="btn btn-danger remove-item float-right text-white">Удалить</button>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" name="{{ $field['name'] }}" value="">

        <div class="items-container w-100">
            @foreach((array) old($field['name'], $value) as $key => $value)
                <div data-item-id="{{ $key }}" class="item-template item-group border border-grey-light">
                    <div class="row align-items-center" style="padding: 10px">
                        <div class="col-md-3">
                            <label>Изображение</label>
                            {{ media_preview_box($field['name'] . '[' . $key . '][image]', $value['image'] ?? null, $errors) }}
                            {{ media_preview_box($field['name'] . '[' . $key . '][image_mob]', $value['image_mob'] ?? null, $errors) }}
                        </div>

                        <div class="col-md-9">
                            <input type="text" name="{{ $field['name'] }}[{{ $key }}][name]" placeholder="Имя"
                                   value="{{ old($field['name'] . '.' . $key . '.name', $value['name'] ?? '') }}"
                                   class="form-control mb-1">
                            <input type="text" name="{{ $field['name'] }}[{{ $key }}][position]" placeholder="Должность"
                                   value="{{ old($field['name'] . '.' . $key . '.position', $value['position'] ?? '') }}"
                                   class="form-control mb-1">
                            <input type="text" name="{{ $field['name'] }}[{{ $key }}][email]" placeholder="E-mail"
                                   value="{{ old($field['name'] . '.' . $key . '.email', $value['email'] ?? '') }}"
                                   class="form-control mb-1">
                            <textarea name="{{ $field['name'] }}[{{ $key }}][text]" placeholder="Текст"
                                      class="summernote form-control">{{ old($field['name'] . '.' . $key . '.text', $value['text'] ?? '') }}</textarea>
                            <h5>Соц. сети</h5>
                            <input type="text" name="{{ $field['name'] }}[{{ $key }}][soc_facebook]"
                                   placeholder="Facebook"
                                   value="{{ old($field['name'] . '.' . $key . '.soc_facebook', $value['soc_facebook'] ?? '') }}"
                                   class="form-control mb-1">
                            <input type="text" name="{{ $field['name'] }}[{{ $key }}][soc_instagram]"
                                   placeholder="Instagram"
                                   value="{{ old($field['name'] . '.' . $key . '.soc_instagram', $value['soc_instagram'] ?? '') }}"
                                   class="form-control mb-1">
                            <input type="text" name="{{ $field['name'] }}[{{ $key }}][soc_telegram]"
                                   placeholder="Telegram"
                                   value="{{ old($field['name'] . '.' . $key . '.soc_telegram', $value['soc_telegram'] ?? '') }}"
                                   class="form-control mb-1">

                        </div>
                    </div>

                    <div class="row" style="padding: 10px">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-danger remove-item float-right text-white">Удалить
                            </button>
                        </div>
                    </div>

                    @error($field['name'] . '.' . $key)
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            @endforeach
        </div>
    </div>

    <button type="button" class="btn btn-info btn-sm add-item-{{ studly_case($field['name']) }}">Добавить</button>
</div>

@push('styles')
    <link rel="stylesheet" href="{{asset('matrix/libs/select2/dist/css/select2.min.css')}}">
@endpush

@push('scripts')
    <script src="{{asset('matrix/libs/select2/dist/js/select2.min.js')}}"></script>
    <script type="text/javascript">

        $(document).on('click', '.add-item-{{ studly_case($field['name']) }}', function () {
            const parent = $(this).parent();
            const template = parent.find('.item-template');
            const container = parent.find('.items-container');

            create_item(template, container, '#dynamicListPlaceholder');

            container.find('input, textarea').prop('disabled', false);

            container.find('textarea').each(function () {
                if ($(this).hasClass('summernote')) {
                    $(this).summernote(summernote_options);
                }
            });

            container.find('.select2-field').each(function () {
                $(this).select2({});
            });
        });

        $('.items-container').find('textarea').each(function () {
            if ($(this).hasClass('summernote')) {
                $(this).summernote(summernote_options);
            }
        });

        $(document).on('click', '.remove-item', function () {
            $(this).parents('.item-group').remove();
        });
    </script>
@endpush
