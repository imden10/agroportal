<div class="form-group">
    <label for="widget{{ studly_case($field['name']) }}">{{ $field['label'] }}</label>

    <div class="input-group mb-3">
        <div style="display: none;">
            <div data-item-id="#dynamicListPlaceholder" class="item-template border border-grey-light">
                <div class="row align-items-center" style="padding: 10px">
                    <div class="col-md-3">
                        <label>Изображение</label>
                        {{ media_preview_box($field['name'] . '[#dynamicListPlaceholder][image]') }}
                    </div>

                    <div class="col-md-9">
                        <input type="text" name="{{ $field['name'] }}[#dynamicListPlaceholder][text]" placeholder="Текст" class="form-control mb-1" disabled>
                        <input type="text" name="{{ $field['name'] }}[#dynamicListPlaceholder][count]" placeholder="Количество" class="form-control mb-1" disabled>
                        <input type="text" name="{{ $field['name'] }}[#dynamicListPlaceholder][link]" placeholder="Ссылка" class="form-control mb-1" disabled>
                    </div>
                </div>

                <div class="row" style="padding: 10px">
                    <div class="col-md-12">
                        <button type="button" class="btn btn-danger remove-item float-right text-white">Удалить</button>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" name="{{ $field['name'] }}" value="">

        <div class="items-container w-100">
            @foreach((array) old($field['name'], $value) as $key => $value)
                <div data-item-id="{{ $key }}" class="item-template border border-grey-light">
                    <div class="row align-items-center" style="padding: 10px">
                        <div class="col-md-3">
                            <label>Изображение</label>
                            {{ media_preview_box($field['name'] . '[' . $key . '][image]', $value['image'] ?? null, $errors) }}
                        </div>

                        <div class="col-md-9">
                            <input type="text" name="{{ $field['name'] }}[{{ $key }}][text]" placeholder="Текст" value="{{ old($field['name'] . '.' . $key . '.text', $value['text'] ?? '') }}" class="form-control mb-1">
                            <input type="text" name="{{ $field['name'] }}[{{ $key }}][count]" placeholder="Количество" value="{{ old($field['name'] . '.' . $key . '.count', $value['count'] ?? '') }}" class="form-control mb-1">
                            <input type="text" name="{{ $field['name'] }}[{{ $key }}][link]" placeholder="Ссылка" value="{{ old($field['name'] . '.' . $key . '.link', $value['link'] ?? '') }}" class="form-control mb-1">
                        </div>
                    </div>

                    <div class="row" style="padding: 10px">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-danger remove-item float-right text-white">Удалить</button>
                        </div>
                    </div>

                    @error($field['name'] . '.' . $key)
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            @endforeach
        </div>
    </div>

    <button type="button" class="btn btn-info btn-sm add-item-{{ studly_case($field['name']) }}">Добавить</button>
</div>

@push('scripts')
    <script type="text/javascript">
        $(document).on('click', '.add-item-{{ studly_case($field['name']) }}', function () {
            const parent = $(this).parent();
            const template = parent.find('.item-template');
            const container = parent.find('.items-container');

            create_item(template, container, '#dynamicListPlaceholder');

            container.find('input, textarea').prop('disabled', false);

            container.find('textarea').each(function () {
                if ($(this).hasClass('summernote')) {
                    $(this).summernote(summernote_options);
                }
            });
        });

        $('.items-container').find('textarea').each(function () {
            if ($(this).hasClass('summernote')) {
                $(this).summernote(summernote_options);
            }
        });

        $(document).on('click', '.remove-item', function () {
            $(this).parents('.item-group').remove();
        });
    </script>
@endpush
