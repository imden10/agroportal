@include('constructor::layouts.header',['lang' => $lang])

<div id="collapse{{ $key }}_{{$lang}}" class="card-body mt-1 collapse show">
    <div class="row">
        <div class="col-4 input-group-sm mb-3">
            <input type="text" placeholder="{{ trans($params['labels']['anker_title']) }}" class="form-control @error(constructor_field_name_dot($key, 'content.anker_title')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.anker_title') }}" value="{{ old(constructor_field_name_dot($key, 'content.anker_title'), $content['anker_title'] ?? '') }}">

            @error(constructor_field_name_dot($key, 'content.anker_title'))
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-4 input-group-sm mb-3">
            <div class="input-group mb-3 ps-elem">
                <div class="input-group-prepend" style="height: 30px;">
                    <div class="input-group-text">
                        <input type="checkbox" @if(($content['anker_link'] ?? '') !== '') checked @endif onclick="$(this).closest('.ps-elem').find('.input-ps').toggleClass('hide')">
                    </div>
                    <span class="input-group-text">Произвольная ссылка</span>
                </div>
                <input type="text" name="{{ constructor_field_name($key, 'content.anker_link') }}" value="{{ old(constructor_field_name_dot($key, 'content.anker_link'), $content['anker_link'] ?? '') }}"  class="form-control input-ps @if(($content['anker_link'] ?? '') === '') hide @endif" style="height: 30px">
            </div>
        </div>

        <div class="col-4 input-group-sm mb-3">
            <div class="input-group mb-3 ps-elem">
                <div class="input-group-prepend" style="height: 30px;">
                    <div class="input-group-text">
                        <input type="checkbox" name="{{ constructor_field_name($key, 'content.anker_top') }}" value="1"  @if(($content['anker_top'] ?? '') !== '') checked @endif>
                    </div>
                    <span class="input-group-text">В топ</span>
                </div>
            </div>
        </div>

        <div class="col-12 input-group-sm mb-3">
            <input type="text" placeholder="{{ trans($params['labels']['title']) }}" class="form-control @error(constructor_field_name_dot($key, 'content.title')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.title') }}" value="{{ old(constructor_field_name_dot($key, 'content.title'), $content['title'] ?? '') }}">

            @error(constructor_field_name_dot($key, 'content.title'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-6 input-group-sm mb-3">
            <label>{{ $params['labels']['width'] }}</label>
            <select class="form-control @error(constructor_field_name_dot($key, 'content.width')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.width') }}">
                @foreach($params['width'] as $listKey => $litItem)
                    <option value="{{ $listKey }}" @if (old(constructor_field_name_dot($key, 'content.width'), $content['width'] ?? '') == $listKey) selected @endif>{{ $litItem }}</option>
                @endforeach
            </select>

            @error(constructor_field_name_dot($key, 'content.anker_title'))
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="d-flex col-12">
            <div>
                {{ media_preview_box(constructor_field_name($key, 'content.image'), $content['image'] ?? null, $errors) }}
            </div>
        </div>

        <div class="d-flex col-12">
            <div>
                {{ file_preview_box(constructor_field_name($key, 'content.video'), $content['video'] ?? null, $errors) }}
            </div>
        </div>
    </div>
</div>

@include('constructor::layouts.footer')
