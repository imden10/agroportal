@include('constructor::layouts.header',['lang' => $lang])

<div id="collapse{{ $key }}_{{$lang}}" class="card-body mt-1 collapse show">
    <div class="row">
        <div class="col-12">
            <div class="form-group input-group-sm mb-12">
                <input type="text" placeholder="{{ $params['labels']['title'] }}" class="form-control @error(constructor_field_name_dot($key, 'content.title')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.title') }}" value="{{ old(constructor_field_name_dot($key, 'content.title'), $content['title'] ?? '') }}">


                @error(constructor_field_name_dot($key, 'content.title'))
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="input-group">
            <div style="display: none;">
                <div data-item-id="#imageInputPlaceholder1" class="list-list-template item-group col-6">
                    <div class="m-1 border border-grey-light p-1">
                        <textarea class="form-control new_sammernote" name="{{ constructor_field_name($key, 'content.list') }}[#imageInputPlaceholder1][item]" placeholder="{{ $params['labels']['item'] }}" disabled></textarea>
                        <button type="button" class="btn btn-danger remove-item text-white">Удалить</button>
                    </div>
                </div>
            </div>

            <input type="hidden" name="{{ constructor_field_name($key, 'content.list') }}" value="">


            <div class="list-list-container w-100 row">
                @foreach((array) old(constructor_field_name($key, 'content.list'), $content['list'] ?? []) as $k => $value)
                    <div data-item-id="{{ $k }}" class="item-template item-group col-6">
                        <div class="m-1 border border-grey-light p-1">
                            <textarea class="form-control summernote" name="{{ constructor_field_name($key, 'content.list') }}[{{ $k }}][item]">{{ $value['item'] ?? '' }}</textarea>
                            <button type="button" class="btn btn-danger remove-item text-white">Удалить</button>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <button type="button" class="btn btn-info btn-sm add-list-list-item_{{$lang}} d-block mt-2">Добавить</button>
    </div>
</div>

@include('constructor::layouts.footer')
