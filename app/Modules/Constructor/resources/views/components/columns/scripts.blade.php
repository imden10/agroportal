<script type="text/javascript">
    (function () {
        const second_summernote_options = {
            lang: 'ru-RU',
            height: 250,
            minHeight: null,
            maxHeight: null,
            toolbar: [
                ['undoredo', ['undo','redo']],
                ['style', ['style']],
                ['font', ['bold','italic', 'underline', 'strikethrough', 'superscript', 'subscript', 'clear']],
                ['fontname', ['fontname']],
                ['fontsize', ['fontsize']],
                ['height', ['height']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table', 'specialchars']],
                ['insert', ['link', 'hr']],
                ['view', ['fullscreen', 'codeview']],
                ['popovers', ['img','myVideo']],
                ['typography', ['typography']],
            ],
            buttons: {
                img: LFMButton,
                myVideo: VideoButton,
                typography:ReplaceButton
            },
            colors: [
                ['#75AD40', '#66B21E', '#A5BE8C','#CEE8B5','#555555','#111325','#31333C','#3C3F49'], //first line of colors
                ['#FFFFFF','#F6F5FA','#DEDDE7','#8A8F9D','#555861','#DCDCE5','#FF8B8B','#DB4343'], //second line of colors
                ['#039BE5','#4F80FF', '#ffff00', '#FF0000'] //second line of colors
            ],
        };

        $(document).on('click', '.add-list-list-item_{{$lang}}', function () {
            const template = $(this).parent().find('.list-list-template');
            const container = $(this).parent().find('.list-list-container');

            create_item(template, container, '#imageInputPlaceholder1');

            container.find('input, textarea').prop('disabled', false);
            container.find('textarea').each(function () {
                        if ($(this).hasClass('new_sammernote')) {
                            $(this).summernote(second_summernote_options);
                        }
                    });
        });

        $(document).on('click', '.remove-item', function () {
            $(this).parents('.item-group').remove();
        });
    })();
</script>
