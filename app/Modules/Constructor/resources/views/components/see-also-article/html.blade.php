@include('constructor::layouts.header',['lang' => $lang])

<div id="collapse{{ $key }}_{{$lang}}" class="card-body mt-1 collapse show">
    <div class="row">
        <div class="col-6 input-group-sm mb-3">
            <input id="{{ $key }}_anker_title" type="text" name="{{ constructor_field_name($key, 'content.anker_title') }}" placeholder="{{ $params['labels']['anker_title'] }}" value="{{ old(constructor_field_name_dot($key, 'content.anker_title'), $content['anker_title'] ?? '') }}" class="form-control ">
        </div>

        <div class="col-6 input-group-sm mb-3">
            <div class="input-group mb-3 ps-elem">
                <div class="input-group-prepend" style="height: 30px;">
                    <div class="input-group-text">
                        <input type="checkbox" @if(($content['anker_link'] ?? '') !== '') checked @endif onclick="$(this).closest('.ps-elem').find('.input-ps').toggleClass('hide')">
                    </div>
                    <span class="input-group-text">Произвольная ссылка</span>
                </div>
                <input type="text" name="{{ constructor_field_name($key, 'content.anker_link') }}" value="{{ old(constructor_field_name_dot($key, 'content.anker_link'), $content['anker_link'] ?? '') }}"  class="form-control input-ps @if(($content['anker_link'] ?? '') === '') hide @endif" style="height: 30px">
            </div>
        </div>

        <div class="col-12">
            <div class="form-group mb-1">
                <label for="anker_title">{{ $params['labels']['title'] }}</label>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-link"></i></span>
                    </div>
                    <input id="{{ $key }}_title" type="text" name="{{ constructor_field_name($key, 'content.title') }}" placeholder="{{ $params['labels']['title'] }}" value="{{ old(constructor_field_name_dot($key, 'content.title'), $content['title'] ?? '') }}" class="form-control ">
                </div>
            </div>
        </div>

        <div class="input-group">
            <div style="display: none;">
                <div data-item-id="#imageInputPlaceholder1" class="see-also-list-template item-group m-1 border border-grey-light p-1 d-flex align-items-center">
                    <div class="col-10">
                        <select name="{{ constructor_field_name($key, 'content.list') }}[#imageInputPlaceholder1][article_id]" class="select2-field2">
                            <option value="">{{ $params['labels']['title'] }}</option>
                        </select>
                    </div>

                    <div class="col-2">
                        <button type="button" class="btn btn-danger remove-item float-right text-white">Удалить</button>
                    </div>
                </div>
            </div>

            <input type="hidden" name="{{ constructor_field_name($key, 'content.list') }}" value="">

            <div class="see-also-list-container w-100">
                @foreach((array) old(constructor_field_name($key, 'content.list'), $content['list'] ?? []) as $k => $value)
                    <div data-item-id="{{ $k }}" class="item-template item-group m-1 border border-grey-light p-1 d-flex align-items-center">
                        <div class="col-10">
                            <?php
                                $val = $value['article_id'] ?? '';
                                $selArt = \App\Models\BlogArticles::query()->where('id',$val)->first();
                            ?>
                            <select name="{{ constructor_field_name($key, 'content.list') }}[{{ $k }}][article_id]" class="select2-field2-shown">
                                @if($selArt)
                                    <option value="{{$val}}">{{$selArt->name}}</option>
                                @endif
                            </select>
                        </div>

                        <div class="col-2">
                            <button type="button" class="btn btn-danger remove-item text-white float-right">Удалить</button>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <button type="button" class="btn btn-info btn-sm add-see-also-list-item_{{$lang}} d-block mt-2">Добавить</button>
    </div>
</div>

@include('constructor::layouts.footer')
