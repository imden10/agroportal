@include('constructor::layouts.header',['lang' => $lang])

<div id="collapse{{ $key }}_{{$lang}}" class="card-body mt-1 collapse show">
    <div class="row">
        <div class="col-12">
            <div class="form-group mb-1">
                <label for="anker_title">{{ $params['labels']['title'] }}</label>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-link"></i></span>
                    </div>
                    <input id="{{ $key }}_title" type="text" name="{{ constructor_field_name($key, 'content.title') }}" placeholder="{{ $params['labels']['title'] }}" value="{{ old(constructor_field_name_dot($key, 'content.title'), $content['title'] ?? '') }}" class="form-control ">
                </div>
            </div>

            <div class="form-group mb-1">
                <select {{ empty(old(constructor_field_name($key, 'content.rows'), $content['rows'] ?? [])) ? '' : 'disabled' }}
                        class="form-control @error(constructor_field_name_dot($key, 'content.table_width')) is-invalid @enderror item-row-template-count"
                        name="{{ constructor_field_name($key, 'content.table_width') }}">
                    {{-- <option value="">{{ $params['labels']['table_width'] }}</option> --}}
                    @foreach ($params['table_width'] as $slug => $position)
                        <option value="{{ $slug }}" @if (old(constructor_field_name_dot($key, 'content.table_width'), $content['table_width'] ?? '') == $slug) selected @endif>{{ $position }} {{ $params['labels']['table_width'] }}</option>
                    @endforeach
                </select>
                @if(!empty(old(constructor_field_name($key, 'content.rows'), $content['rows'] ?? [])))
                    <input type="hidden" name="{{ constructor_field_name($key, 'content.table_width') }}" value="{{ old(constructor_field_name_dot($key, 'content.table_width'), $content['table_width'] ?? '') }}">
                @endif
                @error(constructor_field_name_dot($key, 'content.table_width'))
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="col-12">
            <div class="input-group">
                <div style="display: none;">
                    @foreach ($params['table_width'] as $col_count => $width)
                        <div data-item-id="#blogTableRowPlaceholder{{ $col_count }}"
                             class="item-row-template{{ $col_count }} item-group mb-1 bg-light border border-dark p-1 col-12">
                            <table class="table table-hover text-nowrap">
                                <tbody>
                                <tr>
                                    @for($i = 0; $i  < $col_count; $i++)
                                        <td>
                                            <div class="form-group mb-1">
                                                <label>{{ $params['labels']['column_width'] }}</label>
                                                <select
                                                    class="form-control item-row-template-count"
                                                    name="{{ constructor_field_name($key, 'content.rows') }}[#blogTableRowPlaceholder{{ $col_count }}][{{ $i }}][column_width]">
                                                    {{-- <option value="">{{ $params['labels']['column_width'] }}</option> --}}
                                                    @foreach ($params['cols_width'] as $w_value => $w_text)
                                                        <option value="{{ $w_value }}">{{ $w_text }}</option>
                                                    @endforeach
                                                </select>

                                            </div>

                                            <div class="form-group mb-1">
                                                <label>{{ $params['labels']['column_text'] }}<button type="button" class="btn btn-succes btn-sm edit-item ml-1"><i class="fas fa-edit text-green"></i></button><button type="button" class="btn btn-succes btn-sm edit-item-del"><i class="fa fa-window-close text-red"></i></button></label>
                                                <textarea
                                                    class="form-control small_summernote"
                                                    name="{{ constructor_field_name($key, 'content.rows') }}[#blogTableRowPlaceholder{{ $col_count }}][{{ $i }}][column_text]"></textarea>
                                            </div>
                                        </td>
                                    @endfor
                                </tr>
                                </tbody>
                            </table>
                            <div class="mt-1 text-center">
                                <button type="button" class="btn btn-outline-danger btn-sm remove-row-item">Удалить</button>
                            </div>
                        </div>
                    @endforeach
                    {{-- Mob.rows --}}
                    @foreach ($params['table_width'] as $col_count => $width)
                        <div data-item-id="#blogTableRowPlaceholder{{ $col_count }}"
                             class="item-mob_row-template{{ $col_count }} item-group mb-1 bg-light border border-dark p-1 col-12">
                            <table class="table table-hover text-nowrap">
                                <tbody>
                                <tr>
                                    @for($i = 0; $i  < $col_count; $i++)
                                        <td>
                                            <div class="form-group mb-1">
                                                <label>{{ $params['labels']['column_width'] }}</label>
                                                <select
                                                    class="form-control item-row-template-count"
                                                    name="{{ constructor_field_name($key, 'content.mob_rows') }}[#blogTableRowPlaceholder{{ $col_count }}][{{ $i }}][column_width]">
                                                    {{-- <option value="">{{ $params['labels']['column_width'] }}</option> --}}
                                                    @foreach ($params['cols_width'] as $w_value => $w_text)
                                                        <option value="{{ $w_value }}">{{ $w_text }}</option>
                                                    @endforeach
                                                </select>

                                            </div>

                                            <div class="form-group mb-1">
                                                <label>{{ $params['labels']['column_text'] }}<button type="button" class="btn btn-succes btn-sm edit-item ml-1"><i class="fas fa-edit text-green"></i></button><button type="button" class="btn btn-succes btn-sm edit-item-del"><i class="fa fa-window-close text-red"></i></button></label>
                                                <textarea
                                                    class="form-control small_summernote"
                                                    name="{{ constructor_field_name($key, 'content.mob_rows') }}[#blogTableRowPlaceholder{{ $col_count }}][{{ $i }}][column_text]"></textarea>
                                            </div>
                                        </td>
                                    @endfor
                                </tr>
                                </tbody>
                            </table>
                            <div class="mt-1 text-center">
                                <button type="button" class="btn btn-outline-danger btn-sm remove-row-item">Удалить</button>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>

            <input type="hidden" name="{{ constructor_field_name($key, 'content.rows') }}" value="">
            <div class="items-row-container d-flex justify-content-center flex-wrap row">
                @foreach ((array) old(constructor_field_name($key, 'content.rows'), $content['rows'] ?? []) as $k => $value)
                    @if($value)
                        <div data-item-id="{{ $k }}"
                             class="item-template item-group mb-1 bg-light border border-dark p-1 col-12">

                            <table class="table table-hover text-nowrap">
                                <tbody>
                                <tr>
                                    @foreach((array) $value as $col => $text)
                                        <td>
                                            <div class="form-group mb-1">
                                                <label>{{ $params['labels']['column_width'] }}</label>
                                                <select
                                                    class="form-control item-row-template-count"
                                                    name="{{ constructor_field_name($key, 'content.rows') }}[{{ $k }}][{{ $col }}][column_width]">
                                                    {{-- <option value="">{{ $params['labels']['column_width'] }}</option> --}}
                                                    @foreach ($params['cols_width'] as $w_value => $w_text)
                                                        <option value="{{ $w_value }}" @if (old(constructor_field_name_dot($key, 'content.rows') . '.{{ $k }}.{{ $col }}.column_width', $text['column_width'] ?? '') == $w_value) selected @endif>{{ $w_text }}</option>
                                                    @endforeach
                                                </select>
                                                @error(constructor_field_name_dot($key, 'content.rows') . '.' . $k . '.' . $col . '.column_width')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group mb-1">
                                                <label>{{ $params['labels']['column_text'] }}<button type="button" class="btn btn-succes btn-sm edit-item ml-1"><i class="fas fa-edit text-green"></i></button><button type="button" class="btn btn-succes btn-sm edit-item-del"><i class="fa fa-window-close text-red"></i></button></label>
                                                <textarea
                                                    class="form-control small_summernote @error(constructor_field_name_dot($key, 'content.rows') . '.{{ $k }}.{{ $col }}.column_text') is-invalid @enderror"
                                                    name="{{ constructor_field_name($key, 'content.rows') }}[{{ $k }}][{{ $col }}][column_text]">{{ $text['column_text'] ?? '' }}</textarea>

                                                @error(constructor_field_name_dot($key, 'content.rows') . '.' . $k . '.' . $col . '.column_text')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </td>
                                    @endforeach
                                </tr>
                                </tbody>
                            </table>
                            <div class="mt-1 text-center">
                                <button type="button" class="btn btn-outline-danger btn-sm remove-row-item">Удалить</button>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>

            <hr>
            <h4 class="text-center">Mob. Table</h4>
            <div class="d-flex justify-content-center flex-wrap row">
                <div class="col-12">
                    <div class="form-group mb-1">
                        <select disabled {{ empty(old(constructor_field_name($key, 'content.mob_rows'), $content['mob_rows'] ?? [])) ? '' : 'disabled' }}
                                class="form-control @error(constructor_field_name_dot($key, 'content.table_mob_width')) is-invalid @enderror item-mob_row-template-count"
                                name="{{ constructor_field_name($key, 'content.table_mob_width') }}">
                            {{-- <option value="">{{ $params['labels']['table_width'] }}</option> --}}
                            @foreach ($params['table_width'] as $slug => $position)
                                <option value="{{ $slug }}" @if (old(constructor_field_name_dot($key, 'content.table_mob_width'), $content['table_mob_width'] ?? '') == $slug) selected @endif>{{ $position }} {{ $params['labels']['table_width'] }}</option>
                            @endforeach
                        </select>
                        @if(!empty(old(constructor_field_name($key, 'content.mob_rows'), $content['mob_rows'] ?? [])))
                            <input type="hidden" name="{{ constructor_field_name($key, 'content.table_mob_width') }}" value="{{ old(constructor_field_name_dot($key, 'content.table_mob_width'), $content['table_mob_width'] ?? '') }}">
                        @endif
                        @error(constructor_field_name_dot($key, 'content.table_mob_width'))
                        <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
            </div>

            <input type="hidden" name="{{ constructor_field_name($key, 'content.mob_rows') }}" value="">
            <div class="items-mob_row-container d-flex justify-content-center flex-wrap row">
                @foreach ((array) old(constructor_field_name($key, 'content.mob_rows'), $content['mob_rows'] ?? []) as $k => $value)
                    @if($value)
                        <div data-item-id="{{ $k }}"
                             class="item-template item-group mb-1 bg-light border border-dark p-1 col-12">

                            <table class="table table-hover text-nowrap">
                                <tbody>
                                <tr>
                                    @foreach((array) $value as $col => $text)
                                        <td>
                                            <div class="form-group mb-1">
                                                <label>{{ $params['labels']['column_width'] }}</label>
                                                <select
                                                    class="form-control item-row-template-count"
                                                    name="{{ constructor_field_name($key, 'content.mob_rows') }}[{{ $k }}][{{ $col }}][column_width]">
                                                    {{-- <option value="">{{ $params['labels']['column_width'] }}</option> --}}
                                                    @foreach ($params['cols_width'] as $w_value => $w_text)
                                                        <option value="{{ $w_value }}" @if (old(constructor_field_name_dot($key, 'content.mob_rows') . '.{{ $k }}.{{ $col }}.column_width', $text['column_width'] ?? '') == $w_value) selected @endif>{{ $w_text }}</option>
                                                    @endforeach
                                                </select>
                                                @error(constructor_field_name_dot($key, 'content.mob_rows') . '.' . $k . '.' . $col . '.column_width')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>

                                            <div class="form-group mb-1">
                                                <label>{{ $params['labels']['column_text'] }}<button type="button" class="btn btn-succes btn-sm edit-item ml-1"><i class="fas fa-edit text-green"></i></button><button type="button" class="btn btn-succes btn-sm edit-item-del"><i class="fa fa-window-close text-red"></i></button></label>
                                                <textarea
                                                    class="form-control small_summernote @error(constructor_field_name_dot($key, 'content.mob_rows') . '.{{ $k }}.{{ $col }}.column_text') is-invalid @enderror"
                                                    name="{{ constructor_field_name($key, 'content.mob_rows') }}[{{ $k }}][{{ $col }}][column_text]">{{ $text['column_text'] ?? '' }}</textarea>

                                                @error(constructor_field_name_dot($key, 'content.mob_rows') . '.' . $k . '.' . $col . '.column_text')
                                                <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </td>
                                    @endforeach
                                </tr>
                                </tbody>
                            </table>
                            <div class="mt-1 text-center">
                                <button type="button" class="btn btn-outline-danger btn-sm remove-row-item">Удалить</button>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>

        <button type="button"
                class="add-blogTable-row_{{$lang}} btn btn-success btn-sm d-block mt-2">Добавить ряд</button>
        <button type="button"
                class="add-blogTable-mob_row_{{$lang}} btn btn-success btn-sm d-block mt-2">Добавить ряд (mob.)</button>
    </div>
</div>

@include('constructor::layouts.footer')
