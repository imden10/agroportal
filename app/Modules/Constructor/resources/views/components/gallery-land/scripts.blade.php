<script type="text/javascript">
    (function () {
        $(document).on('click', '.add-gallery4-list-item_{{$lang}}', function () {
            const template = $(this).parent().find('.gallery4-list-template');
            const container = $(this).parent().find('.gallery4-list-container');

            create_item(template, container, '#imageInputPlaceholder1');

            container.find('input, textarea').prop('disabled', false);
        });

        $(document).on('click', '.gallery4-remove-item_{{$lang}}', function () {
            let cardBody = $(this).closest('.card-body');
            $(this).parents('.item-group').remove();
        });
    })();
</script>
