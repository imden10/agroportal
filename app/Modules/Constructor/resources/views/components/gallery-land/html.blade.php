@include('constructor::layouts.header',['lang' => $lang])

<div id="collapse{{ $key }}_{{$lang}}" class="card-body mt-1 collapse show">
    <div class="row">
        <div class="col-4 input-group-sm mb-3">
            <input type="text" placeholder="{{ trans($params['labels']['anker_title']) }}" class="form-control @error(constructor_field_name_dot($key, 'content.anker_title')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.anker_title') }}" value="{{ old(constructor_field_name_dot($key, 'content.anker_title'), $content['anker_title'] ?? '') }}">

            @error(constructor_field_name_dot($key, 'content.anker_title'))
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-4 input-group-sm mb-3">
            <div class="input-group mb-3 ps-elem">
                <div class="input-group-prepend" style="height: 30px;">
                    <div class="input-group-text">
                        <input type="checkbox" @if(($content['anker_link'] ?? '') !== '') checked @endif onclick="$(this).closest('.ps-elem').find('.input-ps').toggleClass('hide')">
                    </div>
                    <span class="input-group-text">Произвольная ссылка</span>
                </div>
                <input type="text" name="{{ constructor_field_name($key, 'content.anker_link') }}" value="{{ old(constructor_field_name_dot($key, 'content.anker_link'), $content['anker_link'] ?? '') }}"  class="form-control input-ps @if(($content['anker_link'] ?? '') === '') hide @endif" style="height: 30px">
            </div>
        </div>

        <div class="col-4 input-group-sm mb-3">
            <div class="input-group mb-3 ps-elem">
                <div class="input-group-prepend" style="height: 30px;">
                    <div class="input-group-text">
                        <input type="checkbox" name="{{ constructor_field_name($key, 'content.anker_top') }}" value="1"  @if(($content['anker_top'] ?? '') !== '') checked @endif>
                    </div>
                    <span class="input-group-text">В топ</span>
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="form-group input-group-sm mb-12">
                <input type="text" placeholder="{{ $params['labels']['title'] }}" class="form-control @error(constructor_field_name_dot($key, 'content.title')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.title') }}" value="{{ old(constructor_field_name_dot($key, 'content.title'), $content['title'] ?? '') }}">

                @error(constructor_field_name_dot($key, 'content.title'))
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>

        <div class="col-12">
            <textarea class="form-control summernote @error(constructor_field_name_dot($key, 'content.text')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.text') }}">{{ old(constructor_field_name_dot($key, 'content.text'), $content['text'] ?? '') }}</textarea>

            @error(constructor_field_name_dot($key, 'content.text'))
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="input-group">
            <div style="display: none;">
                <div data-item-id="#imageInputPlaceholder1" class="gallery4-list-template item-group m-1 border border-grey-light p-1 d-flex align-items-center">
                    <div class="col-8">
                        {{ media_preview_box(constructor_field_name($key, 'content.list') . '[#imageInputPlaceholder1][image]') }}
                    </div>

                    <div class="col-4">
                        <button type="button" class="btn btn-danger gallery4-remove-item_{{$lang}} float-right text-white">Удалить</button>
                    </div>
                </div>
            </div>

            <input type="hidden" name="{{ constructor_field_name($key, 'content.list') }}" value="">

            <div class="gallery4-list-container w-100 gallery3-list-container-{{$lang}}">
                @foreach((array) old(constructor_field_name($key, 'content.list'), $content['list'] ?? []) as $k => $value)
                    <div data-item-id="{{ $k }}" class="item-template item-group m-1 border border-grey-light p-1 d-flex align-items-center">

                        <div class="col-8">
                            {{ media_preview_box(constructor_field_name($key, 'content.list') . '[' . $k . '][image]', $value['image'] ?? null, $errors) }}
                        </div>

                        <div class="col-4">
                            <button type="button" class="btn btn-danger gallery4-remove-item_{{$lang}} text-white float-right">Удалить</button>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <button type="button" class="btn btn-info btn-sm add-gallery4-list-item_{{$lang}} d-block mt-2">Добавить</button>
    </div>
</div>

@include('constructor::layouts.footer')
