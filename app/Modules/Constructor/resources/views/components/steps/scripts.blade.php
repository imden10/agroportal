<script type="text/javascript">
    (function () {
        $(document).on('click', '.add-steps-list-item_{{$lang}}', function () {
            const template = $(this).parent().find('.steps-list-template');
            const container = $(this).parent().find('.steps-list-container');

            create_item(template, container, '#imageInputPlaceholder1');

            container.find('input, textarea').prop('disabled', false);

            container.find('textarea').each(function () {
                if ($(this).hasClass('new_sammernote')) {
                    $(this).summernote(summernote_options);
                }
            });
        });

        $(document).on('click', '.steps-remove-item_{{$lang}}', function () {
            $(this).parents('.item-group').remove();
        });
    })();
</script>
