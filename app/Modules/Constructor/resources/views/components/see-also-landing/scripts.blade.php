<script type="text/javascript">
    (function () {
        setTimeout(function () {
            $('.select2-field2-shown').each(function () {
                let lang = $(this).data('lang');
                $(this).select2({
                    minimumInputLength: 1,
                    allowClear:true,
                    placeholder: 'Начните вводить название',
                    "language": {
                        "noResults": function(){
                            return "Нет результатов!";
                        },
                        "searching": function(){
                            return "поиск";
                        },
                        "inputTooShort": function(){
                            return "Пожалуйста, введите 1 или более символов";
                        },
                    },
                    ajax: {
                        url: "{{route('landing.search')}}",
                        dataType: 'json',
                        type: "GET",
                        quietMillis: 50,
                        data: function (term) {
                            return {
                                term: term,
                                lang:lang
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data.items, function (item) {
                                    return {
                                        text: item.name,
                                        id: item.id
                                    }
                                })
                            };
                        }
                    }
                });
            });
        },1000);

        $(document).on('click', '.add-see-also2-list-item_{{$lang}}', function () {
            const template = $(this).parent().find('.see-also2-list-template');
            const container = $(this).parent().find('.see-also2-list-container');

            create_item(template, container, '#imageInputPlaceholder1');

            container.find('input, textarea').prop('disabled', false);

            container.find('.select2-field2').each(function () {
                let lang = $(this).data('lang');
                $(this).select2({
                    minimumInputLength: 1,
                    allowClear:true,
                    placeholder: 'Начните вводить название',
                    "language": {
                        "noResults": function(){
                            return "Нет результатов!";
                        },
                        "searching": function(){
                            return "поиск";
                        },
                        "inputTooShort": function(){
                            return "Пожалуйста, введите 1 или более символов";
                        },
                    },
                    ajax: {
                        url: "{{route('landing.search')}}",
                        dataType: 'json',
                        type: "GET",
                        quietMillis: 50,
                        data: function (term) {
                            return {
                                term: term,
                                lang:lang
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: $.map(data.items, function (item) {
                                    return {
                                        text: item.name,
                                        id: item.id
                                    }
                                })
                            };
                        }
                    }
                }).on('change', function (e) {
                    $(this).closest('form').find('input[name="name"]').val($(this).find(':selected').text());
                });
            });
        });

        $(document).on('click', '.remove-item', function () {
            $(this).parents('.item-group').remove();
        });
    })();
</script>
