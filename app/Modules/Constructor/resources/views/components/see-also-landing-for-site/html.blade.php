@include('constructor::layouts.header',['lang' => $lang])

<div id="collapse{{ $key }}_{{$lang}}" class="card-body mt-1 collapse show">
    <div class="row">
        <div class="col-12">
            <div class="form-group mb-1">
                <label for="anker_title">{{ $params['labels']['title'] }}</label>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-link"></i></span>
                    </div>
                    <input id="{{ $key }}_title" type="text" name="{{ constructor_field_name($key, 'content.title') }}" placeholder="{{ $params['labels']['title'] }}" value="{{ old(constructor_field_name_dot($key, 'content.title'), $content['title'] ?? '') }}" class="form-control ">
                </div>
            </div>
        </div>

        <div class="col-12 input-group-sm mb-3">
            <label>{{ $params['labels']['type'] }}</label>
            <select class="form-control @error(constructor_field_name_dot($key, 'content.type')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.type') }}">
                @foreach($params['type'] as $listKey => $litItem)
                    <option value="{{ $listKey }}" @if (old(constructor_field_name_dot($key, 'content.type'), $content['type'] ?? '') == $listKey) selected @endif>{{ $litItem }}</option>
                @endforeach
            </select>
        </div>

        <div class="input-group">
            <div style="display: none;">
                <div data-item-id="#imageInputPlaceholder1" class="see-also2-list-template item-group m-1 border border-grey-light p-1 d-flex align-items-center">
                    <div class="col-4">
                        {{ media_preview_box(constructor_field_name($key, 'content.list') . '[#imageInputPlaceholder1][image]') }}
                    </div>
                    <div class="col-6">
                        <textarea name="{{ constructor_field_name($key, 'content.list') }}[#imageInputPlaceholder1][text]" class="form-control" cols="30" rows="10" placeholder="{{ $params['labels']['text'] }}" disabled></textarea>
                        <div style="margin-top: 10px">
                            <select data-lang="{{$lang}}" name="{{ constructor_field_name($key, 'content.list') }}[#imageInputPlaceholder1][item_id]" class="select2-field2" style="width: 100%">
                                <option value="">{{ $params['labels']['title'] }}</option>
                            </select>
                        </div>
                        <input type="text" name="{{ constructor_field_name($key, 'content.list') }}[#imageInputPlaceholder1][link]" placeholder="{{ $params['labels']['link'] }}" class="form-control mt-3 mb-1" disabled>
                    </div>

                    <div class="col-2">
                        <button type="button" class="btn btn-danger remove-item float-right text-white">Удалить</button>
                    </div>
                </div>
            </div>

            <input type="hidden" name="{{ constructor_field_name($key, 'content.list') }}" value="">

            <div class="see-also2-list-container w-100">
                @foreach((array) old(constructor_field_name($key, 'content.list'), $content['list'] ?? []) as $k => $value)
                    <div data-item-id="{{ $k }}" class="item-template item-group m-1 border border-grey-light p-1 d-flex align-items-center">
                        <div class="col-4">
                            {{ media_preview_box(constructor_field_name($key, 'content.list') . '[' . $k . '][image]', $value['image'] ?? null, $errors) }}
                        </div>

                        <div class="col-6">
                            <?php
                                $val = $value['item_id'] ?? '';
                                $selArt = \App\Models\Landing::query()->where('id',$val)->first();
                            ?>
                            <textarea name="{{ constructor_field_name($key, 'content.list') }}[{{ $k }}][text]" class="form-control" cols="30" rows="10" placeholder="{{ $params['labels']['text'] }}">{{ $value['text'] ?? '' }}</textarea>
                            <div style="margin-top: 10px">
                                <select data-lang="{{$lang}}" name="{{ constructor_field_name($key, 'content.list') }}[{{ $k }}][item_id]" class="select2-field2-shown"  style="width: 100%">
                                @if($selArt)
                                    <option value="{{$val}}">{{$selArt->title}}</option>
                                @endif
                                </select>
                            </div>

                                <input type="text" name="{{ constructor_field_name($key, 'content.list') }}[{{ $k }}][link]" placeholder="{{ $params['labels']['link'] }}" class="form-control mt-3 mb-1" value="{{ $value['link'] ?? '' }}">
                        </div>

                        <div class="col-2">
                            <button type="button" class="btn btn-danger remove-item text-white float-right">Удалить</button>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>

        <button type="button" class="btn btn-info btn-sm add-see-also2-list-item_{{$lang}} d-block mt-2">Добавить</button>
    </div>
</div>

@include('constructor::layouts.footer')
