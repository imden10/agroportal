<script type="text/javascript">
    (function () {
        $(document).on('click', '.add-advantages2-list-item_{{$lang}}', function () {
            const template = $(this).parent().find('.advantages2-list-template');
            const container = $(this).parent().find('.advantages2-list-container');

            create_item(template, container, '#imageInputPlaceholder1');

            container.find('input, textarea').prop('disabled', false);

            let eCount = $(this).closest('.card-body').find(".advantages2-list-container-{{$lang}}").find('.advantages2-list-template').length;
            if(eCount >= 3){
                $(this).css('visibility','hidden');
            } else {
                $(this).css('visibility','visible');
            }
        });

        $(document).on('click', '.advantages2-remove-item_{{$lang}}', function () {
            let cardBody = $(this).closest('.card-body');
            $(this).parents('.item-group').remove();

            let eCount = cardBody.find(".advantages2-list-container-{{$lang}}").find('.advantages2-list-template').length;
            if(eCount >= 3){
                cardBody.find(".add-advantages2-list-item_{{$lang}}").css('visibility','hidden');
            } else {
                cardBody.find(".add-advantages2-list-item_{{$lang}}").css('visibility','visible');
            }
        });
    })();
</script>
