<script type="text/javascript">
    (function() {
        $(document).on('click', '.add-blogTable-row_{{$lang}}', function() {
            const count = $(this).parent().find('.item-row-template-count>option:selected').val();
            const template = $(this).parent().find('.item-row-template' + count);
            const container = $(this).parent().find('.items-row-container');

            create_item(template, container, '#blogTableRowPlaceholder' + count);

            container.find('input').prop('disabled', false);
        });
        //Mob
        $(document).on('click', '.add-blogTable-mob_row_{{$lang}}', function() {
            const count = $(this).parent().find('.item-mob_row-template-count>option:selected').val();
            const template = $(this).parent().find('.item-mob_row-template' + count);
            const container = $(this).parent().find('.items-mob_row-container');

            create_item(template, container, '#blogTableRowPlaceholder' + count);

            container.find('input').prop('disabled', false);
        });

        $(document).on('click', '.edit-item', function() {
            console.log($(this).parent().parent());
            init_small_summernote($(this).parent().parent());
        });

        $(document).on('click', '.edit-item-del', function() {
            console.log($(this).parent().parent());
            del_small_summernote($(this).parent().parent());
        });

        $(document).on('click', '.remove-row-item', function() {
            $(this).parents('.item-group').remove();
        });

        function init_small_summernote(component_container) {
            component_container.find('textarea').each(function () {
                if ($(this).hasClass('small_summernote') && $(this).is(':visible')) {
                    $(this).summernote({
                        height: 250,
                        minHeight: null,
                        maxHeight: null,
                        lang: 'ru-RU',
                        toolbar: [
                            ['style', ['style']],
                            ['font', ['bold', 'underline', 'italic', 'clear']],
                            // ['fontname', ['fontname']],
                            // ['color', ['color']],
                            ['para', ['ul', 'ol', 'paragraph']],
                            // ['table', ['table']],
                            ['insert', ['link', /*'picture', 'video'*/]],
                            ['view', ['fullscreen', 'codeview']],
                        ],
                    });
                }
            });
        }
        function del_small_summernote(component_container) {
            component_container.find('textarea').each(function () {
                if ($(this).hasClass('small_summernote')) {
                    $(this).summernote('destroy');
                }
            });
        }
    })();
</script>
