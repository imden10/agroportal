@include('constructor::layouts.header',['lang' => $lang])

<div id="collapse{{ $key }}_{{$lang}}" class="card-body mt-1 collapse show show">
    <div class="row">
        <div class="col-4 input-group-sm mb-3">
            <input type="text" placeholder="{{ trans($params['labels']['anker_title']) }}" class="form-control @error(constructor_field_name_dot($key, 'content.anker_title')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.anker_title') }}" value="{{ old(constructor_field_name_dot($key, 'content.anker_title'), $content['anker_title'] ?? '') }}">

            @error(constructor_field_name_dot($key, 'content.anker_title'))
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-4 input-group-sm mb-3">
            <div class="input-group mb-3 ps-elem">
                <div class="input-group-prepend" style="height: 30px;">
                    <div class="input-group-text">
                        <input type="checkbox" @if(($content['anker_link'] ?? '') !== '') checked @endif onclick="$(this).closest('.ps-elem').find('.input-ps').toggleClass('hide')">
                    </div>
                    <span class="input-group-text">Произвольная ссылка</span>
                </div>
                <input type="text" name="{{ constructor_field_name($key, 'content.anker_link') }}" value="{{ old(constructor_field_name_dot($key, 'content.anker_link'), $content['anker_link'] ?? '') }}"  class="form-control input-ps @if(($content['anker_link'] ?? '') === '') hide @endif" style="height: 30px">
            </div>
        </div>

        <div class="col-4 input-group-sm mb-3">
            <div class="input-group mb-3 ps-elem">
                <div class="input-group-prepend" style="height: 30px;">
                    <div class="input-group-text">
                        <input type="checkbox" name="{{ constructor_field_name($key, 'content.anker_top') }}" value="1"  @if(($content['anker_top'] ?? '') !== '') checked @endif>
                    </div>
                    <span class="input-group-text">В топ</span>
                </div>
            </div>
        </div>

        <div class="col-12">
            <textarea class="form-control summernote @error(constructor_field_name_dot($key, 'content.text')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.text') }}">{{ old(constructor_field_name_dot($key, 'content.text'), $content['text'] ?? '') }}</textarea>

            @error(constructor_field_name_dot($key, 'content.description'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-12 input-group-sm mb-3">
            <input type="text" placeholder="{{ trans($params['labels']['fio']) }}" class="form-control @error(constructor_field_name_dot($key, 'content.fio')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.fio') }}" value="{{ old(constructor_field_name_dot($key, 'content.fio'), $content['fio'] ?? '') }}">

            @error(constructor_field_name_dot($key, 'content.fio'))
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-12 input-group-sm mb-3">
            <input type="text" placeholder="{{ trans($params['labels']['fio_text']) }}" class="form-control @error(constructor_field_name_dot($key, 'content.fio_text')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.fio_text') }}" value="{{ old(constructor_field_name_dot($key, 'content.fio_text'), $content['fio_text'] ?? '') }}">

            @error(constructor_field_name_dot($key, 'content.fio_text'))
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-6 input-group-sm mb-3">
            <label>{{ $params['labels']['background'] }}</label>
            <select class="form-control @error(constructor_field_name_dot($key, 'content.background')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.background') }}">
                @foreach($params['background'] as $listKey => $litItem)
                    <option value="{{ $listKey }}" @if (old(constructor_field_name_dot($key, 'content.background'), $content['background'] ?? '') == $listKey) selected @endif>{{ $litItem }}</option>
                @endforeach
            </select>

            @error(constructor_field_name_dot($key, 'content.anker_title'))
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-6 input-group-sm mb-3">
            <label>{{ $params['labels']['type'] }}</label>
            <select class="form-control @error(constructor_field_name_dot($key, 'content.type')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.type') }}">
                @foreach($params['type'] as $listKey => $litItem)
                    <option value="{{ $listKey }}" @if (old(constructor_field_name_dot($key, 'content.type'), $content['type'] ?? '') == $listKey) selected @endif>{{ $litItem }}</option>
                @endforeach
            </select>

            @error(constructor_field_name_dot($key, 'content.anker_title'))
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-12 input-group-sm mb-3">
            <label>{{ $params['labels']['image'] }}</label>
            {{ media_preview_box(constructor_field_name($key, 'content.image'), $content['image'] ?? null, $errors) }}
        </div>
    </div>
</div>

@include('constructor::layouts.footer')
