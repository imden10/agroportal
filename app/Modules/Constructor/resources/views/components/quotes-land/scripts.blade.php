<script type="text/javascript">
    (function () {
        $(document).on('change',".select-background",function () {
            let val = $(this).val();

            if(val == 'image'){
                $(this).closest('.row').find(".background_image_container").show();
            } else {
                $(this).closest('.row').find(".background_image_container").hide();
            }
        })
    })();
</script>
