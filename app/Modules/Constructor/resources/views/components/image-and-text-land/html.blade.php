@include('constructor::layouts.header',['lang' => $lang])

<div id="collapse{{ $key }}_{{$lang}}" class="card-body mt-1 collapse show">
    <div class="row">
        <div class="col-4 input-group-sm mb-3">
            <input type="text" placeholder="{{ trans($params['labels']['anker_title']) }}"
                   class="form-control @error(constructor_field_name_dot($key, 'content.anker_title')) is-invalid @enderror"
                   name="{{ constructor_field_name($key, 'content.anker_title') }}"
                   value="{{ old(constructor_field_name_dot($key, 'content.anker_title'), $content['anker_title'] ?? '') }}">

            @error(constructor_field_name_dot($key, 'content.anker_title'))
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-4 input-group-sm mb-3">
            <div class="input-group mb-3 ps-elem">
                <div class="input-group-prepend" style="height: 30px;">
                    <div class="input-group-text">
                        <input type="checkbox" @if(($content['anker_link'] ?? '') !== '') checked @endif onclick="$(this).closest('.ps-elem').find('.input-ps').toggleClass('hide')">
                    </div>
                    <span class="input-group-text">Произвольная ссылка</span>
                </div>
                <input type="text" name="{{ constructor_field_name($key, 'content.anker_link') }}" value="{{ old(constructor_field_name_dot($key, 'content.anker_link'), $content['anker_link'] ?? '') }}"  class="form-control input-ps @if(($content['anker_link'] ?? '') === '') hide @endif" style="height: 30px">
            </div>
        </div>

        <div class="col-4 input-group-sm mb-3">
            <div class="input-group mb-3 ps-elem">
                <div class="input-group-prepend" style="height: 30px;">
                    <div class="input-group-text">
                        <input type="checkbox" name="{{ constructor_field_name($key, 'content.anker_top') }}" value="1"  @if(($content['anker_top'] ?? '') !== '') checked @endif>
                    </div>
                    <span class="input-group-text">В топ</span>
                </div>
            </div>
        </div>

        <div class="col-12 input-group-sm mb-3">
            <input type="text" placeholder="{{ trans($params['labels']['title']) }}"
                   class="form-control @error(constructor_field_name_dot($key, 'content.title')) is-invalid @enderror"
                   name="{{ constructor_field_name($key, 'content.title') }}"
                   value="{{ old(constructor_field_name_dot($key, 'content.title'), $content['title'] ?? '') }}">

            @error(constructor_field_name_dot($key, 'content.title'))
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="d-flex col-12">
            <div>
                <div class="form-group input-group-sm">
                    <label>{{ $params['labels']['image_position'] }}</label>
                    <select
                        class="form-control @error(constructor_field_name_dot($key, 'content.image_position')) is-invalid @enderror"
                        name="{{ constructor_field_name($key, 'content.image_position') }}">
                        @foreach($params['image_positions'] as $slug => $position)
                            <option value="{{ $slug }}"
                                    @if (old(constructor_field_name_dot($key, 'content.image_position'), $content['image_position'] ?? '') == $slug) selected @endif>{{ $position }}</option>
                        @endforeach
                    </select>

                    @error(constructor_field_name_dot($key, 'content.image_position'))
                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group input-group-sm">
                    <label>{{ $params['labels']['column_width'] }}</label>
                    <select
                        class="form-control @error(constructor_field_name_dot($key, 'content.column_width')) is-invalid @enderror"
                        name="{{ constructor_field_name($key, 'content.column_width') }}">
                        @foreach($params['column_width'] as $slug2 => $position2)
                            <option value="{{ $slug2 }}"
                                    @if (old(constructor_field_name_dot($key, 'content.column_width'), $content['column_width'] ?? '') == $slug2) selected @endif>{{ $position2 }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group input-group-sm">
                    <label>{{ $params['labels']['background'] }}</label>
                    <select
                        class="form-control @error(constructor_field_name_dot($key, 'content.background')) is-invalid @enderror"
                        name="{{ constructor_field_name($key, 'content.background') }}">
                        @foreach($params['background'] as $slug2 => $position2)
                            <option value="{{ $slug2 }}"
                                    @if (old(constructor_field_name_dot($key, 'content.background'), $content['background'] ?? '') == $slug2) selected @endif>{{ $position2 }}</option>
                        @endforeach
                    </select>
                </div>
                <label>{{ $params['labels']['image'] }}</label>
                {{ media_preview_box(constructor_field_name($key, 'content.image'), $content['image'] ?? null, $errors) }}

                <label style="margin-top: 15px;">{{ $params['labels']['image_mob'] }}</label>
                {{ media_preview_box(constructor_field_name($key, 'content.image_mob'), $content['image_mob'] ?? null, $errors) }}
            </div>

            <div class="w-100 ml-3">
                <textarea
                    class="form-control summernote @error(constructor_field_name_dot($key, 'content.description')) is-invalid @enderror"
                    name="{{ constructor_field_name($key, 'content.description') }}">{{ old(constructor_field_name_dot($key, 'content.description'), $content['description'] ?? '') }}</textarea>

                @error(constructor_field_name_dot($key, 'content.description'))
                <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <input type="text" placeholder="{{ trans($params['labels']['video_link']) }}" class="form-control @error(constructor_field_name_dot($key, 'content.video_link')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.video_link') }}" value="{{ old(constructor_field_name_dot($key, 'content.video_link'), $content['video_link'] ?? '') }}">

                <div class="col-12" style="margin-top: 10px">
                    <h4>Кнопка</h4>
                    <input type="text" placeholder="{{ trans($params['labels']['btn_title']) }}" class="form-control @error(constructor_field_name_dot($key, 'content.btn_title')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.btn_title') }}" value="{{ old(constructor_field_name_dot($key, 'content.btn_title'), $content['btn_title'] ?? '') }}">
                    <input type="text" placeholder="{{ trans($params['labels']['btn_link']) }}" class="form-control @error(constructor_field_name_dot($key, 'content.btn_link')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.btn_link') }}" value="{{ old(constructor_field_name_dot($key, 'content.btn_link'), $content['btn_link'] ?? '') }}">
                    <select class="form-control @error(constructor_field_name_dot($key, 'content.btn_type')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.btn_type') }}">
                        @foreach($params['btn_type'] as $listKey => $litItem)
                            <option value="{{ $listKey }}" @if (old(constructor_field_name_dot($key, 'content.btn_type'), $content['btn_type'] ?? '') == $listKey) selected @endif>{{ $litItem }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>

@include('constructor::layouts.footer')
