<script type="text/javascript">
    (function () {
        $(document).on('click', '.add-accordion-list-item_{{$lang}}', function () {
            const template = $(this).parent().find('.accordion-list-template');
            const container = $(this).parent().find('.accordion-list-container');

            create_item(template, container, '#imageInputPlaceholder1');

            container.find('input, textarea').prop('disabled', false);

            container.find('textarea').each(function () {
                if ($(this).hasClass('new_sammernote')) {
                    $(this).summernote(summernote_options);
                }
            });
        });

        $(document).on('click', '.accordion-remove-item_{{$lang}}', function () {
            $(this).parents('.item-group').remove();
        });
    })();
</script>
