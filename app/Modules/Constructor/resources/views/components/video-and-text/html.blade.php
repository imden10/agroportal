@include('constructor::layouts.header',['lang' => $lang])

<div id="collapse{{ $key }}_{{$lang}}" class="card-body mt-1 collapse show">
    <div class="row">
        <div class="col-4 input-group-sm mb-3">
            <input type="text" placeholder="{{ trans($params['labels']['anker_title']) }}" class="form-control @error(constructor_field_name_dot($key, 'content.anker_title')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.anker_title') }}" value="{{ old(constructor_field_name_dot($key, 'content.anker_title'), $content['anker_title'] ?? '') }}">

            @error(constructor_field_name_dot($key, 'content.anker_title'))
            <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-4 input-group-sm mb-3">
            <div class="input-group mb-3 ps-elem">
                <div class="input-group-prepend" style="height: 30px;">
                    <div class="input-group-text">
                        <input type="checkbox" @if(($content['anker_link'] ?? '') !== '') checked @endif onclick="$(this).closest('.ps-elem').find('.input-ps').toggleClass('hide')">
                    </div>
                    <span class="input-group-text">Произвольная ссылка</span>
                </div>
                <input type="text" name="{{ constructor_field_name($key, 'content.anker_link') }}" value="{{ old(constructor_field_name_dot($key, 'content.anker_link'), $content['anker_link'] ?? '') }}"  class="form-control input-ps @if(($content['anker_link'] ?? '') === '') hide @endif" style="height: 30px">
            </div>
        </div>

        <div class="col-4 input-group-sm mb-3">
            <div class="input-group mb-3 ps-elem">
                <div class="input-group-prepend" style="height: 30px;">
                    <div class="input-group-text">
                        <input type="checkbox" name="{{ constructor_field_name($key, 'content.anker_top') }}" value="1"  @if(($content['anker_top'] ?? '') !== '') checked @endif>
                    </div>
                    <span class="input-group-text">В топ</span>
                </div>
            </div>
        </div>

        <div class="col-12 mb-3 input-group-sm">
            <input type="text" placeholder="{{ trans($params['labels']['title']) }}" class="form-control @error(constructor_field_name_dot($key, 'content.title')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.title') }}" value="{{ old(constructor_field_name_dot($key, 'content.title'), $content['title'] ?? '') }}">

            @error(constructor_field_name_dot($key, 'content.title'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-12 mb-3 input-group-sm">
            {{ media_preview_box(constructor_field_name($key, 'content.image'), $content['image'] ?? null, $errors) }}
        </div>

        <div class="col-12 input-group mb-3">
            <input type="text" placeholder="{{trans($params['labels']['file'])}}" class="form-control file-path-field" name="{{ constructor_field_name($key, 'content.file') }}" value="{{ old(constructor_field_name_dot($key, 'content.file'), $content['file'] ?? '') }}">
            <div class="input-group-append">
                <button type="button" class="btn btn-info select-file-btn">Выберите файл</button>
            </div>
        </div>

        <div class="col-12">
            <textarea class="form-control summernote @error(constructor_field_name_dot($key, 'content.text')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.text') }}">{{ old(constructor_field_name_dot($key, 'content.text'), $content['text'] ?? '') }}</textarea>

            @error(constructor_field_name_dot($key, 'content.text'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>

        <div class="col-12">
            <h4>Кнопка</h4>
            <input type="text" placeholder="{{ trans($params['labels']['btn_title']) }}" class="form-control @error(constructor_field_name_dot($key, 'content.btn_title')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.btn_title') }}" value="{{ old(constructor_field_name_dot($key, 'content.btn_title'), $content['btn_title'] ?? '') }}">
            <input type="text" placeholder="{{ trans($params['labels']['btn_link']) }}" class="form-control @error(constructor_field_name_dot($key, 'content.btn_link')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.btn_link') }}" value="{{ old(constructor_field_name_dot($key, 'content.btn_link'), $content['btn_link'] ?? '') }}">
            <select class="form-control @error(constructor_field_name_dot($key, 'content.btn_type')) is-invalid @enderror" name="{{ constructor_field_name($key, 'content.btn_type') }}">
                @foreach($params['btn_type'] as $listKey => $litItem)
                    <option value="{{ $listKey }}" @if (old(constructor_field_name_dot($key, 'content.btn_type'), $content['btn_type'] ?? '') == $listKey) selected @endif>{{ $litItem }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

@include('constructor::layouts.footer')
