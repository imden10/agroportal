<script type="text/javascript">
    (function () {
        $(document).on('click',".select-file-btn",function () {
            window.open('/filemanager?type=file', 'FileManager', 'width=900,height=600');
            window.SetUrl = function( url ) {
                if(url.length){
                    try {
                        let urlParts = url[0].url.split('files');
                        if(urlParts.length == 2){
                            $(".file-path-field").val(urlParts[1]);
                        }
                    } catch (e) {
                        console.log(e.message);
                    }
                }
            };
        })
    })();
</script>
