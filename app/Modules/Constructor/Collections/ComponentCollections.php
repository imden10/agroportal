<?php

namespace App\Modules\Constructor\Collections;

class ComponentCollections
{
    /**
     * @return array
     */
    public static function widget()
    {
        return [
            'label'  => 'Виджет',
            'params' => [
                'labels'     => [

                ],
                'widgets'    => function ($lang) {
                    $widgets = \App\Modules\Widgets\Models\Widget::where('lang', $lang)->get();

                    return ['' => '---'] + collect($widgets)
                            ->mapWithKeys(function ($widget) {
                                return [$widget->id => $widget->name];
                            })
                            ->toArray();
                },
                'shown_name' => 'widget'
            ],
            'rules'  => [
                'content.widget' => 'nullable|integer',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function imageAndText()
    {
        return [
            'label'  => 'Изображение и текст',
            'params' => [
                'image_positions' => [
                    'right' => 'Справа',
                    'left'  => 'Слева',
                ],
                'column_width'    => [
                    '3' => '30/70',
                    '5' => '50/50',
                    '7' => '70/30',
                ],
                'labels'          => [
                    'title'             => 'Заголовок',
                    'image_position'    => 'Положение изображения',
                    'column_width'      => 'Ширина колонки изображения',
                    'image_description' => 'Описание изображения',
                ],
                'shown_name'      => 'title'
            ],
            'rules'  => [
                'content.title'             => 'nullable|string|max:255',
                'content.image_description' => 'nullable|string|max:255',
                'content.image'             => 'nullable|string|max:255',
                'content.image_position'    => 'required|string|max:255',
                'content.column_width'      => 'required|string|max:255',
                'content.description'       => 'nullable|string|max:65000',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function imageAndTextLand()
    {
        return [
            'label'  => 'Изображение и текст',
            'params' => [
                'image_positions' => [
                    'right' => 'Справа',
                    'left'  => 'Слева',
                ],
                'column_width'    => [
                    '3' => '30/70',
                    '5' => '50/50',
                    '7' => '70/30',
                ],
                'labels'          => [
                    'title'          => 'Заголовок',
                    'image_position' => 'Положение изображения',
                    'column_width'   => 'Ширина колонки изображения',
                    'anker_title'    => 'Заголовок для меню',
                    'background'     => 'Цвет фона',
                    'btn_type'       => 'Тип кнопки',
                    'btn_title'      => 'Текст',
                    'btn_link'       => 'Произвольная ссылка',
                    'video_link'     => 'Ссылка на видео',
                    'image'          => 'Изображение',
                    'image_mob'      => 'Изображение (моб.)',
                ],
                'btn_type'        => [
                    'large'  => 'Большая',
                    'middle' => 'Средняя',
                    'small'  => 'Маленькая',
                ],
                'background'      => [
                    'white'       => 'Белый',
                    'grey'        => 'Серый',
                    'color_theme' => 'Цвет темы',
                ],
                'shown_name'      => 'title'
            ],
            'rules'  => [
                'content.title'          => 'nullable|string|max:255',
                'content.image'          => 'nullable|string|max:255',
                'content.image_mob'      => 'nullable|string|max:255',
                'content.image_position' => 'required|string|max:255',
                'content.column_width'   => 'required|string|max:255',
                'content.description'    => 'nullable|string|max:65000',
                'content.anker_title'    => 'nullable|string|max:255',
                'content.anker_link'     => 'nullable|string|max:255',
                'content.anker_top'      => 'nullable|string|max:255',
                'content.background'     => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function simpleText()
    {
        return [
            'label'  => 'Текст',
            'params' => [
                'labels'     => [
                    'title' => 'Заголовок',
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.title'       => 'nullable|string|max:255',
                'content.description' => 'nullable|string|max:65000',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function simpleTextLand()
    {
        return [
            'label'  => 'Текст',
            'params' => [
                'labels'     => [
                    'title'            => 'Заголовок',
                    'anker_title'      => 'Заголовок для меню',
                    'text_width'       => 'Ширина текста',
                    'text_align'       => 'Выравнивание текста',
                    'text_color'       => 'Цвет текста',
                    'background'       => 'Цвет фона',
                    'background_image' => 'Изображение фона',
                    'font_size'        => 'Размер шрифта',
                    'btn_type'         => 'Тип кнопки',
                    'btn_title'        => 'Текст',
                    'btn_link'         => 'Произвольная ссылка',
                ],
                'btn_type'   => [
                    'large'  => 'Большая',
                    'middle' => 'Средняя',
                    'small'  => 'Маленькая',
                ],
                'text_width' => [
                    '60' => '60%',
                    '90' => '90%',
                ],
                'font_size'  => [
                    'normal' => 'Обычный',
                    'small'  => 'Маленький',
                ],
                'text_align' => [
                    'center' => 'По центру',
                    'left'   => 'Слева',
                    'right'  => 'Справа',
                ],
                'text_color' => [
                    'black' => 'Черный',
                    'white' => 'Белый',
                ],
                'background' => [
                    'white'       => 'Белый',
                    'grey'        => 'Серый',
                    'color_theme' => 'Цвет темы',
                    'image'       => 'Изобржение',
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.title'            => 'nullable|string|max:255',
                'content.anker_title'      => 'nullable|string|max:255',
                'content.anker_link'       => 'nullable|string|max:255',
                'content.anker_top'        => 'nullable|string|max:255',
                'content.description'      => 'nullable|string|max:65000',
                'content.text_width'       => 'nullable|string|max:255',
                'content.text_align'       => 'nullable|string|max:255',
                'content.text_color'       => 'nullable|string|max:255',
                'content.background'       => 'nullable|string|max:255',
                'content.background_image' => 'nullable|string|max:255',
                'content.font_size'        => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function list()
    {
        return [
            'label'  => 'Список',
            'params' => [
                'type'       => [
                    'ul' => 'Обычный',
                    'ol' => 'Нумерованый'
                ],
                'labels'     => [
                    'type'  => 'Тип',
                    'item'  => 'Текст',
                    'title' => 'Заголовок',
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.title'       => 'nullable|string|max:255',
                'content.type'        => 'required|string|max:255',
                'content.list.*.item' => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function columns()
    {
        return [
            'label'  => 'Колонки',
            'params' => [
                'labels'     => [
                    'item'  => 'Текст',
                    'title' => 'Заголовок',
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.title'       => 'nullable|string|max:255',
                'content.list.*.item' => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function button()
    {
        return [
            'label'  => 'Кнопка',
            'params' => [
                'type'       => [
                    'link'  => 'Переход по ссылке',
                    'event' => 'Перейти к форме "Запись онлайн"'
                ],
                'labels'     => [
                    'type'  => 'Тип',
                    'title' => 'Текст кнопки',
                    'link'  => 'Ссылка',
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.title' => 'nullable|string|max:255',
                'content.link'  => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function imageElement()
    {
        return [
            'label'  => 'Изображение на всю ширину',
            'params' => [
                'width'      => [
                    '100%' => '100%',
                    '80%'  => '80%'
                ],
                'labels'     => [
                    'title'       => 'Заголовок',
                    'width'       => 'Ширина',
                    'anker_title' => 'Заголовок для меню',
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.title'       => 'nullable|string|max:255',
                'content.anker_title' => 'nullable|string|max:255',
                'content.anker_link'  => 'nullable|string|max:255',
                'content.anker_top'   => 'nullable|string|max:255',
                'content.image'       => 'nullable|string|max:255',
                'content.video'       => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function text2Columns()
    {
        return [
            'label'  => 'Текст в 2 колонки',
            'params' => [
                'labels'     => [
                    'title' => 'Заголовок',
                    'text1' => 'Текст 1',
                    'text2' => 'Текст 2',
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.title' => 'nullable|string|max:255',
                'content.text1' => 'nullable|string|max:65000',
                'content.text2' => 'nullable|string|max:65000',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function quotes()
    {
        return [
            'label'  => 'Цитата',
            'params' => [
                'labels'     => [
                    'title' => 'ФИО',
                    'text'  => 'Текст',
                    'image' => 'Фото',
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.title' => 'nullable|string|max:255',
                'content.image' => 'nullable|string|max:255',
                'content.text'  => 'nullable|string|max:65000',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function gallery()
    {
        return [
            'label'  => 'Галерея',
            'params' => [
                'labels'     => [
                    'title'       => 'Заголовок',
                    'image'       => 'Изображение',
                    'description' => 'Описание',
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.title'       => 'nullable|string|max:255',
                'content.list.*.item' => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function images()
    {
        return [
            'label'  => 'Изображения',
            'params' => [
                'labels'     => [
                    'title'       => 'Заголовок',
                    'image'       => 'Изображение',
                    'description' => 'Описание',
                    'link'        => 'Ссылка',
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.title'       => 'nullable|string|max:255',
                'content.list.*.item' => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function imagesLand()
    {
        return [
            'label'  => 'Изображения',
            'params' => [
                'labels'     => [
                    'title'       => 'Заголовок',
                    'anker_title' => 'Заголовок для меню',
                    'image'       => 'Изображение',
                    'text'        => 'Текст',
                    'btn_type'    => 'Тип кнопки',
                    'btn_title'   => 'Текст',
                    'btn_link'    => 'Произвольная ссылка',
                ],
                'btn_type'   => [
                    'large'  => 'Большая',
                    'middle' => 'Средняя',
                    'small'  => 'Маленькая',
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.title'       => 'nullable|string|max:255',
                'content.anker_title' => 'nullable|string|max:255',
                'content.anker_link'  => 'nullable|string|max:255',
                'content.anker_top'   => 'nullable|string|max:255',
                'content.image'       => 'nullable|string|max:255',
                'content.list.*.item' => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function galleryLand()
    {
        return [
            'label'  => 'Галерея',
            'params' => [
                'labels'     => [
                    'title'       => 'Заголовок',
                    'anker_title' => 'Заголовок для меню',
                    'image'       => 'Изображение',
                    'text'        => 'Текст',
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.title'       => 'nullable|string|max:255',
                'content.anker_title' => 'nullable|string|max:255',
                'content.anker_link'  => 'nullable|string|max:255',
                'content.anker_top'   => 'nullable|string|max:255',
                'content.image'       => 'nullable|string|max:255',
                'content.list.*.item' => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function seeAlso()
    {
        return [
            'label'  => 'Смотрите также',
            'params' => [
                'labels'     => [
                    'title' => 'Выберите публикацию',
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.list.*.item' => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function videoAndText()
    {
        return [
            'label'  => 'Видео и текст',
            'params' => [
                'labels'     => [
                    'anker_title' => 'Заголовок для меню',
                    'title'       => 'Заголовок',
                    'image'       => 'Изображение (превью)',
                    'file'        => 'Файл',
                    'text'        => 'Текст',
                    'btn_type'    => 'Тип кнопки',
                    'btn_title'   => 'Текст',
                    'btn_link'    => 'Произвольная ссылка',
                ],
                'btn_type'   => [
                    'large'  => 'Большая',
                    'middle' => 'Средняя',
                    'small'  => 'Маленькая',
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.title'       => 'nullable|string|max:255',
                'content.anker_title' => 'nullable|string|max:255',
                'content.anker_link'  => 'nullable|string|max:255',
                'content.anker_top'   => 'nullable|string|max:255',
                'content.file'        => 'nullable|string|max:255',
                'content.image'       => 'nullable|string|max:255',
                'content.text'        => 'nullable|string|max:65000',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function advantages()
    {
        return [
            'label'  => 'Преимущества',
            'params' => [
                'labels'     => [
                    'title'       => 'Заголовок',
                    'num'         => 'Цифра',
                    'anker_title' => 'Заголовок для меню',
                    'text'        => 'Текст',
                    'background'  => 'Цвет фона',
                ],
                'background' => [
                    'white'       => 'Белый',
                    'grey'        => 'Серый',
                    'color_theme' => 'Цвет темы',
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.title'       => 'nullable|string|max:255',
                'content.anker_title' => 'nullable|string|max:255',
                'content.anker_link'  => 'nullable|string|max:255',
                'content.anker_top'   => 'nullable|string|max:255',
                'content.image'       => 'nullable|string|max:255',
                'content.list.*.item' => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function advantagesWithImage()
    {
        return [
            'label'  => 'Преимущества c иконкой',
            'params' => [
                'labels'     => [
                    'title'       => 'Заголовок',
                    'image'       => 'Изображение',
                    'anker_title' => 'Заголовок для меню',
                    'text'        => 'Текст',
                    'background'  => 'Цвет фона',
                ],
                'background' => [
                    'white'       => 'Белый',
                    'grey'        => 'Серый',
                    'color_theme' => 'Цвет темы',
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.title'       => 'nullable|string|max:255',
                'content.anker_title' => 'nullable|string|max:255',
                'content.anker_link'  => 'nullable|string|max:255',
                'content.anker_top'   => 'nullable|string|max:255',
                'content.image'       => 'nullable|string|max:255',
                'content.list.*.item' => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function quotesLand()
    {
        return [
            'label'  => 'Цитата',
            'params' => [
                'labels'     => [
                    'text'        => 'Текст',
                    'fio'         => 'ФИО',
                    'fio_text'    => 'Текст после фио',
                    'image'       => 'Фото',
                    'anker_title' => 'Заголовок для меню',
                    'background'  => 'Цвет фона',
                    'type'        => 'Тип',
                ],
                'background' => [
                    'white'       => 'Белый',
                    'grey'        => 'Серый',
                    'color_theme' => 'Цвет темы',
                ],
                'type'       => [
                    'image_top'  => 'Картинка сверху',
                    'image_left' => 'Картинка слева'
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.fio'         => 'nullable|string|max:255',
                'content.fio_text'    => 'nullable|string|max:255',
                'content.image'       => 'nullable|string|max:255',
                'content.anker_title' => 'nullable|string|max:255',
                'content.anker_link'  => 'nullable|string|max:255',
                'content.anker_top'   => 'nullable|string|max:255',
                'content.text'        => 'nullable|string|max:65000',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function tableComponent()
    {
        $cols = [];
        for ($i = 2; $i <= 6; $i++) {
            $cols[(string)$i] = (string)$i;
        }

        return [
            'label'  => 'Таблица',
            'params' => [
                'table_width' => $cols,
                'cols_width'  => [
                    'auto' => 'auto',
                    '5%'   => '5%',
                    '10%'  => '10%',
                    '16%'  => '16%',
                    '20%'  => '20%',
                    '25%'  => '25%',
                    '33%'  => '33%',
                    '50%'  => '50%',
                    '60%'  => '60%',
                    '75%'  => '75%',
                    '80%'  => '80%',
                    '90%'  => '90%',
                ],
                'labels'      => [
                    'table_width'  => "Кол.",
                    'title'        => "Заголовок",
                    'column_text'  => "Текст",
                    'column_width' => "Ширина колонки",
                    'anker_title'  => 'Заголовок для меню',
                ],
                'shown_name'  => 'title'
            ],
            'rules'  => [
                'content.table_width'        => 'required|string|max:255',
                'content.anker_title'        => 'nullable|string|max:255',
                'content.anker_link'         => 'nullable|string|max:255',
                'content.anker_top'          => 'nullable|string|max:255',
                'content.title'              => 'nullable|string|max:255',
                'content.rows'               => 'nullable|array',
                'content.rows.*.column_text' => 'nullable|array',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function tableComponentForSite()
    {
        $cols = [];
        for ($i = 2; $i <= 6; $i++) {
            $cols[(string)$i] = (string)$i;
        }

        return [
            'label'  => 'Таблица',
            'params' => [
                'table_width' => $cols,
                'cols_width'  => [
                    'auto' => 'auto',
                    '5%'   => '5%',
                    '10%'  => '10%',
                    '16%'  => '16%',
                    '20%'  => '20%',
                    '25%'  => '25%',
                    '33%'  => '33%',
                    '50%'  => '50%',
                    '60%'  => '60%',
                    '75%'  => '75%',
                    '80%'  => '80%',
                    '90%'  => '90%',
                ],
                'labels'      => [
                    'table_width'  => "Кол.",
                    'title'        => "Заголовок",
                    'column_text'  => "Текст",
                    'column_width' => "Ширина колонки",
                ],
                'shown_name'  => 'title'
            ],
            'rules'  => [
                'content.table_width'        => 'required|string|max:255',
                'content.title'              => 'nullable|string|max:255',
                'content.rows'               => 'nullable|array',
                'content.rows.*.column_text' => 'nullable|array',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function accordion()
    {
        return [
            'label'  => 'Аккордеон',
            'params' => [
                'labels'      => [
                    'title'       => 'Заголовок',
                    'anker_title' => 'Заголовок для меню',
                    'text'        => 'Текст',
                    'title_align' => 'Выравнивание заголовка',
                    'type'        => 'Тип',
                ],
                'title_align' => [
                    'left'  => 'Слева',
                    'right' => 'Справа'
                ],
                'type'        => [
                    'with_num'    => 'С нумерацией',
                    'without_num' => 'Без нумерации'
                ],
                'shown_name'  => 'title'
            ],
            'rules'  => [
                'content.title'       => 'nullable|string|max:255',
                'content.anker_title' => 'nullable|string|max:255',
                'content.anker_link'  => 'nullable|string|max:255',
                'content.anker_top'   => 'nullable|string|max:255',
                'content.image'       => 'nullable|string|max:255',
                'content.list.*.item' => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function steps()
    {
        return [
            'label'  => 'Шаги',
            'params' => [
                'labels'      => [
                    'title'       => 'Заголовок',
                    'anker_title' => 'Заголовок для меню',
                    'text'        => 'Текст',
                    'title_align' => 'Выравнивание заголовка',
                ],
                'title_align' => [
                    'left'  => 'Слева',
                    'right' => 'Справа'
                ],
                'shown_name'  => 'title'
            ],
            'rules'  => [
                'content.title'       => 'nullable|string|max:255',
                'content.anker_title' => 'nullable|string|max:255',
                'content.anker_link'  => 'nullable|string|max:255',
                'content.anker_top'   => 'nullable|string|max:255',
                'content.list.*.item' => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function seeAlsoArticle()
    {
        return [
            'label'  => 'Смотрите также (публикации)',
            'params' => [
                'labels'     => [
                    'select_article' => 'Выберите публикацию',
                    'title'          => 'Заголовок',
                    'anker_title'    => 'Заголовок для меню',
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.list.*.item' => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function seeAlsoLanding()
    {
        return [
            'label'  => 'Смотрите также (лендинги)',
            'params' => [
                'labels'     => [
                    'title'       => 'Заголовок',
                    'anker_title' => 'Заголовок для меню',
                    'text'        => 'Описание',
                    'link'        => 'Или произвольная ссылка',
                    'type'        => 'Тип отображения',
                ],
                'type'       => [
                    'small_icon' => 'Маленькие иконки',
                    'large_icon' => 'Большие иконки'
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.list.*.item' => 'nullable|string|max:255',
            ],
        ];
    }

    /**
     * @return array
     */
    public static function seeAlsoLandingForSite()
    {
        return [
            'label'  => 'Смотрите также (лендинги)',
            'params' => [
                'labels'     => [
                    'title'       => 'Заголовок',
                    'text'        => 'Описание',
                    'link'        => 'Или произвольная ссылка',
                    'type'        => 'Тип отображения',
                ],
                'type'       => [
                    'small_icon' => 'Маленькие иконки',
                    'large_icon' => 'Большие иконки'
                ],
                'shown_name' => 'title'
            ],
            'rules'  => [
                'content.list.*.item' => 'nullable|string|max:255',
            ],
        ];
    }
}

