<?php

if (!function_exists('plural_form')) {

    function plural_form($number, $after) {
        $cases = [2, 0, 1, 1, 1, 2];
        return $number . ' ' . $after[($number % 100 > 4 && $number % 100 < 20) ? 2 : $cases[min($number % 10, 5)]];
    }

}

if (!function_exists('convert_array_field_name_to_with_dot')) {

    function convert_array_field_name_to_with_dot(string $name) {
        $modified = preg_replace('/\]\[|\[|\]/', '.', $name);

        return $modified ? rtrim($modified, '.') : $name;
    }

}

if (!function_exists('mb_str_replace')) {
    function mb_str_replace($search, $replace, $string)
    {
        $charset = mb_detect_encoding($string);

        $unicodeString = iconv($charset, "UTF-8", $string);

        return str_replace($search, $replace, $unicodeString);
    }
}
if (!function_exists('media_preview_box')) {

    function media_preview_box($name, $path = null, $errors = null) { ?>
        <div class="media-wrapper" style="width: 200px">
            <div class="text-center">
                <img src="<?php echo get_image_uri(old(convert_array_field_name_to_with_dot($name), $path ?? '')); ?>" class="img-thumbnail image-tag <?php if ($errors && $errors->first(convert_array_field_name_to_with_dot($name))) : ?> border-danger <?php endif; ?>">
            </div>

            <input type="hidden" name="<?php echo $name; ?>" value="<?php echo old(convert_array_field_name_to_with_dot($name), $path ?? ''); ?>" class="media-input">

            <?php if ($errors && $errors->first(convert_array_field_name_to_with_dot($name))) : ?>
                <span class="invalid-feedback d-block" role="alert">
                    <strong><?php echo $errors->first(convert_array_field_name_to_with_dot($name)); ?></strong>
                </span>
            <?php endif; ?>

            <div class="mt-1 text-center">
                <button type="button" class="btn btn-outline-info btn-sm choice-media">Выбрать</button>

                <button type="button" class="btn btn-outline-danger btn-sm remove-media" title="Удалить">
                    <i class="fa fa-trash"></i>
                </button>
            </div>
        </div>
    <?php }

}

if (!function_exists('file_preview_box')) {

    function file_preview_box($name, $path = null, $errors = null) { ?>
        <div class="media-wrapper" style="width: 101px">
            <div class="text-center">
                <img src="<?php echo get_image_uri(old(convert_array_field_name_to_with_dot($name), $path ?? ''),'original',true,'file'); ?>"
                     class="img-thumbnail image-tag <?php if ($errors && $errors->first(convert_array_field_name_to_with_dot($name))) : ?> border-danger <?php endif; ?>"
                     title="<?= $path ? pathinfo($path)['basename'] : 'Файл не выбран' ?>"
                >
            </div>

            <input type="hidden" name="<?php echo $name; ?>" value="<?php echo old(convert_array_field_name_to_with_dot($name), $path ?? ''); ?>" class="file-path-field">

            <?php if ($errors && $errors->first(convert_array_field_name_to_with_dot($name))) : ?>
                <span class="invalid-feedback d-block" role="alert">
                    <strong><?php echo $errors->first(convert_array_field_name_to_with_dot($name)); ?></strong>
                </span>
            <?php endif; ?>

            <div class="mt-1 text-center">
                <button type="button" class="btn btn-outline-info btn-sm choice-file">Выбрать</button>

                <button type="button" class="btn btn-outline-danger btn-sm remove-file" title="Удалить">
                    <i class="fa fa-trash"></i>
                </button>
            </div>
        </div>
    <?php }

}

if (!function_exists('get_main_title')) {
    function get_main_title($lang)
    {
        $data = cache()->rememberForever('get_main_title_' . $lang, function () use($lang){
            $model = \App\Models\Pages::query()
                ->leftJoin('pages_translations', 'pages_translations.pages_id', '=', 'pages.id')
                ->where('pages_translations.lang', $lang)
                ->select([
                    'pages.*',
                    'pages_translations.meta_title AS transMetaTitle',
                    'pages_translations.meta_description AS transMetaDescription',
                ])
                ->where('pages.id', 1)
                ->first();
            return $model->transMetaTitle;
        });

        return $data;
    }
}

if (!function_exists('get_main_description')) {
    function get_main_description($lang)
    {
        $data = cache()->rememberForever('get_main_description_' . $lang, function () use($lang){
            $model = \App\Models\Pages::query()
                ->leftJoin('pages_translations', 'pages_translations.pages_id', '=', 'pages.id')
                ->where('pages_translations.lang', $lang)
                ->select([
                    'pages.*',
                    'pages_translations.meta_title AS transMetaTitle',
                    'pages_translations.meta_description AS transMetaDescription',
                ])
                ->where('pages.id', 1)
                ->first();
            return $model->transMetaDescription;
        });

        return $data;
    }
}
