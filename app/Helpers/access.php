<?php

if (!function_exists('permissions')) {

    function permissions($p) {
        $permissions = session()->get('user_permissions');

        if(is_string($p)){
            return in_array($p,$permissions);
        } elseif(is_array($p)){
            if(count($p)){
                $flag = false;

                foreach ($p as $item){
                    if(in_array($item,$permissions)){
                        $flag = true;
                    }
                }

                return $flag;
            }
        }

        return false;
    }

}
