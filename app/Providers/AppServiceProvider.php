<?php

namespace App\Providers;

use App\Models\Langs;
use App\Modules\Setting\Setting;
use Carbon\Carbon;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Setting::class, function ($app) {
            return new Setting();
        });
        $this->app->alias(Setting::class, 'Setting');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        URL::forceScheme('https');

        Blade::if('admin', function() {
            return auth()->check() && auth()->user()->isAdmin();
        });

        Blade::if('user', function() {
            return auth()->check() && auth()->user()->isUser();
        });

        Blade::if('permission', function($p) {
            $permissions = session()->get('user_permissions');

            if(is_string($p)){
                return in_array($p,$permissions);
            } elseif(is_array($p)){
                if(count($p)){
                    $flag = false;

                    foreach ($p as $item){
                        if(in_array($item,$permissions)){
                            $flag = true;
                        }
                    }

                    return $flag;
                }
            }

            return false;
        });

        $this->includeHelpers();

        Paginator::useBootstrap();

        config(['translatable.locales' => Langs::getLangsWithTitle()]);
        config(['translatable.locale' => Langs::getDefaultLangCode()]);
        config(['translatable.fallback_locale' => Langs::getDefaultLangCode()]);
    }

    /**
     * Include helper functions
     *
     * @return void
     */
    protected function includeHelpers(): void
    {
        foreach (glob(__DIR__ . '/../Helpers/*.php') as $file) {
            require_once $file;
        }
    }
}
