<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class InvoicesExport implements FromView
{
    private $dates;
    private $visit;
    private $visit_unique;
    private $click;
    private $click_unique;
    private $item_name;

    public function __construct($dates, $visit, $visit_unique, $click, $click_unique,$item_name)
    {
        $this->dates        = $dates;
        $this->visit        = $visit;
        $this->visit_unique = $visit_unique;
        $this->click        = $click;
        $this->click_unique = $click_unique;
        $this->item_name = $item_name;
    }

    public function view(): View
    {
        return view('admin.banners.export.table', [
            'dates'        => $this->dates,
            'visit'        => $this->visit,
            'visit_unique' => $this->visit_unique,
            'click'        => $this->click,
            'click_unique' => $this->click_unique,
            'item_name'    => $this->item_name,
        ]);
    }
}
