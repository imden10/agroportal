<?php

namespace App\Http\Controllers\Api;

use App\Core\Error\ErrorManager;
use App\Core\Response\ResponseTrait;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Service\Adapter;
use phpDocumentor\Reflection\File;
use Symfony\Component\HttpFoundation\Response;

class RssController extends Controller
{
    use ResponseTrait;

    private Adapter $adapter;

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function index(Request $request)
    {
        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['id'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['id']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = config('translatable.locale');
        }

        $pathToFile = $this->getFile($lang,$decodedJson['id']);

        if($pathToFile){
            $file = file_get_contents($pathToFile);
            $data['file'] = $file;
        } else {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                Response::HTTP_NOT_FOUND);
        }

        return $this->successResponse($data);
    }

    private function getFile($lang,$id)
    {
        if(file_exists(public_path() . '/rss_files/' . $lang . '/' . $id)){
            return public_path() . '/rss_files/' . $lang . '/' . $id;
        } else {
            return null;
        }
    }
}
