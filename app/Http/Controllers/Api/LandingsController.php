<?php

namespace App\Http\Controllers\Api;

use App\Core\Error\ErrorManager;
use App\Core\Response\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Models\Langs;
use App\Models\Landing;
use App\Modules\Setting\Setting;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\Adapter;

class LandingsController extends Controller
{
    use ResponseTrait;

    private Adapter $adapter;

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function getBySlug(Request $request)
    {

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['slug'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['slug']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = Langs::getDefaultLangCode();
        }

        $model = Landing::query()
            ->leftJoin('landing_translations', 'landing_translations.landing_id', '=', 'landings.id')
            ->where('landing_translations.lang', $lang)
            ->select([
                'landings.*',
                'landing_translations.title AS pageName',
                'landing_translations.meta_title AS transMetaTitle',
                'landing_translations.meta_description AS transMetaDescription',
            ])
            ->active()
            ->whereNotNull('landing_translations.title')
            ->where('landings.slug', $request->get('slug'))
            ->first();

        if (!$model)
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                //Response::HTTP_NOT_FOUND // 404
                Response::HTTP_FOUND //302 to home
            );

        $data = $this->adapter->prepareModelResults($model, $lang);

        // meta ********************************************************************************************************
        $defaultImage = cache()->remember('get_default_image',(60 * 60 * 24), function (){
            return get_image_uri_front(app(Setting::class)->get('default_og_image', \App\Models\Langs::getDefaultLangCode()));
        });


        $pageUrlUk = env('APP_URL') . '/project/' . trim($decodedJson['slug'],'/');
        $pageUrlRu = env('APP_URL') . '/ru/project/' . trim($decodedJson['slug'],'/');
        $pageUrlCurrent = $pageUrlUk;

        if($lang === 'ru'){
            $pageUrlCurrent = $pageUrlRu;
        }

        $meta = [
            'title'       => $model->transMetaTitle ?? get_main_title($lang),
            'description' => $model->transMetaDescription ?? get_main_description($lang),
            'og'          => [
                'title'       => $model->transMetaTitle ?? get_main_title($lang),
                'description' => $model->transMetaDescription ?? get_main_description($lang),
                'url'         => $pageUrlCurrent,
                'type' => 'website',
                'locale' => $lang,
                'site_name' => env('APP_NAME'),
                'image' => $model->image ? get_image_uri_front($model->image) : $defaultImage
            ],
            'links' => [
                [
                    'rel' => 'canonical',
                    'href' => $pageUrlCurrent
                ],
                [
                    'rel' => 'alternate',
                    'href' => $pageUrlUk,
                    'hreflang' => 'uk',
                ],
                [
                    'rel' => 'alternate',
                    'href' => $pageUrlRu,
                    'hreflang' => 'ru',
                ]
            ]
        ];

        $data['meta'] = $meta;

        return $this->successResponse($data);
    }
}
