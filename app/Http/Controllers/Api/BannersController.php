<?php

namespace App\Http\Controllers\Api;

use App\Core\Error\ErrorManager;
use App\Core\Response\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Models\Banners;
use App\Models\Langs;
use App\Service\Adapter;
use App\Traits\Statistics;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;

class BannersController extends Controller
{
    use ResponseTrait;
    use Statistics;

    private Adapter $adapter;

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function getByIds(Request $request)
    {

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['ids'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['ids']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = Langs::getDefaultLangCode();
        }

//        DB::enableQueryLog();

        $model = cache()->remember('banners-get-by-ids_' . implode('_',$decodedJson['ids']), (60 * 60 * 60), function () use($decodedJson) {
            return Banners::query()->whereIn('id',$decodedJson['ids'])->active()->get();
        });

        if(! $model){
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                Response::HTTP_NOT_FOUND);
        }

        $data = $this->adapter->prepareBannersResults($model,$lang);

//        dd(DB::getQueryLog());

        return $this->successResponse($data);
    }

    public function setViews(Request $request)
    {
        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['ids'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['ids']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $this->setViewsMany($decodedJson['ids'],request()->ip());

        return $this->successResponse([]);
    }

}
