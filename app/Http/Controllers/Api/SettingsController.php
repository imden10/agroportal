<?php

namespace App\Http\Controllers\Api;

use App\Core\Error\ErrorManager;
use App\Core\Response\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Models\Langs;
use App\Models\Settings;
use App\Service\Adapter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class SettingsController extends Controller
{
    use ResponseTrait;

    private Adapter $adapter;

    private $exceptions;

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->exceptions = [
            'address', 'email', 'news_title', 'news_description', 'news_per_page', 'main_page_setting'
        ];
    }

    public function getAll(Request $request)
    {
        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['lang'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['lang']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = config('translatable.locale');
        }

        $res = [];

        $result = cache()->rememberForever('get_all_setting_'  . $lang, function () use ($decodedJson, $lang, $res) {

            $model = Settings::query()
                ->select([
                    'code', 'value', 'lang'
                ])
                ->get()
                ->groupBy('lang')->transform(function ($item, $k) {
                    return $item->groupBy('code');
                })
                ->toArray();

            if (!$model)
                return $this->errorResponse(
                    ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                    Response::HTTP_NOT_FOUND
                );


            $footer_menu = $model['uk']['footer_menu'][0]['value'] ?? null;
            $footer_menu2 = $model['uk']['footer_menu2'][0]['value'] ?? null;


            $settings = $model[$lang];

            if (config('translatable.locale') !== $lang) {
                $settingKeys = array_keys($settings);
                $settingDefaultKeys = array_keys($model[config('translatable.locale')]);
                $diffKeys = array_diff($settingDefaultKeys, $settingKeys);

                if (count($diffKeys)) {
                    foreach ($model[config('translatable.locale')] as $key => $val) {
                        if (in_array($key, $diffKeys)) {
                            $settings[$key] = $val;
                        }
                    }
                }
            }

            $a = [];

            foreach ($settings as $key => $item) {
                if ($key == 'footer_cat') {
                    $val = json_decode($item[0]['value'], true);
                    $this->adapter->getBlockData($a, 1, $val['block1_type'], $val['block1_category'] ?? null, $val['block1_tag'] ?? null, $lang, 1);
                    $res['footer_article'] = $a;
                }

                if ($key == 'footer_cat2') {
                    $val = json_decode($item[0]['value'], true);
                    $this->adapter->getBlockData($a, 1, $val['block1_type'], $val['block1_category'] ?? null, $val['block1_tag'] ?? null, $lang, 2);
                }

                if ($key == 'footer_cat3') {
                    $val = json_decode($item[0]['value'], true);
                    $this->adapter->getBlockData($a, 1, $val['block1_type'], $val['block1_category'] ?? null, $val['block1_tag'] ?? null, $lang, 3);
                }


                if ($this->isJSON($item[0]['value'])) {
                    $val = array_values(json_decode($item[0]['value'], true));
                } else {
                    $val = $item[0]['value'];
                }

                if (!in_array($key, $this->exceptions)) {
                    $res[$key] = $val;
                }
            }

            unset($res['footer_cat']);
            unset($res['footer_cat2']);
            unset($res['footer_cat3']);

            $res['footer_article'] = $a;

            if ($footer_menu) {

                $main = \App\Models\Menu::query()
                    ->where('id', $footer_menu)
                    ->where('const',  1)
                    ->first();

                if ($main) {
                    $modelM = \App\Models\Menu::query()
                        ->where('tag', $main->tag)
                        ->where('const', '<>', 1)
                        ->with(['page', 'category', 'tag'])
                        ->defaultOrder()
                        ->get()
                        ->toTree()
                        ->toArray();

                    $res['footer_menu'] = $this->adapter->prepareMenuResults($modelM, $lang);
                }
            }

            if ($footer_menu2) {

                $main = \App\Models\Menu::query()
                    ->where('id', $footer_menu2)
                    ->where('const',  1)
                    ->first();

                if ($main) {
                    $modelM = \App\Models\Menu::query()
                        ->where('tag', $main->tag)
                        ->where('const', '<>', 1)
                        ->with(['page', 'category', 'tag'])
                        ->defaultOrder()
                        ->get()
                        ->toTree()
                        ->toArray();

                    $res['footer_menu2'] = $this->adapter->prepareMenuResults($modelM, $lang);
                }
            }
            return $res;
        });

        return $this->successResponse($result);
    }

    private function isJSON($string)
    {
        return is_string($string) && is_array(json_decode($string, true)) ? true : false;
    }
}
