<?php

namespace App\Http\Controllers\Api;

use App\Core\Error\ErrorManager;
use App\Core\Response\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Models\BannerItems;
use App\Traits\Statistics;
use Illuminate\Http\Request;

use App\Service\Adapter;
use Symfony\Component\HttpFoundation\Response;

class TGController extends Controller
{
    use ResponseTrait, Statistics;

    private Adapter $adapter;

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function index(Request $request)
    {
        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['id'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['id']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['type'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['type']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $data = [];

        $item = BannerItems::query()->where('id',$decodedJson['id'])->first();

        $type = $decodedJson['type'];

        if($item && $type){
            $this->setClick($item->id,request()->ip(), $_SERVER['HTTP_USER_AGENT']);

            $link = '';

            if($type == '1'){
                $link = $item->link;
            } elseif($type == '2'){
                $link = $item->link__2;
            }

            $data['link'] = $link;
        }

        return $this->successResponse($data);
    }
}
