<?php

namespace App\Http\Controllers\Api;

use App\Core\Error\ErrorManager;
use App\Core\Response\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Models\BlogArticles;
use App\Models\Bloggers;
use App\Models\Category;
use App\Models\Langs;
use App\Models\Pages;
use App\Models\SideCategorySettings;
use App\Modules\Setting\Setting;
use App\Modules\Widgets\Models\Widget;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;
use App\Service\Adapter;

class BloggersController extends Controller
{
    use ResponseTrait;

    private Adapter $adapter;

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function getBySlug(Request $request)
    {

        $take = (int)app(Setting::class)->get('news_per_page', Langs::getDefaultLangCode());

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['slug'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['slug']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = Langs::getDefaultLangCode();
        }

        $blogger = Bloggers::query()
            ->leftJoin('blogger_translations', 'blogger_translations.bloggers_id', '=', 'bloggers.id')
            ->where('blogger_translations.lang', $lang)
            ->select([
                'bloggers.*',
                'blogger_translations.name AS bloggerName',
                'blogger_translations.meta_title AS bloggerMetaTitle',
                'blogger_translations.meta_description AS bloggerMetaDescription',
                'blogger_translations.text AS bloggerText',
                'blogger_translations.text_for_main AS bloggerTextForMain',
            ])
            ->active()
            ->where('bloggers.slug', $decodedJson['slug'])
            ->first();

        if (!$blogger)
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                Response::HTTP_NOT_FOUND
            );

        $articlesQuery = BlogArticles::query()
            ->leftJoin('category', 'category.id', '=', 'blog_articles.category_id')
            ->leftJoin('category_translations', 'category_translations.category_id', '=', 'blog_articles.category_id')
            ->where('category_translations.lang', $lang)
            ->orderBy('blog_articles.public_date', 'desc')
            ->select([
                'blog_articles.*',
                'category.path',
                'category_translations.title AS category_name',
            ])
            ->where('blog_articles.status', BlogArticles::STATUS_ACTIVE)
            ->where(function ($q) use ($blogger) {
                $q->where('blog_articles.blogger_id', $blogger->id);
            });

        $total = $articlesQuery->count();

        $currentPage = $decodedJson['page'] ?? 1;

        $articlesQuery
            ->take($take)
            ->skip($currentPage == 1 ? 0 : (($take * $currentPage) - $take));

        $articles = $articlesQuery->get();

        $items = $this->adapter->prepareModelsResults($articles, $lang);

        $paginate = [
            'total'        => $total,
            'per_page'     => $take,
            'current_page' => $currentPage,
        ];

        $items['paginate'] = $paginate;

        $data = [
            'blogger' => [
                'name'             => $blogger->bloggerName,
                'image'            => $blogger->image,
                'image_thumbnail'  => $blogger->image_thumbnail,
                'position'         => $blogger->bloggerTextForMain,
                'text'             => $blogger->bloggerText,
                'slug'             => $decodedJson['slug'],
                'meta_title'       => $blogger->bloggerMetaTitle,
                'meta_description' => $blogger->bloggerMetaDescription,
                'socials'          => [
                    'email'     => $blogger->email,
                    'facebook'  => $blogger->facebook,
                    'instagram' => $blogger->instagram,
                    'telegram'  => $blogger->telegram
                ]
            ],
            'items'   => $items
        ];

        $setting = [
            'news' => [
                'status' => 1,
                'count'  => 10
            ]
        ];

        $data['aside'] = $this->adapter->prepareAsideResults($setting, $lang);

        return $this->successResponse($data);
    }

    public function getBySlugItems(Request $request)
    {

        $take = (int)app(Setting::class)->get('news_per_page', Langs::getDefaultLangCode());

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['slug'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['slug']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = Langs::getDefaultLangCode();
        }

        $blogger = Bloggers::query()
            ->leftJoin('blogger_translations', 'blogger_translations.bloggers_id', '=', 'bloggers.id')
            ->where('blogger_translations.lang', $lang)
            ->select([
                'bloggers.*',
                'blogger_translations.name AS bloggerName',
                'blogger_translations.meta_title AS bloggerMetaTitle',
                'blogger_translations.meta_description AS bloggerMetaDescription',
                'blogger_translations.text AS bloggerText',
                'blogger_translations.text_for_main AS bloggerTextForMain',
            ])
            ->active()
            ->where('bloggers.slug', $decodedJson['slug'])
            ->first();

        if (!$blogger)
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                Response::HTTP_NOT_FOUND
            );

        $articlesQuery = BlogArticles::query()
            ->leftJoin('category', 'category.id', '=', 'blog_articles.category_id')
            ->leftJoin('category_translations', 'category_translations.category_id', '=', 'blog_articles.category_id')
            ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
            ->where('category_translations.lang', $lang)
            ->where('blog_article_translations.lang', $lang)
            ->whereNotNull('blog_article_translations.name')
            ->orderBy('blog_articles.public_date', 'desc')
            ->select([
                'blog_articles.*',
                'category.path',
                'category_translations.title AS category_name',
            ])
            ->active()
            ->where(function ($q) use ($blogger) {
                $q->where('blog_articles.blogger_id', $blogger->id);
            });

        $total = $articlesQuery->count();

        $currentPage = $decodedJson['page'] ?? 1;

        $articlesQuery
            ->take($take)
            ->skip($currentPage == 1 ? 0 : (($take * $currentPage) - $take));

        $articles = $articlesQuery->get();

        $items = $this->adapter->prepareModelsResults($articles, $lang);

        $paginate = [
            'total'        => $total,
            'per_page'     => $take,
            'current_page' => $currentPage,
        ];

        $items['paginate'] = $paginate;

        $data = [
            'blogger' => [
                'name'             => $blogger->bloggerName,
                'image'            => $blogger->image,
                'image_thumbnail'  => $blogger->image_thumbnail,
                'position'         => $blogger->bloggerTextForMain,
                'text'             => $blogger->bloggerText,
                'slug'             => $decodedJson['slug'],
                'meta_title'       => $blogger->bloggerMetaTitle,
                'meta_description' => $blogger->bloggerMetaDescription,
                'socials'          => [
                    'email'     => $blogger->email,
                    'facebook'  => $blogger->facebook,
                    'instagram' => $blogger->instagram,
                    'telegram'  => $blogger->telegram
                ]
            ],
            'items'   => $items
        ];

        // meta ********************************************************************************************************
        $defaultImage = cache()->remember('get_default_image',(60 * 60 * 24), function (){
            return get_image_uri_front(app(Setting::class)->get('default_og_image', \App\Models\Langs::getDefaultLangCode()));
        });


        $pageUrlUk = env('APP_URL') . '/authors/' . trim($decodedJson['slug'],'/');
        $pageUrlRu = env('APP_URL') . '/ru/authors/' . trim($decodedJson['slug'],'/');
        $pageUrlCurrent = $pageUrlUk;

        if($lang === 'ru'){
            $pageUrlCurrent = $pageUrlRu;
        }

        $meta = [
            'title'       => $blogger->bloggerMetaTitle ?? get_main_title($lang),
            'description' => $blogger->bloggerMetaDescription ?? get_main_description($lang),
            'og'          => [
                'title'       => $blogger->bloggerMetaTitle ?? get_main_title($lang),
                'description' => $blogger->bloggerMetaDescription ?? get_main_description($lang),
                'url'         => $pageUrlCurrent,
                'type' => 'website',
                'locale' => $lang,
                'site_name' => env('APP_NAME'),
                'image' => $blogger->image ? get_image_uri_front($blogger->image) : $defaultImage
            ],
            'links' => [
                [
                    'rel' => 'canonical',
                    'href' => $pageUrlCurrent
                ],
                [
                    'rel' => 'alternate',
                    'href' => $pageUrlUk,
                    'hreflang' => 'uk',
                ],
                [
                    'rel' => 'alternate',
                    'href' => $pageUrlRu,
                    'hreflang' => 'ru',
                ]
            ]
        ];

        $data['meta'] = $meta;

        return $this->successResponse($data);
    }

    public function getBySlugAside(Request $request)
    {
        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = Langs::getDefaultLangCode();
        }

        $setting = [
            'news' => [
                'status' => 1,
                'count'  => 10
            ]
        ];

        $data['aside'] = $this->adapter->prepareAsideResults($setting, $lang);

        return $this->successResponse($data);
    }

    public function getAll(Request $request)
    {
        //        DB::enableQueryLog();

        $take = 10;

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = Langs::getDefaultLangCode();
        }

        $currentPage = $decodedJson['page'] ?? 1;

        $articlesQuery = Bloggers::query()
            ->leftJoin('blogger_translations', 'blogger_translations.bloggers_id', '=', 'bloggers.id')
            ->where('blogger_translations.lang', $lang)
            ->select([
                'bloggers.*',
                'blogger_translations.name AS bloggerName',
                'blogger_translations.meta_title AS bloggerMetaTitle',
                'blogger_translations.meta_description AS bloggerMetaDescription',
                'blogger_translations.text AS bloggerText',
                'blogger_translations.text_for_main AS bloggerTextForMain'
            ])
            ->active()
            ->orderBy('bloggers.created_at', 'desc');


        $total = $articlesQuery->count();

        $articlesQuery
            ->take($take)
            ->skip($currentPage == 1 ? 0 : (($take * $currentPage) - $take));

        $articles = $articlesQuery->get();

        //        dd(DB::getQueryLog());

        $items = $this->adapter->prepareModelsResults($articles, $lang);

        $paginate = [
            'total'        => $total,
            'per_page'     => $take,
            'current_page' => $currentPage,
        ];

        $items['paginate'] = $paginate;

        $data = [
            'items' => $items
        ];

        $setting = [
            'news' => [
                'status' => 1,
                'count'  => 10
            ]
        ];

        $data['aside'] = $this->adapter->prepareAsideResults($setting, $lang);

        /*************************************** Breadcrumbs *********************************************/
        $mainPageUrl = config('app.url') . ($lang !== Langs::getDefaultLangCode() ? ('/' . $lang) : '');
        $breadcrumbs = [
            [
                'name' => __('Main page', [], $lang),
                'url'  => $mainPageUrl
            ],
            [
                'name' => __('Bloggers', [], $lang),
                'url'  => null
            ],
        ];

        $data['breadcrumbs'] = $breadcrumbs;

        // meta ********************************************************************************************************
        $defaultImage = cache()->remember('get_default_image',(60 * 60 * 24), function (){
            return get_image_uri_front(app(Setting::class)->get('default_og_image', \App\Models\Langs::getDefaultLangCode()));
        });


        $pageUrlUk = env('APP_URL') . '/authors';
        $pageUrlRu = env('APP_URL') . '/ru/authors';
        $pageUrlCurrent = $pageUrlUk;

        if($lang === 'ru'){
            $pageUrlCurrent = $pageUrlRu;
        }

        $meta = [
            'title'       => get_main_title($lang),
            'description' => get_main_description($lang),
            'og'          => [
                'title'       => get_main_title($lang),
                'description' => get_main_description($lang),
                'url'         => $pageUrlCurrent,
                'type'        => 'website',
                'locale'      => $lang,
                'site_name'   => env('APP_NAME'),
                'image'       => $defaultImage
            ],
            'links'       => [
                [
                    'rel'  => 'canonical',
                    'href' => $pageUrlCurrent
                ],
                [
                    'rel'      => 'alternate',
                    'href'     => $pageUrlUk,
                    'hreflang' => 'uk',
                ],
                [
                    'rel'      => 'alternate',
                    'href'     => $pageUrlRu,
                    'hreflang' => 'ru',
                ]
            ]
        ];

        $data['meta'] = $meta;

        return $this->successResponse($data);
    }
}
