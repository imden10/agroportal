<?php

namespace App\Http\Controllers\Api;

use App\Core\Error\ErrorManager;
use App\Core\Response\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Models\Langs;
use App\Models\Pages;
use App\Modules\Setting\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;
use App\Service\Adapter;

class MainController extends Controller
{
    use ResponseTrait;

    private Adapter $adapter;

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function getMain(Request $request)
    {

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['lang'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['lang']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['news_count'])) {
            $news_count = $decodedJson['news_count'];
        } else {
            $news_count = 10;
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = config('translatable.locale');
        }

        $model = cache()->remember('getMainPageModel',(60 * 5), function (){
           return Pages::query()
               ->active()
               ->where('id', 1)
               ->first();
        });

        if (!$model)
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                Response::HTTP_NOT_FOUND);


        $data = $this->adapter->prepareModelResults($model,$lang);

        /*Settings*/
        $setting = cache()->remember('getMainPageModelSetting',(60 * 5), function (){
           return app(Setting::class)->get('main_page_setting', config('translatable.locale'));
        });

        if($setting){
            $setting = json_decode($setting,true);
        } else {
            $setting = [];
        }

        $data['blocks'] = $this->adapter->prepareMainPageBlocksResults($setting,$lang,$news_count);

        // meta ********************************************************************************************************
        $defaultImage = cache()->remember('get_default_image_2',(60 * 60 * 24), function (){
            return get_image_uri_front(app(Setting::class)->get('default_og_image', \App\Models\Langs::getDefaultLangCode()));
        });

        $pageUrlUk = env('APP_URL');
        $pageUrlRu = env('APP_URL') . '/ru';
        $pageUrlCurrent = $pageUrlUk;

        if($lang === 'ru'){
            $pageUrlCurrent = $pageUrlRu;
        }

        $metaTitle = $model->getTranslation($lang)->meta_title ?? '';
        $metaDescription = $model->getTranslation($lang)->meta_description ?? '';

//        $mr = '{
//          "@context": "http://schema.org/",
//          "@type": "Organization",
//          "name": "'.$metaTitle.'",
//          "logo": "'.$defaultImage.'",
//          "url": "https://agroportal.ua/",
//          "address": {
//            "@type": "PostalAddress",
//            "addressCountry": "Ukraine",
//            "email": "npomianska@gmail.com"
//          },
//          "sameAs": ["https://www.facebook.com/AgroPortalUa", "https://www.instagram.com/agroportal.ua/", "https://www.youtube.com/channel/UCnuqno49s9C8SJTZAUzTDFQ"]
//        }
//        ';

// main&categories
        $microMarking[] = [
            "@context" => "https://schema.org",
            "@type" => "NewsMediaOrganization",
            "name" => "AgroPortal.ua",
            "legalName" => "Агроновини України та світу: рослинництво, тваринництво, агротехнології, органіка, сільгосптехніка, клімат, ринок землі, економіка. ",
            "url" => "https://agroportal.ua/",
            "logo" => "https://agroportal.ua/_nuxt/img/logo.6754696.png",
             "sameAs" => [
                "https://www.facebook.com/AgroPortalUa",
                "https://t.me/agroportalua",
                "https://www.instagram.com/agroportal.ua/",
                "https://www.youtube.com/channel/UCnuqno49s9C8SJTZAUzTDFQ",
                "https://www.linkedin.com/company/agroportalua/",
                "https://www.tiktok.com/@agroportal.ua?"
            ],
            "foundingDate" => "2015",
            "address" => [
                "@type" => "PostalAddress",
                "streetAddress" => "Майдан Незалежності, 2",
                "addressLocality" => "Київ",
                "postalCode" => "01001",
                "addressCountry" => "UA"
            ],
            "contactPoint" => [
                [
                    "@type" => "ContactPoint",
                    "email" => "npomianska@gmail.com",
                    "contactType" => "headquoters",
                    "areaServed" => "UA",
                    "availableLanguage" => ["ru-UA","uk-UA"]
                ]
            ]
           ];


        array_push($microMarking, $data['micro_marking']);
        $mr = [json_encode($microMarking)];
        
        // $mr = '{
        //   "@context": "http://schema.org",
        //   "@type": "Organization",
        //   "name" : "'.$metaTitle.'",
        //   "description" : "'.$metaDescription.'",
        //   "url": "https://agroportal.ua" ,
        //   "address": {  "@type": "PostalAddress",    "addressCountry": "Ukraine",    "addressLocality": "Kiev" },
        //   "email" : "npomianska@gmail.com",
        //   "logo": "'.$defaultImage.'",
        //   "sameAs": ["https://www.facebook.com/AgroPortalUa", "https://www.instagram.com/agroportal.ua/", "https://www.youtube.com/channel/UCnuqno49s9C8SJTZAUzTDFQ"]
        // },
        // {
        //   "@context": "http://schema.org",
        //   "@type": "WebSite",
        //   "url": "https://agroportal.ua",
        //   "potentialAction": {
        //     "@type": "SearchAction",
        //     "target": "https://agroportal.ua/search?s={search_term_string}",
        //     "query-input": "required name=search_term_string"
        //   }
        // }
        // ';

        $meta = [
            'title'       => $metaTitle,
            'description' => $metaDescription,
            'og'          => [
                'title'       => $metaTitle,
                'description' => $metaDescription,
                'url'         => $pageUrlCurrent,
                'type' => 'website',
                'locale' => $lang,
                'site_name' => env('APP_NAME'),
                'image' => $model->image ? get_image_uri($model->image) : $defaultImage
            ],
            'links' => [
                [
                    'rel' => 'canonical',
                    'href' => $pageUrlCurrent
                ],
                [
                    'rel' => 'alternate',
                    'href' => $pageUrlUk,
                    'hreflang' => 'uk',
                ],
                [
                    'rel' => 'alternate',
                    'href' => $pageUrlRu,
                    'hreflang' => 'ru',
                ]
            ],
            'script' => $mr
        ];

        $data['meta'] = $meta;

        return $this->successResponse($data);
    }
}
