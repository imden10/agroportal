<?php

namespace App\Http\Controllers\Api;

use App\Core\Error\ErrorManager;
use App\Core\Response\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Models\Faq;
use App\Models\Langs;
use App\Models\Pages;
use App\Models\PagesMenu;
use App\Modules\Setting\Setting;
use App\Modules\Widgets\Models\Widget;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\Adapter;

class PagesController extends Controller
{
    use ResponseTrait;

    private Adapter $adapter;

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function getBySlug(Request $request)
    {

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['slug'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['slug']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = Langs::getDefaultLangCode();
        }

        $model = Pages::query()
            ->leftJoin('pages_translations', 'pages_translations.pages_id', '=', 'pages.id')
            ->where('pages_translations.lang', $lang)
            ->select([
                'pages.*',
                'pages_translations.title AS pageName',
                'pages_translations.meta_title AS transMetaTitle',
                'pages_translations.meta_description AS transMetaDescription',
            ])
            ->active()
            ->where('pages.slug', $request->get('slug'))
            ->first();

        if (!$model)
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                Response::HTTP_NOT_FOUND);

        $data = $this->adapter->prepareModelResults($model, $lang);

        if ($decodedJson['slug'] === 'faq') {
            $faqModel = Faq::query()->get();
            $faqData  = $this->adapter->prepareFaqsResults($faqModel, $lang);

            $widgetQuestionsForm = Widget::query()->where('instance', 'questions-form')->where('lang', $lang)->first()->toArray();

            if ($widgetQuestionsForm) {
                $data['constructor'] = [
                    'questions-form' => isset($widgetQuestionsForm) ? $widgetQuestionsForm['data'] : [],
                ];
            }

            $data['faq_data'] = $faqData;
        }

        $setting       = [
            'news' => [
                'status' => 1,
                'count'  => 10
            ]
        ];

        $data['aside'] = $this->adapter->prepareAsideResults($setting, $lang);

        /*************************************** Breadcrumbs *********************************************/
        $mainPageUrl = config('app.url') . ($lang !== Langs::getDefaultLangCode() ? ('/' . $lang) : '');
        $breadcrumbs = [
            [
                'name' => __('Main page', [], $lang),
                'url'  => $mainPageUrl
            ]
        ];

        $breadcrumbs[] = [
            'name' => $model->pageName,
            'url'  => $mainPageUrl . '/' . $model->slug
        ];

        $data['breadcrumbs'] = $breadcrumbs;

        // meta ********************************************************************************************************
        $defaultImage = cache()->remember('get_default_image',(60 * 60 * 24), function (){
            return get_image_uri_front(app(Setting::class)->get('default_og_image_front', \App\Models\Langs::getDefaultLangCode()));
        });


        $pageUrlUk = env('APP_URL') . '/' . trim($decodedJson['slug'],'/');
        $pageUrlRu = env('APP_URL') . '/ru/' . trim($decodedJson['slug'],'/');
        $pageUrlCurrent = $pageUrlUk;

        if($lang === 'ru'){
            $pageUrlCurrent = $pageUrlRu;
        }

        $meta = [
            'title'       => $model->transMetaTitle ?? get_main_title($lang),
            'description' => $model->transMetaDescription ?? get_main_description($lang),
            'og'          => [
                'title'       => $model->transMetaTitle ?? get_main_title($lang),
                'description' => $model->transMetaDescription ?? get_main_description($lang),
                'url'         => $pageUrlCurrent,
                'type' => 'website',
                'locale' => $lang,
                'site_name' => env('APP_NAME'),
                'image' => $model->image ? get_image_uri_front($model->image) : $defaultImage
            ],
            'links' => [
                [
                    'rel' => 'canonical',
                    'href' => $pageUrlCurrent
                ],
                [
                    'rel' => 'alternate',
                    'href' => $pageUrlUk,
                    'hreflang' => 'uk',
                ],
                [
                    'rel' => 'alternate',
                    'href' => $pageUrlRu,
                    'hreflang' => 'ru',
                ]
            ]
        ];

        $data['meta'] = $meta;

        return $this->successResponse($data);
    }
}
