<?php

namespace App\Http\Controllers\Api;

use App\Core\Error\ErrorManager;
use App\Core\Response\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Models\Plasmacenters;
use App\Service\TelegramBot;
use Carbon\Carbon;
use Carbon\Exceptions\InvalidFormatException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\Adapter;

class RequestController extends Controller
{
    use ResponseTrait;

    private Adapter $adapter;

    private $bot;

    public function __construct(Adapter $adapter, TelegramBot $telegramBot)
    {
        $this->adapter = $adapter;
        $this->bot     = $telegramBot;
    }

    public function addRequest(Request $request)
    {

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['name'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['name']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['phone'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['phone']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['plasmacenter_id'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['plasmacenter_id']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!Plasmacenters::query()->where('id', $decodedJson['plasmacenter_id'])->exists()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_PLASMACENTER_NOT_FOUND, ['plasmacenter_id:' . $decodedJson['plasmacenter_id']]),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['date'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['date']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        try {
            $date = Carbon::createFromFormat('Y-m-d', $decodedJson['date']);
        } catch (InvalidFormatException $e) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_DATE_FORMAT_NOT_VALID, []),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['time'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['time']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        try {
            $datetime = Carbon::createFromFormat('Y-m-d H:i', $decodedJson['date'] . ' ' . $decodedJson['time']);
        } catch (InvalidFormatException $e) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_TIME_FORMAT_NOT_VALID, []),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        /* @var $model \App\Models\Request */
        $model = \App\Models\Request::create([
            'name'            => $decodedJson['name'],
            'phone'           => $decodedJson['phone'],
            'plasmacenter_id' => $decodedJson['plasmacenter_id'],
            'datetime'        => $datetime->format('Y-m-d H:i'),
            'status'          => \App\Models\Request::STATUS_NEW
        ]);

        $data = $model->toArray();

        $model->load('plasmacenter');

        $message = "Новая запись" . PHP_EOL;

        $message .= "Имя - " . $decodedJson['name'] . PHP_EOL;

        $message .= "Телефон - " . $decodedJson['phone'] . PHP_EOL;

        $message .= "Плазмацентр - " . $model->plasmacenter->title . PHP_EOL;

        $message .= "На дату - " . $datetime->format('d-m-Y H:i') . PHP_EOL;


        $flag = $this->bot->sendMessage($message);

        return $this->successResponse($data);
    }

    public function feedback(Request $request)
    {

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['company'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['company']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['name'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['name']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['phone'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['phone']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['email'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['email']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        //        if (!isset($decodedJson['text'])) {
        //            return $this->errorResponse(
        //                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['text']),
        //                Response::HTTP_UNPROCESSABLE_ENTITY
        //            );
        //        }

        $message = "Новый запрос" . PHP_EOL;

        $message .= "Имя - " . $decodedJson['name'] . PHP_EOL;

        $message .= "Компания - " . $decodedJson['company'] . PHP_EOL;

        $message .= "Телефон - " . $decodedJson['phone'] . PHP_EOL;

        $message .= "E-mail - " . $decodedJson['email'] . PHP_EOL;

        if (isset($decodedJson['text'])) {
            $message .= "Вопрос - " . $decodedJson['text'] . PHP_EOL;
        }

        $flag = $this->bot->sendMessage($message);

        return $this->successResponse([]);
    }

    public function callback(Request $request)
    {
        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['phone'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['phone']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $message = "Новый запрос на обратный звонок" . PHP_EOL;

        if (isset($decodedJson['promo'])) {
            $message .= "Источник: promo-" . $decodedJson['promo'] . PHP_EOL;
        }

        if (isset($decodedJson['plasmacentr'])) {
            $message .= "Плазмацентр - " . $decodedJson['plasmacentr'] . PHP_EOL;
        }

        $message .= "Телефон - " . $decodedJson['phone'] . PHP_EOL;

        $flag = $this->bot->sendMessage($message);

        if (isset($decodedJson['promo']) && in_array($decodedJson['promo'], ['1', '2'])) {
            /* @var $model \App\Models\Request */
            $model = \App\Models\Request::create([
                'phone'             => $decodedJson['phone'],
                'plasmacenter_name' => $decodedJson['plasmacentr'] ?? '',
                'promo'             => $decodedJson['promo'],
                'status'            => \App\Models\Request::STATUS_NEW
            ]);
        }

        return $this->successResponse([]);
    }
}
