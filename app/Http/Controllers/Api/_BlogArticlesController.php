<?php

namespace App\Http\Controllers\Api;

use App\Core\Error\ErrorManager;
use App\Core\Response\ResponseTrait;
use App\Http\Controllers\Controller;
use App\Models\BlogArticles;
use App\Models\BlogArticlesNewTags;
use App\Models\Category;
use App\Models\Faq;
use App\Models\Langs;
use App\Models\NewTag;
use App\Models\Pages;
use App\Models\SideCategorySettings;
use App\Models\Translations\NewTagTranslation;
use App\Modules\Setting\Setting;
use App\Modules\Widgets\Models\Widget;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;
use App\Service\Adapter;

class BlogArticlesController extends Controller
{
    use ResponseTrait;

    private Adapter $adapter;

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    public function getBySlug(Request $request)
    {

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['slug'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['slug']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = config('translatable.locale');
        }


        $m_query = BlogArticles::query()
            ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
            ->where('blog_article_translations.lang', $lang)
            ->whereNotNull('blog_article_translations.name')
            ->select([
                'blog_articles.*',
                'blog_article_translations.name AS articleName'
            ]);

        if (isset($decodedJson['prevw'])) {
            if ($decodedJson['prevw'] == crc32($request->get('slug'))) { // check
                // good
            } else {
                $m_query->active();
            }

        } else {
            $m_query->active();
        }


        $model = $m_query
            ->where('blog_articles.slug', $request->get('slug'))
            ->with('gallery')
            ->first();

        if (!$model)
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                //Response::HTTP_NOT_FOUND // 404
                Response::HTTP_FOUND //302 to home
            );


        $model->views = ($model->views + 1);
        $model->save();

        $data = $this->adapter->prepareModelResults($model, $lang);

        //        dd($data);

        $category = Category::query()
            ->leftJoin('category_translations', 'category_translations.category_id', '=', 'category.id')
            ->where('category_translations.lang', $lang)
            ->select([
                'category.*',
                'category_translations.title AS categoryName',
                'category_translations.meta_title AS categoryMetaTitle',
                'category_translations.meta_description AS categoryMetaDescription',
            ])
            ->where('category.id', $model->category_id)
            ->first();


        // Aside Setting
        if ($category) {
            $mainCatId = $category->id;
            if ($category->parent_id !== 0) {
                $mainCatId = $category->parent_id;
            }

            $setting = SideCategorySettings::query()->where('category_id', $mainCatId)->first();

            if ($setting) {
                $setting       = json_decode($setting->setting, true);
                $data['aside'] = $this->adapter->prepareAsideResults($setting, $lang);
            } else {
                $data['aside'] = [];
            }
        }

        $articleMultimedia = app(Setting::class)->get('article_multimedia', config('translatable.locale'));

        if ($articleMultimedia) {
            $articleMultimedia = json_decode($articleMultimedia, true);
            $this->adapter->getBlockData($a, 3, $articleMultimedia['type'], $articleMultimedia['category'] ?? null, $articleMultimedia['tag'] ?? null, $lang, 1);
            $data['article_multimedia'] = $a ?? null;
        }


        if ($data['translate']['tags']) {
            $tags     = explode(',', $data['translate']['tags']);
            $addModel = null;
            $ad       = [];
            $countAd  = 0;
            $ids      = [];


            foreach ($tags as $key => $tag) {
                $ad[$key] = BlogArticles::query()
                    ->leftJoin('category', 'category.id', '=', 'blog_articles.category_id')
                    ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
                    ->where('blog_article_translations.lang', $lang)
                    ->where('blog_article_translations.tags', 'like', '%' . $tag . '%')
                    ->where('blog_articles.id', '<>', $model->id)
                    ->orderBy('blog_articles.public_date', 'desc')
                    ->select([
                        'blog_articles.*',
                        'category.path',
                        'blog_article_translations.tags'
                    ])
                    ->active()
                    ->limit('4')
                    ->get();

                $countAd += count($ad[$key]);
            }

            //            dd($ad,$countAd,$ids);

            $counterBreak = 16;

            while ($countAd > 0 && count($ids) < 4 && $counterBreak > 0) {
                foreach ($ad as $a) {
                    foreach ($a as $it) {
                        if (!in_array($it->id, $ids)) {
                            $ids[] = $it->id;
                            $countAd--;

                            break;
                        }
                        $counterBreak--;
                    }
                }
            }

            if (count($ids) > 4) {
                $ids = array_slice($ids, 0, 4);
            }

            //            dd($ad,$ids);

            if (count($ids)) {
                $addModel = BlogArticles::query()
                    ->leftJoin('category', 'category.id', '=', 'blog_articles.category_id')
                    ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
                    ->where('blog_article_translations.lang', $lang)
                    ->whereIn('blog_articles.id', $ids)
                    ->orderBy('blog_articles.public_date', 'desc')
                    ->select([
                        'blog_articles.*',
                        'category.path'
                    ])
                    ->active()
                    ->limit('4')
                    ->get();
            }

            if ($addModel && count($addModel)) {
                $items                 = $this->adapter->prepareModelsResults($addModel, $lang);
                $data['news_by_theme'] = $items['models'];
            }
        }

        /*************************************** Breadcrumbs *********************************************/
        $mainPageUrl = config('app.url') . ($lang !== config('translatable.locale') ? ('/' . $lang) : '');
        $breadcrumbs = [
            [
                'name' => __('Main page', [], $lang),
                'url'  => $mainPageUrl
            ]
        ];


        if (isset($category->parent_id)) {
            if ($category->parent_id) {
                $mainCat = Category::query()
                    ->where('category.id', $category->parent_id)
                    ->leftJoin('category_translations', 'category_translations.category_id', '=', 'category.id')
                    ->where('category_translations.lang', $lang)
                    ->select([
                        'category.*',
                        'category_translations.title AS categoryName',
                        'category_translations.meta_title AS categoryMetaTitle',
                        'category_translations.meta_description AS categoryMetaDescription',
                    ])
                    ->first();

                if ($mainCat) {
                    $breadcrumbs[] = [
                        'name' => $mainCat->categoryName,
                        'url'  => $mainPageUrl . '/' . $mainCat->path
                    ];
                }

                $breadcrumbs[] = [
                    'name' => $category->categoryName,
                    'url'  => $mainPageUrl . '/' . $category->path
                ];
            } else {
                $breadcrumbs[] = [
                    'name' => $category->categoryName,
                    'url'  => $mainPageUrl . '/' . $category->path
                ];
            }
        }


        //        $breadcrumbs[] = [
        //            'name' => $model->articleName,
        //            'url' => $mainPageUrl . '/' . $category->path . '/' . $model->slug
        //        ];

        $data['breadcrumbs'] = $breadcrumbs;

        return $this->successResponse($data);
    }

    /**
     * Получить только модель (основные данные)
     *
     * data in {slug,lang}
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBySlugModel(Request $request)
    {

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['slug'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['slug']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = config('translatable.locale');
        }

        $m = BlogArticles::query()->where('blog_articles.slug', $decodedJson['slug'])->first();

        if ($m) {
            $m->views = ($m->views + 1);
            $m->save();
        }

        $model = cache()->remember('getBySlugModel_' . $decodedJson['slug'] . '_' . $lang, (60 * 5), function () use ($decodedJson, $lang) {
            $m_query = BlogArticles::query()
                ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
                ->leftJoin('category', 'category.id', '=', 'blog_articles.category_id')
                ->where('blog_article_translations.lang', $lang)
                ->whereNotNull('blog_article_translations.name')
                ->select([
                    'blog_articles.*',
                    'blog_article_translations.name AS articleName',
                    'blog_article_translations.meta_title AS transMetaTitle',
                    'blog_article_translations.meta_description AS transMetaDescription',
                    'category.path AS categoryPath',
                ]);

            if (isset($decodedJson['prevw'])) {
                if ($decodedJson['prevw'] == crc32($decodedJson['slug'])) { // check
                    // good
                } else {
                    $m_query->active();
                }

            } else {
                $m_query->active();
            }

            return $m_query
                ->where('blog_articles.slug', $decodedJson['slug'])
                ->with('gallery')
                ->first();
        });

        if (!$model)
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                //Response::HTTP_NOT_FOUND // 404
                Response::HTTP_FOUND //302 to home
            );

        $model->views = $m->views;

        $data = $this->adapter->prepareModelResults($model, $lang);

        // meta ********************************************************************************************************

        $defaultImage = cache()->remember('get_default_image_front',(60 * 60 * 24), function (){
            return get_image_uri_front(app(Setting::class)->get('default_og_image', \App\Models\Langs::getDefaultLangCode()));
        });

        $pageUrlUk = env('APP_URL') . '/' . $model->categoryPath . '/' . $model->slug;
        $pageUrlRu = env('APP_URL') . '/ru/' . $model->categoryPath . '/' . $model->slug;
        $pageUrlCurrent = $pageUrlUk;

        if($lang === 'ru'){
            $pageUrlCurrent = $pageUrlRu;
        }

//        $mr = '{
//                  "@context": "http://schema.org/",
//                  "@type": "Article",
//                  "mainEntityOfPage": {
//                    "@type": "WebPage",
//                    "@id": "'.$pageUrlCurrent.'"
//                  },
//                  "author": {
//                    "@type": "Organization",
//                    "name": "AgroPortal"
//                  },
//                  "publisher": {
//                    "@type": "Organization",
//                    "name": "AgroPortal.ua",
//                    "logo": {
//                      "@type": "ImageObject",
//                      "url": "'.$defaultImage.'"
//                    }
//                  },
//                  "headline": "'.$model->articleName.'",
//                  "image": "'.get_image_uri_front($model->image).'",
//                  "datePublished": "'.\Carbon\Carbon::parse($model->public_date)->format('Y-m-d').'"
//                }
//        ';

        $imgWidth = 0;
        $imgHeight = 0;
        try {
            if($model->image){
                [$imgWidth, $imgHeight] = getimagesize(get_image_uri_front($model->image));
            }
        } catch( \Exception $e){
        }

        $imgWidth2 = 0;
        $imgHeight2 = 0;
        try {
            [$imgWidth2, $imgHeight2] = getimagesize($defaultImage);
        } catch( \Exception $e){}

        $mr = '{
                  "@context": "http://schema.org",
                  "@type": "NewsArticle",
                  "mainEntityOfPage":{
                    "@type":"WebPage",
                    "@id":"'.$pageUrlCurrent.'"
                  },
                  "headline": "'.$model->articleName.'",
                  "description": "'.$model->transMetaDescription.'",
                  "image": {
                    "@type": "ImageObject",
                    "url": "'.get_image_uri_front($model->image).'",
                    "width": '.$imgWidth.',
                    "height": '.$imgHeight.'
                  },
                  "datePublished": "'.\Carbon\Carbon::parse($model->public_date)->toAtomString().'",
                  "dateModified": "'.\Carbon\Carbon::parse($model->updated_at_date)->toAtomString().'",
                  "author": {
                    "@type": "Organization",
                    "name": "Агроновини"
                  },
                  "publisher": {
                    "@type": "Organization",
                    "name": "AgroPortal.ua",
                    "logo": {
                      "@type": "ImageObject",
                      "url": "'.$defaultImage.'",
                      "width": '.$imgWidth2.',
                      "height": '.$imgHeight2.'
                    }
                  }
                }';

        $meta = [
            'title'       => $model->transMetaTitle ?? get_main_title($lang),
            'description' => $model->transMetaDescription ?? get_main_description($lang),
            'og'          => [
                'title'       => $model->transMetaTitle ?? get_main_title($lang),
                'description' => $model->transMetaDescription ?? get_main_description($lang),
                'url'         => $pageUrlCurrent,
                'type' => 'website',
                'locale' => $lang,
                'site_name' => env('APP_NAME'),
                'image' => $model->image ? get_image_uri_front($model->image) : $defaultImage
            ],
            'links' => [
                [
                    'rel' => 'canonical',
                    'href' => $pageUrlCurrent
                ],
                [
                    'rel' => 'alternate',
                    'href' => $pageUrlUk,
                    'hreflang' => 'uk',
                ],
                [
                    'rel' => 'alternate',
                    'href' => $pageUrlRu,
                    'hreflang' => 'ru',
                ]
            ],
            'script' => $mr
        ];

        $data['meta'] = $meta;

        return $this->successResponse($data);
    }

    /**
     * Получить aside и хлебные крошки
     *
     * data in {category_id,lang}
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBySlugAside(Request $request)
    {
        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['category_id'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['category_id']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = config('translatable.locale');
        }

        $result = cache()->remember('getBySlugAside_' . $decodedJson['category_id'] . '_' . $lang, (60 * 60), function () use ($decodedJson, $lang) {
            $category = cache()->remember('getCategoryById_' . $decodedJson['category_id'] . '_' . $lang, (60 * 60 * 24), function () use ($decodedJson, $lang) {
                return Category::query()
                    ->leftJoin('category_translations', 'category_translations.category_id', '=', 'category.id')
                    ->where('category_translations.lang', $lang)
                    ->select([
                        'category.*',
                        'category_translations.title AS categoryName',
                        'category_translations.meta_title AS categoryMetaTitle',
                        'category_translations.meta_description AS categoryMetaDescription',
                    ])
                    ->where('category.id', $decodedJson['category_id'])
                    ->first();
            });

            // Aside Setting
            if ($category) {
                $mainCatId = $category->id;
                if ($category->parent_id !== 0) {
                    $mainCatId = $category->parent_id;
                }

                $setting = cache()->remember('SideCategorySettings_' . $mainCatId,(60*60*24), function () use($mainCatId){
                   return SideCategorySettings::query()->where('category_id', $mainCatId)->first();
                });

                if ($setting) {
                    $setting       = json_decode($setting->setting, true);
                    $data['aside'] = $this->adapter->prepareAsideResults($setting, $lang);
                } else {
                    $data['aside'] = [];
                }
            }

            /*************************************** Breadcrumbs *********************************************/
            $mainPageUrl = config('app.url') . ($lang !== config('translatable.locale') ? ('/' . $lang) : '');
            $breadcrumbs = [
                [
                    'name' => __('Main page', [], $lang),
                    'url'  => $mainPageUrl
                ]
            ];


            if (isset($category->parent_id)) {
                if ($category->parent_id) {
                    $mainCat = cache()->remember('getCategoryById_' . $category->parent_id . '_' . $lang, (60 * 60 * 24), function () use ($category, $lang) {
                        return Category::query()
                            ->where('category.id', $category->parent_id)
                            ->leftJoin('category_translations', 'category_translations.category_id', '=', 'category.id')
                            ->where('category_translations.lang', $lang)
                            ->select([
                                'category.*',
                                'category_translations.title AS categoryName',
                                'category_translations.meta_title AS categoryMetaTitle',
                                'category_translations.meta_description AS categoryMetaDescription',
                            ])
                            ->first();
                    });

                    if ($mainCat) {
                        $breadcrumbs[] = [
                            'name' => $mainCat->categoryName,
                            'url'  => $mainPageUrl . '/' . $mainCat->path
                        ];
                    }

                    $breadcrumbs[] = [
                        'name' => $category->categoryName,
                        'url'  => $mainPageUrl . '/' . $category->path
                    ];
                } else {
                    $breadcrumbs[] = [
                        'name' => $category->categoryName,
                        'url'  => $mainPageUrl . '/' . $category->path
                    ];
                }
            }

            $data['breadcrumbs'] = $breadcrumbs;

            return $data;
        });

        return $this->successResponse($result);
    }

    /**
     * Получить блок мультимедиа
     *
     * data in {lang}
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getBySlugMultimedia(Request $request)
    {
        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = config('translatable.locale');
        }

        $result = cache()->remember('getBySlugMultimedia_' . $decodedJson['category_id'] . '_' . $lang, (60 * 30), function () use ($decodedJson, $lang) {
            $articleMultimedia = app(Setting::class)->get('article_multimedia', config('translatable.locale'));

            $data = [];

            if ($articleMultimedia) {
                $articleMultimedia = json_decode($articleMultimedia, true);
                $this->adapter->getBlockData($a, 3, $articleMultimedia['type'], $articleMultimedia['category'] ?? null, $articleMultimedia['tag'] ?? null, $lang, 1);
                $data['article_multimedia'] = $a ?? null;
            }

            return $data;
        });

        return $this->successResponse($result);
    }

    /**
     * Получить новости по теме
     *
     * data in {model_id,tags_str,lang}
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getBySlugNewsByTheme(Request $request)
    {

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['tags_str'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['tags_str']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['model_id'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['model_id']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = config('translatable.locale');
        }

        $result = [];

        if ($decodedJson['tags_str']) {
            $result = cache()->remember('getBySlugNewsByTheme_' . $decodedJson['model_id'] . '_' . $lang, (60 * 30), function () use ($decodedJson, $lang) {
                $take     = 10;
                $data     = [];
                $tags     = explode(',', $decodedJson['tags_str']);
                $addModel = null;
                $ad       = [];
                $countAd  = 0;
                $ids      = [];
                $counterBreak = pow($take, 2);

//                DB::enableQueryLog();


//                foreach ($tags as $key => $tag) {
//                    $ad[$key] = BlogArticles::query()
//                        ->leftJoin('category', 'category.id', '=', 'blog_articles.category_id')
//                        ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
//                        ->where('blog_article_translations.lang', $lang)
//                        ->where('blog_article_translations.tags', 'like', '%' . $tag . '%')
//                        ->where('blog_articles.id', '<>', $decodedJson['model_id'])
//                        ->orderBy('blog_articles.public_date', 'desc')
//                        ->select([
//                            'blog_articles.*',
//                            'category.path',
//                            'blog_article_translations.tags'
//                        ])
//                        ->active()
//                        ->limit($take)
//                        ->get();
//
//                    $countAd += count($ad[$key]);
//                }
//
//                while ($countAd > 0 && count($ids) < $take && $counterBreak > 0) {
//                    foreach ($ad as $a) {
//                        foreach ($a as $it) {
//                            if (!in_array($it->id, $ids)) {
//                                $ids[] = $it->id;
//                                $countAd--;
//
//                                break;
//                            }
//                            $counterBreak--;
//                        }
//                    }
//                }




                $tagIds = NewTagTranslation::query()
                    ->where('lang',$lang)
                    ->whereIn('name',$tags)
                    ->pluck('new_tag_id')
                    ->toArray();

                $adNew = BlogArticlesNewTags::query()
                    ->whereIn('new_tag_id',$tagIds)
                    ->where('blog_articles_id','<>',$decodedJson['model_id'])
                    ->orderBy('blog_articles_id', 'desc')
                    ->get()
                    ->groupBy('new_tag_id')
                    ->toArray();

                foreach ($adNew as $adNewItem){
                    $countAd += count($adNewItem);
                }

                while ($countAd > 0 && count($ids) < $take && $counterBreak > 0) {
                    foreach ($adNew as $a) {
                        foreach ($a as $it) {
                            if (!in_array($it['blog_articles_id'], $ids)) {
                                $ids[] = $it['blog_articles_id'];
                                $countAd--;

                                break;
                            }
                            $counterBreak--;
                        }
                    }
                }

                if (count($ids) > $take) {
                    $ids = array_slice($ids, 0, $take);
                }

                if (count($ids)) {
                    $addModel = BlogArticles::query()
                        ->leftJoin('category', 'category.id', '=', 'blog_articles.category_id')
                        ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
                        ->where('blog_article_translations.lang', $lang)
                        ->whereIn('blog_articles.id', $ids)
                        ->orderBy('blog_articles.public_date', 'desc')
                        ->select([
                            'blog_articles.*',
                            'category.path'
                        ])
                        ->active()
                        ->limit($take)
                        ->get();
                }

                if ($addModel && count($addModel)) {
                    $items                 = $this->adapter->prepareModelsResults($addModel, $lang);
                    $data['news_by_theme'] = $items['models'];
                }

                return $data;
            });
        }

        return $this->successResponse($result);
    }

    public function getAll(Request $request)
    {
        $take = (int)app(Setting::class)->get('news_per_page', config('translatable.locale'));

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = config('translatable.locale');
        }

        $currentPage = $decodedJson['page'] ?? 1;

        $model = BlogArticles::query()
            ->orderBy('created_at', 'desc')
            ->active()
            ->with('audio.category', function ($q) use ($lang) {
                $q->leftJoin('audio_category_translations', 'audio_category_translations.audio_categories_id', '=', 'audio_categories.id')
                    ->where('audio_category_translations.lang', $lang)
                    ->select(['audio_categories.*', 'audio_category_translations.title AS categoryName']);
            })
            ->take($take)
            ->skip($currentPage == 1 ? 0 : (($take * $currentPage) - $take))
            ->get();

        $data = $this->adapter->prepareModelsResults($model, $lang);

        $paginate = [
            'total'        => BlogArticles::query()->active()->count(),
            'per_page'     => $take,
            'current_page' => $currentPage,
        ];

        $data['paginate'] = $paginate;

        $widgetSubscribe = Widget::query()->where('instance', 'blog-subscribe')->where('lang', $lang)->first()->toArray();
        $widgetJoinUs    = Widget::query()->where('instance', 'blog-join-us')->where('lang', $lang)->first()->toArray();

        $data['constructor'] = [
            'subscribe' => isset($widgetSubscribe) ? $widgetSubscribe['data'] : [],
            'join_us'   => isset($widgetJoinUs) ? $widgetJoinUs['data'] : []
        ];

        return $this->successResponse($data);
    }

    public function category(Request $request)
    {
        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = config('translatable.locale');
        }

        if (!isset($decodedJson['category_slug'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['category_slug']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $data = [];

        $data = cache()->remember('cat_' . implode('_', $decodedJson), 120, function () use ($decodedJson, $lang, $data) {

            $take        = (int)app(Setting::class)->get('news_per_page', config('translatable.locale'));
            $currentPage = $decodedJson['page'] ?? 1;

            $category = Category::query()
                ->leftJoin('category_translations', 'category_translations.category_id', '=', 'category.id')
                ->where('category_translations.lang', $lang)
                ->select([
                    'category.*',
                    'category_translations.title AS categoryName',
                    'category_translations.meta_title AS categoryMetaTitle',
                    'category_translations.meta_description AS categoryMetaDescription',
                ])
                ->where('category.path', $decodedJson['category_slug'])
                ->first();

            if (!$category) {
                return $this->errorResponse(
                    ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                    Response::HTTP_NOT_FOUND
                );
            }

            $countSubCategories = $category->countSubCategories();

            $catIds = [];

            if ($countSubCategories > 0) {
                $catIds = Category::query()->where('parent_id', $category->id)->pluck('id')->toArray();
            }

            $articlesQuery = BlogArticles::query()
                ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
                ->leftJoin('category', 'category.id', '=', 'blog_articles.category_id')
                ->leftJoin('category_translations', 'category_translations.category_id', '=', 'blog_articles.category_id')
                ->where('blog_article_translations.lang', $lang)
                ->whereNotNull('blog_article_translations.name')
                ->where('category_translations.lang', $lang)
                ->select([
                    'blog_articles.*',
                    'category.path',
                    'category_translations.title as category_title',
                ])
                ->orderBy('blog_articles.public_date', 'desc')
                ->active()
                ->where(function ($q) use ($category, $catIds) {
                    if (count($catIds)) {
                        $q->whereIn('blog_articles.category_id', $catIds);
                    } else {
                        $q->where('blog_articles.category_id', $category->id);
                    }
                });

            $total = $articlesQuery->count();

            $articlesQuery
                ->take($take)
                ->skip($currentPage == 1 ? 0 : (($take * $currentPage) - $take));

            $articles = $articlesQuery->get();

            $items = $this->adapter->prepareModelsResults($articles, $lang);

            $paginate = [
                'total'        => $total,
                'per_page'     => $take,
                'current_page' => $currentPage,
            ];

            $items['paginate'] = $paginate;

            $data = [
                'category' => [
                    'name'             => $category->categoryName,
                    'slug'             => $decodedJson['category_slug'],
                    'meta_title'       => $category->categoryMetaTitle,
                    'meta_description' => $category->categoryMetaDescription,
                ],
                'items'    => $items
            ];

            // Aside Setting
            $mainCatId = $category->id;
            if ($category->parent_id !== 0) {
                $mainCatId = $category->parent_id;
            }

            $setting = SideCategorySettings::query()->where('category_id', $mainCatId)->first();

            if ($setting) {
                $setting       = json_decode($setting->setting, true);
                $data['aside'] = $this->adapter->prepareAsideResults($setting, $lang);
            } else {
                $data['aside'] = [];
            }

            /*************************************** Breadcrumbs *********************************************/
            $mainPageUrl = config('app.url') . ($lang !== config('translatable.locale') ? ('/' . $lang) : '');
            $breadcrumbs = [
                [
                    'name' => __('Main page', [], $lang),
                    'url'  => $mainPageUrl
                ]
            ];


            if ($category->parent_id) {
                $mainCat = Category::query()
                    ->where('category.id', $category->parent_id)
                    ->leftJoin('category_translations', 'category_translations.category_id', '=', 'category.id')
                    ->where('category_translations.lang', $lang)
                    ->select([
                        'category.*',
                        'category_translations.title AS categoryName',
                        'category_translations.meta_title AS categoryMetaTitle',
                        'category_translations.meta_description AS categoryMetaDescription',
                    ])
                    ->first();

                if ($mainCat) {
                    $breadcrumbs[] = [
                        'name' => $mainCat->categoryName,
                        'url'  => $mainPageUrl . '/' . $mainCat->path
                    ];
                }

                $breadcrumbs[] = [
                    'name' => $category->categoryName,
                    'url'  => $mainPageUrl . '/' . $category->path
                ];
            } else {
                $breadcrumbs[] = [
                    'name' => $category->categoryName,
                    'url'  => $mainPageUrl . '/' . $category->path
                ];
            }

            $data['breadcrumbs'] = $breadcrumbs;

            return $data;
        });

        return $this->successResponse($data);
    }

    /**
     * Получить только items и category
     *
     * data in [category_slug,lang,page]
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function categoryOnlyItems(Request $request)
    {
        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = config('translatable.locale');
        }

        if (!isset($decodedJson['category_slug'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['category_slug']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $data = cache()->remember('cat_items_' . implode('_', $decodedJson), (60 * 10), function () use ($decodedJson, $lang) {
            $data        = [];
            $take        = cache()->rememberForever('get_blog_news_per_page',function (){
                return (int)app(Setting::class)->get('news_per_page', config('translatable.locale'));
            });
            $currentPage = $decodedJson['page'] ?? 1;

            $category = Category::query()
                ->leftJoin('category_translations', 'category_translations.category_id', '=', 'category.id')
                ->where('category_translations.lang', $lang)
                ->select([
                    'category.*',
                    'category_translations.title AS categoryName',
                    'category_translations.description AS categoryDescription',
                    'category_translations.excerpt AS categoryExcerpt',
                    'category_translations.h2 AS categoryH2',
                    'category_translations.meta_title AS categoryMetaTitle',
                    'category_translations.meta_description AS categoryMetaDescription',
                ])
                ->where('category.path', $decodedJson['category_slug'])
                ->first();

            if (!$category) {
                return $this->errorResponse(
                    ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                    Response::HTTP_NOT_FOUND
                );
            }

            $countSubCategories = cache()->rememberForever('getCountSubCategories_'.$category->id,function () use($category) {
                return $category->countSubCategories();
            });

            $catIds = [];

            if ($countSubCategories > 0) {
                $catIds = Category::query()->where('parent_id', $category->id)->pluck('id')->toArray();
            }

            $articlesQuery = BlogArticles::query()
                ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
                ->leftJoin('category', 'category.id', '=', 'blog_articles.category_id')
                ->leftJoin('category_translations', 'category_translations.category_id', '=', 'blog_articles.category_id')
                ->where('blog_article_translations.lang', $lang)
                ->whereNotNull('blog_article_translations.name')
                ->where('category_translations.lang', $lang)
                ->select([
                    'blog_articles.*',
                    'category.path',
                    'category_translations.title as category_title'
                ])
                ->orderBy('blog_articles.public_date', 'desc')
                ->active()
                ->where(function ($q) use ($category, $catIds) {
                    if (count($catIds)) {
                        $q->whereIn('blog_articles.category_id', $catIds);
                    } else {
                        $q->where('blog_articles.category_id', $category->id);
                    }
                });

            $total = $articlesQuery->count();

            try {
                $articlesQuery
                    ->take($take)
                    ->skip($currentPage == 1 ? 0 : (($take * $currentPage) - $take));
            } catch (\Exception $e){
                Log::info('categoryOnlyItems',[
                    'currentPage' => $currentPage,
                    'take'        => $take,
                    'error'       => $e->getMessage()
                ]);
                $articlesQuery
                    ->take($take)
                    ->skip($currentPage == 1 ? 0 : ((20 * $currentPage) - 20));
            }

            $articles = $articlesQuery->get();

            $items = $this->adapter->prepareModelsResults($articles, $lang, true);

            $paginate = [
                'total'        => $total,
                'per_page'     => $take,
                'current_page' => $currentPage,
            ];

            $items['paginate'] = $paginate;

            $data = [
                'category' => [
                    'id'               => $category->id,
                    'parent_id'        => $category->parent_id,
                    'name'             => $category->categoryName,
                    'h2'               => $category->categoryH2,
                    'description'      => $category->categoryDescription,
                    'description_hide' => $category->categoryExcerpt,
                    'slug'             => $decodedJson['category_slug'],
                    'path'             => $category->path,
                    'meta_title'       => $category->categoryMetaTitle,
                    'meta_description' => $category->categoryMetaDescription,
                ],
                'items'    => $items
            ];

            $pageNames = [
                'uk' => ' - Сторінка ',
                'ru' => ' - Страница ',
            ];

            if ($currentPage > 1) {
                if ($data['category']['meta_title'] !== '') {
                    $data['category']['meta_title'] .= $pageNames[$lang] . $currentPage;
                }
                if ($data['category']['meta_description'] !== '') {
                    $data['category']['meta_description'] .= $pageNames[$lang] . $currentPage;
                }
            }

            // meta ********************************************************************************************************
            $defaultImage = cache()->remember('get_default_image',(60 * 60 * 24), function (){
                return get_image_uri_front(app(Setting::class)->get('default_og_image', \App\Models\Langs::getDefaultLangCode()));
            });

            $pageUrl = $category->path;

            $pageUrlUk = env('APP_URL') . '/' . $pageUrl;
            $pageUrlRu = env('APP_URL') . '/ru/' . $pageUrl;
            $pageUrlCurrent = $pageUrlUk;

            if($lang === 'ru'){
                $pageUrlCurrent = $pageUrlRu;
            }

            $links = [];

            switch ($currentPage){
                case "1":
                    $links = [
                        [
                            'rel'  => 'canonical',
                            'href' => $pageUrlCurrent
                        ],
                        [
                            'rel'      => 'alternate',
                            'href'     => $pageUrlUk,
                            'hreflang' => 'uk',
                        ],
                        [
                            'rel'      => 'alternate',
                            'href'     => $pageUrlRu,
                            'hreflang' => 'ru',
                        ],
                        [
                            'rel'      => 'next',
                            'href'     => $pageUrlCurrent . "?page=2",
                        ]
                    ];
                    break;
                case "2":
                    $links = [
                        [
                            'rel'  => 'canonical',
                            'href' => $pageUrlCurrent
                        ],
                        [
                            'rel'      => 'alternate',
                            'href'     => $pageUrlUk . "?page=2",
                            'hreflang' => 'uk',
                        ],
                        [
                            'rel'      => 'alternate',
                            'href'     => $pageUrlRu . "?page=2",
                            'hreflang' => 'ru',
                        ],
                        [
                            'rel'      => 'prev',
                            'href'     => $pageUrlCurrent,
                        ],
                        [
                            'rel'      => 'next',
                            'href'     => $pageUrlCurrent . "?page=3",
                        ]
                    ];
                    break;
                default:
                    $links = [
                        [
                            'rel'  => 'canonical',
                            'href' => $pageUrlCurrent
                        ],
                        [
                            'rel'      => 'alternate',
                            'href'     => $pageUrlUk . "?page=" . $currentPage,
                            'hreflang' => 'uk',
                        ],
                        [
                            'rel'      => 'alternate',
                            'href'     => $pageUrlRu . "?page=" . $currentPage,
                            'hreflang' => 'ru',
                        ],
                        [
                            'rel'      => 'prev',
                            'href'     => $pageUrlCurrent . "?page=" . ($currentPage - 1),
                        ],
                        [
                            'rel'      => 'next',
                            'href'     => $pageUrlCurrent . "?page=" . ($currentPage + 1),
                        ]
                    ];
                    break;
            }

            $imgWidth2 = 0;
            $imgHeight2 = 0;
            try {
                [$imgWidth2, $imgHeight2] = getimagesize($defaultImage);
            } catch( \Exception $e){}

            $mr = '{
                  "@context": "http://schema.org",
                  "@type": "NewsArticle",
                  "mainEntityOfPage":{
                    "@type":"WebPage",
                    "@id":"'.$pageUrlCurrent.'"
                  },
                  "headline": "'.$category->categoryName.'",
                  "description": "'.$category->categoryMetaDescription.'",
                  "image": {
                    "@type": "ImageObject",
                    "url": "'.$defaultImage.'",
                    "width": '.$imgWidth2.',
                    "height": '.$imgHeight2.'
                  },
                  "datePublished": "'.\Carbon\Carbon::parse($category->created_at)->toAtomString().'",
                  "dateModified": "'.\Carbon\Carbon::parse($category->updated_at)->toAtomString().'",
                  "author": {
                    "@type": "Organization",
                    "name": "Агроновини"
                  },
                  "publisher": {
                    "@type": "Organization",
                    "name": "AgroPortal.ua",
                    "logo": {
                      "@type": "ImageObject",
                     "url": "'.$defaultImage.'",
                      "width": '.$imgWidth2.',
                      "height": '.$imgHeight2.'
                    }
                  }
                }';

            $meta = [
                'title'       => $data['category']['meta_title'] ?? get_main_title($lang),
                'description' => $data['category']['meta_description'] ?? get_main_description($lang),
                'og'          => [
                    'title'       => $data['category']['meta_title'] ?? get_main_title($lang),
                    'description' => $data['category']['meta_description'] ?? get_main_description($lang),
                    'url'         => $pageUrlCurrent,
                    'type' => 'website',
                    'locale' => $lang,
                    'site_name' => env('APP_NAME'),
                    'image' => $category->image ? get_image_uri_front($category->image) : $defaultImage
                ],
                'links' => $links,
                'script' => $mr
            ];

            $data['meta'] = $meta;

            return $data;
        });

        return $this->successResponse($data);
    }

    /**
     * Получить только aside и breadcrumbs
     *
     * data in [category_id,category_parent_id,category_name,category_path,lang]
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function categoryOnlyAsideAndBreadcrumbs(Request $request)
    {
        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['category_id'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['category_id']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['category_parent_id'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['category_parent_id']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['category_name'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['category_name']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = config('translatable.locale');
        }

        $data = [];

        $data = cache()->remember('cat_aside_' . implode('_', $decodedJson), (60 * 60), function () use ($decodedJson, $lang, $data) {
            // Aside Setting
            $mainCatId = $decodedJson['category_id'];
            if ($decodedJson['category_parent_id'] !== 0) {
                $mainCatId = $decodedJson['category_parent_id'];
            }

            $setting = SideCategorySettings::query()->where('category_id', $mainCatId)->first();

            if ($setting) {
                $setting       = json_decode($setting->setting, true);
                $data['aside'] = $this->adapter->prepareAsideResults($setting, $lang);
            } else {
                $data['aside'] = [];
            }

            /*************************************** Breadcrumbs *********************************************/
            $mainPageUrl = config('app.url') . ($lang !== config('translatable.locale') ? ('/' . $lang) : '');
            $breadcrumbs = [
                [
                    'name' => __('Main page', [], $lang),
                    'url'  => $mainPageUrl
                ]
            ];


            if ($decodedJson['category_parent_id']) {
                $mainCat = Category::query()
                    ->where('category.id', $decodedJson['category_parent_id'])
                    ->leftJoin('category_translations', 'category_translations.category_id', '=', 'category.id')
                    ->where('category_translations.lang', $lang)
                    ->select([
                        'category.*',
                        'category_translations.title AS categoryName',
                        'category_translations.meta_title AS categoryMetaTitle',
                        'category_translations.meta_description AS categoryMetaDescription',
                    ])
                    ->first();

                if ($mainCat) {
                    $breadcrumbs[] = [
                        'name' => $mainCat->categoryName,
                        'url'  => $mainPageUrl . '/' . $mainCat->path
                    ];
                }

                $breadcrumbs[] = [
                    'name' => $decodedJson['category_name'],
                    'url'  => $mainPageUrl . '/' . $decodedJson['category_path']
                ];
            } else {
                $breadcrumbs[] = [
                    'name' => $decodedJson['category_name'],
                    'url'  => $mainPageUrl . '/' . $decodedJson['category_path']
                ];
            }

            $data['breadcrumbs'] = $breadcrumbs;

            return $data;
        });

        return $this->successResponse($data);
    }

    public function getByTag(Request $request)
    {
        //        DB::enableQueryLog();

        $take = (int)app(Setting::class)->get('news_per_page', 'uk');

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = config('translatable.locale');
        }

        if (!isset($decodedJson['tag_slug'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['tag_slug']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $tag = NewTag::query()
            ->leftJoin('new_tag_translations', 'new_tag_translations.new_tag_id', '=', 'new_tags.id')
            ->where('new_tag_translations.lang', $lang)
            ->where('new_tags.slug', $decodedJson['tag_slug'])
            ->select([
                'new_tags.*',
                'new_tag_translations.name AS transName',
                'new_tag_translations.meta_title AS transMetaTitle',
                'new_tag_translations.meta_description AS transMetaDescription',
            ])
            ->first();

        if (!$tag)
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                //Response::HTTP_NOT_FOUND // 404
                Response::HTTP_FOUND //302 to home
            );

        $currentPage = $decodedJson['page'] ?? 1;

        $articlesQuery = BlogArticles::query()
            ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
            ->leftJoin('category', 'category.id', '=', 'blog_articles.category_id')
            ->where('blog_article_translations.lang', $lang)
            ->whereNotNull('blog_article_translations.name')
            ->select([
                'blog_articles.*',
                'category.path'
            ])
            ->orderBy('blog_articles.public_date', 'desc')
            ->active()
            ->where('blog_article_translations.tags', 'like', '%' . $tag->transName . '%');

        $total = $articlesQuery->count();

        $articlesQuery
            ->take($take)
            ->skip($currentPage == 1 ? 0 : (($take * $currentPage) - $take));

        $articles = $articlesQuery->get();

        //        dd(DB::getQueryLog());

        $items = $this->adapter->prepareModelsResults($articles, $lang);

        $paginate = [
            'total'        => $total,
            'per_page'     => $take,
            'current_page' => $currentPage,
        ];

        $items['paginate'] = $paginate;

        $data = [
            'tag'   => $tag,
            'items' => $items
        ];

        $setting       = [
            'news' => [
                'status' => 1,
                'count'  => 10
            ]
        ];
        $data['aside'] = $this->adapter->prepareAsideResults($setting, $lang);

        /*************************************** Breadcrumbs *********************************************/
        $mainPageUrl = config('app.url') . ($lang !== config('translatable.locale') ? ('/' . $lang) : '');
        $breadcrumbs = [
            [
                'name' => __('Main page', [], $lang),
                'url'  => $mainPageUrl
            ],
            [
                'name' => $tag->transName,
                'url'  => null
            ],
        ];

        $data['breadcrumbs'] = $breadcrumbs;

        return $this->successResponse($data);
    }

    /**
     * Достать только items
     *
     * data in [tag_slug,page,lang]
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getByTagItems(Request $request)
    {
        //        DB::enableQueryLog();

        $take = cache()->remember('getByTagItems_take', (60 * 60 * 24), function () {
            return (int)app(Setting::class)->get('news_per_page', 'uk');
        });

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = config('translatable.locale');
        }

        if (!isset($decodedJson['tag_slug'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['tag_slug']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $tag = cache()->remember('getByTagItemsTag_' . $decodedJson['tag_slug'] . '_' . $lang, (60 * 5), function () use ($decodedJson, $lang) {
            return NewTag::query()
                ->leftJoin('new_tag_translations', 'new_tag_translations.new_tag_id', '=', 'new_tags.id')
                ->where('new_tag_translations.lang', $lang)
                ->where('new_tags.slug', $decodedJson['tag_slug'])
                ->select([
                    'new_tags.*',
                    'new_tag_translations.name AS transName',
                    'new_tag_translations.description AS transDescription',
                    'new_tag_translations.description_short AS transDescriptionShort',
                    'new_tag_translations.h2 AS transH2',
                    'new_tag_translations.description_hide AS transDescriptionHide',
                    'new_tag_translations.meta_title AS transMetaTitle',
                    'new_tag_translations.meta_description AS transMetaDescription',
                ])
                ->first();
        });

        if (!$tag)
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                //Response::HTTP_NOT_FOUND // 404
                Response::HTTP_FOUND //302 to home
            );

        $tag->description       = $tag->transDescription;
        $tag->description_short = $tag->transDescriptionShort != "<p><br></p>" ? $tag->transDescriptionShort : '';
        $tag->h2                = $tag->transH2;
        $tag->description_hide  = $tag->transDescriptionHide;

        $currentPage = $decodedJson['page'] ?? 1;

        $pageNames = [
            'uk' => ' - Сторінка ',
            'ru' => ' - Страница ',
        ];

        if ($currentPage > 1) {
            if ($tag->transMetaTitle) {
                $tag->transMetaTitle .= $pageNames[$lang] . $currentPage;
            }
            if ($tag->transMetaDescription) {
                $tag->transMetaDescription .= $pageNames[$lang] . $currentPage;
            }
        }

//        DB::enableQueryLog();

        $articleIdsByTags = BlogArticlesNewTags::query()
            ->where('new_tag_id',$tag->id)
            ->pluck('blog_articles_id')
            ->toArray();

        $articlesQuery = BlogArticles::query()
            ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
            ->leftJoin('category', 'category.id', '=', 'blog_articles.category_id')
            ->where('blog_article_translations.lang', $lang)
            ->whereNotNull('blog_article_translations.name')
            ->select([
                'blog_articles.*',
                'category.path'
            ])
            ->orderBy('blog_articles.public_date', 'desc')
            ->active()
            ->whereIn('blog_articles.id',$articleIdsByTags);
//            ->where('blog_article_translations.tags', 'like', '%' . $tag->transName . '%');

        $total = $articlesQuery->count();

        $articlesQuery
            ->take($take)
            ->skip($currentPage == 1 ? 0 : (($take * $currentPage) - $take));

        $articles = $articlesQuery->get();

//        $queries = DB::getQueryLog();
//        $lastQueryDurationMillis = end($queries)['time'];
//        $lastQueryDurationSecs = $lastQueryDurationMillis / 1000;
//        dd('Тривалість запиту: ' . $lastQueryDurationSecs . ' сек.');

        $items = $this->adapter->prepareModelsResults($articles, $lang,true);

        $paginate = [
            'total'        => $total,
            'per_page'     => $take,
            'current_page' => (int)$currentPage,
        ];

        $items['paginate'] = $paginate;

        $data = [
            'tag'   => $tag,
            'items' => $items
        ];

        // meta ********************************************************************************************************
        $defaultImage = cache()->remember('get_default_image',(60 * 60 * 24), function (){
            return get_image_uri_front(app(Setting::class)->get('default_og_image', \App\Models\Langs::getDefaultLangCode()));
        });

        $pageUrlUk = env('APP_URL') . '/tags/' . $tag->slug;
        $pageUrlRu = env('APP_URL') . '/ru/tags/' . $tag->slug;
        $pageUrlCurrent = $pageUrlUk;

        if($lang === 'ru'){
            $pageUrlCurrent = $pageUrlRu;
        }

        $links = [];

        switch ($currentPage){
            case "1":
                $links = [
                    [
                        'rel'  => 'canonical',
                        'href' => $pageUrlCurrent
                    ],
                    [
                        'rel'      => 'alternate',
                        'href'     => $pageUrlUk,
                        'hreflang' => 'uk',
                    ],
                    [
                        'rel'      => 'alternate',
                        'href'     => $pageUrlRu,
                        'hreflang' => 'ru',
                    ],
                    [
                        'rel'      => 'next',
                        'href'     => $pageUrlCurrent . "?page=2",
                    ]
                ];
                break;
            case "2":
                $links = [
                    [
                        'rel'  => 'canonical',
                        'href' => $pageUrlCurrent
                    ],
                    [
                        'rel'      => 'alternate',
                        'href'     => $pageUrlUk . "?page=2",
                        'hreflang' => 'uk',
                    ],
                    [
                        'rel'      => 'alternate',
                        'href'     => $pageUrlRu . "?page=2",
                        'hreflang' => 'ru',
                    ],
                    [
                        'rel'      => 'prev',
                        'href'     => $pageUrlCurrent,
                    ],
                    [
                        'rel'      => 'next',
                        'href'     => $pageUrlCurrent . "?page=3",
                    ]
                ];
                break;
            default:
                $links = [
                    [
                        'rel'  => 'canonical',
                        'href' => $pageUrlCurrent
                    ],
                    [
                        'rel'      => 'alternate',
                        'href'     => $pageUrlUk . "?page=" . $currentPage,
                        'hreflang' => 'uk',
                    ],
                    [
                        'rel'      => 'alternate',
                        'href'     => $pageUrlRu . "?page=" . $currentPage,
                        'hreflang' => 'ru',
                    ],
                    [
                        'rel'      => 'prev',
                        'href'     => $pageUrlCurrent . "?page=" . ($currentPage - 1),
                    ],
                    [
                        'rel'      => 'next',
                        'href'     => $pageUrlCurrent . "?page=" . ($currentPage + 1),
                    ]
                ];
                break;
        }

        $meta = [
            'title'       => $tag->transMetaTitle ?? get_main_title($lang),
            'description' => $tag->transMetaDescription ?? get_main_description($lang),
            'og'          => [
                'title'       => $tag->transMetaTitle ?? get_main_title($lang),
                'description' => $tag->transMetaDescription ?? get_main_description($lang),
                'url'         => $pageUrlCurrent,
                'type' => 'website',
                'locale' => $lang,
                'site_name' => env('APP_NAME'),
                'image' => $defaultImage
            ],
            'links' => $links
        ];

        $data['meta'] = $meta;

        return $this->successResponse($data);
    }

    /**
     * достать только aside и breadcrumbs
     *
     * data in [lang,tag_name]
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function getByTagAside(Request $request)
    {
        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = config('translatable.locale');
        }

        if (!isset($decodedJson['tag_name'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['tag_name']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        $setting       = [
            'news' => [
                'status' => 1,
                'count'  => 10
            ]
        ];
        $data['aside'] = $this->adapter->prepareAsideResults($setting, $lang);

        /*************************************** Breadcrumbs *********************************************/
        $mainPageUrl = config('app.url') . ($lang !== config('translatable.locale') ? ('/' . $lang) : '');
        $breadcrumbs = [
            [
                'name' => __('Main page', [], $lang),
                'url'  => $mainPageUrl
            ],
            [
                'name' => $decodedJson['tag_name'],
                'url'  => null
            ],
        ];

        $data['breadcrumbs'] = $breadcrumbs;

        return $this->successResponse($data);
    }

    public function subCategoryList(Request $request)
    {
        $data = cache()->remember('categories_get-slug-list', (60 * 60 * 24), function () {
            $list  = Category::query()->where('parent_id', '>', 0)->pluck('slug')->toArray();
            $main  = Category::query()->where('parent_id', 0)->pluck('slug')->toArray();
            $pages = Pages::query()->pluck('slug')->toArray();

            return [
                'list'  => $list,
                'main'  => $main,
                'pages' => $pages
            ];
        });

        return $this->successResponse($data);
    }

    public function search(Request $request)
    {
        //        DB::enableQueryLog();

        $take = (int)app(Setting::class)->get('news_per_page', 'uk');

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = config('translatable.locale');
        }

        $currentPage = $decodedJson['page'] ?? 1;

        $articlesQuery = BlogArticles::query()
            ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
            ->leftJoin('category', 'category.id', '=', 'blog_articles.category_id')
            ->leftJoin('category_translations', 'category_translations.category_id', '=', 'blog_articles.category_id')
            ->where('blog_article_translations.lang', $lang)
            ->whereNotNull('blog_article_translations.name')
            ->where('category_translations.lang', $lang)
            ->select([
                'blog_articles.*',
                'category.path',
                'category_translations.title as category_title',
            ])
            ->orderBy('blog_articles.public_date', 'desc')
            ->active()
            ->where(function ($q) use ($decodedJson) {
                if ($decodedJson['q']) {
                    $q->where('blog_article_translations.name', 'like', '%' . $decodedJson['q'] . '%')
                        ->orWhere('blog_article_translations.text', 'like', '%' . $decodedJson['q'] . '%')
                        ->orWhere('blog_article_translations.annot', 'like', '%' . $decodedJson['q'] . '%')
                        ->orWhere('blog_article_translations.tags', 'like', '%' . $decodedJson['q'] . '%');
                }
            })
            ->where(function ($q) use ($decodedJson) {
                if (isset($decodedJson['from']) && isset($decodedJson['to'])) {
                    $dateFrom = Carbon::parse($decodedJson['from'])->format('Y-m-d') . ' 00:00:00';
                    $dateTo   = Carbon::parse($decodedJson['to'])->format('Y-m-d') . ' 23:59:59';


                    $q->whereBetween('public_date', [$dateFrom, $dateTo]);
                }
            });


        $total = $articlesQuery->count();

        $articlesQuery
            ->take($take)
            ->skip($currentPage == 1 ? 0 : (($take * $currentPage) - $take));

        $articles = $articlesQuery->get();

        //        dd(DB::getQueryLog());

        $items = $this->adapter->prepareModelsResults($articles, $lang);

        $paginate = [
            'total'        => $total,
            'per_page'     => $take,
            'current_page' => $currentPage,
        ];

        $items['paginate'] = $paginate;

        $data = [
            'items' => $items
        ];

        $setting       = [
            'news' => [
                'status' => 1,
                'count'  => 10
            ]
        ];
        $data['aside'] = $this->adapter->prepareAsideResults($setting, $lang);

        /*************************************** Breadcrumbs *********************************************/
        $mainPageUrl = config('app.url') . ($lang !== config('translatable.locale') ? ('/' . $lang) : '');
        $breadcrumbs = [
            [
                'name' => __('Main page', [], $lang),
                'url'  => $mainPageUrl
            ],
            [
                'name' => __('Search', [], $lang),
                'url'  => null
            ],
        ];

        $data['breadcrumbs'] = $breadcrumbs;

        return $this->successResponse($data);
    }

    public function searchItems(Request $request)
    {
        //        DB::enableQueryLog();

        $take = (int)app(Setting::class)->get('news_per_page', 'uk');

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = config('translatable.locale');
        }

        // t=1 - искать только по тегам
        $t = 0;

        if ($request->has('t') && $request->get('t') == '1') {
            $t = 1;
        }

        $currentPage = $decodedJson['page'] ?? 1;

        if ($t) {
            $tagIds = NewTagTranslation::query()
                ->where('name',$decodedJson['q'])
                ->where('lang',$lang)
                ->pluck('new_tag_id')
                ->toArray();

            $articlesIds = BlogArticlesNewTags::query()
                ->whereIn('new_tag_id',$tagIds)
                ->pluck('blog_articles_id')
                ->toArray();

            $articlesQuery = BlogArticles::query()
                ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
                ->leftJoin('category', 'category.id', '=', 'blog_articles.category_id')
                ->leftJoin('category_translations', 'category_translations.category_id', '=', 'blog_articles.category_id')
                ->where('blog_article_translations.lang', $lang)
                ->whereNotNull('blog_article_translations.name')
                ->where('category_translations.lang', $lang)
                ->select([
                    'blog_articles.*',
                    'category.path',
                    'category_translations.title as category_title',
                    'blog_article_translations.tags as transTags',
                ])
                ->active()
                ->whereIn('blog_articles.id',$articlesIds)
                ->orderBy('blog_articles.public_date', 'desc');
//                ->where(function ($q) use ($decodedJson) {
//                    if ($decodedJson['q']) {
//                        $q->where('blog_article_translations.tags', 'like', '%' . $decodedJson['q'] . '%');
//                    }
//                });
            $total = $articlesQuery->count();



            $articlesQuery
                ->take($take)
                ->skip($currentPage == 1 ? 0 : (($take * $currentPage) - $take));

            $articles = $articlesQuery->get();

            $items = $this->adapter->prepareModelsResults($articles, $lang,true);



//            $articles = $articlesQuery->get();

//            $sorted     = collect();
//            $sortedTags = collect();
//
//            $q = $decodedJson['q'] ?? '11111111111111111111111111111111111111111111111111111111111111';
//
//            foreach ($articles as $item) {
//                if (mb_stripos($item->transTags, $q) === FALSE) {
//                    $sorted->add($item);
//                } else {
//                    $sortedTags->add($item);
//                }
//            }
//
//            $sorted     = $sorted->SortByDesc('public_date');
//            $sortedTags = $sortedTags->SortByDesc('public_date');
//
//            $sortedTags = $sortedTags->merge($sorted);
//
//            $sortedTagsPaginate = $sortedTags->forPage($currentPage, $take);
//            $items = $this->adapter->prepareModelsResults($sortedTagsPaginate, $lang);

        } else {
            $articlesQuery = BlogArticles::query()
                ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
                ->leftJoin('category', 'category.id', '=', 'blog_articles.category_id')
                ->leftJoin('category_translations', 'category_translations.category_id', '=', 'blog_articles.category_id')
                ->where('blog_article_translations.lang', $lang)
                ->whereNotNull('blog_article_translations.name')
                ->where('category_translations.lang', $lang)
                ->orderBy('blog_articles.public_date', 'desc')
                ->active()
                ->where(function ($q) use ($decodedJson) {
                    if (isset($decodedJson['from']) && isset($decodedJson['to'])) {
                        $dateFrom = Carbon::parse($decodedJson['from'])->format('Y-m-d') . ' 00:00:00';
                        $dateTo   = Carbon::parse($decodedJson['to'])->format('Y-m-d') . ' 23:59:59';


                        $q->whereBetween('public_date', [$dateFrom, $dateTo]);
                    }
                })
                ->where(function ($q) use ($decodedJson) {
                    if ($decodedJson['q']) {
                        $q->whereRaw('MATCH(blog_article_translations.tags,blog_article_translations.name,blog_article_translations.search_field) AGAINST(? IN BOOLEAN MODE)', [$decodedJson['q']]);
//                        $q->where('blog_article_translations.tags', 'like', '%' . $decodedJson['q'] . '%')
//                            ->orWhere('blog_article_translations.name', 'like', '%' . $decodedJson['q'] . '%')
//                            ->orWhere('blog_article_translations.search_field', 'like', '%' . $decodedJson['q'] . '%');
                    }
                })
                ->select([
                    'blog_articles.*',
                    'category.path',
                    'category_translations.title as category_title',
                    'blog_article_translations.tags as transTags',
                ]);


            $total = $articlesQuery->count();
            $articlesQuery
                ->take($take)
                ->skip($currentPage == 1 ? 0 : (($take * $currentPage) - $take));

            $articles = $articlesQuery->get();

            $items = $this->adapter->prepareModelsResults($articles, $lang);
        }

        $paginate = [
            'total'        => $total,
            'per_page'     => $take,
            'current_page' => $currentPage,
        ];

        $items['paginate'] = $paginate;

        $data = [
            'items' => $items
        ];

        // meta ********************************************************************************************************
        $defaultImage = cache()->remember('get_default_image',(60 * 60 * 24), function (){
            return get_image_uri_front(app(Setting::class)->get('default_og_image', \App\Models\Langs::getDefaultLangCode()));
        });

        $pageUrlUk = env('APP_URL') . '/search?q=' . $decodedJson['q'];
        $pageUrlRu = env('APP_URL') . '/ru/search?q=' . $decodedJson['q'];
        $pageUrlCurrent = $pageUrlUk;

        if($lang === 'ru'){
            $pageUrlCurrent = $pageUrlRu;
        }

        $links = [];

        switch ($currentPage){
            case "1":
                $links = [
                    [
                        'rel'  => 'canonical',
                        'href' => $pageUrlCurrent
                    ],
                    [
                        'rel'      => 'alternate',
                        'href'     => $pageUrlUk,
                        'hreflang' => 'uk',
                    ],
                    [
                        'rel'      => 'alternate',
                        'href'     => $pageUrlRu,
                        'hreflang' => 'ru',
                    ],
                    [
                        'rel'      => 'next',
                        'href'     => $pageUrlCurrent . "&page=2",
                    ]
                ];
                break;
            case "2":
                $links = [
                    [
                        'rel'  => 'canonical',
                        'href' => $pageUrlCurrent
                    ],
                    [
                        'rel'      => 'alternate',
                        'href'     => $pageUrlUk . "&page=2",
                        'hreflang' => 'uk',
                    ],
                    [
                        'rel'      => 'alternate',
                        'href'     => $pageUrlRu . "&page=2",
                        'hreflang' => 'ru',
                    ],
                    [
                        'rel'      => 'prev',
                        'href'     => $pageUrlCurrent,
                    ],
                    [
                        'rel'      => 'next',
                        'href'     => $pageUrlCurrent . "&page=3",
                    ]
                ];
                break;
            default:
                $links = [
                    [
                        'rel'  => 'canonical',
                        'href' => $pageUrlCurrent
                    ],
                    [
                        'rel'      => 'alternate',
                        'href'     => $pageUrlUk . "&page=" . $currentPage,
                        'hreflang' => 'uk',
                    ],
                    [
                        'rel'      => 'alternate',
                        'href'     => $pageUrlRu . "&page=" . $currentPage,
                        'hreflang' => 'ru',
                    ],
                    [
                        'rel'      => 'prev',
                        'href'     => $pageUrlCurrent . "&page=" . ($currentPage - 1),
                    ],
                    [
                        'rel'      => 'next',
                        'href'     => $pageUrlCurrent . "&page=" . ($currentPage + 1),
                    ]
                ];
                break;
        }

        $meta = [
            'title'       => get_main_title($lang),
            'description' => get_main_description($lang),
            'og'          => [
                'title'       => get_main_title($lang),
                'description' => get_main_description($lang),
                'url'         => $pageUrlCurrent,
                'type' => 'website',
                'locale' => $lang,
                'site_name' => env('APP_NAME'),
                'image' => $defaultImage
            ],
            'links' => $links
        ];

        $data['meta'] = $meta;

        return $this->successResponse($data);
    }

    public function searchAside(Request $request)
    {

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = config('translatable.locale');
        }

        $setting       = [
            'news' => [
                'status' => 1,
                'count'  => 10
            ]
        ];
        $data['aside'] = $this->adapter->prepareAsideResults($setting, $lang);

        /*************************************** Breadcrumbs *********************************************/
        $mainPageUrl = config('app.url') . ($lang !== config('translatable.locale') ? ('/' . $lang) : '');
        $breadcrumbs = [
            [
                'name' => __('Main page', [], $lang),
                'url'  => $mainPageUrl
            ],
            [
                'name' => __('Search', [], $lang),
                'url'  => null
            ],
        ];

        $data['breadcrumbs'] = $breadcrumbs;

        return $this->successResponse($data);
    }

    public function pageGetBySlug(Request $request)
    {

        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['slug'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['slug']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = Langs::getDefaultLangCode();
        }

        $model = Pages::query()
            ->leftJoin('pages_translations', 'pages_translations.pages_id', '=', 'pages.id')
            ->where('pages_translations.lang', $lang)
            ->select([
                'pages.*',
                'pages_translations.title AS pageName',
                'pages_translations.meta_title AS transMetaTitle',
                'pages_translations.meta_description AS transMetaDescription',
            ])
            ->active()
            ->where('pages.slug', $request->get('slug'))
            ->first();

        if (!$model)
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
                Response::HTTP_NOT_FOUND);

        $data = $this->adapter->prepareModelResults($model, $lang);

        if ($decodedJson['slug'] === 'faq') {
            $faqModel = Faq::query()->get();
            $faqData  = $this->adapter->prepareFaqsResults($faqModel, $lang);

            $widgetQuestionsForm = Widget::query()->where('instance', 'questions-form')->where('lang', $lang)->first()->toArray();

            if ($widgetQuestionsForm) {
                $data['constructor'] = [
                    'questions-form' => isset($widgetQuestionsForm) ? $widgetQuestionsForm['data'] : [],
                ];
            }

            $data['faq_data'] = $faqData;
        }

        $setting = [
            'news' => [
                'status' => 1,
                'count'  => 10
            ]
        ];

        $data['aside'] = $this->adapter->prepareAsideResults($setting, $lang);

        /*************************************** Breadcrumbs *********************************************/
        $mainPageUrl = config('app.url') . ($lang !== Langs::getDefaultLangCode() ? ('/' . $lang) : '');
        $breadcrumbs = [
            [
                'name' => __('Main page', [], $lang),
                'url'  => $mainPageUrl
            ]
        ];

        $breadcrumbs[] = [
            'name' => $model->pageName,
            'url'  => $mainPageUrl . '/' . $model->slug
        ];

        $data['breadcrumbs'] = $breadcrumbs;

        // meta ********************************************************************************************************
        $defaultImage = cache()->remember('get_default_image',(60 * 60 * 24), function (){
            return get_image_uri_front(app(Setting::class)->get('default_og_image_front', \App\Models\Langs::getDefaultLangCode()));
        });


        $pageUrlUk = env('APP_URL') . '/' . trim($decodedJson['slug'],'/');
        $pageUrlRu = env('APP_URL') . '/ru/' . trim($decodedJson['slug'],'/');
        $pageUrlCurrent = $pageUrlUk;

        if($lang === 'ru'){
            $pageUrlCurrent = $pageUrlRu;
        }

        $meta = [
            'title'       => $model->transMetaTitle ?? get_main_title($lang),
            'description' => $model->transMetaDescription ?? get_main_description($lang),
            'og'          => [
                'title'       => $model->transMetaTitle ?? get_main_title($lang),
                'description' => $model->transMetaDescription ?? get_main_description($lang),
                'url'         => $pageUrlCurrent,
                'type' => 'website',
                'locale' => $lang,
                'site_name' => env('APP_NAME'),
                'image' => $model->image ? get_image_uri_front($model->image) : $defaultImage
            ],
            'links' => [
                [
                  'rel' => 'canonical',
                  'href' => $pageUrlCurrent
                ],
                [
                    'rel' => 'alternate',
                    'href' => $pageUrlUk,
                    'hreflang' => 'uk',
                ],
                [
                    'rel' => 'alternate',
                    'href' => $pageUrlRu,
                    'hreflang' => 'ru',
                ]
            ]
        ];

        $data['meta'] = $meta;

        return $this->successResponse($data);
    }

    public function getEntityByUrl(Request $request)
    {
        if (!$decodedJson = $request->json()->all()) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUEST_JSON_EXPECTED),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (!isset($decodedJson['url'])) {
            return $this->errorResponse(
                ErrorManager::buildError(VALIDATION_REQUIRED_FIELD, ['url']),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }

        if (isset($decodedJson['lang'])) {
            $lang = $decodedJson['lang'];
        } else {
            $lang = config('translatable.locale');
        }

        $path = cache()->remember('categories_get-slug-list', (60 * 60 * 24), function () {
            $list  = Category::query()->where('parent_id', '>', 0)->pluck('slug')->toArray();
            $main  = Category::query()->where('parent_id', 0)->pluck('slug')->toArray();
            $pages = Pages::query()->pluck('slug')->toArray();

            return [
                'list'  => $list,
                'main'  => $main,
                'pages' => $pages
            ];
        });

        $parts = explode("/", $decodedJson['url']);

        switch (count($parts)) {
            case 1:
                if (in_array($parts[0], $path['pages'])) {
                    $slug = $parts[0];
                    $request->request->add(['slug' => $slug, 'url' => $decodedJson['url']]);
                    $dataResponse          = current($this->pageGetBySlug($request));
                    $dataResponse          = json_decode($dataResponse, true);
                    $data                  = $dataResponse['data'];
                    $data['response_type'] = 'page';

                    if(isset($data["errors"])){
                        return  response()->json([
                            'success' => false,
                            'http_code' => 404,
                            'data' => $data
                        ],404);
                    }

                    return $this->successResponse($data);
                } elseif (in_array($parts[0], $path['main'])) {
                    $slug = $parts[0];
                    $request->request->add(['category_slug' => $slug, 'url' => $decodedJson['url']]);
                    $dataResponse          = current($this->categoryOnlyItems($request));
                    $dataResponse          = json_decode($dataResponse, true);
                    $data                  = $dataResponse['data'];
                    $data['response_type'] = 'category';

                    if(isset($data["errors"])){
                        return  response()->json([
                            'success' => false,
                            'http_code' => 404,
                            'data' => $data
                        ],404);
                    }

                    return $this->successResponse($data);
                }
                break;
            case 2:
                if (in_array($parts[0], $path['main']) && in_array($parts[1], $path['list'])) {
                    $slug = $decodedJson['url'];
                    $request->request->add(['category_slug' => $slug, 'url' => $decodedJson['url']]);
                    $dataResponse          = current($this->categoryOnlyItems($request));
                    $dataResponse          = json_decode($dataResponse, true);
                    $data                  = $dataResponse['data'];
                    $data['response_type'] = 'category';

                    if(isset($data["errors"])){
                        return  response()->json([
                            'success' => false,
                            'http_code' => 404,
                            'data' => $data
                        ],404);
                    }

                    return $this->successResponse($data);
                } elseif (in_array($parts[0], $path['main'])) {
                    $slug = $parts[1];
                    $request->request->add(['slug' => $slug, 'url' => $decodedJson['url']]);
                    $dataResponse          = current($this->getBySlugModel($request));
                    $dataResponse          = json_decode($dataResponse, true);
                    $data                  = $dataResponse['data'];
                    $data['response_type'] = 'article';

                    if(isset($data["errors"])){
                        return  response()->json([
                            'success' => false,
                            'http_code' => 404,
                            'data' => $data
                        ],404);
                    }

                    return $this->successResponse($data);
                }
                break;
            case 3:
                $slug = $parts[2];
                $request->request->add(['slug' => $slug, 'url' => $decodedJson['url']]);
                $dataResponse          = current($this->getBySlugModel($request));
                $dataResponse          = json_decode($dataResponse, true);
                $data                  = $dataResponse['data'];
                $data['response_type'] = 'article';

                if(isset($data["errors"])){
                    return  response()->json([
                        'success' => false,
                        'http_code' => 404,
                        'data' => $data
                    ],404);
                }

                return $this->successResponse($data);
                break;
        }

        return $this->errorResponse(
            ErrorManager::buildError(VALIDATION_MODEL_NOT_FOUND),
            Response::HTTP_UNPROCESSABLE_ENTITY
        );
    }
}
