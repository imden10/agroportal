<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Landing;
use App\Models\Langs;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LandingController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        // access
        if(! permissions('landings__landings__view')) abort(404);

        $status = $request->get('status');
        $title = $request->get('title');

        $model = Landing::query()
            ->leftJoin('landing_translations', 'landing_translations.landing_id', '=', 'landings.id')
            ->where('landing_translations.lang',Langs::getDefaultLangCode())
            ->select([
                'landings.*',
                'landing_translations.title'
            ])
            ->orderBy('landings.created_at', 'desc')
            ->where(function ($q) use ($status,$title) {
                if ($status != '') {
                    $q->where('landings.status', $status);
                }
                if ($title != '') {
                    $q->where('landing_translations.title', 'like', '%' . $title . '%');
                }
            })
            ->paginate(20);

        return view('admin.landing.index', [
            'model' => $model
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // access
        if(! permissions('landings__landings__create')) abort(404);

        $model = new Landing();

        $localizations = Langs::getLangsWithTitle();

        return view('admin.landing.create', [
            'model'         => $model,
            'localizations' => $localizations
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        // access
        if(! permissions('landings__landings__create')){
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        }

        $model = new Landing();

        DB::beginTransaction();

        try {
            if (empty($request->input('slug', ''))) {
                $request->merge(array('slug' => SlugService::createSlug(Landing::class, 'slug', $request->input('page_data.' . Langs::getDefaultLangCode() . '.title'))));
            }

            $model->fill($request->all());

            $model->status = $request->get('status') ?? false;

            if (!$model->save()) {
                DB::rollBack();
            }

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($request->input('page_data.' . $lang, []), $constructorData[$lang] ?? []));

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('landing.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->route('landing.index')->with('success', 'Лендинг успешно добавлен!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Landing $landing
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Landing $landing)
    {
        // access
        if(! permissions('landings__landings__edit')) abort(404);

        $localizations = Langs::getLangsWithTitle();

        return view('admin.landing.edit', [
            'model'         => $landing,
            'data'          => $landing->getTranslationsArray(),
            'localizations' => $localizations
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        // access
        if(! permissions('landings__landings__edit')){
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        }

        /* @var $model Landing */
        $model = Landing::query()->where('id', $id)->first();

        DB::beginTransaction();

        try {
            $model->fill($request->all());

            $model->status = $request->get('status') ?? false;

            if (!$model->save()) {
                DB::rollBack();
            }

            $model->deleteTranslations();

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($request->input('page_data.' . $lang, []), $constructorData[$lang] ?? []));

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('landing.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->back()->with('success', 'Лендинг успешно обновлен!');
    }

    /**
     * @param Landing $landing
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Landing $landing)
    {
        // access
        if(! permissions('landings__landings__delete')){
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        };

        $landing->deleteTranslations();
        $landing->delete();

        return redirect()->back()->with('success', 'Лендинг успешно удален!');
    }

    public function search(Request $request)
    {
        $term = $request->get('term');
        $name = $term['term'];
        $lang = $request->get('lang');

        $items = Landing::query()
            ->leftJoin('landing_translations', 'landing_translations.landing_id', '=', 'landings.id')
            ->where('landing_translations.lang', $lang)
            ->select([
                'landings.id',
                'landing_translations.title AS art_name'
            ])
            ->where(function ($q) use ($name) {
                if ($name != '') {
                    $q->where('landing_translations.title', 'like', '%' . $name . '%');
                }
            })
            ->limit(20)
            ->get()
            ->toArray();

        $newItems = [];

        if(count($items)){
            foreach ($items as $item){
                $newItems[] = [
                    'id' => $item['id'],
                    'name' => $item['art_name']
                ];
            }
        }

        return [
            'items' => $newItems
        ];
    }
}
