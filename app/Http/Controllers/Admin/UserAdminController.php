<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\UserAdminCreateRequest;
use App\Http\Requests\UserAdminUpdateRequest;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UserAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // access
        if(! permissions('admins__users__view')) abort(404);

        $users = User::query()
            ->leftJoin('roles', 'roles.id', '=', 'users.role_id')
            ->select([
                'users.*',
                'roles.name AS role_name'
            ])
            ->orderBy('users.created_at', 'desc')
            ->where(function ($q) {
                /* @var $q Builder */
                $q->where('users.role_id', 1);
            })
            ->paginate(20);

        return view('admin.users-admin.index', [
            'users' => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // access
        if(! permissions('admins__users__create')) abort(404);

        $model = new User();

        return view('admin.users-admin.create', [
            'model' => $model
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserAdminCreateRequest $request)
    {
        // access
        if(! permissions('admins__users__create')){
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        };

        /* @var $user User */
        $user = User::create([
            'name'           => $request->get('name'),
            'email'          => $request->get('email'),
            'password'       => Hash::make($request->get('password')),
            'status'         => User::STATUS_REGISTER,
            'role_id'        => 1, // admin role
        ]);

        $user->groups()->sync($request->get('group_ids'));

        return redirect()->route('users-admin.index')->with('success','Пользоателя успешно добавлено!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // access
        if(! permissions('admins__users__edit')) abort(404);

        $model = User::query()->where('id',$id)->first();

        return view('admin.users-admin.edit', [
            'model' => $model
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserAdminUpdateRequest $request, $id)
    {
        // access
        if(! permissions('admins__users__edit')){
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        };

        /* @var $model User */
        $model = User::query()->where('id',$id)->first();

        try {
            $model->name = $request->get('name');
            $model->email = $request->get('email');

            if($request->has('password') && $request->get('password')){
                $model->password = Hash::make($request->get('password'));
            }

            $model->save();

            $model->groups()->sync($request->get('group_ids'));
        } catch (\Throwable $e){
            return redirect()->back()->with('error', $e->getMessage());
        }

        return redirect()->back()->with('success','Пользоателя успешно обновлено!');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        // access
        if(! permissions('admins__users__delete')){
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        };

        $model = User::query()->where('id',$id)->first();

        $model->groups()->sync([]);

        User::query()->where('id',$id)->delete();

        return redirect()->back()->with('success', 'Пользоателя успешно удалено!');
    }
}
