<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Langs;
use App\Models\NewTag;
use App\Models\Tags;
use App\Models\Translations\NewTagTranslation;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TagsController extends Controller
{

    public function index(Request $request)
    {
        // access
        if(! permissions('tags__tags__view')) abort(404);

        $name = $request->get('name');

        $model = NewTag::query()
            ->leftJoin('new_tag_translations','new_tag_translations.new_tag_id', '=', 'new_tags.id')
            ->where('new_tag_translations.lang',Langs::getDefaultLangCode())
            ->select([
                'new_tags.*',
                'new_tag_translations.name AS tagName'
            ])
            ->where(function ($q) use ($name) {
                if ($name != '') {
                    $q->where('new_tag_translations.name', 'like', '%' . $name . '%');
                }
            })
            ->paginate(20);

        return view('admin.tags.index', [
            'model' => $model
        ]);
    }

    public function create()
    {
        // access
        if (!permissions('tags__tags__create'))
            abort(404);

        $model = new NewTag();

        $localizations = Langs::getLangsWithTitle();

        return view('admin.tags.create', [
            'model'         => $model,
            'localizations' => $localizations
        ]);
    }

    public function store(Request $request)
    {
        // access
        if (!permissions('tags__tags__create')) {
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        };

        $model = new NewTag();

        DB::beginTransaction();

        try {
            if (empty($request->input('slug', ''))) {
                $request->merge(array('slug' => SlugService::createSlug(NewTag::class, 'slug', $request->input('page_data.' . Langs::getDefaultLangCode() . '.name'))));
            }

            $model->fill($request->all());

            if (!$model->save()) {
                DB::rollBack();
            }

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $data = $request->input('page_data.' . $lang, []);

                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($data, $constructorData[$lang] ?? []));

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->back()->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->route('tag.index')->with('success', 'Тег успешно добавлен!');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        // access
        if (!permissions('tags__tags__edit'))
            abort(404);

        $model = NewTag::query()->where('id', $id)->first();

        $localizations = Langs::getLangsWithTitle();

        return view('admin.tags.edit', [
            'model'         => $model,
            'data'          => $model->getTranslationsArray(),
            'localizations' => $localizations,
        ]);
    }

    public function update(Request $request, $id)
    {
        // access
        if (!permissions('tags__tags__edit')) {
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        };

        /* @var $model NewTag */
        $model = NewTag::query()->where('id', $id)->first();

        DB::beginTransaction();

        try {
            $model->fill($request->all());

            if (!$model->save()) {
                DB::rollBack();
            }

            $model->deleteTranslations();

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $data = $request->input('page_data.' . $lang, []);

                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($data, $constructorData[$lang] ?? []));

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('tag.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->back()->with('success', 'Тег успешно обновлен!');
    }

    public function destroy($id)
    {
        // access
        if (!permissions('tags__tags__delete')) {
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        };

        $model = NewTag::query()->where('id', $id)->first();
        $model->deleteTranslations();
        NewTag::query()->where('id', $id)->delete();

        return redirect()->back()->with('success', 'Тег успешно удален!');
    }

    /**
     * @param Request $request
     * @return array
     */
    public function search(Request $request)
    {
        $term = $request->get('term');
        $q = $term['term'];

        $items = NewTag::query()
            ->leftJoin('new_tag_translations','new_tag_translations.new_tag_id','=','new_tags.id')
//            ->where('new_tag_translations.lang',Langs::getDefaultLangCode())
            ->select([
                'new_tags.id',
                'new_tag_translations.name'
            ])
            ->where('new_tag_translations.name','like','%' . $q . '%')
            ->get()
            ->toArray();

        return [
            'items' => $items
        ];
    }

    public function binding(Request $request)
    {
        $data = [];

        $data['ru'] = Tags::query()
            ->where('related',0)
            ->where('lang','ru')
            ->pluck('name','id')
            ->toArray();

        $data['uk'] = Tags::query()
            ->where('related',0)
            ->where('lang','uk')
            ->pluck('name','id')
            ->toArray();

        if($request->has('a')){
            $data['ru'] = Tags::query()
                ->where('related',0)
                ->where('lang','ru')
                ->where('name', 'LIKE', $request->get('a').'%')
                ->pluck('name','id')
                ->toArray();
        }

        if($request->has('b')){
            $data['uk'] = Tags::query()
                ->where('related',0)
                ->where('lang','uk')
                ->where('name', 'LIKE', $request->get('b').'%')
                ->pluck('name','id')
                ->toArray();
        }

        return view('admin.tags.binding', [
            'data' => $data
        ]);
    }

    public function tie(Request $request)
    {
        if($request->has('ruId') && $request->has('ukId')){
            $ruTag = Tags::query()->where('id',$request->get('ruId'))->where('related',0)->first();
            $ukTag = Tags::query()->where('id',$request->get('ukId'))->where('related',0)->first();

            if($ruTag && $ukTag){
                $newTag = new NewTag();
                $newTag->slug = SlugService::createSlug(NewTag::class, 'slug', $ukTag->name);

                if($newTag->save()){
                    $newTagTransUk = new NewTagTranslation();
                    $newTagTransUk->new_tag_id = $newTag->id;
                    $newTagTransUk->lang = 'uk';
                    $newTagTransUk->name = $ukTag->name;
                    $newTagTransUk->save();

                    $newTagTransRu = new NewTagTranslation();
                    $newTagTransRu->new_tag_id = $newTag->id;
                    $newTagTransRu->lang = 'ru';
                    $newTagTransRu->name = $ruTag->name;
                    $newTagTransRu->save();

                    $ruTag->related = 1;
                    $ruTag->save();

                    $ukTag->related = 1;
                    $ukTag->save();

                   return [
                       'success' => true
                   ];
                }
            }
        }

        return [
            'success' => false,
            'message' => 'Не удалось связать теги, возможно они уже связанные, перезагрузите страничку.'
        ];
    }
}
