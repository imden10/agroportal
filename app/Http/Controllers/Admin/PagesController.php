<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Langs;
use App\Models\Pages;
use App\Models\PagesMenu;
use App\Models\Settings;
use App\Service\MenuHelper;
use Illuminate\Http\Request;
use App\Http\Requests\PagesRequest;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;

class PagesController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        // access
        if(! permissions('pages__pages__view')) abort(404);

        $status = $request->get('status');
        $title = $request->get('title');

        $model = Pages::query()
            ->leftJoin('pages_translations', 'pages_translations.pages_id', '=', 'pages.id')
            ->where('pages_translations.lang',Langs::getDefaultLangCode())
            ->select([
                'pages.*',
                'pages_translations.title'
            ])
            ->orderBy('pages.status', 'desc')
            ->orderBy('pages.created_at', 'desc')
            ->where(function ($q) use ($status,$title) {
                if ($status != '') {
                    $q->where('pages.status', $status);
                }
                if ($title != '') {
                    $q->where('pages_translations.title', 'like', '%' . $title . '%');
                }
            })
            ->paginate(20);

        return view('admin.pages.index', [
            'model' => $model
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // access
        if(! permissions('pages__pages__create')) abort(404);

        $model = new Pages();

        $localizations = Langs::getLangsWithTitle();

        return view('admin.pages.create', [
            'model'         => $model,
            'localizations' => $localizations
        ]);
    }

    /**
     * @param PagesRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        // access
        if(! permissions('pages__pages__create')){
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        }

        $model = new Pages();

        DB::beginTransaction();

        try {
            $model->fill($request->all());

            $model->status = $request->get('status') ?? false;

            if (!$model->save()) {
                DB::rollBack();
            }

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($request->input('page_data.' . $lang, []), $constructorData[$lang] ?? []));

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('pages.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->route('pages.index')->with('success', 'Страница успешно добавлена!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Pages $page
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Pages $page)
    {
        // access
        if(! permissions('pages__pages__edit')) abort(404);

        $localizations = Langs::getLangsWithTitle();


        $setting = Settings::query()
            ->select([
                'code', 'value', 'lang'
            ])
            ->get()
            ->groupBy('lang')->transform(function ($item, $k) {
                return $item->groupBy('code');
            })
            ->toArray();

        return view('admin.pages.edit', [
            'model'         => $page,
            'data'          => $page->getTranslationsArray(),
            'localizations' => $localizations,
            'setting' => $setting
        ]);
    }

    /**
     * @param PagesRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        // access
        if(! permissions('pages__pages__edit')){
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        }

        /* @var $model Pages */
        $model = Pages::query()->where('id', $id)->first();

        DB::beginTransaction();

        try {
            $model->fill($request->all());

            $model->status = $request->get('status') ?? false;

            if (!$model->save()) {
                DB::rollBack();
            }

            $settingData = $request->get('setting_data')[Langs::getDefaultLangCode()] ?? null;

            if ($settingData) {
                foreach ($settingData as $code => $value) {
                    $item = Settings::firstOrNew([
                        'code' => $code,
                        'lang' => Langs::getDefaultLangCode()
                    ]);

                    $newVal = $value;

                    if (is_array($value)) {
                        $newVal = json_encode($value, JSON_UNESCAPED_UNICODE);
                    }

                    $item->value = $newVal;
                    $item->save();
                }
            }

            $model->deleteTranslations();

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($request->input('page_data.' . $lang, []), $constructorData[$lang] ?? []));

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('pages.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->back()->with('success', 'Страница успешно обновлена!');
    }

    /**
     * @param Pages $page
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Pages $page)
    {
        // access
        if(! permissions('pages__pages__delete')){
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        };

        $page->deleteTranslations();
        $page->delete();

        return redirect()->back()->with('success', 'Страницу успешно удалено!');
    }

}
