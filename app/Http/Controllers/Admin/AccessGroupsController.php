<?php

namespace App\Http\Controllers\Admin;

use App\Handlers\Events\AuthLoginEventHandler;
use App\Http\Controllers\Controller;
use App\Http\Requests\AccessGroupCreateRequest;
use App\Models\AccessGroupPermissions;
use App\Models\AccessGroups;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Auth\Events\Login;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AccessGroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // access
        if(! permissions('admins__user_groups__view')) abort(404);

        $model = AccessGroups::query()
            ->paginate(50);

        return view('admin.access.groups.index', [
            'model' => $model
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // access
        if(! permissions('admins__user_groups__create')) abort(404);

        $model = new AccessGroups();

        return view('admin.access.groups.create', [
            'model' => $model
        ]);
    }

    /**
     * @param AccessGroupCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(AccessGroupCreateRequest $request)
    {
        // access
        if(! permissions('admins__user_groups__create')){
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        };

        $model = new AccessGroups();

        DB::beginTransaction();

        try {
            if (empty($request->input('code', ''))) {
                $request->merge(array('code' => SlugService::createSlug(AccessGroups::class, 'code', $request->input('name'))));
            }

            $model->fill($request->all());

            if (!$model->save()) {
                DB::rollBack();
            }

            // Добавить пермишены
            $permissions = $request->get('permissions');
            $allPermissions = \App\Models\AccessPermissions::DATA;

            $tree = [];

            foreach($allPermissions as $item){
                foreach($item['blocks'] as $block){
                    foreach($block['permissions'] as $permission){
                        $newPermission = new AccessGroupPermissions();
                        $newPermission->group_id = $model->id;
                        $newPermission->section_code = $item['section_code'];
                        $newPermission->block_code = $block['code'];
                        $newPermission->permission = $permission;
                        $newPermission->on = isset($permissions[$item['section_code']][$block['code']][$permission]);
                        $newPermission->save();

                        $tree[$item['section_code']][$block['code']][$permission] = isset($permissions[$item['section_code']][$block['code']][$permission]);
                    }
                }
            }

            $model->tree_permission = json_encode($tree);

            if (!$model->save()) {
                DB::rollBack();
            }

        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('access-groups.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->route('access-groups.index')->with('success', 'Группа успешно добавлен!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // access
        if(! permissions('admins__user_groups__edit')) abort(404);

        $model = AccessGroups::query()->where('id', $id)->first();

        return view('admin.access.groups.edit', [
            'model' => $model
        ]);
    }

    /**
     * @param AccessGroupCreateRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AccessGroupCreateRequest $request, $id)
    {
        // access
        if(! permissions('admins__user_groups__edit')){
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        };

        /* @var $model AccessGroups */
        $model = AccessGroups::query()->where('id', $id)->first();

        DB::beginTransaction();

        try {
            $model->fill($request->all());

            if (!$model->save()) {
                DB::rollBack();
            }

            // Обновить пермишены
            $permissions = $request->get('permissions');
            $allPermissions = \App\Models\AccessPermissions::DATA;
            AccessGroupPermissions::query()->where('group_id',$model->id)->delete();

            $tree = [];

            foreach($allPermissions as $item){
                foreach($item['blocks'] as $block){
                    foreach($block['permissions'] as $permission){
                        $newPermission = new AccessGroupPermissions();
                        $newPermission->group_id = $model->id;
                        $newPermission->section_code = $item['section_code'];
                        $newPermission->block_code = $block['code'];
                        $newPermission->permission = $permission;
                        $newPermission->on = isset($permissions[$item['section_code']][$block['code']][$permission]);
                        $newPermission->save();

                        $tree[$item['section_code']][$block['code']][$permission] = isset($permissions[$item['section_code']][$block['code']][$permission]);
                    }
                }
            }

            $model->tree_permission = json_encode($tree);

            if (!$model->save()) {
                DB::rollBack();
            }

            // перезаписать права в сессии
            $login = new Login('web',Auth::user(),false);
            app(AuthLoginEventHandler::class)->handle($login);

        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('access-groups.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->back()->with('success', 'Группа успешно обновлена!');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        // access
        if(! permissions('admins__user_groups__delete')){
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        };

        $accessGroups = AccessGroups::query()->where('id',$id)->first();

        AccessGroupPermissions::query()->where('group_id',$accessGroups->id)->delete();

        AccessGroups::query()->where('id',$id)->delete();

        return redirect()->back()->with('success', 'Группа успешно удалена!');
    }
}
