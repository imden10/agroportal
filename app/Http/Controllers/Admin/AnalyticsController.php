<?php

namespace App\Http\Controllers\Admin;

use App\Exports\InvoicesExport;
use App\Models\BannerItems;
use App\Traits\Statistics;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class AnalyticsController extends Controller
{
    use Statistics;

    public function index(Request $request)
    {

        if ($request->has('p')) {
            $p     = explode(' - ', $request->get('p'));
            $pFrom = $p[0];
            $pTo   = $p[1];
        } else {
            $pFrom = Carbon::today()->addWeek(-1)->format("Y-m-d");
            $pTo   = Carbon::today()->format("Y-m-d");
        }

        if ($request->has('item')) {
            $item = $request->get('item');
        } else {
            $item = \App\Models\BannerItems::query()->first();

            if ($item) {
                $item = $item->id;
            }
        }

        $dates = $this->generateDateRange(Carbon::parse($pFrom), Carbon::parse($pTo));

        /*********************** VISITS *********************/
        $visit       = $this->getViewsByPeriod2($item, $pFrom, $pTo);
        $visitUnique = $this->getViewsUniqueByPeriod2($item, $pFrom, $pTo);
        $click       = $this->getClickByPeriod($item, $pFrom, $pTo);
        $clickUnique = $this->getClickUniqueByPeriod($item, $pFrom, $pTo);

        $visitData       = [];
        $visitUniqueData = [];

        $clickData       = [];
        $clickUniqueData = [];


        foreach ($dates as $date) {
            if (isset($visit[$date])) {
                $visitData[] = $visit[$date];
            } else {
                $visitData[] = 0;
            }

            if (isset($visitUnique[$date])) {
                $visitUniqueData[] = $visitUnique[$date];
            } else {
                $visitUniqueData[] = 0;
            }

            if (isset($click[$date])) {
                $clickData[] = $click[$date];
            } else {
                $clickData[] = 0;
            }

            if (isset($clickUnique[$date])) {
                $clickUniqueData[] = $clickUnique[$date];
            } else {
                $clickUniqueData[] = 0;
            }
        }
        /********************************************************/

        $httpUserAgents = [];
        $bots           = '';

        //        if ($request->method('post') && $request->has('bots')) {
        //            $botArr = explode(',', $request->get('bots'));
        //
        //            $data = [];
        //
        //            foreach ($botArr as $bot) {
        //                $data[] = ['name' => $bot];
        //            }
        //
        //            BotExceptions::query()->delete();
        //            BotExceptions::insert($data);
        //
        //            return redirect()->back()->with('success', 'Bot exceptions successfully updated!');
        //        }

        return view('admin.banners.stat.index', [
            'visitData'        => $visitData,
            'visitUniqueData'  => $visitUniqueData,
            'clickData'        => $clickData,
            'clickUniqueData'  => $clickUniqueData,
            'dates'            => $dates,
            'dateFrom'         => $pFrom,
            'dateTo'           => $pTo,
            'httpUserAgents'   => $httpUserAgents,
            'bots'             => $bots,
        ]);
    }

    private function generateDateRange(Carbon $start_date, Carbon $end_date)
    {
        $dates = [];
        for ($date = $start_date; $date->lte($end_date); $date->addDay()) {
            $dates[] = $date->format('Y-m-d');
        }
        return $dates;
    }

    public function export(Request $request)
    {
        $dates = json_decode($request->get('dates'));
        $visit = json_decode($request->get('visit'));
        $click = json_decode($request->get('click'));
        $visit_unique = json_decode($request->get('visit_unique'));
        $click_unique = json_decode($request->get('click_unique'));

        $item = BannerItems::query()->where('id',$request->get('item_id'))->first();

        $fileName = 'Stat_' . $dates[0] . '-' . end($dates) . '.xlsx';

        return Excel::download(new InvoicesExport($dates,$visit,$visit_unique,$click,$click_unique,$item->name), $fileName);
    }

}
