<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\BlogArticleRequest;
use App\Models\BlogArticles;
use App\Models\Category;
use App\Models\Gallery;
use App\Models\Labels;
use App\Models\Langs;
use App\Models\NewTag;
use App\Models\Translations\BlogArticleTranslation;
use App\Models\Translations\NewTagTranslation;
use App\Service\Adapter;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BlogArticlesController extends Controller
{
    private Adapter $adapter;

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // access
        if (!permissions('categories__articles__view'))
            abort(404);

//        DB::enableQueryLog();

        $status   = $request->get('status');
        $name     = $request->get('name');
        $category = $request->get('category');
        $date     = $request->get('p');

        /* Sorting */
        $prefix = 'blog_articles.';
        $sort   = 'public_date';
        $order  = 'desc';

        if ($request->has('sort') && $request->has('order')) {
            $sort  = $request->get('sort');
            $order = $request->get('order');

            if ($sort == 'name') {
                $prefix = 'blog_article_translations.';
            }
        }

        $query = BlogArticles::query()
            ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
            ->where('blog_article_translations.lang', $request->get('search_lang','uk'))
            ->select([
                'blog_articles.*',
                'blog_article_translations.name AS name2'
            ])
            ->with('category');

        if ($request->has('sort') && $request->has('order')) {
            $query->orderBy($prefix . $sort, $order);
        } else {
//            $query->orderBy('blog_articles.status', 'desc');
            $query->orderBy('blog_articles.public_date', 'desc');
        }

        $query->where(function ($q) use ($status, $name, $category, $date) {
            if ($status != '') {
                $q->where('blog_articles.status', $status);
            }
            if ($name != '') {
                $q->where('blog_article_translations.name', 'like', '%' . $name . '%');
            }

            if ($category != '') {
                if ($category !== '-1') {
                    $q->where('blog_articles.category_id', $category);
                } else {
                    $q->whereNull('blog_articles.category_id');
                }
            }

            if ($date) {
                $dateFrom = substr($date, 0, 10);
                $dateFrom = Carbon::parse($dateFrom)->format('Y-m-d') . ' 00:00:00';
                $dateTo   = substr($date, 13, 10);
                $dateTo   = Carbon::parse($dateTo)->format('Y-m-d') . ' 23:59:59';

                $q->whereBetween('public_date', [$dateFrom, $dateTo]);
            }
        });

        $model = $query->paginate(50);

        $first = BlogArticles::query()->orderBy('public_date', 'asc')->first();
        $from = Carbon::parse($first->public_date)->format('d-m-Y');

        $last = BlogArticles::query()->orderBy('public_date', 'desc')->first();
        $to = Carbon::parse($last->public_date)->format('d-m-Y');

        return view('admin.blog.articles.index', [
            'model'        => $model,
            'fromCategory' => ($category && $category !== '-1') ? true : false,
            'category'     => ($category && $category !== '-1') ? Category::query()->where('id', $category)->first() : null,
            'from'         => $from,
            'to'           => $to
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // access
        if (!permissions('categories__articles__create'))
            abort(404);

        $model = new BlogArticles();

        $categories = app(Category::class)->treeStructure();

        $localizations = Langs::getLangsWithTitle();

        return view('admin.blog.articles.create', [
            'model'         => $model,
            'localizations' => $localizations,
            'categories'    => $categories
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogArticleRequest $request)
    {
        // access
        if (!permissions('categories__articles__create')) {
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        };

        $model = new BlogArticles();

        $post['tags'] = $request->get('tags') ?? [];
        $post['labels'] = $request->get('labels') ?? [];

        DB::beginTransaction();

        try {
            if (empty($request->input('slug', ''))) {
                $request->merge(array('slug' => SlugService::createSlug(BlogArticles::class, 'slug', $request->input('page_data.' . Langs::getDefaultLangCode() . '.name'))));
            }

            $request->merge(['public_date' => Carbon::parse($request->get('public_date'))->format('Y-m-d H:i:00')]);

            if ($request->has('blogger_id')) {
                $request->merge([
                    'blogger_id' => $request->get('blogger_id'),
                    'is_blog'    => 1
                ]);
            } else {
                $request->merge([
                    'blogger_id' => null,
                    'is_blog'    => 0
                ]);
            }

            $model->fill($request->all());

            $model->status = $request->get('status') ?? false;
            $model->interview = $request->get('interview') ?? false;
            $model->mark = $request->get('mark') ?? false;
            $model->views = $request->get('views') ?? 0;
            $videoNews = $request->get('videonews') ?? false;

            if (!$model->save()) {
                DB::rollBack();
            }

            if ($request->get('categories')[0]) {
                $model->category_id = $request->get('categories')[0];
                $model->save();
            }

            $model->newTags()->sync($request->get('tags'));

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $data = $request->input('page_data.' . $lang, []);

                $data['video'] = $videoNews ?? 0;

                $labelsArray    = $data['labels'] ?? [];
                $labels         = count($labelsArray) ? implode(',', $labelsArray) : null;
                $data['labels'] = $labels;
                $data['tags'] = "";

                if($request->has('tags')){
                    $tags = NewTagTranslation::query()
                        ->whereIn('new_tag_id',$request->get('tags'))
                        ->where('lang',$lang)
                        ->pluck('name')
                        ->toArray();

                    if(count($tags)){
                        $data['tags'] = implode(',',$tags);
                    }
                }

                if (count($labelsArray)) {
                    foreach ($labelsArray as $label) {
                        Labels::firstOrCreate([
                            'name' => $label,
                            'lang' => $lang
                        ]);
                    }
                }

                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($data, $constructorData[$lang] ?? []));

                if (!$model->save()) {
                    DB::rollBack();
                }
            }

            try {
                $this->adapter->renderConstructorHTML($model);
            } catch (\Exception $e){

            }

            $modelTrans = BlogArticleTranslation::query()
                ->where('blog_articles_id',$model->id)
                ->get();

            foreach ($modelTrans as $item){
                try {
                    $search_field = strip_tags($item->text . $item->annot . $item->constructor_html);
                    $item->search_field = $search_field;
                    $item->is_search_render = 1;
                    $item->save();
                } catch (\Exception $e){
                    Log::error('Blog Article Controller store',$e);
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->back()->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        \Illuminate\Support\Facades\Artisan::call('cache:clear');
        \Illuminate\Support\Facades\Artisan::call('view:clear');
//        \Illuminate\Support\Facades\Artisan::call('rss:generate');

        return redirect()->route('articles.edit',[$model->id])->with('success', 'Публикация успешно добавлена!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param BlogArticles $blogArticle
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        // access
        if (!permissions('categories__articles__edit'))
            abort(404);

        $blogArticle = BlogArticles::query()
            ->where('id', $id)
            ->with('category')
            ->first();

        $data = $blogArticle->getTranslationsArray();

        $isVideo = current($data)['video'] ?? 0;

        $localizations = Langs::getLangsWithTitle();

        $blogArticle->public_date = Carbon::create($blogArticle->public_date)->format('d-m-Y H:i');

        $categories = app(Category::class)->treeStructure();

        return view('admin.blog.articles.edit', [
            'model'         => $blogArticle,
            'data'          => $data,
            'localizations' => $localizations,
            'categories'    => $categories,
            'isVideo'       => $isVideo
        ]);
    }

    private function logExecutionTime($startTime, $caption)
    {
        // Отримуємо час завершення виконання
        $endTime = microtime(true);
        // Розраховуємо час виконання в мілісекундах
        $executionTime = ($endTime - $startTime) * 1000;
        // Перетворюємо час в секунди з точністю до трьох знаків після коми
        $executionTimeSeconds = number_format($executionTime / 1000, 3);
        // Виводимо повідомлення про час виконання
        \Illuminate\Support\Facades\Log::info("Log time " . $caption . ": " . $executionTimeSeconds . " с");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // access
        if (!permissions('categories__articles__edit')) {
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        };

        $post['tags'] = $request->get('tags') ?? [];
        $post['label'] = $request->get('label') ?? [];

        /* @var $model BlogArticles */
        $model = BlogArticles::query()->where('id', $id)->first();

        DB::beginTransaction();

        try {
            $request->merge(['public_date' => Carbon::parse($request->get('public_date'))->format('Y-m-d H:i:00')]);

            if ($request->has('blogger_id')) {
                $request->merge([
                    'blogger_id' => $request->get('blogger_id'),
                    'is_blog'    => 1
                ]);
            } else {
                $request->merge([
                    'blogger_id' => null,
                    'is_blog'    => 0
                ]);
            }

            $model->fill($request->all());

            $model->status = $request->get('status') ?? false;
            $model->interview = $request->get('interview') ?? false;
            $model->mark = $request->get('mark') ?? false;
            $videoNews = $request->get('videonews') ?? false;

            if (!$model->save()) {
                DB::rollBack();
            }

//            $model->deleteTranslations();

            if ($request->get('categories')[0]) {
                $model->category_id = (int)$request->get('categories')[0];
                $model->save();
            } else {
                $model->category_id = null;
                $model->save();
            }

            $model->newTags()->sync($request->get('tags'));

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $data = $request->input('page_data.' . $lang, []);

                $data['video'] = $videoNews ?? 0;
                $data['landing_link'] = $data['landing_link'] ?? null;

                $labelsArray    = $data['labels'] ?? [];
                $labels         = count($labelsArray) ? implode(',', $labelsArray) : null;
                $data['labels'] = $labels;
                $data['tags'] = "";

                if($request->has('tags')){
                    $tags = NewTagTranslation::query()
                        ->whereIn('new_tag_id',$request->get('tags'))
                        ->where('lang',$lang)
                        ->pluck('name')
                        ->toArray();

                    if(count($tags)){
                        $data['tags'] = implode(',',$tags);
                    }
                }

                if (count($labelsArray)) {
                    foreach ($labelsArray as $label) {
                        Labels::firstOrCreate([
                            'name' => $label,
                            'lang' => $lang
                        ]);
                    }
                }

                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($data, $constructorData[$lang] ?? []));

                if (!$model->save()) {
                    DB::rollBack();
                }
            }

            if($request->has('gallery')){
                $gallery = $request->get('gallery');
                foreach ($gallery as $galleryId => $galleryTitle){
                    $galleryModel = Gallery::query()->where('id',$galleryId)->first();
                    $galleryModel->title = json_encode($galleryTitle['title']);
                    $galleryModel->save();
                }

            }

            try {
                $this->adapter->renderConstructorHTML($model);
            } catch (\Exception $e){

            }

            $modelTrans = BlogArticleTranslation::query()
                ->where('blog_articles_id',$model->id)
                ->get();

            foreach ($modelTrans as $item){
                try {
                    $search_field = strip_tags($item->text . $item->annot . $item->constructor_html);
                    $item->search_field = $search_field;
                    $item->is_search_render = 1;
                    $item->save();
                } catch (\Exception $e){
                    Log::error('Blog Article Controller update',$e);
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->back()->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

//        \Illuminate\Support\Facades\Artisan::call('rss:generate');

        return redirect()->back()->with('success', 'Публикация успешно обновлена!');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        // access
        if (!permissions('categories__articles__delete')) {
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        };

        $blogArticle = BlogArticles::query()->where('id', $id)->first();
        $blogArticle->deleteTranslations();
        $blogArticle->newTags()->sync([]);
        BlogArticles::query()->where('id', $id)->delete();

        return redirect()->back()->with('success', 'Публикацию успешно удалено!');
    }

    public function deleteSelected(Request $request)
    {
        // access
        if (!permissions('categories__articles__delete')) {
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        };

        $ids = json_decode($request->get('ids'), true);

        if (count($ids)) {
            foreach ($ids as $id) {
                $blogArticle = BlogArticles::query()->where('id', $id)->first();
                $blogArticle->deleteTranslations();
                $blogArticle->newTags()->sync([]);
                BlogArticles::query()->where('id', $id)->delete();
            }

            return redirect()->back()->with('success', 'Публикации успешно удалены!');
        }

        return redirect()->back()->with('error', 'Ошибка удаления!');
    }

    public function search(Request $request)
    {
        $term = $request->get('term');
        $name = $term['term'];
        $lang = $request->get('lang');

        $items = BlogArticles::query()
            ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
            ->where('blog_article_translations.lang', $lang)
            ->select([
                'blog_articles.id',
                'blog_article_translations.name AS art_name'
            ])
            ->where(function ($q) use ($name) {
                if ($name != '') {
                    $q->where('blog_article_translations.name', 'like', '%' . $name . '%');
                }
            })
            ->limit(20)
            ->get()
            ->toArray();

        $newItems = [];

        if(count($items)){
            foreach ($items as $item){
                $newItems[] = [
                    'id' => $item['id'],
                    'name' => $item['art_name']
                ];
            }
        }

        return [
            'items' => $newItems
        ];
    }

    public function addGallery(Request $request)
    {
        $urls = $request->get('urls');

        $countUrls = count($urls);

        $countCreated = 0;

        foreach ($urls as $item){
            $parts = explode('/media/',$item);

            if(isset($parts[1])){
                $url = '/' . $parts[1];
                $isG = Gallery::create([
                    'article_id' => $request->get('article_id'),
                    'image' => $url
                ]);

                if($isG){
                    $countCreated++;
                }
            }
        }

        if($countCreated == $countUrls){
            session()->flash('success','Изображения успешно добавленны');
            return [
                "success" => true
            ];
        }

        return [
            "success" => false,
            "message" => "Произошла ошибка добавления"
        ];
    }

    public function deleteFromGallery(Request $request)
    {
        Gallery::query()->where('id',$request->get('id'))->delete();

        return [
            'success' => true
        ];
    }
}
