<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BlogArticles;
use App\Models\Category;
use App\Models\Langs;
use App\Models\SideCategorySettings;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CategoriesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

    }

    public function category($path, $sub = null)
    {
        $slug = $path;

        if ($sub) {
            $slug .= '/' . $sub;
        }

        $category = Category::query()->where('path', $slug)->first();

        $countSubCategories = $category->countSubCategories();

        if ($countSubCategories > 0) {

            $subCategories = Category::query()->where('parent_id', $category->id)->paginate(20);

            return view('admin.categories.categories-list', [
                'category'      => $category,
                'subCategories' => $subCategories
            ]);
        } else {
            return redirect('/admin/blog/articles?category=' . $category->id);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // access
        if (!permissions('categories__categories__create'))
            abort(404);

        $model = new Category();

        $model->order = 0;

        $localizations = Langs::getLangsWithTitle();

        $categories = $model->treeStructure();

        return view('admin.categories.create', [
            'model'         => $model,
            'localizations' => $localizations,
            'categories'    => $categories,
        ]);
    }


    public function createArticleWithCategory(Request $request)
    {
        // access
        if (!permissions('categories__articles__create'))
            abort(404);

        $category = Category::query()->where('path', $request->get('path'))->first();

        $model = new BlogArticles();

        $categories = app(Category::class)->treeStructure();

        $localizations = Langs::getLangsWithTitle();

        return view('admin.blog.articles.create', [
            'model'         => $model,
            'localizations' => $localizations,
            'categories'    => $categories,
            'selCat'        => $category->id ?? null
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // access
        if (!permissions('categories__categories__create')) {
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        };

        $model = new Category();

        DB::beginTransaction();

        try {
            if (empty($request->input('slug', ''))) {
                $request->merge(array('slug' => SlugService::createSlug(Category::class, 'slug', $request->input('page_data.' . Langs::getDefaultLangCode() . '.title'))));
            }

            $model->status = $request->get('status') ?? false;
            $model->active = $request->get('active') ?? false;

            if($request->get('rss_sub_category_id') > 17){
                $model->rss_category_id = $request->get('rss_sub_category_id');
            } else {
                $model->rss_category_id = $request->get('rss_category_id');
            }

            $model->fill($request->all());

            if (!$model->save()) {
                DB::rollBack();
            }

            $this->setPath($model);

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $data = $request->input('page_data.' . $lang, []);

                $model->translateOrNew($lang)->fill($data);

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->back()->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->route('categories.edit', $model->id)->with('success', 'Рубрика успешно добавлена!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // access
        if (!permissions('categories__categories__edit'))
            abort(404);

        $model = Category::query()->where('id', $id)->first();

        $localizations = Langs::getLangsWithTitle();

        $categories = $model->treeStructure();

        return view('admin.categories.edit', [
            'model'         => $model,
            'data'          => $model->getTranslationsArray(),
            'localizations' => $localizations,
            'categories'    => $categories,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // access
        if (!permissions('categories__categories__edit')) {
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        };

        /* @var $model Category */
        $model = Category::query()->where('id', $id)->first();

        DB::beginTransaction();

        try {
            $model->status = $request->get('status') ?? false;
            $model->active = $request->get('active') ?? false;

            if($request->get('rss_sub_category_id') > 17){
                $model->rss_category_id = $request->get('rss_sub_category_id');
            } else {
                $model->rss_category_id = $request->get('rss_category_id');
            }

            $model->fill($request->all());

            if (!$model->save()) {
                DB::rollBack();
            }

            $this->setPath($model);

            $model->deleteTranslations();

            if($request->has('aside_setting')){
                SideCategorySettings::updateOrCreate(
                    [
                        'category_id' => $model->id
                    ],
                    [
                        'category_id' => $model->id,
                        'setting' => json_encode($request->get('aside_setting'))
                    ]
                );
            }

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $data            = $request->input('page_data.' . $lang, []);

                $model->translateOrNew($lang)->fill($data);

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->back()->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        $mainCategories = Category::query()->where('parent_id',0)->get();

        foreach ($mainCategories as $cat){
            $childrenIds = Category::query()->where('parent_id',$cat->id)->pluck('id')->toJson();
            $cat->children_ids = $childrenIds;
            $cat->save();
        }

        return redirect()->back()->with('success', 'Категория успешно обновлена!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        // access
        if (!permissions('categories__categories__delete')) {
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        };

        $articles = BlogArticles::query()->where('category_id',$category->id)->get();

        if($articles){
            foreach ($articles as $item){
                $item->category_id = null;
                $item->save();
            }
        }

        $category->deleteTranslations();
        $category->delete();

        return redirect()->route('admin')->with('success', 'Категорию успешно удалено!');
    }

    /* Generate category path */
    public function setPath($model)
    {
        try {
            $path   = '';
            $parent = 0;

            do {
                if (!$parent) {
                    $tmp_slug = $model->slug;
                    $parent   = $model->parent_id;
                } else {
                    //                    $parent_model = $this->find((int)$parent);
                    $parent_model = app(Category::class)->find((int)$parent);
                    $tmp_slug     = $parent_model->slug;
                    $parent       = $parent_model->parent_id;
                }
                if ($path) {
                    $path = $tmp_slug . '/' . $path;
                } else {
                    $path = $tmp_slug;
                }
            } while ($parent != 0);
            //dd($path);

            $model->path = $path;
            $model->save();

            $children = app(Category::class)->where('parent_id', '=', (int)$model->id)->pluck('id')->toArray();
            if ($children) {
                //dd($children);
                foreach ($children as $child) {
                    $this->setPath((int)$child);
                }
            }
            return $path;
        } catch (\Exception $e) {
            //echo $e;
            return null;
        }
    }

    public function collapse(Request $request)
    {
        /* @var $category Category */
        $category = Category::query()->where('id', $request->get('id'))->first();

        $category->collapse = $request->get('collapse') ?? false;

        $category->save();

        return [
            'status' => true
        ];
    }
}
