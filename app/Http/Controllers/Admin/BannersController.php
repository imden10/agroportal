<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\BannerItems;
use App\Models\Banners;
use App\Models\BlogArticles;
use App\Models\Langs;
use App\Models\Translations\BannerItemTranslation;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BannersController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        //         access
        if (!permissions('banners__banners__view'))
            abort(404);

        $type = $request->get('type');

        /* Sorting */
        $prefix = 'banners.';
        $sort   = 'created_at';
        $order  = 'desc';

        if ($request->has('sort') && $request->has('order')) {
            $sort  = $request->get('sort');
            $order = $request->get('order');
        }

        $query = Banners::query()
            ->where(function ($q) use ($type) {
                if ($type) {
                    $q->where('type', $type);
                }
            });

        if ($request->has('sort') && $request->has('order')) {
            $query->orderBy($prefix . $sort, $order);
        } else {
            $query->orderBy('banners.status', 'desc');
            $query->orderBy('banners.created_at', 'desc');
        }

        $model = $query->paginate(50);

        return view('admin.banners.index', [
            'model' => $model
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Banners();

        $localizations = Langs::getLangsWithTitle();

        return view('admin.banners.create', [
            'model'         => $model,
            'localizations' => $localizations
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $model = new Banners();

        DB::beginTransaction();

        try {
            $model->fill($request->all());

            $model->status = $request->get('status') ?? false;

            if (!$model->save()) {
                DB::rollBack();
            }

        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->back()->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->route('banners.index')->with('success', 'Баннер успешно добавлен!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param BlogArticles $blogArticle
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        // access
        if (!permissions('banners__banners__edit'))
            abort(404);

        $model = Banners::query()->where('id', $id)->first();

        return view('admin.banners.edit', [
            'model' => $model
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // access
        if (!permissions('banners__banners__edit')) {
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        };

//        dd($request->all());

        /* @var $model Banners */
        $model = Banners::query()->where('id', $id)->first();

        DB::beginTransaction();

        try {
            $model->fill($request->all());

            $model->status = $request->get('status') ?? false;

            if (!$model->save()) {
                DB::rollBack();
            }

            if($request->has('items')){
                foreach ($request->get('items') as $itemId => $item){
                    $itemModel = BannerItems::query()->where('id',$itemId)->first();

                    $item['active_from'] = Carbon::parse($item['active_from'])->format('Y-m-d H:i:00');
                    $item['active_to'] = Carbon::parse($item['active_to'])->format('Y-m-d H:i:00');
                    $item['active_from__2'] = Carbon::parse($item['active_from__2'])->format('Y-m-d H:i:00');
                    $item['active_to__2'] = Carbon::parse($item['active_to__2'])->format('Y-m-d H:i:00');

                    if($itemModel){
                        $itemModel->fill($item);

                        if(! $itemModel->save()){
                            DB::rollBack();
                        }

                        foreach (Langs::getLangsWithTitle() as $lang => $item2) {
                            $data = $item[$lang] ?? [];
                            $itemModel->translateOrNew($lang)->fill($data);

                            if (!$itemModel->save()) {
                                DB::rollBack();
                            }
                        }
                    }
                }
            }

        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('banners.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->back()->with('success', 'Баннер успешно обновлен!');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        // access
        if (!permissions('banners__banners__delete')) {
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        };

        $model = Banners::query()->where('id', $id)->first();
        $model->deleteTranslations();
        Banners::query()->where('id', $id)->delete();

        return redirect()->back()->with('success', 'Баннер успешно удалено!');
    }

    public function addEmptyItem(Request $request)
    {
        $item = new BannerItems();
        $item->banners_id = $request->get('banner_id');
        $item->save();

        foreach (Langs::getLangCodes() as $lang){
            $itemTrans = new BannerItemTranslation();
            $itemTrans->banner_items_id = $item->id;
            $itemTrans->lang = $lang;
            $itemTrans->save();
        }

        return redirect()->back()->with('success', 'Блок баннера успешно добавлен!');
    }

    public function removeItem(Request $request)
    {
        BannerItemTranslation::query()->where('banner_items_id',$request->get('item_id'))->delete();
        BannerItems::query()->where('id',$request->get('item_id'))->delete();

        return [
            'success' => true
        ];
    }

}
