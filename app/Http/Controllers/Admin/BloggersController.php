<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\BlogArticleRequest;
use App\Models\BlogArticles;
use App\Models\Bloggers;
use App\Models\Category;
use App\Models\Langs;
use App\Models\Tags;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BloggersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//         access
            if (!permissions('bloggers__bloggers__view'))
                abort(404);

        $name = $request->get('name');


        /* Sorting */
        $prefix = 'bloggers.';
        $sort   = 'created_at';
        $order  = 'desc';

        if ($request->has('sort') && $request->has('order')) {
            $sort  = $request->get('sort');
            $order = $request->get('order');

            if ($sort == 'name') {
                $prefix = 'blogger_translations.';
            }
        }

        $query = Bloggers::query()
            ->leftJoin('blogger_translations', 'blogger_translations.bloggers_id', '=', 'bloggers.id')
            ->where('blogger_translations.lang', 'ru')
            ->select([
                'bloggers.*',
                'blogger_translations.name AS name2'
            ]);

        if ($request->has('sort') && $request->has('order')) {
            $query->orderBy($prefix . $sort, $order);
        } else {
            $query->orderBy('bloggers.status', 'desc');
            $query->orderBy('bloggers.created_at', 'desc');
        }

        $query->where(function ($q) use ($name) {
            if ($name != '') {
                $q->where('blogger_translations.name', 'like', '%' . $name . '%');
            }
        });

        $model = $query->paginate(50);

        return view('admin.bloggers.index', [
            'model' => $model
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // access
        if (!permissions('bloggers__bloggers__create'))
            abort(404);

        $model = new Bloggers();

        $localizations = Langs::getLangsWithTitle();

        return view('admin.bloggers.create', [
            'model'         => $model,
            'localizations' => $localizations
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogArticleRequest $request)
    {
        // access
        if (!permissions('bloggers__bloggers__create')) {
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        };

        $model = new Bloggers();

        DB::beginTransaction();

        try {
            if (empty($request->input('slug', ''))) {
                $request->merge(array('slug' => SlugService::createSlug(Bloggers::class, 'slug', $request->input('page_data.' . Langs::getDefaultLangCode() . '.name'))));
            }

            $model->fill($request->all());

            $model->status = $request->get('status') ?? false;

            if (!$model->save()) {
                DB::rollBack();
            }

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $data = $request->input('page_data.' . $lang, []);

                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($data, $constructorData[$lang] ?? []));

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->back()->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->route('bloggers.index')->with('success', 'Публикация успешно добавлена!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param BlogArticles $blogArticle
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        // access
        if (!permissions('bloggers__bloggers__edit'))
            abort(404);

        $model = Bloggers::query()->where('id', $id)->first();

        $localizations = Langs::getLangsWithTitle();

        return view('admin.bloggers.edit', [
            'model'         => $model,
            'data'          => $model->getTranslationsArray(),
            'localizations' => $localizations,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // access
        if (!permissions('bloggers__bloggers__edit')) {
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        };

        /* @var $model Bloggers */
        $model = Bloggers::query()->where('id', $id)->first();

        DB::beginTransaction();

        try {
            $model->fill($request->all());

            $model->status = $request->get('status') ?? false;

            if (!$model->save()) {
                DB::rollBack();
            }

            $model->deleteTranslations();

            foreach (Langs::getLangsWithTitle() as $lang => $item) {
                $data = $request->input('page_data.' . $lang, []);

                $constructorData = $request->get('constructorData');
                $model->translateOrNew($lang)->fill(array_merge($data, $constructorData[$lang] ?? []));

                if (!$model->save()) {
                    DB::rollBack();
                }
            }
        } catch (\Throwable $e) {
            DB::rollBack();

            return redirect()->route('bloggers.index')->with('error', 'Ошибка! ' . $e->getMessage());
        }

        DB::commit();

        return redirect()->back()->with('success', 'Автор успешно обновлен!');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        // access
        if (!permissions('bloggers__bloggers__delete')) {
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        };

        $model = Bloggers::query()->where('id', $id)->first();
        $model->deleteTranslations();
        Bloggers::query()->where('id', $id)->delete();

        return redirect()->back()->with('success', 'Автора успешно удалено!');
    }

    public function deleteSelected(Request $request)
    {
        // access
        if (!permissions('bloggers__bloggers__delete')) {
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        };

        $ids = json_decode($request->get('ids'), true);

        if (count($ids)) {
            foreach ($ids as $id) {
                $model = Bloggers::query()->where('id', $id)->first();
                $model->deleteTranslations();
                Bloggers::query()->where('id', $id)->delete();
            }

            return redirect()->back()->with('success', 'Авторы успешно удалены!');
        }

        return redirect()->back()->with('error', 'Ошибка удаления!');
    }
}
