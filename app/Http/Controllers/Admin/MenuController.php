<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Langs;
use App\Models\Menu;
use App\Models\Translations\MenuTranslation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MenuController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        // access
        if (!permissions('appearance__menu__view')) abort(404);

        $tag = $request->get('tag');

        $tags = Menu::getTags();

        if (!$tag) {
            $tag = count($tags) ? array_shift($tags) : null;
        }

        $menuId = Menu::getMenuId($tag);

        $model = Menu::query()
            ->where('tag', $tag)
            ->where('const', '<>', 1)
            ->defaultOrder()
            ->get()
            ->toTree();

        return view('admin.menu.index', [
            'model' => json_encode($model, JSON_UNESCAPED_UNICODE),
            'tag'   => $tag,
            'menu_id' => $menuId ?? null
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // access
        if (!permissions('appearance__menu__create')) abort(404);

        $model = new Menu();

        return view('admin.menu.form', [
            'model' => $model
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // access
        if (!permissions('appearance__menu__create')) {
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        }

        $post = $request->all();

        Menu::create($post);

        cache()->flush();

        return redirect()->route('menu.index')->with('success', 'Пункт меню успішно створено!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // access
        if (!permissions('appearance__menu__edit')) abort(404);

        $model = Menu::query()->where('id', $id)->first();

        $localizations = Langs::getLangsWithTitle();

        return view('admin.menu.form', [
            'model'         => $model,
            'data'          => $model->getTranslationsArray(),
            'localizations' => $localizations,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // access
        if (!permissions('appearance__menu__edit')) {
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        }

        /* @var $model Menu */
        $model = Menu::query()->where('id', $id)->first();

        $post = $request->all();

        $model->fill($post);
        $model->save();

        $model->deleteTranslations();

        foreach (Langs::getLangsWithTitle() as $lang => $item) {
            $model->translateOrNew($lang)->fill($request->input('page_data.' . $lang, []));
            $model->save();
        }

        cache()->flush();

        return redirect()->route('menu.index', ['tag' => $model->tag])->with('success', 'Пункт меню успешно обновлено!');
    }

    /**
     * @param Request $request
     * @param $id
     * @return array
     */
    public function destroy(Request $request, $id)
    {
        // access
        if (!permissions('appearance__menu__edit')) {
            return [
                'status'  => false,
                'message' => 'У вас нет прав на выполнение данного действия!'
            ];
        };

        $confirm = $request->get('c') ?? 1;

        $node = Menu::query()->where('id', $id)->with('children')->first();

        $tag = $node->tag;

        if ($confirm && count($node->children)) {
            return [
                'status'      => false,
                'hasChildren' => true,
                'message'     => 'Этот пункт меню содержит в себе другие пункты, которые тоже будут удалены!'
            ];
        }

        $node->deleteTranslations();

        $node->delete();

        $tree = Menu::query()
            ->where('tag', $tag)
            ->where('const', '<>', 1)
            ->defaultOrder()
            ->get()
            ->toTree();

        cache()->flush();

        return [
            'status'  => true,
            'tree'    => $tree,
            'message' => 'Пункт меню успешно удалено!'
        ];
    }

    private function treeGetIds($tree, $ids = [])
    {
        foreach ($tree as $item) {
            $ids[] = $item['id'];

            if (count($item['children'])) {
                $ids = array_merge($ids, $this->treeGetIds($item['children'], $ids));
            }
        }

        return $ids;
    }

    public function rebuild(Request $request)
    {
        // access
        if (!permissions('appearance__menu__edit')) {
            return [
                'status'  => false,
                'message' => 'У вас нет прав на выполнение данного действия!'
            ];
        };

        $ids = $this->treeGetIds($request->get('tree'));
        $ids = array_unique($ids);

        $tree = Menu::query()
            ->whereNotIn('id', $ids)
            ->defaultOrder()
            ->get()
            ->toTree()
            ->toArray();

        $isRebuild = Menu::rebuildTree(array_merge($tree, $request->get('tree')));

        if ($isRebuild) {
            return [
                'status'  => true,
                'message' => 'Меню успішно оновлено!'
            ];
        }
    }

    public function addMenu(Request $request)
    {
        // access
        if (!permissions('appearance__menu__create')) {
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        }

        $post = $request->all();

        Menu::create($post);

        cache()->flush();

        return redirect()->route('menu.index', ['tag' => $request->get('tag')])->with('success', 'Меню успішно створено!');
    }

    public function addItem(Request $request)
    {
        // access
        if (!permissions('appearance__menu__edit')) {
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        }

        $currentTree = json_decode($request->get('tree'), true);

        $ids = $this->treeGetIds($currentTree);
        $ids = array_unique($ids);

        $tree = Menu::query()
            ->whereNotIn('id', $ids)
            ->defaultOrder()
            ->get()
            ->toTree()
            ->toArray();

        $currentTree[] = $request->except(['_token', 'name', 'url', 'tree']);

        $isRebuild = Menu::rebuildTree(array_merge($tree, $currentTree));

        $model = Menu::query()
            ->where('tag', $request->get('tag'))
            ->orderBy('id', 'desc')
            ->first();

        $modelTrans          = new MenuTranslation();
        $modelTrans->menu_id = $model->id;
        $modelTrans->name    = $request->get('name');
        $modelTrans->url     = $request->get('url') ?? null;
        $modelTrans->lang    = config('translatable.locale');
        $modelTrans->save();

        cache()->flush();

        return redirect()->back()->with('success', 'Пункт меню успешно создано!');
    }

    public function deleteMenu(Request $request)
    {
        // access
        if (!permissions('appearance__menu__delete')) {
            return redirect()->back()->with('error', 'У вас нет прав на выполнение данного действия!');
        }

        $menuIds =  Menu::query()
            ->where('tag', $request->get('tag'))
            ->where('const', 0)
            ->pluck('id')
            ->toArray();

        Menu::query()
            ->where('tag', $request->get('tag'))
            ->delete();

        MenuTranslation::query()
            ->whereIn('menu_id', $menuIds)
            ->delete();

        cache()->flush();

        return redirect()->route('menu.index')->with('success', 'Меню успішног видалено!');
    }

    public function move(Request $request)
    {
        // access
        if (!permissions('appearance__menu__edit')) {
            return [
                'status'  => false,
                'message' => 'У вас нет прав на выполнение данного действия!'
            ];
        }

        $action = $request->get('action');
        $node   = Menu::query()->where('id', $request->get('id'))->first();
        $node->$action();

        $tree = Menu::query()
            ->where('tag', $node->tag)
            ->where('const', '<>', 1)
            ->defaultOrder()
            ->get()
            ->toTree();

        cache()->flush();

        return [
            'status'  => true,
            'tree'    => $tree,
            'message' => ''
        ];
    }

    /* Скрыть/розкрыть - запомнить состояние */
    public function toggleCollapse(Request $request)
    {
        $model = Menu::query()->where('id', $request->get('id'))->first();

        if ($model) {
            $model->open = $request->get('collapseState');
            $model->save();

            cache()->flush();

            return [
                'success' => true
            ];
        }

        return [
            'success' => false
        ];
    }
}
