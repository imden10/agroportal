<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\BannerItems;
use App\Models\Langs;
use App\Traits\Statistics;

class RssController extends Controller
{
    use Statistics;

    /**
     * @param null $lang
     * @param $id
     */
    public function index($lang,$id)
    {
        $pathToFile = $this->getFile($lang,$id);

        if($pathToFile){
            return response()->file($pathToFile,['Content-Type' => 'text/xml']);
        } else {
            return redirect('/');
        }
    }

    public function indexUk($id)
    {
        $lang = 'uk';

        $pathToFile = $this->getFile($lang,$id);

        if($pathToFile){
            return response()->file($pathToFile,['Content-Type' => 'text/xml']);
        } else {
            return redirect('/');
        }
    }

    private function getFile($lang,$id)
    {
        if(file_exists(public_path() . '/rss_files/' . $lang . '/' . $id)){
            return public_path() . '/rss_files/' . $lang . '/' . $id;
        } else {
            return null;
        }
    }

    public function sitemap()
    {
        $pathToFile = public_path() . '/sitemap_file.xml';

        return response()->file($pathToFile,['Content-Type' => 'text/xml']);
    }
}
