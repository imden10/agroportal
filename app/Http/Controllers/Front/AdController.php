<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\BannerItems;
use App\Traits\Statistics;

class AdController extends Controller
{
    use Statistics;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($id)
    {
        $item = BannerItems::query()->where('id',$id)->first();

        $type = request()->get('type');

        if($item && $type){
            $this->setClick($item->id,request()->ip(), $_SERVER['HTTP_USER_AGENT']);

            $link = '';

            if($type === '1'){
                $link = $item->link;
            } elseif($type === '2'){
                $link = $item->link__2;
            }

            return redirect($link);
        }
    }
}
