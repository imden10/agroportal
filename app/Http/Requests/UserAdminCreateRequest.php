<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserAdminCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|min:2',
            'email'     => 'required|string|email|max:255|unique:users',
            'password'  => 'required|min:8',
            'group_ids' => 'required|array',
        ];
    }

    public function messages()
    {
        return [
            'name.required'      => 'Имя - обязательное поле для заполнения',
            'name.min'           => 'Имя - минимум 2 символа',
            'email.required'     => 'E-mail - обязательное поле для заполнения',
            'email.email'        => 'E-mail - не валидный',
            'email.unique'       => 'Такой e-mail уже существует в базе данных',
            'password.required'  => 'Пароль - обязательное поле для заполнения',
            'password.min'       => 'Пароль - минимум 8 символов',
            'group_ids.required' => 'Группы пользователя - обязательное поле для заполнения',
        ];
    }
}
