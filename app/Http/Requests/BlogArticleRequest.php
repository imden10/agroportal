<?php

namespace App\Http\Requests;

use App\Models\Langs;
use Illuminate\Foundation\Http\FormRequest;

class BlogArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $lang = Langs::getDefaultLangCode();

        return [
            'page_data.'.$lang.'.name' => 'required'
        ];
    }

    public function messages()
    {
        $lang = Langs::getDefaultLangCode();

        return [
            'page_data.'.$lang.'.name.required' => 'Заголовок - обязательное поле'
        ];
    }
}
