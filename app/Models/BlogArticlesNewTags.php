<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * Class CarOptions
 * @package App
 *
 * @property integer $cars_id
 * @property integer $option_id
 */
class BlogArticlesNewTags extends Pivot
{
    use HasFactory;

    protected $table = 'blog_articles_new_tag';

    public $timestamps = false;

    protected $guarded = [];
}
