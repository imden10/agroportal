<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Labels
 * @package App\Models
 *
 * @property string $lang
 * @property string $name
 */
class Labels extends Model
{
    use HasFactory;

    protected $table = 'labels';

    protected $fillable = [
        'lang',
        'name'
    ];

    public $timestamps = false;
}
