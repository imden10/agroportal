<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BannerStatisticClicks extends Model
{
    use HasFactory;

    protected $table = 'banner_statistic_clicks';

    protected $guarded = [];
}
