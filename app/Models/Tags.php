<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Tags
 * @package App\Models
 *
 * @property string $lang
 * @property string $name
 */
class Tags extends Model
{
    use HasFactory;

    protected $table = 'tags';

    protected $fillable = [
        'lang',
        'name'
    ];

    public $timestamps = false;
}
