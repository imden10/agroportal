<?php

namespace App\Models\Orig;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Struct extends Model
{
    use HasFactory;

    protected $table = 'struct';

    protected $guarded = [];

    protected $connection = 'orig';

    const ARTICLES_ALIAS = [
        'news',
        'pubs',
        'specprojects',
        'blogs',
        'opinions',
        'survey',
        'multimedias',
    ];

    public function children()
    {
        return $this->hasMany(self::class,'parent_id','struct_id')
            ->where('showmode','<>','trash')
            ->whereNotIn('alias', self::ARTICLES_ALIAS);
    }

    public function articles()
    {
        return $this->hasMany(self::class,'parent_id','struct_id')
            ->where('showmode','<>','trash')
            ->whereIn('alias', self::ARTICLES_ALIAS);
    }

    public function items()
    {
        return $this->hasMany(Items::class,'struct_id','struct_id');
    }

    public function combo()
    {
        return $this->hasMany(Combo::class,'struct_id','struct_id');
    }
}
