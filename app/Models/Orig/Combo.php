<?php

namespace App\Models\Orig;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Combo extends Model
{
    use HasFactory;

    protected $table = 'combo';

    protected $guarded = [];

    protected $connection = 'orig';
}
