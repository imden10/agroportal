<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\Models\Translations\PagesTranslation;

/**
 * Class Pages
 * @package App
 *
 * @property string $slug
 * @property integer $status
 * @property string $image
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property array $getStatuses
 */
class Pages extends Model
{
    use Sluggable;
    use Translatable;

    protected $table = 'pages';

    public $translationModel = 'App\Models\Translations\PagesTranslation';

    public $translatedAttributes = [
        'title',
        'description',
        'meta_title',
        'meta_keywords',
        'meta_description'
    ];

    protected $fillable = [
        'slug',
        'status',
        'image',
    ];

    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE     = 1;

    public function getDefaultTitleAttribute()
    {
        return $this->translateOrDefault()->title;
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public static function getStatuses(): array
    {
        return [
            self::STATUS_NOT_ACTIVE => [
                'title'    => 'Не активная',
                'bg_color' => '#ff3838',
                'color'    => '#fdfdfd'
            ],
            self::STATUS_ACTIVE     => [
                'title'    => 'Активная',
                'bg_color' => '#49cc00',
                'color'    => 'white'
            ]
        ];
    }

    public function showStatus()
    {
        return '<span class="badge" style="color: '.self::getStatuses()[$this->status]["color"].'; background-color: '.self::getStatuses()[$this->status]["bg_color"].'">'.self::getStatuses()[$this->status]["title"].'</span>';
    }

    public function scopeActive($query)
    {
        return $query->where('pages.status', self::STATUS_ACTIVE);
    }

    /* Есть ли языковая версия */
    public function hasLang($lang){
        return PagesTranslation::query()
            ->where('pages_id',$this->id)
            ->where('lang',$lang)
            ->where('title','<>','')
            ->exists();
    }
}
