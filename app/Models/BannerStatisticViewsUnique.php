<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BannerStatisticViewsUnique extends Model
{
    use HasFactory;

    protected $table = 'banner_statistic_views_uniques';

    protected $guarded = [];

    public $timestamps = false;
}
