<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewTagTranslation extends Model
{
    use HasFactory;

    protected $table = 'new_tag_translations';

    protected $fillable = [
        'name',
        'description_short',
        'h2',
        'description_hide',
        'description',
        'meta_title',
        'meta_description'
    ];
}
