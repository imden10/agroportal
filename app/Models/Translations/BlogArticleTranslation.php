<?php

namespace App\Models\Translations;

use App\Modules\Constructor\Collections\ComponentCollections;
use App\Modules\Constructor\Contracts\HasConstructor;
use App\Modules\Constructor\Traits\Constructorable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlogArticleTranslation extends Model implements HasConstructor
{
    use HasFactory;
    use Constructorable;

    protected $table = 'blog_article_translations';

    private string $entityAttribute = 'blog_articles_id';


    protected $fillable = [
        'name',
        'excerpt',
        'text',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'tags',
        'labels',
        'annot',
        'addtext',
        'img_alt',
        'video',
        'photo',
        'iframe',
        'img_desc',
        'seo_title',
        'landing_link',
        'constructor_html',
        'search_field',
        'is_search_render',
    ];

    /**
     * @param array $attributes
     * @return Model
     */
    public function fill(array $attributes = [])
    {
        $this->fillConstructorable($attributes);

        return parent::fill($attributes);
    }

    public $timestamps = false;

    /**
     * @inheritDoc
     */
    public function constructorComponents(): array
    {
        return [
            'simple-text'               => ComponentCollections::simpleText(),
            'image-and-text'            => ComponentCollections::imageAndText(),
            'quotes'                    => ComponentCollections::quotes(),
            'list'                      => ComponentCollections::list(),
            'see-also'                  => ComponentCollections::seeAlso(),
            'gallery'                   => ComponentCollections::gallery(),
            'images'                    => ComponentCollections::images(),
            'table-component-for-site'  => ComponentCollections::tableComponentForSite(),
            'see-also-landing-for-site' => ComponentCollections::seeAlsoLandingForSite(),
            'widget'                    => ComponentCollections::widget(),
        ];
    }
}
