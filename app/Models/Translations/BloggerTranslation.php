<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BloggerTranslation extends Model
{
    use HasFactory;

    protected $table = 'blogger_translations';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'img_alt',
        'text_for_main',
        'text',
        'meta_title',
        'meta_description'
    ];
}
