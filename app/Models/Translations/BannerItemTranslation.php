<?php

namespace App\Models\Translations;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BannerItemTranslation extends Model
{
    use HasFactory;

    protected $table = 'banner_item_translations';

    public $timestamps = false;

    protected $fillable = [
        'text',
        'btn_name',
        'text__2',
        'btn_name__2',
    ];
}
