<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Bloggers
 * @package App\Models
 *
 * @property integer $agro_id
 * @property string $slug
 * @property string $image
 * @property string $image_thumbnail
 * @property string $creator
 * @property string $changer
 * @property integer $status
 * @property integer $views
 * @property string $email
 * @property string $facebook
 * @property string $instagram
 * @property string $telegram
 */

class Bloggers extends Model
{
    use HasFactory;

    use Sluggable;
    use Translatable;
    use SluggableScopeHelpers;

    protected $table = 'bloggers';

    public $translationModel = 'App\Models\Translations\BloggerTranslation';

    public $translatedAttributes = [
        'name',
        'img_alt',
        'text_for_main',
        'text',
        'meta_title',
        'meta_description'
    ];

    protected $fillable = [
        'agro_id',
        'slug',
        'image',
        'image_thumbnail',
        'creator',
        'changer',
        'status',
        'views',
        'email',
        'facebook',
        'instagram',
        'telegram'
    ];

    const STATUS_NOT_ACTIVE = false;
    const STATUS_ACTIVE     = true;

    public function getDefaultTitleAttribute()
    {
        return $this->translateOrDefault()->name;
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'defaultTitle'
            ]
        ];
    }

    public static function getStatuses(): array
    {
        return [
            self::STATUS_NOT_ACTIVE => [
                'title'    => 'Не активный',
                'bg_color' => '#ff3838',
                'color'    => '#fdfdfd'
            ],
            self::STATUS_ACTIVE     => [
                'title'    => 'Активный',
                'bg_color' => '#49cc00',
                'color'    => 'white'
            ]
        ];
    }

    public function showStatus()
    {
        return '<span class="badge" style="color: ' . self::getStatuses()[$this->status]["color"] . '; background-color: ' . self::getStatuses()[$this->status]["bg_color"] . '">' . self::getStatuses()[$this->status]["title"] . '</span>';
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }
}
