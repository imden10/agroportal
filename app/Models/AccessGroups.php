<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AccessGroups
 * @package App\Models
 *
 * @property string $name
 * @property string $code
 * @property string $description
 * @property string $tree_permission
 *
 * @property AccessGroupPermissions[] $permissions
 */

class AccessGroups extends Model
{
    use HasFactory;
    use Sluggable;

    protected $table = 'access_groups';

    protected $fillable = [
        'name',
        'code',
        'description'
    ];

    public function sluggable(): array
    {
        return [
            'code' => [
                'source' => 'name'
            ]
        ];
    }

    public function permissions()
    {
        return $this->hasMany(AccessGroupPermissions::class,'group_id','id');
    }
}
