<?php

namespace App\Models;

class AccessPermissions
{
    const DATA = [
        'dashboard' => [
            'section_name' => 'Панель управления',
            'section_code' => 'dashboard',
            'blocks' => [
                'dashboard' => [
                    'name'        => 'Панель управления',
                    'code'        => 'dashboard',
                    'permissions' => [
                        'view'
                    ]
                ]
            ]
        ],
        'admins' => [
            'section_name' => 'Пользователи',
            'section_code' => 'admins',
            'blocks' => [
                'users' => [
                    'name'        => 'Пользователи',
                    'code'        => 'users',
                    'permissions' => [
                        'view', 'create', 'edit', 'delete'
                    ]
                ],
                'user_groups' => [
                    'name'        => 'Группы пользователей',
                    'code'        => 'user_groups',
                    'permissions' => [
                        'view', 'create', 'edit', 'delete'
                    ]
                ]
            ]
        ],
        'pages' => [
            'section_name' => 'Страницы',
            'section_code' => 'pages',
            'blocks' => [
                'pages' => [
                    'name'        => 'Страницы',
                    'code'        => 'pages',
                    'permissions' => [
                        'view', 'create', 'edit', 'delete'
                    ]
                ]
            ]
        ],
        'landings' => [
            'section_name' => 'Лендинги',
            'section_code' => 'landings',
            'blocks' => [
                'landings' => [
                    'name'        => 'Лендинги',
                    'code'        => 'landings',
                    'permissions' => [
                        'view', 'create', 'edit', 'delete'
                    ]
                ]
            ]
        ],
        'tags' => [
            'section_name' => 'Теги',
            'section_code' => 'tags',
            'blocks' => [
                'tags' => [
                    'name'        => 'Теги',
                    'code'        => 'tags',
                    'permissions' => [
                        'view', 'create', 'edit', 'delete'
                    ]
                ]
            ]
        ],
        'blog' => [
            'section_name' => 'Рубрики',
            'section_code' => 'categories',
            'blocks' => [
                'articles' => [
                    'name'        => 'Публикации',
                    'code'        => 'articles',
                    'permissions' => [
                        'view', 'create', 'edit', 'delete'
                    ]
                ],
                'categories' => [
                    'name'        => 'Рубрики',
                    'code'        => 'categories',
                    'permissions' => [
                        'view', 'create', 'edit', 'delete'
                    ]
                ],
                'subscribe' => [
                    'name'        => 'Подписка',
                    'code'        => 'subscribe',
                    'permissions' => [
                        'view', 'export'
                    ]
                ]
            ]
        ],
        'bloggers' => [
            'section_name' => 'Стрница автора',
            'section_code' => 'bloggers',
            'blocks' => [
                'bloggers' => [
                    'name'        => 'Стрница автора',
                    'code'        => 'bloggers',
                    'permissions' => [
                        'view', 'create', 'edit', 'delete'
                    ]
                ]
            ]
        ],
        'fm' => [
            'section_name' => 'Мультимедиа',
            'section_code' => 'fm',
            'blocks' => [
                'fm' => [
                    'name'        => 'Мультимедиа',
                    'code'        => 'fm',
                    'permissions' => [
                        'view'
                    ]
                ]
            ]
        ],
        'banners' => [
            'section_name' => 'Беннеры',
            'section_code' => 'banners',
            'blocks' => [
                'banners' => [
                    'name'        => 'Беннеры',
                    'code'        => 'banners',
                    'permissions' => [
                        'view', 'edit'
                    ]
                ],
                'stat' => [
                    'name'        => 'Статистика',
                    'code'        => 'stat',
                    'permissions' => [
                        'view'
                    ]
                ]
            ]
        ],
        'appearance' => [
            'section_name' => 'Внешний вид',
            'section_code' => 'appearance',
            'blocks' => [
                'widgets' => [
                    'name'        => 'Виджеты',
                    'code'        => 'widgets',
                    'permissions' => [
                        'view', 'create', 'edit', 'delete'
                    ]
                ],
                'menu' => [
                    'name'        => 'Меню',
                    'code'        => 'menu',
                    'permissions' => [
                        'view', 'create', 'edit', 'delete'
                    ]
                ]
            ]
        ],
        'settings' => [
            'section_name' => 'Настройки',
            'section_code' => 'settings',
            'blocks' => [
                'main' => [
                    'name'        => 'Главные',
                    'code'        => 'main',
                    'permissions' => [
                        'view', 'edit'
                    ]
                ],
                'contacts' => [
                    'name'        => 'Контакты',
                    'code'        => 'contacts',
                    'permissions' => [
                        'view', 'edit'
                    ]
                ],
                'blog' => [
                    'name'        => 'Блог',
                    'code'        => 'blog',
                    'permissions' => [
                        'view', 'edit'
                    ]
                ],
                'page' => [
                    'name'        => 'Страницы',
                    'code'        => 'page',
                    'permissions' => [
                        'view', 'edit'
                    ]
                ],
                'footer' => [
                    'name'        => 'Футер',
                    'code'        => 'footer',
                    'permissions' => [
                        'view', 'edit'
                    ]
                ],
            ]
        ]
    ];

    const CODE_NAMES = [
        'view'   => 'Просмотр',
        'create' => 'Создание',
        'edit'   => 'Редактирование',
        'delete' => 'Удаление',
        'export' => 'Экспорт',
    ];
}
