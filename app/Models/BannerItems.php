<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;

class BannerItems extends Model
{
    use HasFactory;
    use Translatable;

    public $translationModel = 'App\Models\Translations\BannerItemTranslation';

    protected $fillable = [
        'img_desktop',
        'img2_desktop',
        'img_desktop_alt',
        'img_desktop2',
        'img_desktop3',
        'img2_desktop2',
        'img_desktop2_alt',
        'img_desktop3_alt',
        'img_tablet',
        'img_tablet_alt',
        'img_mob',
        'img_mob_alt',
        'link',
        'html',
        'name',
        'active_from',
        'active_to',
        'img_desktop__2',
        'img2_desktop__2',
        'img_desktop_alt__2',
        'img_desktop2__2',
        'img2_desktop2__2',
        'img_desktop2_alt__2',
        'img_tablet__2',
        'img_tablet_alt__2',
        'img_mob__2',
        'img_mob_alt__2',
        'link__2',
        'img_desktop3__2',
        'img_desktop3_alt__2',
        'html__2',
        'active_from__2',
        'active_to__2',
    ];

    public $translatedAttributes = [
        'text',
        'btn_name',
        'text__2',
        'btn_name__2',
    ];

    public function parent()
    {
        return $this->hasOne(Banners::class,'id','banners_id');
    }
}
