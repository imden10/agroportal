<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BannerStatisticViews extends Model
{
    use HasFactory;

    protected $table = 'banner_statistic_views';

    protected $guarded = [];
}
