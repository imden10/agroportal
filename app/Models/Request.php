<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Request
 * @package App\Models
 *
 * @property string $name
 * @property string $phone
 * @property integer $plasmacenter_id
 * @property string $plasmacenter_name
 * @property string $datetime
 * @property integer $status
 * @property integer $promo
 *
 * @property Plasmacenters $plasmacenter
 */
class Request extends Model
{
    use HasFactory;

    protected $table = 'requests';

    protected $guarded = [];

    const STATUS_NEW  = 0;
    const STATUS_VIEW = 1;

    public static function getStatuses(): array
    {
        return [
            self::STATUS_NEW  => 'Новая',
            self::STATUS_VIEW => 'Просмотренная'
        ];
    }

    public function plasmacenter()
    {
        return $this->hasOne(Plasmacenters::class,'id','plasmacenter_id');
    }
}
