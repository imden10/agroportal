<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Landing extends Model
{
    use HasFactory;

    use Sluggable;
    use Translatable;

    protected $table = 'landings';

    public $translationModel = 'App\Models\Translations\LandingTranslation';

    public $translatedAttributes = [
        'title',
        'description',
        'meta_title',
        'meta_keywords',
        'meta_description'
    ];

    protected $fillable = [
        'slug',
        'status',
        'template',
        'color_theme',
        'image'
    ];

    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE     = 1;

    public function getDefaultTitleAttribute()
    {
        return $this->translateOrDefault()->title;
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * @return array
     */
    public static function getStatuses(): array
    {
        return [
            self::STATUS_NOT_ACTIVE => [
                'title'    => 'Не активный',
                'bg_color' => '#ff3838',
                'color'    => '#fdfdfd'
            ],
            self::STATUS_ACTIVE     => [
                'title'    => 'Активный',
                'bg_color' => '#49cc00',
                'color'    => 'white'
            ]
        ];
    }

    /**
     * @return string
     */
    public function showStatus()
    {
        return '<span class="badge" style="color: ' . self::getStatuses()[$this->status]["color"] . '; background-color: ' . self::getStatuses()[$this->status]["bg_color"] . '">' . self::getStatuses()[$this->status]["title"] . '</span>';
    }

    /**
     * @param Builder $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('landings.status', self::STATUS_ACTIVE);
    }
}
