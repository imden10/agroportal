<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\Pivot;

class CategoryArticles extends Pivot
{
    use HasFactory;

    protected $table = 'category_articles';

    public $timestamps = false;

    protected $guarded = [];
}
