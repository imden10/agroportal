<?php

namespace App\Models;

use App\Models\Translations\BlogArticleTranslation;
use Astrotomic\Translatable\Translatable;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class BlogArticles
 * @package App\Models
 *
 * @property integer $id
 * @property string $slug
 * @property integer $status
 * @property integer $views
 * @property integer $agro_id
 * @property integer $user_id
 * @property string $creator
 * @property string $changer
 * @property Carbon $public_date
 * @property string $image
 * @property integer $is_blog
 * @property integer $blogger_id
 * @property integer $interview
 * @property integer $mark
 * @property integer $category_id
 *
 *  @property Bloggers $blogger
 *  @property Category $category
 *  @property Gallery[] $gallery
 */
class BlogArticles extends Model
{
    use HasFactory;
    use Sluggable;
    use Translatable;

    protected $table = 'blog_articles';

    public $translationModel = 'App\Models\Translations\BlogArticleTranslation';

    public $translatedAttributes = [
        'name',
        'excerpt',
        'text',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'tags',
        'labels',
        'annot',
        'addtext',
        'img_alt',
        'video',
        'photo',
        'iframe',
        'img_desc',
        'seo_title',
        'landing_link',
        'constructor_html',
        'search_field',
        'is_search_render',
    ];

    protected $fillable = [
        'slug',
        'views',
        'user_id',
        'image',
        'public_date',
        'agro_id',
        'is_blog',
        'blogger_id',
        'creator',
        'changer',
        'interview',
        'mark',
        'category_id',
        'constructor_rerender'
    ];

    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE     = 1;

    public function getDefaultTitleAttribute()
    {
        return $this->translateOrDefault()->name;
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'defaultTitle'
            ]
        ];
    }

    public static function getStatuses(): array
    {
        return [
            self::STATUS_NOT_ACTIVE => [
                'title'    => 'Не активная',
                'bg_color' => '#ff3838',
                'color'    => '#fdfdfd'
            ],
            self::STATUS_ACTIVE     => [
                'title'    => 'Активная',
                'bg_color' => '#49cc00',
                'color'    => 'white'
            ]
        ];
    }

    public function showStatus()
    {
        return '<span class="badge" style="color: '.self::getStatuses()[$this->status]["color"].'; background-color: '.self::getStatuses()[$this->status]["bg_color"].'">'.self::getStatuses()[$this->status]["title"].'</span>';
    }

    public function scopeActive($query)
    {
        return $query->where('blog_articles.status', self::STATUS_ACTIVE)
            ->where('blog_articles.public_date', '<=', Carbon::now());
    }

    public function blogger()
    {
        return $this->hasOne(Bloggers::class,'id','blogger_id');
    }

    public function category(){
        return $this->hasOne(Category::class,'id','category_id');
    }

    public function gallery()
    {
        return $this->hasMany(Gallery::class,'article_id','id');
    }

    /* Есть ли языковая версия */
    public function hasLang($lang){
        return BlogArticleTranslation::query()
            ->where('blog_articles_id',$this->id)
            ->where('lang',$lang)
            ->where('name','<>','')
            ->exists();
    }

    public function getAllLangsNotEmpty()
    {
        $id = $this->id;
        $res = cache()->rememberForever('getAllLangsNotEmpty_' . $id,function () use($id){
            return BlogArticleTranslation::query()
                ->where('blog_articles_id',$id)
                ->where('name','<>','')
                ->orderBy('lang','desc')
                ->pluck('lang')
                ->toArray();
        });

        return $res;
    }

    public function showAllLangsNotEmpty()
    {
        $langs = $this->getAllLangsNotEmpty();

        $colors = [
            'uk' => '#ff9800',
            'ru' => '#2196f3'
        ];

        $str = '';

        if(count($langs)){
            foreach($langs as $lang){
                if(in_array($lang,['uk','ru'])){
                    $str .= '<span class="badge" style="color: #fff; background-color: '.$colors[$lang].'; margin: 0 2px">'.$lang.'</span>';
                }
            }
        }

        return $str;
    }

    /* Есть ли изобржение */
    public function isWithImage()
    {
        return $this->image ? true : false;
    }

    /* вывести лейбл есть ли изображение */
    public function showIsImageLabel()
    {
        $isWithImg = $this->isWithImage();

        $str = '';

        if($isWithImg){
            $str = '<span class="badge" style="color: #fff; background-color: #49cc00; margin: 0 2px"><i class="fa fa-check"></i></span>';
        } else {
            $str = '<span class="badge" style="color: #fdfdfd; background-color: #ff3838; margin: 0 2px"><i class="fa fa-ban"></i></span>';
        }

        return $str;
    }

    /**
     * @return BelongsToMany
     */
    public function newTags():BelongsToMany
    {
        return $this->belongsToMany(NewTag::class)->using(BlogArticlesNewTags::class);
    }
}
