<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RssCategory extends Model
{
    use HasFactory;

    protected $table = 'rss_categories';

    public $timestamps = false;

    public static function getList($lang = 'uk')
    {
        return self::query()
            ->leftJoin('rss_category_translations','rss_category_translations.category_id','=','rss_categories.id')
            ->where('rss_category_translations.lang',$lang)
            ->select([
                'rss_categories.*',
                'rss_category_translations.name'
            ])
            ->where('rss_categories.parent_id',0)
            ->pluck('rss_category_translations.name','rss_categories.id')
            ->toArray();
    }

    public static function getSubList($id,$lang = 'uk')
    {
        return self::query()
            ->leftJoin('rss_category_translations','rss_category_translations.category_id','=','rss_categories.id')
            ->where('rss_category_translations.lang',$lang)
            ->select([
                'rss_categories.*',
                'rss_category_translations.name'
            ])
            ->where('rss_categories.parent_id',$id)
            ->pluck('rss_category_translations.name','rss_categories.id')
            ->toArray();
    }
}
