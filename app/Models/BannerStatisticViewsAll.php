<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BannerStatisticViewsAll extends Model
{
    use HasFactory;

    protected $table = 'banner_statistic_views_all';

    protected $guarded = [];

    public $timestamps = false;
}
