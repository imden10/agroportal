<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AccessGroupPermissions
 * @package App\Models
 *
 * @property integer $group_id
 * @property string $section_code
 * @property string $block_code
 * @property string $permission
 * @property boolean $on
 */

class AccessGroupPermissions extends Model
{
    use HasFactory;

    protected $table = 'access_group_permissions';

    protected $guarded = [];

    public $timestamps = false;

    protected $casts = [
        'on' => 'boolean'
    ];
}
