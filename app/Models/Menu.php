<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class Menu extends Model
{
    use NodeTrait;
    use Translatable;

    protected $table = "menu";

    public $translationModel = 'App\Models\Translations\MenuTranslation';

    public $translatedAttributes = [
        'name',
        'url'
    ];

    protected $fillable = [
        'visibility',
        'parent_id ',
        'tag',
        'const',
        'type',
        'model_id',
        'icon',
        'banner_item_id',
        'open'
    ];

    public $timestamps = false;

    const VISIBILITY_OFF = 0;
    const VISIBILITY_ON  = 1;

    const TYPE_ARBITRARY = 0;
    const TYPE_PAGE      = 1;
    const TYPE_CATEGORY  = 2;
    const TYPE_TAG       = 3;

    public static function getVisibility(): array
    {
        return [
            self::VISIBILITY_OFF => 'Не показувати',
            self::VISIBILITY_ON  => 'Показувати'
        ];
    }

    /**
     * @return array
     */
    public static function getTags(): array
    {
        return self::query()
                ->where('const', 1)
                ->pluck('tag', 'tag')
                ->toArray() ?? [];
    }

    public static function getMenuId($tag)
    {
        $model = self::query()
            ->where('const', 1)
            ->where('tag',$tag)
            ->first();

        return $model ? $model->id : null;
    }

    /**
     * @return array
     */
    public static function getTagsWithId(): array
    {
        return self::query()
                ->where('const', 1)
                ->pluck('tag', 'id')
                ->toArray() ?? [];
    }

    public static function getTypes(): array
    {
        return [
            self::TYPE_PAGE      => 'Страницы',
            self::TYPE_CATEGORY  => 'Рубрики',
            self::TYPE_TAG       => 'Теги',
            self::TYPE_ARBITRARY => 'Произвольные ссылки'
        ];
    }

    public static function getTypesOne(): array
    {
        return [
            self::TYPE_PAGE      => 'Страница',
            self::TYPE_CATEGORY  => 'Рубрика',
            self::TYPE_TAG       => 'Тег',
            self::TYPE_ARBITRARY => 'Произвольная ссылка'
        ];
    }

    public static function getTypesModel(): array
    {
        return [
            self::TYPE_PAGE => [
                'rel'        => 'page',
                'name'       => 'title',
                'url_prefix' => ''
            ],
            self::TYPE_CATEGORY => [
                'rel'        => 'category',
                'name'       => 'title',
                'url_prefix' => ''
            ],
            self::TYPE_TAG => [
                'rel'        => 'tag',
                'name'       => 'name',
                'url_prefix' => 'tags/'
            ]
        ];
    }

    public function page()
    {
        return $this->hasOne(Pages::class, 'id', 'model_id');
    }

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'model_id');
    }

//    public function tag()
//    {
//        return $this->hasOne(Tags::class, 'id', 'model_id');
//    }

    public function tag()
    {
        return $this->hasOne(NewTag::class, 'id', 'model_id');
    }

//    public function tags()
//    {
//        return $this->hasMany(Tags::class, 'id', 'model_id');
//    }
}
