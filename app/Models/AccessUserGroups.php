<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccessUserGroups extends Model
{
    use HasFactory;
    protected $table = 'access_user_groups';

    protected $guarded = [];

    public $timestamps = false;
}
