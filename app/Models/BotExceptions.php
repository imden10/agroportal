<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BotExceptions
 * @package App
 *
 * @property integer $id
 * @property string $name
 */

class BotExceptions extends Model
{
    protected $table = 'bot_exceptions';

    protected $guarded = [];

    public $timestamps = false;
}
