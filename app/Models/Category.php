<?php

namespace App\Models;

use Astrotomic\Translatable\Translatable;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 * @package App\Models
 *
 * @property string $slug
 * @property string $image
 * @property string $path
 * @property string $creator
 * @property string $changer
 * @property integer $parent_id
 * @property integer $order
 * @property integer $agro_id
 * @property boolean $status
 * @property boolean $active
 * @property boolean $collapse
 *
 * @property BlogArticles[] $articles
 * @property BlogArticles[] $articlesInSubCategories
 * @property SideCategorySettings $setting
 */
class Category extends Model
{
    use HasFactory;

    use Sluggable;
    use Translatable;
    use SluggableScopeHelpers;

    protected $table = 'category';

    public $translationModel = 'App\Models\Translations\CategoryTranslation';

    public $translatedAttributes = [
        'title',
        'excerpt',
        'description',
        'h2',
        'meta_title',
        'meta_keywords',
        'meta_description'
    ];

    protected $fillable = [
        'slug',
        'image',
        'order',
        'status',
        'parent_id',
        'collapse',
        'agro_id',
        'path',
        'creator',
        'changer',
        'active'
    ];

    protected $casts = [
        'status'   => 'boolean',
        'active'   => 'boolean',
        'collapse' => 'boolean',
    ];

    const STATUS_NOT_ACTIVE = false;
    const STATUS_ACTIVE     = true;

    public function getDefaultTitleAttribute()
    {
        return $this->translateOrDefault()->title;
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'defaultTitle'
            ]
        ];
    }

    public static function getStatuses(): array
    {
        return [
            self::STATUS_NOT_ACTIVE => [
                'title'    => 'Не активная',
                'bg_color' => '#ff3838',
                'color'    => '#fdfdfd'
            ],
            self::STATUS_ACTIVE     => [
                'title'    => 'Активная',
                'bg_color' => '#49cc00',
                'color'    => 'white'
            ]
        ];
    }

    public function showStatus()
    {
        return '<span class="badge" style="color: ' . self::getStatuses()[$this->status]["color"] . '; background-color: ' . self::getStatuses()[$this->status]["bg_color"] . '">' . self::getStatuses()[$this->status]["title"] . '</span>';
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    /* Generate tree structure data */
    public function treeStructure()
    {
        $cat = $this;
        $data = cache()->rememberForever('treeStructure',function() use($cat){
            $collection = $cat->query()->withTranslation()
                ->get()
                ->toArray();

            $normalize = [];

            foreach ($collection as $item) {
                $normalize[$item['parent_id']][$item['id']] = $item;
            }
            if (!empty($normalize) && isset($normalize[0])) {
                $node = $normalize[0];

                $cat->treeNode($node, $normalize);
            } else {
                $node = $normalize;
            }

            return collect($node);
        });
        return $data;
    }

    public function treeNode(&$node, $normalize)
    {
        foreach ($node as $key => $item) {
            if (!isset($item['children'])) {
                $node[$key]['children'] = [];
            }

            if (array_key_exists($key, $normalize)) {
                $node[$key]['children'] = $normalize[$key];
                $this->treeNode($node[$key]['children'], $normalize);
            }
        }
    }

    public function parent()
    {
        return $this->belongsTo(self::class, 'parent_id');
    }

    private function getCategories($cat, $res = [])
    {
        if (isset($cat->parent->title)) {
            $res[] = $cat->parent->title;
            return $this->getCategories($cat->parent, $res);
        }

        return $res;
    }

    public function getNameWithPath()
    {
        $cat = $this;
        $data = cache()->rememberForever('getNameWithPath_'.$cat->id,function () use($cat){
            $arr = $cat->getCategories($cat);

            $arr = array_reverse($arr);

            $res = '';

            foreach ($arr as $item) {
                $res .= $item . ' > ';
            }

            $res .= $cat->title;

            return $res;
        });

        return $data;
    }

    /**
     * Публикаии в текущей категории, без учета вложинных подкатегория
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function articles()
    {
        return $this->hasMany(BlogArticles::class,'category_id','id');
    }

    /**
     * Подсчет количества публикаций в категории
     * @return int
     */
    public function countArticles(): int
    {
        return $this->articles()->active()->count();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childs()
    {
        return $this->hasMany(Category::class, 'parent_id', 'id');
    }

    /**
     * Подсчет количества публикаций в категории с учетом всех подкатегорий
     * @return int
     */
    public function countArticlesInSubCategories(): int
    {
        $self  = $this;
        $count = 0;
        $count += $this->countArticles() +
            $self->childs()->get()
                ->sum(function (Category $category) {
                    return $category->countArticlesInSubCategories();
                });
        return $count;
    }

    public function getAllChildCategoryIds($children = null, $ids = [])
    {
        if ($children) {
            foreach ($children->get() as $child) {
                $ids[] = $child->id;
                $ids   = $this->getAllChildCategoryIds($child->childs(), $ids);
            }

        }

        return $ids;
    }

    /**
     * публикации в категории с учетом всех подкатегорий
     */
    public function articlesInSubCategories()
    {
        $ids   = [];
        $ids[] = $this->id;

        $ids = array_merge($ids, $this->getAllChildCategoryIds($this->childs()));

        return BlogArticles::query()
            ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
            ->where('blog_article_translations.lang', Langs::getDefaultLangCode())
            ->where('blog_articles.status', BlogArticles::STATUS_ACTIVE)
            ->whereIn('blog_articles.category_id', $ids)
            ->select([
                'blog_articles.*',
                'blog_article_translations.name'
            ])
            ->get();
    }

    /**
     * Подсчет количества категорий с учетом всех подкатегорий
     * @return int
     */
    public function countSubCategories(): int
    {
        $self  = $this;
        $count = 0;
        $count += $this->childs()->count() +
            $self->childs()->get()
                ->sum(function (Category $category) {
                    return $category->countSubCategories();
                });
        return $count;
    }

    public function setting()
    {
        return $this->hasOne(SideCategorySettings::class,'category_id','id');
    }
}
