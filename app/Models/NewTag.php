<?php

namespace App\Models;

use App\Models\Translations\NewTagTranslation;
use Astrotomic\Translatable\Translatable;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewTag extends Model
{
    use HasFactory;

    use Sluggable;
    use Translatable;
    use SluggableScopeHelpers;

    protected $table = 'new_tags';

    public $translationModel = 'App\Models\Translations\NewTagTranslation';

    public $translatedAttributes = [
        'name',
        'description_short',
        'h2',
        'description_hide',
        'description',
        'meta_title',
        'meta_description'
    ];

    protected $fillable = [
        'slug'
    ];

    public function getDefaultTitleAttribute()
    {
        return $this->translateOrDefault()->name;
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'defaultTitle'
            ]
        ];
    }

    /* Есть ли языковая версия */
    public function hasLang($lang){
        return NewTagTranslation::query()
            ->where('new_tag_id',$this->id)
            ->where('lang',$lang)
            ->where('name','<>','')
            ->exists();
    }
}
