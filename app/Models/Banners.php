<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Banners
 * @package App\Models
 *
 * @property BannerItems[] $items
 */
class Banners extends Model
{
    use HasFactory;

    protected $table = 'banners';

    protected $fillable = [
        'name',
        'type',
        'status',
        'img_desktop_size',
        'img_desktop2_size',
        'img_desktop3_size',
        'img_tablet_size',
        'img_mob_size'
    ];

    const STATUS_NOT_ACTIVE = false;
    const STATUS_ACTIVE     = true;

    const TYPE_DEFAULT = 1;
    const TYPE_NATIVE  = 3;
    const TYPE_2_IMG   = 4;

    public static function getStatuses(): array
    {
        return [
            self::STATUS_NOT_ACTIVE => [
                'title'    => 'Не активный',
                'bg_color' => '#ff3838',
                'color'    => '#fdfdfd'
            ],
            self::STATUS_ACTIVE     => [
                'title'    => 'Активный',
                'bg_color' => '#49cc00',
                'color'    => 'white'
            ]
        ];
    }

    public function showStatus()
    {
        return '<span class="badge" style="color: ' . self::getStatuses()[$this->status]["color"] . '; background-color: ' . self::getStatuses()[$this->status]["bg_color"] . '">' . self::getStatuses()[$this->status]["title"] . '</span>';
    }

    public function scopeActive($query)
    {
        return $query->where('status', self::STATUS_ACTIVE);
    }

    public static function getTypes(): array
    {
        return [
            self::TYPE_DEFAULT => 'Изображение и ссылка',
            self::TYPE_NATIVE  => 'С текстом и кнопкой',
            self::TYPE_2_IMG   => 'Подложка'
        ];
    }

    public function items()
    {
        return $this->hasMany(BannerItems::class,'banners_id','id');
    }

    public function itemsWithTrans($lang)
    {
        return $this->hasMany(BannerItems::class,'banners_id','id')
            ->translatedIn($lang)
            ->with('translations');
    }
}
