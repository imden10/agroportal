<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RssCategoryTranslations extends Model
{
    use HasFactory;

    protected $table = 'rss_category_translations';

    public $timestamps = false;
}
