<?php

namespace App\Service;

use App\Models\BlogArticles;
use App\Models\Category;
use App\Models\Langs;
use App\Models\Translations\CategoryTranslation;
use App\Modules\Setting\Setting;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;


class Rss
{
    private $langs;
    private $defaultLangCode;

    public function __construct()
    {
        $this->langs = Langs::getLangCodes();
        $this->defaultLangCode = Langs::getDefaultLangCode();
    }

    public function generate()
    {
        $siteLogo = get_image_uri(app(Setting::class)->get('default_og_image', \App\Models\Langs::getDefaultLangCode()));

        $data = [
            [
                'title' => 'Все материалы',
                'file_name' => 'type_all',
                'agro_category_id' => null,
                'is_replace_category_name' => true,
                'children' => []
            ],
            [
                'title' => 'Блоги',
                'file_name' => 'type_blogs',
                'agro_category_id' => 444,
                'is_replace_category_name' => true,
                'children' => []
            ],
            [
                'title' => 'Публикации',
                'file_name' => 'type_pubs',
                'agro_category_id' => 436,
                'is_replace_category_name' => true,
                'children' => []
            ],
            [
                'title' => 'Новости',
                'file_name' => 'type_news',
                'agro_category_id' => 416,
                'is_replace_category_name' => true,
                'children' => [416,444,436,9999,9911]
            ],
            [
                'title' => 'AgroCheck',
                'file_name' => 'type_agrocheck',
                'agro_category_id' => 9999,
                'is_replace_category_name' => true,
                'children' => []
            ],
            [
                'title' => 'Проєкти',
                'file_name' => 'type_proyekti',
                'agro_category_id' => 9911,
                'is_replace_category_name' => true,
                'children' => []
            ],
            [
                'title' => 'Новости',
                'file_name' => 'type-news',
                'agro_category_id' => 416,
                'is_replace_category_name' => false,
                'children' => []
            ],
        ];

        echo "Start " . PHP_EOL;

        Log::info('RSS generate start');

        foreach ($this->langs as $lang){
            if(! file_exists(public_path() . '/rss_files/' . $lang)){
                mkdir(public_path() . '/rss_files/' . $lang,0777);
            }

            foreach ($data as $d){
                echo "Start" . $d['title'] . PHP_EOL;
                $category = null;

                $flag = false;

                if($d['agro_category_id']){
                    if(count($d['children']) == 0){
                        $category = Category::query()
                            ->leftJoin('rss_category_translations','rss_category_translations.category_id', '=','category.rss_category_id')
                            ->leftJoin('category_translations','category_translations.category_id', '=','category.id')
                            ->where('rss_category_translations.lang',$lang)
                            ->where('category_translations.lang',$lang)
                            ->select([
                                'category.*',
                                'rss_category_translations.name AS categoryTitle',
                                'category_translations.title AS origTitle'
                            ])
                            ->where('category.agro_id',$d['agro_category_id'])
                            ->first();

                        if($category){
                            $flag = true;
                            $countSubCategories = $category->countSubCategories();

                            if($countSubCategories == 0){
                                $catIds = [$category->id];
                            } else {
                                $catIds = Category::query()->where('parent_id',$category->id)->pluck('id')->toArray();
                            }
                            $model = BlogArticles::query()
                                ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id','=','blog_articles.id')
                                ->leftJoin('category','category.id','=','blog_articles.category_id')
                                ->where('blog_article_translations.lang',$lang)
                                ->select([
                                    'blog_articles.*',
                                    'blog_article_translations.name as itemName',
                                    'blog_article_translations.text as itemText',
                                    'blog_article_translations.annot as itemAnnot',
                                    'category.path'
                                ])
                                ->active()
                                ->whereIn('category_id',$catIds)
                                ->orderBy('public_date','desc')
                                ->limit(10)
                                ->get();
                        }
                    } else {
                        $allCatIds = [];

                        foreach ($d['children'] as $child){
                            $mainCategory = Category::query()
                                ->where('category.agro_id',$child)
                                ->first();

                            if($mainCategory){
                                $countSubCategories = $mainCategory->countSubCategories();

                                if($countSubCategories == 0){
                                    $allCatIds[] = $mainCategory->id;
                                } else {
                                    $allCatIds = array_merge($allCatIds,Category::query()->where('parent_id',$mainCategory->id)->pluck('id')->toArray());
                                }
                            }
                        }
                        $model = BlogArticles::query()
                            ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id','=','blog_articles.id')
                            ->leftJoin('category','category.id','=','blog_articles.category_id')
                            ->where('blog_article_translations.lang',$lang)
                            ->whereIn('blog_articles.category_id',$allCatIds)
                            ->select([
                                'blog_articles.*',
                                'blog_article_translations.name as itemName',
                                'blog_article_translations.text as itemText',
                                'blog_article_translations.annot as itemAnnot',
                                'category.path'
                            ])
                            ->active()
                            ->orderBy('public_date','desc')
                            ->limit(10)
                            ->get();
                    }
                } else {
                    $model = BlogArticles::query()
                        ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id','=','blog_articles.id')
                        ->leftJoin('category','category.id','=','blog_articles.category_id')
                        ->where('blog_article_translations.lang',$lang)
                        ->select([
                            'blog_articles.*',
                            'blog_article_translations.name as itemName',
                            'blog_article_translations.text as itemText',
                            'blog_article_translations.annot as itemAnnot',
                            'category.path'
                        ])
                        ->active()
                        ->orderBy('public_date','desc')
                        ->limit(10)
                        ->get();
                }

                $xml = '<rss xmlns:yandex="http://news.yandex.ru" xmlns:media="http://search.yahoo.com/mrss/" xmlns:g="http://base.google.com/ns/1.0" version="2.0">'.PHP_EOL.
                    '<channel>'.PHP_EOL;

                $xml .= '<image>'.PHP_EOL;
                $xml .= '<url>'.$siteLogo.'</url>'.PHP_EOL;
                $xml .= '<title>'.env('APP_NAME');

                if($category){
                    if($d['is_replace_category_name']){
                        $xml .= ' - ' . $category->categoryTitle;
                    } else {
                        $xml .= ' - ' . $category->origTitle;
                    }
                }

                $xml .= '</title>' . PHP_EOL;

                $xml .= '<link>'.env('APP_URL').'</link>'.PHP_EOL;
                $xml .= '</image>'.PHP_EOL;

                $xml .= '<title>'.env('APP_NAME');

                if($category){
                    if($d['is_replace_category_name']){
                        $xml .= ' - ' . $category->categoryTitle;
                    } else {
                        $xml .= ' - ' . $category->origTitle;
                    }
                }

                $xml .= '</title>' . PHP_EOL;

                $xml .= '<link>'.env('APP_URL').'</link>'.PHP_EOL;

                foreach ($model as $item){
                    echo $item->slug . PHP_EOL;

                    if(! $flag){
                        $cat = Category::query()
                            ->leftJoin('rss_category_translations','rss_category_translations.category_id', '=','category.rss_category_id')
                            ->leftJoin('category_translations','category_translations.category_id', '=','category.id')
                            ->where('rss_category_translations.lang',$lang)
                            ->where('category_translations.lang',$lang)
                            ->select([
                                'category.*',
                                'rss_category_translations.name AS categoryTitle',
                                'category_translations.title AS origTitle'
                            ])
                            ->where('category.id',$item->category_id)
                            ->first();
                    }

                    $xml .= $this->item($d,$flag ? $category : $cat, $item, $lang);
                }


                $xml .= '</channel>'.PHP_EOL;
                $xml .= '</rss>';

                file_put_contents(public_path() . '/rss_files/' . $lang . '/' . $d['file_name'], $xml);

                try {
                    chmod(public_path() . '/rss_files/' . $lang . '/' . $d['file_name'], 0665);
                } catch (\Exception $e) {
                    Log::info('Can not chmod file ' . $d['file_name']);
                }

            }
        }

        Log::info('RSS generate end');
    }

    public function item($data, $category, $item, $lang)
    {
        $xml = '<item>' . PHP_EOL;
        $xml .= '<title><![CDATA[ '.$item->itemName.' ]]></title>' . PHP_EOL;

        $url = '/' . $item->path . '/' . $item->slug;

        if($this->defaultLangCode != $lang){
            $url = '/' . $lang . $url;
        }

        $url = str_replace('//','/',$url);
        $link = env('APP_URL') . $url;
        $xml .= '<link>'.$link.'</link>' . PHP_EOL;

        if(isset($category)){
            if($data['is_replace_category_name']){
                $xml .= '<category>'.$category->categoryTitle.'</category>';
            } else {
                $xml .= '<category>'.$category->origTitle.'</category>';
            }

        }

        $imgData = $this->getImgData($item);

        $xml .= '<enclosure url="'.$imgData['url'].'" type="'.$imgData['mime'].'" length="'.$imgData['length'].'"/>';
        $xml .= '<g:image_link>'.$imgData['url'].'</g:image_link>';

        $date = Carbon::parse($item->public_date);
        $xml .= '<pubDate>'.$date->toRssString().'</pubDate>';

        $xml .= '<description><![CDATA[ '.strip_tags($item->itemAnnot).' ]]></description>';

        $xml .= '<yandex:full-text><![CDATA[ '.$item->itemText.' ]]></yandex:full-text>';
        $xml .= '<full-text><![CDATA[ '.$item->itemText.' ]]></full-text>';

        $xml .= '<guid>'.$link.'</guid>' . PHP_EOL;

        $xml .= '</item>' . PHP_EOL;

        return $xml;
    }

    public function getImgData($item)
    {
        $imgData = [];
        $url = get_image_uri($item->image);

        $imgData['url'] = $url;

        if($item->image){
            $imgPath = storage_path() . '/app/public/media' . $item->image;
        } else {
            $imgPath = public_path() . '/images/no-image.png';
        }

        try {
            //extremely slow, downloads image
            //$imgInfo = getimagesize($url);
            // $imgData['mime'] = $imgInfo['mime'];

            //fast
            $imgData['mime'] = mime_content_type(storage_path() . '/app/public/media' . $item->image);
        } catch (\Exception $e){
            $imgData['mime'] = '';
        }

        try {
            $imgData['length'] = filesize($imgPath);
        } catch (\Exception $e){
            $imgData['length'] = '0';
        }

        return $imgData;
    }
}
