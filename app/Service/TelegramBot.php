<?php

namespace App\Service;

class TelegramBot
{
    /**
     * @var string
     */
    protected $token;

    /**
     * @var string
     */
    protected $chatId;

    function __construct()
    {
        $this->token  = env('TELEGRAM_BOT_TOKEN');
        $this->chatId = env('TELEGRAM_BOT_CHAT_ID');
    }

    /**
     * @param $message
     * @return mixed
     */
    public function sendMessage($message)
    {
        $curlProps = [
            CURLOPT_URL            => 'https://api.telegram.org/bot' . $this->token . '/sendMessage',
            CURLOPT_POST           => TRUE,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_TIMEOUT        => 10,
            CURLOPT_POSTFIELDS     => array(
                'chat_id'    => $this->chatId,
                'text'       => $message
            ),
        ];
        $curl      = curl_init();

        curl_setopt_array($curl, $curlProps);
        $response = curl_exec($curl);

        $error = curl_error($curl);

        curl_close($curl);

        if ($error) {
            //
        }

        return json_decode($response, true);
    }

}
