<?php


namespace App\Service;


use App\Models\BannerItems;
use App\Models\Banners;
use App\Models\BlogArticles;
use App\Models\Bloggers;
use App\Models\Category;
use App\Models\Landing;
use App\Models\Menu;
use App\Models\NewTag;
use App\Models\Translations\BloggerTranslation;
use App\Modules\Setting\Setting;
use App\Modules\Widgets\Models\Widget;
use App\Traits\Statistics;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Adapter
{
    use Statistics;

    private static function cmp($a, $b)
    {
        return $a['position'] - $b['position'];
    }

    /**
     * @param $model
     * @param $lang
     * @return array
     */
    public function prepareModelResults($model, $lang): array
    {
            $translate = $model->getTranslation($lang);

            $constructor = $this->getConstructorData($model,$lang,$translate->constructor->data);

            $model = $model->toArray();

            if (isset($model['is_blog']) && $model['is_blog']) {
                $blogger = Bloggers::query()
                    ->leftJoin('blogger_translations','blogger_translations.bloggers_id','=','bloggers.id')
                    ->where('blogger_translations.lang',$lang)
                    ->select([
                        'bloggers.*',
                        'blogger_translations.name AS transName'
                    ])
                    ->where('bloggers.id', $model['blogger_id'])
                    ->first();

                if ($blogger) {
                    $url = '/authors/' . $blogger->slug;

                    if ($lang != config('translatable.locale')) {
                        $url = '/' . $lang . $url;
                    }

                    $model['blogger_name'] = $blogger->transName;
                    $model['blogger_slug'] = $blogger->slug;
                    $model['blogger_url'] = $url;
                }
            }

            if (isset($model['path'])) {
                $url = '/' . $model['path'] . '/' . $model['slug'];
                $cat_url = '/' . $model['path'];

                if ($lang !== 'uk') {
                    $url = '/' . $lang . $url;
                    $cat_url = '/' . $lang . $cat_url;
                }

                $model['category_url'] = $cat_url;
                $model['url'] = $url;
            }

            unset($model['translations']);
            unset($translate['constructor']);

            if (isset($translate['annot'])) {
                $translate['annot'] = str_replace('&nbsp;',' ',$translate['annot']);
            }

            $tagsObj = [];

            if (isset($translate['tags'])) {
                try {
                    $tags = explode(',', $translate['tags']);

                    if (is_array($tags) && count($tags)) {

                        foreach ($tags as $tag) {
                            $url = '/tags/' . $tag;

                            if ($lang !== config('translatable.locale')) {
                                $url = '/' . $lang . $url;
                            }

                            $tagsObj[] = [
                                'name' => $tag,
                                'url' => $url
                            ];
                        }
                    }

                    $translate['tags_obj'] = $tagsObj;
                } catch (\Exception $e) {
                }
            }

            if (isset($model['public_date'])) {
                $pd = Carbon::parse($model['public_date']);
                if ($lang == 'uk') {
                    $pd->setLocale('uk_UA');
                } elseif ($lang == 'ru') {
                    $pd->setLocale('ru_RU');
                }
                $model['public_date'] = $pd->translatedFormat('d F Y, H:i');
            }

            if (isset($translate['landing_link']) && !is_null($translate['landing_link'])) {
                $landing = Landing::query()->where('id',$translate['landing_link'])->first();

                if($landing){
                    $landUrl = '/project/' . $landing->slug;

                    if ($lang !== config('translatable.locale')) {
                        $landUrl = '/' . $lang . $landUrl;
                    }

                    $model['landing_link'] = $landUrl;
                }
            } else {
                $model['landing_link'] = null;
            }

            if(isset($model['gallery']) && is_array($model['gallery']) && count($model['gallery'])){
                foreach ($model['gallery'] as $gKey => $galleryItem){
                    $title = '';

                    try {
                        $name = json_decode($galleryItem['title'],true);
                        $title = $name[$lang];
                    } catch (\Exception $e){

                    }

                    $model['gallery'][$gKey]['title'] = $title;
                }
            }

            return [
                'model'       => $model,
                'translate'   => $translate,
                'constructor' => $constructor
            ];


        return $data;
    }

    private function getConstructorData($model, $lang, $constructor = null)
    {
        try {
            if(!$constructor){
                $constructor = $model->getTranslation($lang)->constructor->data;
            }
        } catch (\Throwable $e) {
            $constructor = null;
        }


        if (!is_null($constructor)) {
            if (!is_array(current($constructor)))
                $constructor = [];
        } else {
            $constructor = [];
        }

        if (count($constructor)) {
            usort($constructor, array('App\Service\Adapter', 'cmp'));

            $allWidgets = cache()->remember('allWidgets_' . $lang,(60*60*24), function() use($lang){
                return Widget::query()->where('lang', $lang)->pluck('data', 'id')->toArray();
            });

            $allWidgetsName = cache()->remember('allWidgetsName_' . $lang,(60*60*24), function() use($lang){
                return Widget::query()->where('lang', $lang)->pluck('instance', 'id')->toArray();
            });

            foreach ($constructor as $key => $item) {
                if ($item['component'] === 'widget') {
                    if (isset($allWidgetsName[$item['content']['widget']])) {
                        $constructor[$key]['content']['instance'] = $allWidgetsName[$item['content']['widget']];
                        $widgetClassName                          = config('widgets.widgets')[$allWidgetsName[$item['content']['widget']]];
                        $widgetClass                              = app($widgetClassName);

                        if (method_exists($widgetClass, 'adapter')) {
                            $constructor[$key]['content']['data'] = $widgetClass->adapter($allWidgets[$item['content']['widget']], $lang);
                        } else {
                            $constructor[$key]['content']['data'] = $allWidgets[$item['content']['widget']];
                        }

                        $constructor[$key]['component'] = $constructor[$key]['content']['instance'];
                        $constructor[$key]['content']   = $constructor[$key]['content']['data'];

                        if($constructor[$key]['component'] === 'footer-landing'){
                            $icons = app(Setting::class)->get('links');

                            try {
                                $icons = json_decode($icons,true);

                                if(count($icons)){
                                    $ic = [];
                                    foreach ($icons as $icon){
                                        $ic[] = $icon;
                                    }

                                    $constructor[$key]['content']['links'] = $ic;
                                }

                            } catch (\Exception $e){}
                        }
                    } else {
                        unset($constructor[$key]);
                    }
                } elseif ($item['component'] === 'see-also') {
                    $list = [];
                    $ids  = [];

                    if(isset($item['content']['list']) && count($item['content']['list'])){
                        foreach ($item['content']['list'] as $l) {
                            $ids[] = (int)$l['article_id'];
                        }
                    }

                    if (count($ids)) {
                        $articles = BlogArticles::query()
                            ->leftJoin('category', 'category.id', '=', 'blog_articles.category_id')
                            ->whereIn('blog_articles.id', $ids)
                            ->active()
                            ->orderBy('blog_articles.public_date', 'desc')
                            ->select([
                                'blog_articles.*',
                                'category.path as path'
                            ])
                            ->get()
                            ->toArray();

                        if (count($articles)) {
                            foreach ($articles as $pageKey => $article) {
                                $url = '/' . $article['path'] . '/' . $article['slug'];

                                if ($lang !== config('translatable.locale')) {
                                    $url = '/' . $lang . $url;
                                }

                                $list[$pageKey]['url'] = $url;
                                $list[$pageKey]['image_full'] = get_image_uri($article['image']);
                                $list[$pageKey]['image'] = $article['image'];

                                foreach ($article['translations'] as $trans) {
                                    if ($trans['lang'] === $lang) {
                                        $list[$pageKey]['title'] = $trans['name'];
                                        $list[$pageKey]['text'] = $trans['annot'];
                                    }
                                }
                            }

                            $constructor[$key]['content']['list'] = $list;
                        }
                    }
                } elseif ($item['component'] === 'see-also-article') {
                    $list = [];
                    $ids  = [];

                    if(isset($item['content']['list']) && count($item['content']['list'])){
                        foreach ($item['content']['list'] as $l) {
                            $ids[] = (int)$l['article_id'];
                        }
                    }

                    if (count($ids)) {
                        $articles = BlogArticles::query()
                            ->leftJoin('category', 'category.id', '=', 'blog_articles.category_id')
                            ->whereIn('blog_articles.id', $ids)
                            ->active()
                            ->orderBy('blog_articles.public_date', 'desc')
                            ->select([
                                'blog_articles.*',
                                'category.path as path'
                            ])
                            ->get()
                            ->toArray();

                        if (count($articles)) {
                            foreach ($articles as $pageKey => $article) {
                                $url = '/' . $article['path'] . '/' . $article['slug'];

                                if ($lang !== config('translatable.locale')) {
                                    $url = '/' . $lang . $url;
                                }

                                $list[$pageKey]['url'] = $url;
                                $list[$pageKey]['image'] = $article['image'];
                                $pd = Carbon::parse($article['public_date']);
                                if ($lang == 'uk') {
                                    $pd->setLocale('uk_UA');
                                } elseif ($lang == 'ru') {
                                    $pd->setLocale('ru_RU');
                                }
                                $list[$pageKey]['public_date'] = $pd->translatedFormat('d F Y, H:i');


                                foreach ($article['translations'] as $trans) {
                                    if ($trans['lang'] === $lang) {
                                        $list[$pageKey]['title'] = $trans['name'];
                                    }
                                }
                            }

                            $constructor[$key]['content']['list'] = $list;
                        }
                    }
                } elseif ($item['component'] === 'see-also-landing') {
                    $list = [];

                    if(isset($item['content']['list']) && count($item['content']['list'])){
                        foreach ($item['content']['list'] as $key2 => $l) {

                            if(isset($l['item_id'])){
                                $article = Landing::query()
                                    ->where('id', $l['item_id'])
                                    ->active()
                                    ->first();
                            }

                            $list[$key2] = $l;

                            if(isset($article)){
                                $url = '/' . $article->slug;

                                if ($lang !== config('translatable.locale')) {
                                    $url = '/' . $lang . $url;
                                }

                                $list[$key2]['url'] = $url;
                            }
                        }

                        $constructor[$key]['content']['list'] = $list;
                    }
                } elseif ($item['component'] === 'see-also-landing-for-site') {
                    $list = [];

                    if(isset($item['content']['list']) && count($item['content']['list'])){
                        foreach ($item['content']['list'] as $key2 => $l) {

                            if(isset($l['item_id'])){
                                $article = Landing::query()
                                    ->where('id', $l['item_id'])
                                    ->active()
                                    ->first();
                            }

                            $list[$key2] = $l;

                            if(isset($article)){
                                $url = '/project/' . $article->slug;

                                if ($lang !== config('translatable.locale')) {
                                    $url = '/' . $lang . $url;
                                }

                                $list[$key2]['url'] = $url;
                            }
                        }

                        $constructor[$key]['content']['list'] = $list;
                    }
                }

                if(isset($constructor[$key]['content']['anker_title'])){
                    $constructor[$key]['content']['anker_id'] = str_slug($constructor[$key]['content']['anker_title'] , "_");
                }
            }
        }

        return array_values($constructor);
    }

    public function renderConstructorHTML(&$model)
    {
        foreach ($model->translations as &$item){
            $constructor = $this->getConstructorData($model,$item->lang);
            $html = view('front.site.layouts.includes.constructor',['constructor' => $constructor])->render();
            $item->constructor_html = $html;
            $item->save();
        }
    }

    public function prepareAsideResults($setting, $lang)
    {
        $res = [];

        if (isset($setting['cat1']['status'])) {
            $category = cache()->remember('getCategoryById_' . $setting['cat1']['category_id'] . '_' . $lang, (60 * 60 * 24), function () use ($setting, $lang) {
                return Category::query()
                    ->leftJoin('category_translations','category_translations.category_id','=','category.id')
                    ->where('category_translations.lang',$lang)
                    ->select([
                        'category.*',
                        'category_translations.title AS categoryName',
                        'category_translations.meta_title AS categoryMetaTitle',
                        'category_translations.meta_description AS categoryMetaDescription',
                    ])
                    ->where('category.id', $setting['cat1']['category_id'])
                    ->first();
            });

            $ids = [$category->id];

            if (!$category->parent_id) {
                try {
                    $catChildren = json_decode($category->children_ids,true);
                } catch (\Exception $e){
                    $catChildren = [];
                }

                if ($catChildren && count($catChildren)) {
                    $ids = array_merge($ids, $catChildren);
                }
            }

            $articles = BlogArticles::query()
                ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
                ->where('blog_article_translations.lang', $lang)
                ->whereNotNull('blog_article_translations.name')
                ->whereIn('blog_articles.category_id',$ids)
                ->select([
                    'blog_articles.*',
                    'blog_article_translations.name AS transName',
                    'blog_article_translations.landing_link',
                ])
                ->active()
                ->orderBy('blog_articles.created_at', 'desc')
                ->limit($setting['cat1']['count'])
                ->get();

            $data = [
                'category' => [
                    'name' => $category->categoryName,
                    'url' => (($lang !== config('translatable.locale')) ? '/' . $lang : '') . '/' . $category->path,
                ],
                'items'    => []
            ];

            if (count($articles)) {
                foreach ($articles as $key => $article) {
                    $data['items'][$key]['url'] = '/' . $category->path . '/' . $article->slug;

                    if ($lang !== config('translatable.locale')) {
                        $data['items'][$key]['url'] = '/' . $lang . $data['items'][$key]['url'];
                    }


                    $data['items'][$key]['slug']        = $article->slug;
                    $data['items'][$key]['image']       = $article->image;

                    $pd = Carbon::parse($article->public_date);
                    if ($lang == 'uk') {
                        $pd->setLocale('uk_UA');
                    } elseif ($lang == 'ru') {
                        $pd->setLocale('ru_RU');
                    }
                    $data['items'][$key]['public_date'] = $pd->translatedFormat('d F Y, H:i');
                    $data['items'][$key]['views']       = $article->views;
                    $data['items'][$key]['name']        = $article->transName;

                    if (isset($article->landing_link)) {
                        $landing = Landing::query()->where('id',$article->landing_link)->first();

                        if($landing){
                            $landUrl = '/project/' . $landing->slug;

                            if ($lang !== config('translatable.locale')) {
                                $landUrl = '/' . $lang . $landUrl;
                            }

                            $data['items'][$key]['landing_link'] = $landUrl;
                        }
                    }
                }

                $res['cat1'] = $data;
            }
        }

        if (isset($setting['news']['status'])) {
            $catNewsId = 60;

            $catIds = cache()->remember('cat_news_id' . $lang, (60 * 60 * 60), function () use ($catNewsId) {
                return Category::query()->where('parent_id', $catNewsId)->pluck('id')->toArray();
            });

            $articles = BlogArticles::query()
                ->leftJoin('category', 'category.id', '=', 'blog_articles.category_id')
                ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
                ->where('blog_article_translations.lang', $lang)
                ->whereNotNull('blog_article_translations.name')
                ->whereIn('blog_articles.category_id', $catIds)
                ->active()
                ->orderBy('blog_articles.public_date', 'desc')
                ->select([
                    'blog_articles.*',
                    'category.path as path',
                    'blog_article_translations.name AS transName',
                    'blog_article_translations.labels AS transLabels',
                    'blog_article_translations.landing_link',
                ])
                ->limit($setting['news']['count'])
                ->get();

            if (count($articles)) {
                $data = [];
                foreach ($articles as $key => $article) {
                    $data[$key]['url'] = '/' . $article->path . '/' . $article->slug;

                    if ($lang !== config('translatable.locale')) {
                        $data[$key]['url'] = '/' . $lang . $data[$key]['url'];
                    }

                    $data[$key]['type']        = 'article';
                    $data[$key]['slug']        = $article->slug;
                    $data[$key]['image']       = $article->image;
                    $data[$key]['public_date']  = $article->public_date;

                    $pd = Carbon::parse($article->public_date);
                    if ($lang == 'uk') {
                        $pd->setLocale('uk_UA');
                    } elseif ($lang == 'ru') {
                        $pd->setLocale('ru_RU');
                    }
                    $data[$key]['date'] = $pd->translatedFormat('d F Y, H:i');

                    $data[$key]['views']       = $article->views;
                    $data[$key]['interview']   = $article->interview;

                    $data[$key]['name'] = $article->transName;

                    if (isset($article->transLabels) && $article->transLabels) {
                        $data[$key]['labels'] = explode(',', $article->transLabels);
                    } else {
                        $data[$key]['labels'] = [];
                    }

                    if (isset($article->landing_link)) {
                        $landing = Landing::query()->where('id',$article->landing_link)->first();

                        if($landing){
                            $landUrl = '/project/' . $landing->slug;

                            if ($lang !== config('translatable.locale')) {
                                $landUrl = '/' . $lang . $landUrl;
                            }

                            $data[$key]['landing_link'] = $landUrl;
                        }
                    }
                }

                //                $res['news'] = $data;
                $pDate = '';

                $newData = [];


                function mb_ucfirst($str, $encoding = 'UTF-8')
                {
                    $str = mb_ereg_replace('^[\ ]+', '', $str);
                    $str = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding) .
                        mb_substr($str, 1, mb_strlen($str), $encoding);
                    return $str;
                }

                foreach ($data as $item) {
                    $date = substr($item['public_date'], 0, 10);
                    if ($pDate != $date) {
                        $cDate = Carbon::parse($date);
                        if ($lang == 'uk') {
                            $cDate->setLocale('uk_UA');
                        } elseif ($lang == 'ru') {
                            $cDate->setLocale('ru_RU');
                        }

                        $day = $cDate->translatedFormat('d');
                        $str = $cDate->translatedFormat('d F');
                        $str = str_replace($day, '', $str);
                        $str = trim($str);

                        $newData[] = [
                            'type' => 'date',
                            'date' => [
                                'd' => $cDate->translatedFormat('d'),
                                'm' => $str,
                                'w' => mb_ucfirst($cDate->translatedFormat('l'))
                            ]
                        ];
                        $pDate = $date;
                    }

                    $newData[] = $item;
                }


                $res['news']['latest'] = $newData;
            }

            $articlesInterview = BlogArticles::query()
                ->leftJoin('category', 'category.id', '=', 'blog_articles.category_id')
                ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
                ->select([
                    'blog_articles.*',
                    'category.path as path',
                    'blog_article_translations.name AS transName',
                    'blog_article_translations.labels AS transLabels',
                    'blog_article_translations.landing_link',
                ])
                ->where('blog_article_translations.lang', $lang)
                ->where('blog_articles.interview', 1)
                ->whereNotNull('blog_article_translations.name')
                ->whereIn('blog_articles.category_id', $catIds)
                ->active()
                ->orderBy('blog_articles.public_date', 'desc')
                ->limit($setting['news']['count'])
                ->get();

            if (count($articlesInterview)) {
                $data = [];
                foreach ($articlesInterview as $key => $article) {
                    $data[$key]['type']        = 'article';
                    $data[$key]['url'] = '/' . $article->path . '/' . $article->slug;

                    if ($lang !== config('translatable.locale')) {
                        $data[$key]['url'] = '/' . $lang . $data[$key]['url'];
                    }

                    $data[$key]['slug']        = $article->slug;
                    $data[$key]['image']       = $article->image;
                    $data[$key]['public_date'] = $article->public_date;

                    $pd = Carbon::parse($article->public_date);
                    if ($lang == 'uk') {
                        $pd->setLocale('uk_UA');
                    } elseif ($lang == 'ru') {
                        $pd->setLocale('ru_RU');
                    }
                    $data[$key]['date'] = $pd->translatedFormat('d F Y, H:i');

                    $data[$key]['views']     = $article->views;
                    $data[$key]['interview'] = $article->interview;
                    $data[$key]['name']      = $article->transName;

                    if (isset($article->transLabels) && $article->transLabels) {
                        $data[$key]['labels'] = explode(',', $article->transLabels);
                    } else {
                        $data[$key]['labels'] = [];
                    }

                    if (isset($article->landing_link)) {
                        $landing = Landing::query()->where('id',$article->landing_link)->first();

                        if($landing){
                            $landUrl = '/project/' . $landing->slug;

                            if ($lang !== config('translatable.locale')) {
                                $landUrl = '/' . $lang . $landUrl;
                            }

                            $data[$key]['landing_link'] = $landUrl;
                        }
                    }
                }

                $pDate = '';

                $newData = [];

                foreach ($data as $item) {
                    $date = substr($item['public_date'], 0, 10);
                    if ($pDate != $date) {
                        $cDate = Carbon::parse($date);
                        if ($lang == 'uk') {
                            $cDate->setLocale('uk_UA');
                        } elseif ($lang == 'ru') {
                            $cDate->setLocale('ru_RU');
                        }

                        $day = $cDate->translatedFormat('d');
                        $str = $cDate->translatedFormat('d F');
                        $str = str_replace($day, '', $str);
                        $str = trim($str);

                        $newData[] = [
                            'type' => 'date',
                            'date' => [
                                'd' => $cDate->translatedFormat('d'),
                                'm' => $str,
                                'w' => mb_ucfirst($cDate->translatedFormat('l'))
                            ]
                        ];
                        $pDate = $date;
                    }

                    $newData[] = $item;
                }

                $res['news']['interview'] = $newData;
            }
        }

        if (isset($setting['cat2']['status'])) {
            $category = cache()->remember('getCategoryById_' . $setting['cat2']['category_id'] . '_' . $lang, (60 * 60 * 24), function () use ($setting, $lang) {
                return Category::query()
                    ->leftJoin('category_translations','category_translations.category_id','=','category.id')
                    ->where('category_translations.lang',$lang)
                    ->select([
                        'category.*',
                        'category_translations.title AS categoryName',
                        'category_translations.meta_title AS categoryMetaTitle',
                        'category_translations.meta_description AS categoryMetaDescription',
                    ])
                    ->where('category.id', $setting['cat2']['category_id'])
                    ->first();
            });

            $ids = [$category->id];

            if (!$category->parent_id) {
                try {
                    $catChildren = json_decode($category->children_ids,true);
                } catch (\Exception $e){
                    $catChildren = [];
                }

                if ($catChildren && count($catChildren)) {
                    $ids = array_merge($ids, $catChildren);
                }
            }

            $articles = BlogArticles::query()
                ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
                ->where('blog_article_translations.lang', $lang)
                ->whereNotNull('blog_article_translations.name')
                ->whereIn('blog_articles.category_id',$ids)
                ->select([
                    'blog_articles.*',
                    'blog_article_translations.name AS transName',
                    'blog_article_translations.landing_link',
                ])
                ->active()
                ->orderBy('blog_articles.created_at', 'desc')
                ->limit($setting['cat2']['count'])
                ->get();

            $data = [
                'category' => [
                    'name' => $category->categoryName,
                    'url' => (($lang !== config('translatable.locale')) ? '/' . $lang : '') . '/' . $category->path,
                ],
                'items'    => []
            ];

            if (count($articles)) {
                foreach ($articles as $key => $article) {
                    $data['items'][$key]['url'] = '/' . $category->path . '/' . $article->slug;

                    if ($lang !== config('translatable.locale')) {
                        $data['items'][$key]['url'] = '/' . $lang . $data['items'][$key]['url'];
                    }

                    $data['items'][$key]['slug']        = $article->slug;
                    $data['items'][$key]['image']       = $article->image;

                    $pd = Carbon::parse($article->public_date);
                    if ($lang == 'uk') {
                        $pd->setLocale('uk_UA');
                    } elseif ($lang == 'ru') {
                        $pd->setLocale('ru_RU');
                    }
                    $data['items'][$key]['public_date'] = $pd->translatedFormat('d F Y, H:i');

                    $data['items'][$key]['views'] = $article->views;
                    $data['items'][$key]['name']  = $article->transName;

                    if (isset($article->landing_link)) {
                        $landing = Landing::query()->where('id',$article->landing_link)->first();

                        if($landing){
                            $landUrl = '/project/' . $landing->slug;

                            if ($lang !== config('translatable.locale')) {
                                $landUrl = '/' . $lang . $landUrl;
                            }

                            $data['items'][$key]['landing_link'] = $landUrl;
                        }
                    }
                }

                $res['cat2'] = $data;
            }
        }

        if (isset($setting['blog']['status'])) {
            $articles = BlogArticles::query()
                ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
                ->leftJoin('bloggers', 'bloggers.id', '=', 'blog_articles.blogger_id')
                ->leftJoin('blogger_translations', 'blogger_translations.bloggers_id', '=', 'bloggers.id')
                ->where([
                    ['blog_article_translations.lang', $lang],
                    ['blogger_translations.lang', $lang],
                    ['blog_articles.is_blog', 1],
                ])
                ->select([
                    'blog_articles.*',
                    'blog_article_translations.name AS transName',
                    'blog_article_translations.landing_link',
                    'bloggers.slug AS blogger_slug',
                    'bloggers.image_thumbnail AS blogger_image_thumbnail',
                    'blogger_translations.name AS blogger_trans_name',
                    'blogger_translations.text_for_main AS blogger_trans_text',
                ])
                ->whereNotNull('blog_article_translations.name')
                ->active()
                ->orderBy('blog_articles.public_date', 'desc')
                ->limit($setting['blog']['count'])
                ->get();

            if (count($articles)) {
                $data = [];
                foreach ($articles as $key => $article) {
                    $data[$key]['url'] = '/blogs/' . $article->slug;

                    if ($lang !== config('translatable.locale')) {
                        $data[$key]['url'] = '/' . $lang . $data[$key]['url'];
                    }

                    $data[$key]['slug']        = $article->slug;

                    $pd = Carbon::parse($article->public_date);
                    if ($lang == 'uk') {
                        $pd->setLocale('uk_UA');
                    } elseif ($lang == 'ru') {
                        $pd->setLocale('ru_RU');
                    }
                    $data[$key]['public_date'] = $pd->translatedFormat('d F Y, H:i');

                    $data[$key]['views']       = $article->views;
                    $data[$key]['name']        = $article->transName;


                    $data[$key]['blogger_url'] = '/authors/' . $article->blogger_slug;

                    if ($lang !== config('translatable.locale')) {
                        $data[$key]['blogger_url'] = '/' . $lang . $data[$key]['blogger_url'];
                    }

                    $data[$key]['blogger_slug']  = $article->blogger_slug;
                    $data[$key]['blogger_image'] = $article->blogger_image_thumbnail;

                    $data[$key]['blogger_name'] = $article->blogger_trans_name;
                    $data[$key]['blogger_text'] = $article->blogger_trans_text;

                    if (isset($article->landing_link)) {
                        $landing = Landing::query()->where('id',$article->landing_link)->first();

                        if($landing){
                            $landUrl = '/project/' . $landing->slug;

                            if ($lang !== config('translatable.locale')) {
                                $landUrl = '/' . $lang . $landUrl;
                            }

                            $data[$key]['landing_link'] = $landUrl;
                        }
                    }
                }

                $res['blog'] = $data;
            }
        }

        return $res;
    }

    public function prepareMainPageBlocksResults($setting, $lang, $news_count = 10)
    {
        $res = [];

        $res = cache()->remember('home_' . $lang, 240, function () use ($setting, $lang, $news_count, $res) {
            if (isset($news_count)) {
                $catNewsId = 60;
//                            $catNewsId = 93; // local

                $catIds = cache()->remember('cat_news_id' . $lang, (60 * 60 * 60), function () use ($catNewsId) {
                    return Category::query()->where('parent_id', $catNewsId)->pluck('id')->toArray();
                });

                $articles = BlogArticles::query()
                    ->leftJoin('category', 'category.id', '=', 'blog_articles.category_id')
                    ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
                    ->select([
                        'blog_articles.*',
                        'category.path as path',
                        'blog_article_translations.name AS transName',
                        'blog_article_translations.labels AS transLabels',
                        'blog_article_translations.landing_link',
                    ])
                    ->where('blog_article_translations.lang', $lang)
                    ->whereNotNull('blog_article_translations.name')
                    ->whereIn('blog_articles.category_id', $catIds)
                    ->active()
                    ->orderBy('blog_articles.public_date', 'desc')
                    ->limit($news_count)
                    ->get();

                if (count($articles)) {
                    $data = [];
                    foreach ($articles as $key => $article) {
                        $data[$key]['type']        = 'article';
                        $data[$key]['url'] = '/' . $article->path . '/' . $article->slug;

                        if ($lang !== config('translatable.locale')) {
                            $data[$key]['url'] = '/' . $lang . $data[$key]['url'];
                        }

                        $data[$key]['slug']        = $article->slug;
                        $data[$key]['image']       = $article->image;
                        $data[$key]['public_date'] = $article->public_date;

                        $pd = Carbon::parse($article->public_date);
                        if ($lang == 'uk') {
                            $pd->setLocale('uk_UA');
                        } elseif ($lang == 'ru') {
                            $pd->setLocale('ru_RU');
                        }
                        $data[$key]['date'] = $pd->translatedFormat('d F Y, H:i');

                        $data[$key]['views']     = $article->views;
                        $data[$key]['interview'] = $article->interview;
                        $data[$key]['name']      = $article->transName;

                        if (isset($article->transLabels) && $article->transLabels) {
                            $data[$key]['labels'] = explode(',', $article->transLabels);
                        } else {
                            $data[$key]['labels'] = [];
                        }

                        if (isset($article->landing_link)) {
                            $landing = Landing::query()->where('id',$article->landing_link)->first();

                            if($landing){
                                $landUrl = '/project/' . $landing->slug;

                                if ($lang !== config('translatable.locale')) {
                                    $landUrl = '/' . $lang . $landUrl;
                                }

                                $data[$key]['landing_link'] = $landUrl;
                            }
                        }
                    }

                    $pDate = '';

                    $newData = [];

                    function mb_ucfirst($str, $encoding = 'UTF-8')
                    {
                        $str = mb_ereg_replace('^[\ ]+', '', $str);
                        $str = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding) .
                            mb_substr($str, 1, mb_strlen($str), $encoding);
                        return $str;
                    }

                    foreach ($data as $item) {
                        $date = substr($item['public_date'], 0, 10);
                        if ($pDate != $date) {
                            $cDate = Carbon::parse($date);
                            if ($lang == 'uk') {
                                $cDate->setLocale('uk_UA');
                            } elseif ($lang == 'ru') {
                                $cDate->setLocale('ru_RU');
                            }

                            $day = $cDate->translatedFormat('d');
                            $str = $cDate->translatedFormat('d F');
                            $str = str_replace($day, '', $str);
                            $str = trim($str);

                            $newData[] = [
                                'type' => 'date',
                                'date' => [
                                    'd' => $cDate->translatedFormat('d'),
                                    'm' => $str,
                                    'w' => mb_ucfirst($cDate->translatedFormat('l'))
                                ]
                            ];
                            $pDate = $date;
                        }

                        $newData[] = $item;
                    }

                    $res['news']['latest'] = $newData;
                }

                $articlesInterview = BlogArticles::query()
                    ->leftJoin('category', 'category.id', '=', 'blog_articles.category_id')
                    ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
                    ->select([
                        'blog_articles.*',
                        'category.path as path',
                        'blog_article_translations.name AS transName',
                        'blog_article_translations.labels AS transLabels',
                        'blog_article_translations.landing_link',
                    ])
                    ->where('blog_article_translations.lang', $lang)
                    ->where('blog_articles.interview', 1)
                    ->whereNotNull('blog_article_translations.name')
                    ->whereIn('blog_articles.category_id', $catIds)
                    ->active()
                    ->orderBy('blog_articles.public_date', 'desc')
                    ->limit($news_count)
                    ->get();

                if (count($articlesInterview)) {
                    $data = [];
                    foreach ($articlesInterview as $key => $article) {
                        $data[$key]['type']        = 'article';
                        $data[$key]['url'] = '/' . $article->path . '/' . $article->slug;

                        if ($lang !== config('translatable.locale')) {
                            $data[$key]['url'] = '/' . $lang . $data[$key]['url'];
                        }

                        $data[$key]['slug']        = $article->slug;
                        $data[$key]['image']       = $article->image;
                        $data[$key]['public_date'] = $article->public_date;

                        $pd = Carbon::parse($article->public_date);
                        if ($lang == 'uk') {
                            $pd->setLocale('uk_UA');
                        } elseif ($lang == 'ru') {
                            $pd->setLocale('ru_RU');
                        }
                        $data[$key]['date'] = $pd->translatedFormat('d F Y, H:i');

                        $data[$key]['views']     = $article->views;
                        $data[$key]['interview'] = $article->interview;
                        $data[$key]['name']      = $article->transName;

                        if (isset($article->transLabels) && $article->transLabels) {
                            $data[$key]['labels'] = explode(',', $article->transLabels);
                        } else {
                            $data[$key]['labels'] = [];
                        }

                        if (isset($article->landing_link)) {
                            $landing = Landing::query()->where('id',$article->landing_link)->first();

                            if($landing){
                                $landUrl = '/project/' . $landing->slug;

                                if ($lang !== config('translatable.locale')) {
                                    $landUrl = '/' . $lang . $landUrl;
                                }

                                $data[$key]['landing_link'] = $landUrl;
                            }
                        }
                    }

                    $pDate = '';

                    $newData = [];

                    foreach ($data as $item) {
                        $date = substr($item['public_date'], 0, 10);
                        if ($pDate != $date) {
                            $cDate = Carbon::parse($date);
                            if ($lang == 'uk') {
                                $cDate->setLocale('uk_UA');
                            } elseif ($lang == 'ru') {
                                $cDate->setLocale('ru_RU');
                            }

                            $day = $cDate->translatedFormat('d');
                            $str = $cDate->translatedFormat('d F');
                            $str = str_replace($day, '', $str);
                            $str = trim($str);

                            $newData[] = [
                                'type' => 'date',
                                'date' => [
                                    'd' => $cDate->translatedFormat('d'),
                                    'm' => $str,
                                    'w' => mb_ucfirst($cDate->translatedFormat('l'))
                                ]
                            ];
                            $pDate = $date;
                        }

                        $newData[] = $item;
                    }

                    $res['news']['interview'] = $newData;
                }
            }

            if (isset($setting['blog_count'])) {
                $articles = BlogArticles::query()
                    ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
                    ->leftJoin('bloggers', 'bloggers.id', '=', 'blog_articles.blogger_id')
                    ->leftJoin('blogger_translations', 'blogger_translations.bloggers_id', '=', 'bloggers.id')
                    ->where("blogger_translations.lang",$lang)
                    ->where([
                        ['blog_article_translations.lang', $lang],
                        ['blog_articles.is_blog', 1],
                    ])
                    ->select([
                        'blog_articles.*',
                        'blog_article_translations.name AS transName',
                        'blog_article_translations.landing_link',
                        'bloggers.slug AS blogger_slug',
                        'bloggers.image_thumbnail AS blogger_image_thumbnail',
                        'blogger_translations.name AS blogger_trans_name',
                        'blogger_translations.text_for_main AS blogger_trans_text',
                    ])
                    ->whereNotNull('blog_article_translations.name')
                    ->active()
                    ->orderBy('blog_articles.public_date', 'desc')
                    ->limit($setting['blog_count'])
                    ->get();

                if (count($articles)) {
                    $data = [];
                    foreach ($articles as $key => $article) {
                        $data[$key]['url'] = '/blogs/' . $article->slug;

                        if ($lang !== config('translatable.locale')) {
                            $data[$key]['url'] = '/' . $lang . $data[$key]['url'];
                        }

                        $data[$key]['slug']        = $article->slug;

                        $pd = Carbon::parse($article->public_date);
                        if ($lang == 'uk') {
                            $pd->setLocale('uk_UA');
                        } elseif ($lang == 'ru') {
                            $pd->setLocale('ru_RU');
                        }
                        $data[$key]['public_date'] = $pd->translatedFormat('d F Y, H:i');

                        $data[$key]['views']       = $article->views;

                        $data[$key]['name']        = $article->transName;


                        $data[$key]['blogger_url'] = '/authors/' . $article->blogger_slug;

                        if ($lang !== config('translatable.locale')) {
                            $data[$key]['blogger_url'] = '/' . $lang . $data[$key]['blogger_url'];
                        }

                        $data[$key]['blogger_slug']  = $article->blogger_slug;
                        $data[$key]['blogger_image'] = $article->blogger_image_thumbnail;

                        $data[$key]['blogger_name'] = $article->blogger_trans_name;
                        $data[$key]['blogger_text'] = $article->blogger_trans_text;

                        if (isset($article->landing_link)) {
                            $landing = Landing::query()->where('id',$article->landing_link)->first();

                            if($landing){
                                $landUrl = '/project/' . $landing->slug;

                                if ($lang !== config('translatable.locale')) {
                                    $landUrl = '/' . $lang . $landUrl;
                                }

                                $data[$key]['landing_link'] = $landUrl;
                            }
                        }
                    }

                    $res['blog'] = $data;
                }
            }

            if (isset($setting['block1_count'])) {
                $this->getBlockData($res, $setting['block1_count'], $setting['block1_type'], $setting['block1_category'] ?? null, $setting['block1_tag'] ?? null, $lang, 1);
            }

            if (isset($setting['block2_count'])) {
                $this->getBlockData($res, $setting['block2_count'], $setting['block2_type'], $setting['block2_category'] ?? null, $setting['block2_tag'] ?? null, $lang, 2);
            }

            if (isset($setting['block3_count'])) {
                $this->getBlockData($res, $setting['block3_count'], $setting['block3_type'], $setting['block3_category'] ?? null, $setting['block3_tag'] ?? null, $lang, 3);
            }

            if (isset($setting['block4_count'])) {
                $this->getBlockData($res, $setting['block4_count'], $setting['block4_type'], $setting['block4_category'] ?? null, $setting['block4_tag'] ?? null, $lang, 4);
            }

            $articlesTop = BlogArticles::query()
                ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
                ->leftJoin('category', 'category.id', '=', 'blog_articles.category_id')
                ->where('blog_article_translations.lang', $lang)
                ->whereNotNull('blog_article_translations.name')
                ->where('blog_articles.mark', 1)
                ->active()
                ->orderBy('blog_articles.public_date', 'desc')
                ->select([
                    'blog_articles.*',
                    'blog_article_translations.name AS transName',
                    'blog_article_translations.annot AS transAnnot',
                    'blog_article_translations.landing_link',
                    'blog_article_translations.img_alt AS transImgAlt',
                    'blog_article_translations.video AS transVideo',
                    'category.path',
                ])
                ->limit(5)
                ->get();

            if (count($articlesTop)) {
                $data = [];
                foreach ($articlesTop as $key => $article) {
                    $data[$key]['url'] = '/' . $article->path . '/' . $article->slug;

                    if ($lang !== config('translatable.locale')) {
                        $data[$key]['url'] = '/' . $lang . $data[$key]['url'];
                    }

                    $data[$key]['slug']        = $article->slug;
                    $data[$key]['image']       = $article->image;
                    $pd = Carbon::parse($article->public_date);
                    if ($lang == 'uk') {
                        $pd->setLocale('uk_UA');
                    } elseif ($lang == 'ru') {
                        $pd->setLocale('ru_RU');
                    }
                    $data[$key]['public_date'] = $pd->translatedFormat('d F Y, H:i');
                    $data[$key]['views']       = $article->views;
                    $data[$key]['interview']   = $article->interview;
                    $data[$key]['name']        = $article->transName;
                    $data[$key]['annot']       = str_replace('&nbsp;',' ',$article->transAnnot);
                    $data[$key]['video']       = $article->transVideo ? 1 : 0;
                    $data[$key]['img_alt']     = $article->transImgAlt;

                    if (isset($article->landing_link)) {
                        $landing = Landing::query()->where('id',$article->landing_link)->first();

                        if($landing){
                            $landUrl = '/project/' . $landing->slug;

                            if ($lang !== config('translatable.locale')) {
                                $landUrl = '/' . $lang . $landUrl;
                            }

                            $data[$key]['landing_link'] = $landUrl;
                        }
                    }
                }

                $res['main'] = $data;
            }

            return $res;
        });

        return $res;
    }

    public function getBlockData(&$res, $count, $type, $category, $tag, $lang, $block_num)
    {
        if ($type === 'category') {
            $titleModel = Category::query()
                ->leftJoin('category_translations', 'category_translations.category_id', '=', 'category.id')
                ->where('category_translations.lang', $lang)
                ->select([
                    'category.*',
                    'category_translations.title AS cat_name'
                ])
                ->where('category.id', $category)
                ->first();

            if ($titleModel) {
                $ids = [$titleModel->id];

                if (!$titleModel->parent_id) {
                    try {
                        $catChildren = json_decode($titleModel->children_ids,true);
                    } catch (\Exception $e){
                        $catChildren = [];
                    }

                    if ($catChildren && count($catChildren)) {
                        $ids = array_merge($ids, $catChildren);
                    }
                }

                $url = '/' . $titleModel['path'];

                if ($lang !== config('translatable.locale')) {
                    $url = '/' . $lang . $url;
                }

                $res['block' . $block_num] = [
                    'category' => [
                        'name' => $titleModel->cat_name,
                        'url'  => $url,
                        'type' => 'category'
                    ],
                    'items'    => []
                ];

                $articles = BlogArticles::query()
                    ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
                    ->leftJoin('category', 'category.id', '=', 'blog_articles.category_id')
                    ->where('blog_article_translations.lang', $lang)
                    ->whereNotNull('blog_article_translations.name')
                    ->whereIn('blog_articles.category_id', $ids)
                    ->active()
                    ->orderBy('blog_articles.public_date', 'desc')
                    ->select([
                        'blog_articles.*',
                        'blog_article_translations.name as article_name',
                        'blog_article_translations.landing_link',
                        'blog_article_translations.img_alt as article_img_alt',
                        'blog_article_translations.video AS article_video',
                        'blog_article_translations.annot AS article_annot',
                        'category.path',
                    ])
                    ->limit($count)
                    ->get();
            }
        } elseif ($type === 'tag') {
            $titleModel = NewTag::query()
                ->leftJoin('new_tag_translations','new_tag_translations.new_tag_id','=','new_tags.id')
                ->where('new_tag_translations.lang',$lang)
                ->select([
                    'new_tags.*',
                    'new_tag_translations.name AS transName'
                ])
                ->where('new_tags.id', $tag)
                ->first();

            if ($titleModel) {
                $url = '/tags/' . $titleModel->slug;

                if ($lang !== config('translatable.locale')) {
                    $url = '/' . $lang . $url;
                }

                $res['block' . $block_num] = [
                    'category' => [
                        'name' => $titleModel->transName,
                        'url'  => $url,
                        'type' => 'tag'
                    ],
                    'items'    => []
                ];

                $articles = BlogArticles::query()
                    ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
                    ->leftJoin('category', 'category.id', '=', 'blog_articles.category_id')
                    ->where('blog_article_translations.lang', $lang)
                    ->whereNotNull('blog_article_translations.name')
                    ->where('blog_article_translations.tags', 'like', '%' . $titleModel->transName . '%')
                    ->active()
                    ->orderBy('blog_articles.public_date', 'desc')
                    ->select([
                        'blog_articles.*',
                        'blog_article_translations.name as article_name',
                        'blog_article_translations.landing_link',
                        'blog_article_translations.img_alt as article_img_alt',
                        'blog_article_translations.video AS article_video',
                        'blog_article_translations.annot AS article_annot',
                        'category.path',
                    ])
                    ->limit($count)
                    ->get();
            }
        }

        if (isset($articles) && count($articles)) {
            $data = [];
            foreach ($articles as $key => $article) {
                $data[$key]['url'] = '/' . $article->path . '/' . $article->slug;

                if ($lang !== config('translatable.locale')) {
                    $data[$key]['url'] = '/' . $lang . $data[$key]['url'];
                }

                $data[$key]['slug']        = $article->slug;
                $data[$key]['image']       = $article->image;
                $pd = Carbon::parse($article->public_date);
                if ($lang == 'uk') {
                    $pd->setLocale('uk_UA');
                } elseif ($lang == 'ru') {
                    $pd->setLocale('ru_RU');
                }
                $data[$key]['public_date'] = $pd->translatedFormat('d F Y, H:i');
                $data[$key]['views']       = $article->views;
                $data[$key]['interview']   = $article->interview;
                $data[$key]['name']        = $article->article_name;
                $data[$key]['img_alt']     = $article->article_img_alt;
                $data[$key]['video']       = $article->article_video ? 1 : 0;
                $data[$key]['annot']       = strip_tags($article->article_annot);

                if (isset($article->landing_link)) {
                    $landing = Landing::query()->where('id',$article->landing_link)->first();

                    if($landing){
                        $landUrl = '/project/' . $landing->slug;

                        if ($lang !== config('translatable.locale')) {
                            $landUrl = '/' . $lang . $landUrl;
                        }

                        $data[$key]['landing_link'] = $landUrl;
                    }
                }
            }

            $res['block' . $block_num]['items'] = $data;
        }
    }

    /**
     * @param $model
     * @param $lang
     * @return array
     */
    public function prepareModelsResults($model, $lang): array
    {
        $models = [];

        foreach ($model as $item) {
            $models[] = $this->prepareModelResults($item, $lang);
        }

        return [
            'models' => $models
        ];
    }

    public function prepareBannersResults($model, $lang): array
    {
        $res = [];
        $data = [];
        $ip = request()->ip();
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        $date = Carbon::today()->format('Y-m-d');

        foreach ($model as $ad) {
            $item = $ad->itemsWithTrans($lang)->inRandomOrder()->first();

            $currentTime = \Carbon\Carbon::now()->timestamp;

            if ($item) {
                $data[] = [$item->id, $ip, $date, $user_agent];

                if(\Carbon\Carbon::parse($item->active_from)->timestamp <= $currentTime && \Carbon\Carbon::parse($item->active_to)->timestamp > $currentTime){
                    $res[$ad->id] = [
                        'name'             => $ad->name,
                        'type'             => $ad->type,
                        'block_id'         => $item->id,
                        'block_name'       => $item->name,
                        'img_desktop'      => $item->img_desktop,
                        'img_desktop_alt'  => $item->img_desktop_alt,
                        'img_desktop2'     => $item->img_desktop2,
                        'img_desktop3'     => $item->img_desktop3,
                        'img2_desktop2'    => $item->img2_desktop2,
                        'img_desktop2_alt' => $item->img_desktop2_alt,
                        'img_desktop3_alt' => $item->img_desktop3_alt,
                        'img_tablet'       => $item->img_tablet,
                        'img_tablet_alt'   => $item->img_tablet_alt,
                        'img_mob'          => $item->img_mob,
                        'img_mob_alt'      => $item->img_mob_alt,
                        'link'             => '/tg/' . $item->id . '?type=1',
                        'text'             => $item->translate($lang)->text,
                        'btn_name'         => $item->translate($lang)->btn_name,
                    ];
                } elseif(\Carbon\Carbon::parse($item->active_from__2)->timestamp <= $currentTime && \Carbon\Carbon::parse($item->active_to__2)->timestamp > $currentTime) {
                    $res[$ad->id] = [
                        'name'             => $ad->name,
                        'type'             => $ad->type,
                        'block_id'         => $item->id,
                        'block_name'       => $item->name,
                        'img_desktop'      => $item->img_desktop__2,
                        'img_desktop_alt'  => $item->img_desktop_alt__2,
                        'img_desktop2'     => $item->img_desktop2__2,
                        'img_desktop3'     => $item->img_desktop3__2,
                        'img2_desktop2'    => $item->img2_desktop2__2,
                        'img_desktop2_alt' => $item->img_desktop2_alt__2,
                        'img_desktop3_alt' => $item->img_desktop3_alt__2,
                        'img_tablet'       => $item->img_tablet__2,
                        'img_tablet_alt'   => $item->img_tablet_alt__2,
                        'img_mob'          => $item->img_mob__2,
                        'img_mob_alt'      => $item->img_mob_alt__2,
                        'link'             => '/tg/' . $item->id . '?type=2',
                        'text'             => $item->translate($lang)->text__2,
                        'btn_name'         => $item->translate($lang)->btn_name__2,
                    ];
                }
            }
        }

        $this->postponeSetViews($data);

        return $res;
    }

    public function prepareMenuResults($model, $lang): array
    {
        $res = [];

        $res = $this->getChildrenMenu($model, $lang, $res);

        return $res;
    }

    public function getChildrenMenu($children, $lang, $res = [])
    {
        foreach ($children as $key => $item) {
            $slug = 'slug';

            if ($item['type'] == Menu::TYPE_CATEGORY) {
                $slug = 'path';
            }

            if ($item['type'] == Menu::TYPE_ARBITRARY) {
                $res[$key]['name'] = $item['name'];
                $res[$key]['type'] = $item['type'];
                $res[$key]['icon'] = $item['icon'];

                foreach ($item['translations'] as $trans) {
                    if ($trans['lang'] === $lang) {
                        $res[$key]['name'] = $trans['name'];
                        $res[$key]['url']  = $trans['url'];
                        $res[$key]['slug'] = null;
                    }
                }
            } else {
                $modelRel = $item[\App\Models\Menu::getTypesModel()[$item['type']]['rel']];

                if (is_null($modelRel)) {
                    continue;
                }

                $modelRelSlug = $modelRel[$slug];

                $res[$key]['url'] = \App\Models\Menu::getTypesModel()[$item['type']]['url_prefix'] . $modelRelSlug;

                if ($lang !== config('translatable.locale')) {
                    $res[$key]['url'] = '/' . $lang . '/' . $res[$key]['url'];
                } else {
                    $res[$key]['url'] = '/' . $res[$key]['url'];
                }

                if ($res[$key]['url'] == '//') {
                    $res[$key]['url'] = '/';
                }

                if (substr($res[$key]['url'], -2) == '//') {
                    $res[$key]['url'] = substr($res[$key]['url'], 0, -1);
                }

                $res[$key]['name'] = $modelRel[\App\Models\Menu::getTypesModel()[$item['type']]['name']];
                $res[$key]['type'] = $item['type'];
                $res[$key]['icon'] = $item['icon'];

                if ($item['type'] !== Menu::TYPE_TAG) {
                    foreach ($modelRel['translations'] as $trans) {
                        if ($trans['lang'] === $lang) {
                            $res[$key]['name'] = $trans[\App\Models\Menu::getTypesModel()[$item['type']]['name']];
                            $res[$key]['slug'] = $modelRel[$slug];
                        }
                    }
                }

                foreach ($item['translations'] as $trans) {
                    if ($trans['lang'] === $lang) {
                        if (!empty($trans['name'])) {
                            $res[$key]['name'] = $trans['name'];
                        }
                    }
                }
            }

            if (isset($item['banner_item_id'])) {
                $b = Banners::query()->where('id', $item['banner_item_id'])->first();

                $bItems = BannerItems::query()->where('banners_id', $b->id)->get();

                $countItems = count($bItems);

                $r = rand(1, $countItems) - 1;

                $bItem = $bItems[$r];
                if ($bItem) {
                    $ad = $bItem->parent;

                    $res[$key]['banner'] = [
                        'name'             => $ad->name,
                        'type'             => $ad->type,
                        'block_name'       => $bItem->name,
                        'img_desktop'      => $bItem->img_desktop,
                        'img_desktop_alt'  => $bItem->img_desktop_alt,
                        'img_desktop2'     => $bItem->img_desktop2,
                        'img2_desktop2'    => $bItem->img2_desktop2,
                        'img_desktop2_alt' => $bItem->img_desktop2_alt,
                        'img_tablet'       => $bItem->img_tablet,
                        'img_tablet_alt'   => $bItem->img_tablet_alt,
                        'img_mob'          => $bItem->img_mob,
                        'img_mob_alt'      => $bItem->img_mob_alt,
                        'link'             => '/tg/' . $bItem->id,
                        'text'             => $bItem->text,
                        'btn_name'         => $bItem->btn_name,
                    ];
                }
            }

            if (count($item['children'])) {
                $res[$key]['children'] = $this->getChildrenMenu($item['children'], $lang, []);
            }
        }

        return $res;
    }
}
