<?php

namespace App\Service;

use App\Models\BlogArticles;
use App\Models\Category;
use App\Models\Langs;
use App\Models\NewTag;
use App\Models\Pages;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class Sitemap
{
    private $rows = 1;
    private $langs;
    private $defaultLangCode;
    private $sitemapIndex = [];

    public function __construct()
    {
        $this->langs = Langs::getLangCodes();
        $this->defaultLangCode = Langs::getDefaultLangCode();
    }

    private function generatePages()
    {
        $xml = '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL.
            '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">'.PHP_EOL;


        $xml .= "<url>
                    <loc>".url('/')."</loc>
                    <lastmod>".Carbon::now()->format('Y-m-d')."</lastmod>
                    <changefreq>daily</changefreq>
                    <priority>1.0</priority>
                </url>";

        $list = Pages::query()->active()->get();

        foreach ($list as $item) {
            echo "Page: " . $item->slug . PHP_EOL;
            $xml .= $this->row('/' . $item->slug, 'daily', '0.9',true, $item);
        }

        $xml .= '</urlset>';

        file_put_contents(public_path('sitemap_pages.xml'), $xml);
        chmod(public_path('sitemap_pages.xml'), 0665);

        $this->sitemapIndex[] = [
            'loc' => env('APP_URL') . '/sitemap_pages.xml',
            'lastmod' => Carbon::now()->format('Y-m-d')
        ];
    }

    private function generateNewTag()
    {
        $xml = '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL.
            '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">'.PHP_EOL;

        $list = NewTag::query()->get();
        $lastmodItem = NewTag::query()->orderBy('updated_at', 'desc')->first();

        $lastmod = null;

        if(isset($lastmodItem->updated_at)){
            $lastmod = $lastmodItem->updated_at->format('Y-m-d');
        } else {
            $lastmod = Carbon::now()->format('Y-m-d');
        }

        foreach ($list as $item) {
            echo "Tags: " . $item->slug . PHP_EOL;
            $xml .= $this->row('/tags/' . $item->slug, 'daily', '0.8',true, $item);
        }

        $xml .= '</urlset>';

        file_put_contents(public_path('sitemap_tags.xml'), $xml);
        chmod(public_path('sitemap_tags.xml'), 0665);

        $this->sitemapIndex[] = [
            'loc' => env('APP_URL') . '/sitemap_tags.xml',
            'lastmod' => $lastmod
        ];
    }

    private function generateCatalog()
    {
        $categories = Category::query()->active()->get();

        foreach ($categories as $category){
            $countItems = '';
            echo "News: " . $category->slug . PHP_EOL;
            $xml = '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL.
                '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">'.PHP_EOL;

            $list = BlogArticles::query()
                ->where('category_id',$category->id)
                ->active()
                ->get();

            $lastmodItem = BlogArticles::query()
                ->where('category_id',$category->id)
                ->orderBy('updated_at', 'desc')
                ->first();
            $lastmod = null;

            if(isset($lastmodItem->updated_at)){
                $lastmod = $lastmodItem->updated_at->format('Y-m-d');
            } else {
                $lastmod = Carbon::now()->format('Y-m-d');
            }

            foreach ($list as $key => $item) {
                echo "News: " . $category->slug  . ':' . $item->slug . PHP_EOL;
                $countItems .= $this->row('/' . $category->path . '/' . $item->slug, 'daily', '0.8', true, $item);
            }

            $xml .= $countItems;

            $xml .= '</urlset>';

            if($countItems != '') {
                file_put_contents(public_path('sitemap_catalog_'.$category->slug.'.xml'), $xml);
                chmod(public_path('sitemap_catalog_'.$category->slug.'.xml'), 0665);

                $this->sitemapIndex[] = [
                    'loc' => env('APP_URL') . '/sitemap_catalog_'.$category->slug.'.xml',
                    'lastmod' => $lastmod
                ];
            }
        }
    }

    public function generate(){
        Log::info('sitemap:start',[]);
        $this->generatePages();
        $this->generateCatalog();
        $this->generateNewTag();

        if(count($this->sitemapIndex)){
            $xml = '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">'.PHP_EOL;
                foreach ($this->sitemapIndex as $item){
                    $xml .= '<sitemap>';
                    $xml .= '<loc>'.$item['loc'].'</loc>';
                    $xml .= '<lastmod>'.$item['lastmod'].'</lastmod>';
                    $xml .= '</sitemap>';
                }
            $xml .= '</sitemapindex>';

            file_put_contents(public_path('sitemap_file.xml'), $xml);
            chmod(public_path('sitemap_file.xml'), 0665);
        }
        Log::info('sitemap:end',[]);
    }

//    public function generate()
//    {
//        echo "Start" . PHP_EOL;
//        Log::info('sitemap generate start');
/*        $xml = '<?xml version="1.0" encoding="UTF-8"?>'.PHP_EOL.*/
//            '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">'.PHP_EOL;
//
//
//        $xml .= "<url>
//                    <loc>".url('/')."</loc>
//                    <lastmod>".Carbon::now()->format('Y-m-d')."</lastmod>
//                    <changefreq>daily</changefreq>
//                    <priority>1.0</priority>
//                </url>";
//
//        $list = Pages::query()->active()->get();
//        foreach ($list as $item) {
//            echo "Page: " . $item->slug . PHP_EOL;
//            $xml .= $this->row('/' . $item->slug, 'daily', '0.9',true, $item);
//        }
//
//        $list = BlogArticles::query()
//            ->leftJoin('category', 'category.id', '=', 'blog_articles.category_id')
//            ->select([
//                'blog_articles.*',
//                'category.path',
//            ])
//            ->active()
//            ->get();
//
//        $count = BlogArticles::query()->active()->count();
//
//        foreach ($list as $key => $item) {
//            echo "News: " . $key . '/' . $count . ' - ' . $item->slug . PHP_EOL;
//            $xml .= $this->row('/' . $item->path . '/' . $item->slug, 'daily', '0.8', true, $item);
//        }
//
//        $list = NewTag::query()->get();
//        foreach ($list as $item) {
//            echo "Tags: " . $item->slug . PHP_EOL;
//            $xml .= $this->row('/tags/' . $item->slug, 'daily', '0.8',true, $item);
//        }
//
//        $xml .= '</urlset>';
//
//        file_put_contents(public_path('sitemap_file.xml'), $xml);
//        chmod(public_path('sitemap_file.xml'), 0665);
//
//        Log::info('sitemap generate end');
//
//        return $this->rows;
//    }

    private function isValidUri($uri, $isNews = false, $item = null, $lang = null)
    {
        if($isNews){
            return $item->hasLang($lang);
        }

        return true;

//        $hds = @get_headers($uri);

//        return !$hds || (strpos($hds[0], ' 404 ') !== false ) ? false : true;
    }

    public function row($loc, $changefreq, $priority, $isNews = false, $item = null)
    {
        $this->rows++;

        if($this->rows == 2){
            return '';
        }

        $url = url($loc);

        if(!$this->isValidUri($url,$isNews,$item, $this->defaultLangCode)) return '';

        $res = '';

        foreach ($this->langs as $lang){
            if($this->defaultLangCode == $lang){
                $locLang = $url;

                if($url == '//'){
                    $url = env('APP_URL');
                }

                if($this->isValidUri($locLang, $isNews, $item, $lang)){
                    $res .= '<url><loc>'.$url.'</loc>'.PHP_EOL;

                    if($item){
                        $res .= '<lastmod>'.$item->updated_at->format('Y-m-d').'</lastmod>'.PHP_EOL;
                    } else {
                        $res .= '<lastmod>'.Carbon::now()->format('Y-m-d').'</lastmod>'.PHP_EOL;
                    }

                    $res .= '<changefreq>'.$changefreq.'</changefreq><priority>'.$priority.'</priority></url>'.PHP_EOL;
                };
            }
        }

        foreach ($this->langs as $lang){
            if($this->defaultLangCode !== $lang){
                $locLang = url($lang . $loc);
                if($this->isValidUri($locLang, $isNews, $item, $lang)){
                    $res .= '<url><loc>'.$locLang.'</loc>'.PHP_EOL;

                    if($item){
                        $res .= '<lastmod>'.$item->updated_at->format('Y-m-d').'</lastmod>'.PHP_EOL;
                    } else {
                        $res .= '<lastmod>'.Carbon::now()->format('Y-m-d').'</lastmod>'.PHP_EOL;
                    }

                    $res .= '<changefreq>'.$changefreq.'</changefreq><priority>'.$priority.'</priority></url>'.PHP_EOL;
                }
            }
        }

        return $res;
    }
}
