<?php


namespace App\Core\Response;

use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

trait ResponseTrait
{
    public function errorResponse(array $errors, int $httpCode = Response::HTTP_OK)
    {
        $data = ['errors' => $errors];

        $data = [
            'success'   => false,
            'http_code' => $httpCode,
            'data'      => $data
        ];

        return response()->json($data, $httpCode, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE);
    }

    public function successResponse($data, int $httpCode = Response::HTTP_OK)
    {
        try {
            $res = [
                'success'   => true,
                'http_code' => $httpCode,
                'data'      => $data
            ];
        } catch (\Exception $e){
            $res = [];
            Log::error('SuccessResponse data is not array',[$data]);
        }


        return response()->json($res, $httpCode, ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE);
    }
}
