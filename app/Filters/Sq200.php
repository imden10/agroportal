<?php

namespace App\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Sq200 implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->resize(200, null, function ($constraint) {
            $constraint->aspectRatio();
        })->encode('webp', 90);
    }
}
