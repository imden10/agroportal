<?php

namespace App\Console\Commands;

use App\Models\BannerItems;
use App\Models\BannerStatisticViews;
use App\Models\BannerStatisticViewsAll;
use App\Models\BannerStatisticViewsUnique;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class StatAllParse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'statAll:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $getViewsByPeriodData = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $start_date = Carbon::today()->addDays(-1);

        $dateToModel = BannerStatisticViews::query()
            ->orderBy('date','desc')
            ->first();

        $dateTo = $dateToModel->date;
        $end_date = Carbon::parse($dateTo);

        $banner_items_ids = BannerItems::query()->pluck('id')->toArray();

        for ($date = $start_date; $date->lte($end_date); $date->addDay()) {
            $d = $date->format('Y-m-d');
            foreach ($banner_items_ids as $banner_items_id){
                $this->getViewsByPeriodData = [];
                try {
                    BannerStatisticViews::query()
                        ->where('date',  $d)
                        ->where('item_id',$banner_items_id)
                        ->chunk(1000, function ($models, $i) {
                            foreach ($models as $key => $visit) {
                                if (!isset($this->getViewsByPeriodData[$visit->date])) {
                                    $this->getViewsByPeriodData[$visit->date] = 0;
                                }

                                $this->getViewsByPeriodData[$visit->date] += $visit->count;
                            }
                        });

                    foreach ($this->getViewsByPeriodData as $dateKey => $count) {
                        if($count){
                            if($banner_items_id == 37){
                                echo $dateKey . " " . $count . PHP_EOL;
                            }
//                            $bannerStatUnique = new BannerStatisticViewsAll();
//                            $bannerStatUnique->item_id = $banner_items_id;
//                            $bannerStatUnique->date = $dateKey;
//                            $bannerStatUnique->count = $count;
//                            $bannerStatUnique->save();
                        }
                    }
                } catch (\Exception $e){

                }
            }
            echo $d . PHP_EOL;
        }
    }
}
