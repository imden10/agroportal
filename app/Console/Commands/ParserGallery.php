<?php

namespace App\Console\Commands;

use App\Models\BlogArticles;
use App\Models\Orig\Struct;
use Illuminate\Console\Command;

/* todo Первым делом обновить блогеров */

/* todo Галерею в последнюю очередь */

class ParserGallery extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parserGallery:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse gallery';

    /**
     * @var string
     *
     * Путь к изображениям статей
     */
    private $articleImgPath;

    private $uploadPath;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->articleImgPath = 'https://agroportal.ua';
        $this->uploadPath     = 'public/storage/media/uploads/galleries/';
        parent::__construct();
    }

    private function getImgFormat($src)
    {
        return substr($src, 0, 1) . '/' . substr($src, 1, 1) . '/';
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo 'Start' . PHP_EOL;
        echo '------------------------------' . PHP_EOL;

        /* multimedia */
        $galleries = Struct::query()
            ->whereIn('struct_id', [438])->get();

        foreach ($galleries as $gallery) {
            $items = $gallery->articles;

            $countItems = count($items);

            if ($countItems) {
                foreach ($items as $key => $item) {
                    $articleTranslations = $item->items;

                    if (count($articleTranslations)) {
                        foreach ($articleTranslations as $articleTranslation) {
                            if ($articleTranslation->lang === 'ru') {
                                $multiimg = $articleTranslation->multiimg;

                                if($multiimg){
                                    preg_match_all("/\"image\"\:\".+?\"/", $multiimg, $matches);

                                    if(isset($matches[0]) && count($matches[0])){
                                        foreach ($matches[0] as $match){
                                            $parts = explode('/images/gallery/',$match);

                                            if(isset($parts[1])){
                                                $img = substr($parts[1],0,-1);
                                                $imgPath = $this->articleImgPath . '/images/gallery/' .$img;

                                                $parts =  explode('/',$img);

                                                if(isset($parts[1])){
                                                    $img = $parts[1];

                                                    if ($img) {
                                                        $modelGallery = \App\Models\Gallery::query()
                                                            ->where('agro_id', $item->struct_id)
                                                            ->where('image', '/uploads/galleries/' . $img)
                                                            ->first();
                                                        // если нет, создем ее
                                                        if (!$modelGallery) {
                                                            $modelGallery          = new \App\Models\Gallery();
                                                            $modelGallery->agro_id = $item->struct_id;

                                                            $blogArticleModel = BlogArticles::query()
                                                                ->where('agro_id', $item->struct_id)
                                                                ->first();

                                                            if($blogArticleModel){
                                                                $modelGallery->article_id = $blogArticleModel->id;
                                                            }

                                                            if (!file_exists($this->uploadPath . $img)) {
                                                                try {
                                                                    file_put_contents($this->uploadPath . $img, file_get_contents($imgPath));
                                                                    echo "img orig uploading" . PHP_EOL;
                                                                    $modelGallery->image = '/uploads/galleries/' . $img;
                                                                } catch (\Throwable $e) {
                                                                    echo $e->getMessage() . PHP_EOL;
                                                                }
                                                            } else {
                                                                $modelGallery->image = '/uploads/galleries/' . $img;
                                                                echo "img orig exits" . PHP_EOL;
                                                            }

                                                            $modelGallery->save();

                                                            echo $key . '/' . $countItems . ': ' . $img . PHP_EOL;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
