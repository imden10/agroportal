<?php

namespace App\Console\Commands;

use App\Traits\Statistics;
use Illuminate\Console\Command;

class PostponeSetBannersViews extends Command
{
    use Statistics;
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'postpone:setBannersViews';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = public_path() . '/banner_views.txt';

        $handle = fopen($file, "r");
        if ($handle) {
            while (($line = fgets($handle)) !== false) {
                try {
                    $row = json_decode($line,true);

                    if(is_array($row) && count($row)){
                        foreach ($row as $item){
                            $this->setViews($item[0],$item[1],$item[2],$item[3]);
                        }
                    }
                } catch (\Exception $e){

                }
            }

            fclose($handle);
        }

        file_put_contents(public_path() . '/banner_views.txt', "");
    }
}
