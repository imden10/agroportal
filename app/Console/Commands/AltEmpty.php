<?php

namespace App\Console\Commands;

use App\Models\BlogArticles;
use App\Models\Translations\BlogArticleTranslation;
use Illuminate\Console\Command;

class AltEmpty extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'altEmpty:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate alt';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $model = BlogArticleTranslation::query()->where('img_alt','')->get();

        $count = count($model);

        foreach ($model as $key => $item){
            $item->img_alt = $item->name . ' фото';
            $item->save();
            echo $key . '/' . $count . PHP_EOL;
        }
    }
}
