<?php

namespace App\Console\Commands;

use App\Models\BlogArticles;
use App\Models\Bloggers;
use App\Models\Category;
use App\Models\CategoryArticles;
use App\Models\Langs;
use App\Models\Orig\Struct;
use App\Models\Tags;
use App\Models\Translations\BlogArticleTranslation;
use App\Models\Translations\CategoryTranslation;
use App\Models\Translations\NewTagTranslation;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Console\Command;

/* todo Первым делом обновить блогеров */

class ParserMetaTags extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ParserMetaTags:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse meta from self name and desc';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo 'Start' . PHP_EOL;
        echo '------------------------------' . PHP_EOL;

        $model = NewTagTranslation::query()->get();

        $count = count($model);

        $suffTitle = [
            'uk' => ' — АгроНовини онлайн AgroPortal',
            'ru' => ' — АгроНовости онлайн AgroPortal'
        ];

        $suffDescription = [
            'uk' => " ► Найсвіжіші аграрні новини, статті, інтерв'ю, аналітика ринку сільського господарства в Україні та у світі на аграрному порталі AgroPortal.ua!",
            'ru' => " ► Самые свежие аграрные новости, статьи, интервью, аналитика рынка сельского хозяйства в Украине и мире на аграрном портале AgroPortal.ua!"
        ];

        if($count){
            foreach ($model as $j => $item){
                $item->meta_title = $item->name . $suffTitle[$item->lang];
                $item->meta_description = $item->name . $suffDescription[$item->lang];

                $item->save();
                echo "save " . $j . '/' . $count . PHP_EOL;
            }
        }

        echo "ready";
    }
}
