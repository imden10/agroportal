<?php

namespace App\Console\Commands;

use App\Models\BlogArticles;
use App\Models\Bloggers;
use App\Models\CategoryArticles;
use App\Models\Orig\Struct;
use App\Models\Translations\BloggerTranslation;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Console\Command;

class ParserCat extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ParserCat:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse bloggers from agroportal original';

    /**
     * @var string
     *
     * Путь к изображениям статей
     */
    private $articleImgPath;

    private $uploadPath;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->articleImgPath = 'https://agroportal.ua/images/doc/';
        $this->uploadPath     = 'public/storage/media/uploads/authors/';
        parent::__construct();
    }

    private function getImgFormat($src)
    {
        return substr($src, 0, 1) . '/' . substr($src, 1, 1) . '/';
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $articles = BlogArticles::query()->get();

        foreach ($articles as $key => $article) {
            $cat = CategoryArticles::query()->where('articles_id',$article->id)->first();

            if($cat){
                $article->category_id = $cat->category_id;
                $article->save();
                echo $key . ': ' .$article->slug . PHP_EOL;
            }
        }
    }
}
