<?php

namespace App\Console\Commands;

use App\Models\BlogArticles;
use App\Models\Translations\BlogArticleTranslation;
use App\Service\Adapter;
use Illuminate\Console\Command;


class ParserRenderConstructor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ParserRender:constructor';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Перебрати всі публікації і перегенерувати конструктор';

    private Adapter $adapter;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo 'Start' . PHP_EOL;

        $model = BlogArticles::query()
            ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
            ->where('blog_article_translations.lang', 'uk')
            ->where('blog_articles.constructor_rerender', 0)
            ->whereNotNull('blog_article_translations.constructor_html')
            ->select([
                'blog_articles.*',
            ])
            ->get();


        $count = count($model);

        if($count){
            echo $count . PHP_EOL;
            foreach ($model as $key => $item){
                try {
                    $this->adapter->renderConstructorHTML($item);
                    $item->constructor_rerender = 1;
                    $item->save();
                } catch (\Exception $e){
                    echo $e->getMessage() . PHP_EOL;
                    $item->constructor_rerender = 2;
                    $item->save();
                }
                echo "save " . $key . '/' . $count . PHP_EOL;
            }
        }

    }
}
