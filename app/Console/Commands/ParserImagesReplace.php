<?php

namespace App\Console\Commands;

use App\Models\BlogArticles;
use App\Models\Bloggers;
use App\Models\Category;
use App\Models\CategoryArticles;
use App\Models\Langs;
use App\Models\Orig\Struct;
use App\Models\Tags;
use App\Models\Translations\BlogArticleTranslation;
use App\Models\Translations\CategoryTranslation;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Console\Command;

/* todo Первым делом обновить блогеров */

class ParserImagesReplace extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ParserImagesReplace:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse content from agroportal original';

    /**
     * @var bool
     *
     * Обновлять существующие записи
     */
    private $replace;

    /**
     * @var string
     *
     * Путь к изображениям статей
     */
    private $articleImgPath;

    private $uploadPath;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->replace = false;
        $this->articleImgPath = 'https://agroportal.ua';
        $this->uploadPath = 'public/storage/media/uploads/articles/';
        parent::__construct();
    }

    private function getImgFormat($src)
    {
        return substr($src,0,1) . '/' . substr($src,1,1) . '/';
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo 'Start' . PHP_EOL;
        echo '------------------------------' . PHP_EOL;

        $model = BlogArticleTranslation::query()->where('text','like','%src="/uploads/articles/%')->get();

        $count = count($model);

        if($count){
            foreach ($model as $j => $item){
                $text = $item->text;

                preg_match_all("/<img[^>]+src=[\"\']([^\'\"]+)[\"\']/", $text, $matches);

                if(isset($matches[1]) && count($matches[1])){
                    $replaceArr = [];

                    foreach ($matches[1] as $key => $match){
                        $urlOrig = $this->articleImgPath . $match;
                        $item->text = str_replace('src="/uploads/articles/','src="/storage/media/uploads/articles/',$item->text);
                        $item->save();
                        echo "replace". PHP_EOL;
                    }

                    $item->save();
                    echo "save " . $j . '/' . $count . PHP_EOL;
                }
            }
        }
    }
}
