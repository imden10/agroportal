<?php

namespace App\Console\Commands;

use App\Models\BlogArticles;
use App\Models\NewTag;
use App\Modules\Constructor\Models\Constructor;
use App\Service\Adapter;
use Illuminate\Console\Command;

class ParserNewTag extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parserNewTag:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Перебираємо теги словесні в публікаціях, знаходимо їх id та пов'язуємо з публікацією";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo 'Start' . PHP_EOL;
        echo '------------------------------' . PHP_EOL;

        $articles = BlogArticles::query()
            ->leftJoin('blog_article_translations','blog_article_translations.blog_articles_id','=','blog_articles.id')
            ->where('blog_article_translations.lang',config('translatable.locale'))
            ->where('blog_articles.tag_rel',0)
            ->select([
                'blog_articles.*',
                'blog_article_translations.tags'
            ])
            ->get();

        $countArticles = count($articles);

        if($countArticles){
            echo 'Всього ' . $countArticles . ' потрібно опрацювати' . PHP_EOL;

            foreach ($articles as $key => $article){
                $tags = explode(',',$article->tags);
                $tagIds = NewTag::query()
                    ->leftJoin('new_tag_translations','new_tag_translations.new_tag_id','=','new_tags.id')
                    ->where('new_tag_translations.lang',config('translatable.locale'))
                    ->whereIn('new_tag_translations.name',$tags)
                    ->pluck('new_tags.id')
                    ->toArray();

                $article->newTags()->sync($tagIds);
                $article->tag_rel = 1;
                $article->save();

                echo 'Опрацьовано: ' . ($key+1) . ' з '. $countArticles . PHP_EOL;
            }
        }

        echo PHP_EOL . "ready all";
    }
}
