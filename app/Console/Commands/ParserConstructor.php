<?php

namespace App\Console\Commands;

use App\Models\BlogArticles;
use App\Modules\Constructor\Models\Constructor;
use App\Service\Adapter;
use Illuminate\Console\Command;

class ParserConstructor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ParserConstructor:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse constructor';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo 'Start' . PHP_EOL;
        echo '------------------------------' . PHP_EOL;

        $blogArticleTransIds = Constructor::query()
            ->where('constructorable_type','like',"%BlogArticleTranslation%")
            ->pluck('constructorable_id')
            ->toArray();

        $blogArticleIds = BlogArticles::query()
            ->leftJoin('blog_article_translations','blog_article_translations.blog_articles_id','=','blog_articles.id')
            ->whereIn('blog_article_translations.id',$blogArticleTransIds)
            ->pluck('blog_articles.id')->toArray();

        $blogArticleIds = array_unique($blogArticleIds);

        $model = BlogArticles::query()
            ->whereIn('id',$blogArticleIds)
            ->get();

        $i = 1;

        $exc = [];

        foreach ($model as $item){
            try {
                app(Adapter::class)->renderConstructorHTML($item);
            } catch (\Exception $e){
                $exc[] = $item->id;
                echo $e->getMessage();
            }

            echo $i . ") Ready: " . $item->id , PHP_EOL;
            $i++;
        }

        print_r($exc);
        
        echo PHP_EOL . "ready all";
    }
}
