<?php

namespace App\Console\Commands;

use App\Models\Translations\BlogArticleTranslation;
use Illuminate\Console\Command;

class ParseSearchField extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ParseSearch:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'blog_article_translations перебрати поля text annot constructor_html і і обєднати в одне поле search_field тільки без html тегів';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo 'Start' . PHP_EOL;
        echo '------------------------------' . PHP_EOL;

        $model = BlogArticleTranslation::query()
            ->where('is_search_render',0)
            ->get();

        $count = count($model);

        if($count){
            foreach ($model as $i => $item){
                try {
                    $search_field = strip_tags($item->text . $item->annot . $item->constructor_html);
                    $item->search_field = $search_field;
                    $item->is_search_render = 1;
                    $item->save();
                    echo "save " . ($i+1) . '/' . $count . PHP_EOL;
                } catch (\Exception $e){
                    echo $e->getMessage() . PHP_EOL;
                }
            }
        }

        echo 'READY' . PHP_EOL;
    }
}
