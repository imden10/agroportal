<?php

namespace App\Console\Commands;

use App\Models\BlogArticles;
use App\Models\Bloggers;
use App\Models\Category;
use App\Models\CategoryArticles;
use App\Models\Langs;
use App\Models\Orig\Struct;
use App\Models\Tags;
use App\Models\Translations\BlogArticleTranslation;
use App\Models\Translations\CategoryTranslation;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Console\Command;

/* todo Первым делом обновить блогеров */

class ParserMeta extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ParserMeta:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse meta from self name and desc';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo 'Start' . PHP_EOL;
        echo '------------------------------' . PHP_EOL;

        $model = BlogArticleTranslation::query()
            ->whereNull('meta_title')
            ->orWhereNull('meta_description')
            ->get();

        $count = count($model);

        if($count){
            foreach ($model as $j => $item){
                if(is_null($item->meta_title)){
                    $item->meta_title = $item->name . ' - AgroPortal.ua';
                }

                if(is_null($item->meta_description)){
                    $item->meta_description = mb_strimwidth(strip_tags($item->annot), 0, 170, "...");
                }

                $item->save();
                echo "save " . $j . '/' . $count . PHP_EOL;
            }
        }
    }
}
