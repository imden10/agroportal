<?php

namespace App\Console\Commands;

use App\Models\BannerItems;
use App\Models\BannerStatisticViews;
use App\Models\BannerStatisticViewsUnique;
use Carbon\Carbon;
use Illuminate\Console\Command;

class AddStatUnique extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'statUnique:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';

    private $getViewsUniqueByPeriodData = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = Carbon::now()->addDay(-1);

        $banner_items_ids = BannerItems::query()->pluck('id')->toArray();

        $d = $date->format('Y-m-d');
        foreach ($banner_items_ids as $banner_items_id){
            $this->getViewsUniqueByPeriodData = [];
            try {
                $t = [];
                BannerStatisticViews::query()
                    ->where('date', $d)
                    ->where('item_id',$banner_items_id)
                    ->chunk(1000, function ($models, $i) {
                        foreach ($models as $key => $visit) {
                            if(! isset($this->getViewsUniqueByPeriodData[$visit->date])){
                                $this->getViewsUniqueByPeriodData[$visit->date] = [[
                                    'date' => $visit->date,
                                    'ip' => $visit->ip,
                                ]];
                            } else {
                                $this->getViewsUniqueByPeriodData[$visit->date][] = [
                                    'date' => $visit->date,
                                    'ip' => $visit->ip,
                                ];
                            }
                        }
                    });

                foreach ($this->getViewsUniqueByPeriodData as $date2) {
                    $ips = [];
                    foreach ($date2 as $item) {
                        if (!isset($t[$item['date']])) {
                            $t[$item['date']] = 0;
                        }

                        if (!in_array($item['ip'], $ips)) {
                            $t[$item['date']]++;
                            $ips[] = $item['ip'];
                        }
                    }
                }

                $count = isset($t[$d]) ? $t[$d] : 0;

                if($count){
                    $bannerStatUnique = new BannerStatisticViewsUnique();
                    $bannerStatUnique->item_id = $banner_items_id;
                    $bannerStatUnique->date = $d;
                    $bannerStatUnique->count = isset($t[$d]) ? $t[$d] : 0;
                    $bannerStatUnique->save();
                }
            } catch (\Exception $e){

            }
        }
        echo $d . PHP_EOL;
    }
}
