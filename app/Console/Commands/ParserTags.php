<?php

namespace App\Console\Commands;

use App\Models\NewTag;
use App\Models\Tags;
use App\Models\Translations\NewTagTranslation;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Console\Command;

class ParserTags extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ParserTags:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse tags';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo 'Start' . PHP_EOL;
        echo '------------------------------' . PHP_EOL;

        $model = Tags::query()
            ->where('lang','ru')
            ->get();

        $count = count($model);

        if($count){
            foreach ($model as $j => $item){
                $ukModel =  Tags::query()
                    ->where('lang','uk')
                    ->where('name',$item->name)
                    ->first();

                if($ukModel){
                    $newTag = new NewTag();
                    $newTag->slug = SlugService::createSlug(NewTag::class, 'slug', $ukModel->name);

                    if($newTag->save()){
                        $newTagTransUk = new NewTagTranslation();
                        $newTagTransUk->new_tag_id = $newTag->id;
                        $newTagTransUk->lang = 'uk';
                        $newTagTransUk->name = $ukModel->name;
                        $newTagTransUk->save();

                        $newTagTransRu = new NewTagTranslation();
                        $newTagTransRu->new_tag_id = $newTag->id;
                        $newTagTransRu->lang = 'ru';
                        $newTagTransRu->name = $item->name;
                        $newTagTransRu->save();

                        $item->related = 1;
                        $item->save();

                        $ukModel->related = 1;
                        $ukModel->save();

                        echo "related " . $j . '/' . $count . PHP_EOL;
                    }
                }
            }
        }
    }
}
