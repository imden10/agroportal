<?php

namespace App\Console\Commands;

use App\Models\BlogArticles;
use App\Models\Bloggers;
use App\Models\Category;
use App\Models\CategoryArticles;
use App\Models\Langs;
use App\Models\Orig\Struct;
use App\Models\Tags;
use App\Models\Translations\BlogArticleTranslation;
use App\Models\Translations\CategoryTranslation;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Console\Command;

/* todo Первым делом обновить блогеров */

class Parser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse content from agroportal original';

    /**
     * @var bool
     *
     * Обновлять существующие записи
     */
    private $replace;

    /**
     * @var string
     *
     * Путь к изображениям статей
     */
    private $articleImgPath;

    private $uploadPath;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->replace = false;
        $this->articleImgPath = 'https://agroportal.ua/images/doc/';
        $this->uploadPath = 'public/storage/media/uploads/articles/';
        parent::__construct();
    }

    private function getImgFormat($src)
    {
        return substr($src,0,1) . '/' . substr($src,1,1) . '/';
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo 'Start' . PHP_EOL;
        echo '------------------------------' . PHP_EOL;

         $stuct = Struct::query()
             ->whereIn('struct_id',[416,436,442,441,438,443,3927,13219])
             ->get();


        // multimedia
//            $stuct = Struct::query()
//            ->whereIn('struct_id',[438])
//            ->get();

        // mneniya
//        $stuct = Struct::query()
//            ->whereIn('struct_id',[441])
//            ->get();


        foreach ($stuct as $key => $item){
            // todo создаем категорию
            $category = Category::query()->where('agro_id',$item->struct_id)->first();

            // если нет категории, создем ее
            if(! $category) {
                $category = new Category();
                $category->agro_id = $item->struct_id;
                $category->parent_id = 0;

                $slug = SlugService::createSlug(Category::class, 'slug', $item->dir);

                $category->slug = $slug;
                $category->status = Category::STATUS_ACTIVE;
                $category->order = $key;
                $category->path = $item->dir;
                $category->collapse = 0;
                $category->creator = $item->creator;
                $category->changer = $item->changer;
                $category->save();

                foreach (Langs::getLangCodes() as $lang){
                    $categoryTrans = new CategoryTranslation();
                    $categoryTrans->category_id = $category->id;
                    $categoryTrans->lang = $lang;
                    $categoryTrans->title = $item->name;
                    $categoryTrans->save();
                }

                echo "Create new category: " . $item->name . PHP_EOL;
            }

            $children = $item->children;

            if(count($children)){
                foreach ($children as $key2 => $child){
                    $articles = $child->articles;

                    // todo создаем подкатегорию
                    $subCategory = Category::query()->where('agro_id',$child->struct_id)->first();

                    if(! $subCategory){
                        $subCategory = new Category();
                        $subCategory->agro_id = $child->struct_id;
                        $subCategory->parent_id = $category->id;

                        $slug = SlugService::createSlug(Category::class, 'slug', $child->dir);

                        $subCategory->slug = $slug;
                        $subCategory->status = $child->showmode == 'show' ? Category::STATUS_ACTIVE : Category::STATUS_NOT_ACTIVE;
                        $subCategory->order = $key2;
                        $subCategory->path = $category->slug . '/' . $child->dir;
                        $subCategory->collapse = 0;
                        $subCategory->creator = $child->creator;
                        $subCategory->changer = $child->changer;
                        $subCategory->save();

                        foreach (Langs::getLangCodes() as $lang){
                            $subCategoryTrans = new CategoryTranslation();
                            $subCategoryTrans->category_id = $subCategory->id;
                            $subCategoryTrans->lang = $lang;
                            $subCategoryTrans->title = $child->name;
                            $subCategoryTrans->save();
                        }

                        echo "Create new subcategory: " . $child->name . PHP_EOL;
                    }

                    if(count($articles)){
                        foreach ($articles as $article){
                            // todo создаем статью

                            $articleModel = BlogArticles::query()->where('agro_id',$article->struct_id)->first();

                            if(! $articleModel){
                                $articleModel = new BlogArticles();
                                $articleModel->agro_id = $article->struct_id;

                                $slug = SlugService::createSlug(BlogArticles::class, 'slug', $article->dir);

                                $articleModel->slug = $slug;
                                $articleModel->status =  $article->showmode == 'show' ? BlogArticles::STATUS_ACTIVE : BlogArticles::STATUS_NOT_ACTIVE;
                                $articleModel->views = $article->views;
                                $articleModel->public_date = $article->date_start;
                                $articleModel->is_blog = $article->alias == 'blogs' ? 1 : 0;
                                $articleModel->creator = $article->creator;
                                $articleModel->changer = $article->changer;
                                $articleModel->category_id = $subCategory->id;
                                $articleModel->save();

                                echo "Create new article: " . $articleModel->id . PHP_EOL;

                                $articleTranslations = $article->items;

                                if(count($articleTranslations)){
                                    foreach ($articleTranslations as $articleTranslation){
                                        // todo перевод до статьи

                                        if($articleTranslation->lang === 'ua') $articleTranslation->lang = 'uk';

                                        $articleModelTrans = new BlogArticleTranslation();
                                        $articleModelTrans->blog_articles_id = $articleModel->id;
                                        $articleModelTrans->lang = $articleTranslation->lang;
                                        $articleModelTrans->name = $articleTranslation->title;
                                        $articleModelTrans->annot = $articleTranslation->annot;
                                        $articleModelTrans->text = $articleTranslation->bodytext;
                                        $articleModelTrans->tags = $articleTranslation->tags;
                                        $articleModelTrans->addtext = $articleTranslation->addtext;
                                        $articleModelTrans->img_alt = $articleTranslation->img_alt1;
                                        $articleModelTrans->video = $articleTranslation->video;
                                        $articleModelTrans->photo = $articleTranslation->photo;
                                         $articleModelTrans->iframe = $articleTranslation->iframe;
                                        $articleModelTrans->save();

                                        $articleModel->interview = $articleTranslation->interview;
                                        $articleModel->save();

                                        if($articleTranslation->author_blog){
                                            $blogger = Bloggers::query()->where('agro_id',$articleTranslation->author_blog)->first();

                                            if($blogger){
                                                $articleModel->blogger_id = $blogger->id;
                                                $articleModel->save();
                                            }
                                        }

                                        // записываем теги в таблиу tags
                                        $newTags = $articleTranslation->tags;
                                        $tagsArray  = $articleTranslation->tags ? explode(',',$newTags) : [];

                                        $lang = $articleTranslation->lang;

                                        if($lang === 'ua') $lang = 'uk';

                                        if (count($tagsArray)) {
                                            foreach ($tagsArray as $tag) {
                                                $t = Tags::firstOrCreate([
                                                    'name' => $tag,
                                                    'lang' => $lang
                                                ]);
                                            }
                                        }

                                        echo "Create new translation from article: " . $articleTranslation->lang . ' - ' . $articleTranslation->title . PHP_EOL;

                                        // если есть изображение
                                        if($articleTranslation->img_src_orig && $articleTranslation->lang == 'ru'){
                                            $imgPath = $this->articleImgPath . $this->getImgFormat($articleTranslation->img_src_orig) . $articleTranslation->img_src_orig;

                                            if(!file_exists($this->uploadPath . $articleTranslation->img_src_orig)){
                                                try {
                                                    file_put_contents($this->uploadPath . $articleTranslation->img_src_orig, file_get_contents($imgPath));
                                                    echo "img uploading" . PHP_EOL;
                                                    $articleModel->image = '/uploads/articles/' . $articleTranslation->img_src_orig;
                                                    $articleModel->save();
                                                } catch (\Throwable $e){
                                                    echo $e->getMessage() . PHP_EOL;
                                                }
                                            } else {
                                                $articleModel->image = '/uploads/articles/' . $articleTranslation->img_src_orig;
                                                $articleModel->save();
                                                echo "img exists" . PHP_EOL;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                // Если нет подкатегорий, могут быть статьи
                $articles = $item->articles;

                if(count($articles)){
                    foreach ($articles as $article){
                        // todo создаем статью

                        $articleModel = BlogArticles::query()->where('agro_id',$article->struct_id)->first();

                        if(! $articleModel){
                            $articleModel = new BlogArticles();
                            $articleModel->agro_id = $article->struct_id;

                            $slug = SlugService::createSlug(BlogArticles::class, 'slug', $article->dir);

                            $articleModel->slug = $slug;

                            $articleModel->status = $article->showmode == 'show' ? BlogArticles::STATUS_ACTIVE : BlogArticles::STATUS_NOT_ACTIVE;
                            $articleModel->views = $article->views;
                            $articleModel->public_date = $article->date_start;
                            $articleModel->is_blog = $article->alias == 'blogs' ? 1 : 0;
                            $articleModel->creator = $article->creator;
                            $articleModel->changer = $article->changer;
                            $articleModel->category_id = $category->id;
                            $articleModel->save();

                            echo "Create new article: " . $articleModel->id . PHP_EOL;

                            $articleTranslations = $article->items;

                            if(count($articleTranslations)){
                                foreach ($articleTranslations as $articleTranslation){
                                    // todo перевод до статьи

                                    if($articleTranslation->lang === 'ua') $articleTranslation->lang = 'uk';

                                    $articleModelTrans = new BlogArticleTranslation();
                                    $articleModelTrans->blog_articles_id = $articleModel->id;
                                    $articleModelTrans->lang = $articleTranslation->lang;
                                    $articleModelTrans->name = $articleTranslation->title;
                                    $articleModelTrans->annot = $articleTranslation->annot;
                                    $articleModelTrans->text = $articleTranslation->bodytext;
                                    $articleModelTrans->tags = $articleTranslation->tags;
                                    $articleModelTrans->addtext = $articleTranslation->addtext;
                                    $articleModelTrans->img_alt = $articleTranslation->img_alt1;
                                    $articleModelTrans->video = $articleTranslation->video;
                                    $articleModelTrans->photo = $articleTranslation->photo;
                                    $articleModelTrans->iframe = $articleTranslation->iframe;
                                    $articleModelTrans->save();

                                    $articleModel->interview = $articleTranslation->interview;
                                    $articleModel->save();

                                    if($articleTranslation->author_blog){
                                        $blogger = Bloggers::query()->where('agro_id',$articleTranslation->author_blog)->first();

                                        if($blogger){
                                            $articleModel->blogger_id = $blogger->id;
                                            $articleModel->save();
                                        }
                                    }

                                    // записываем теги в таблиу tags
                                    $newTags = $articleTranslation->tags;
                                    $tagsArray  = $articleTranslation->tags ? explode(',',$newTags) : [];

                                    $lang = $articleTranslation->lang;

                                    if($lang === 'ua') $lang = 'uk';

                                    if (count($tagsArray)) {
                                        foreach ($tagsArray as $tag) {
                                            $t = Tags::firstOrCreate([
                                                'name' => $tag,
                                                'lang' => $lang
                                            ]);
                                        }
                                    }

                                    echo "Create new translation from article: " . $articleTranslation->lang . ' - ' . $articleTranslation->title . PHP_EOL;

                                    // если есть изображение
                                    if($articleTranslation->img_src_orig && $articleTranslation->lang == 'ru'){
                                        $imgPath = $this->articleImgPath . $this->getImgFormat($articleTranslation->img_src_orig) . $articleTranslation->img_src_orig;
                                        if(!file_exists($this->uploadPath . $articleTranslation->img_src_orig)){
                                            try {
                                                file_put_contents($this->uploadPath . $articleTranslation->img_src_orig, file_get_contents($imgPath));
                                                echo "img uploading" . PHP_EOL;
                                                $articleModel->image = '/uploads/articles/' . $articleTranslation->img_src_orig;
                                                $articleModel->save();
                                            } catch (\Throwable $e){
                                                echo $e->getMessage() . PHP_EOL;
                                            }
                                        } else {
                                            $articleModel->image = '/uploads/articles/' . $articleTranslation->img_src_orig;
                                            $articleModel->save();
                                            echo "img exists" . PHP_EOL;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }
    }
}
