<?php

namespace App\Console\Commands;

use App\Models\Bloggers;
use App\Models\Orig\Struct;
use App\Models\Translations\BloggerTranslation;
use Cviebrock\EloquentSluggable\Services\SlugService;
use Illuminate\Console\Command;

/* todo Первым делом обновить блогеров */

class ParserBloggers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parserBloggers:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse bloggers from agroportal original';

    /**
     * @var string
     *
     * Путь к изображениям статей
     */
    private $articleImgPath;

    private $uploadPath;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->articleImgPath = 'https://agroportal.ua/images/doc/';
        $this->uploadPath     = 'public/storage/media/uploads/authors/';
        parent::__construct();
    }

    private function getImgFormat($src)
    {
        return substr($src, 0, 1) . '/' . substr($src, 1, 1) . '/';
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo 'Start' . PHP_EOL;
        echo '------------------------------' . PHP_EOL;

        $bloggers = Struct::query()
            ->where('alias', 'authors')
            ->where('showmode', 'show')
            ->where('struct_id', '<>', 485)
            ->get();


        foreach ($bloggers as $blogger) {
            $modelBloggers = Bloggers::query()->where('agro_id', $blogger->struct_id)->first();

            // если нет категории, создем ее
            if (!$modelBloggers) {
                $modelBloggers          = new Bloggers();
                $modelBloggers->agro_id = $blogger->struct_id;

                $slug = SlugService::createSlug(Bloggers::class, 'slug', $blogger->dir);

                $modelBloggers->slug    = $slug;
                $modelBloggers->creator = $blogger->creator;
                $modelBloggers->changer = $blogger->changer;
                $modelBloggers->status  = $blogger->showmode == 'show' ? Bloggers::STATUS_ACTIVE : Bloggers::STATUS_NOT_ACTIVE;
                $modelBloggers->views   = $blogger->views;
                $modelBloggers->save();

                $bloggerTranslations = $blogger->combo;

                if (count($bloggerTranslations)) {
                    foreach ($bloggerTranslations as $bloggerTranslation) {
                        if ($bloggerTranslation->lang === 'ua')
                            $bloggerTranslation->lang = 'uk';

                        $modelBloggerTranslation                   = new BloggerTranslation();
                        $modelBloggerTranslation->bloggers_id      = $modelBloggers->id;
                        $modelBloggerTranslation->lang             = $bloggerTranslation->lang;
                        $modelBloggerTranslation->name             = $bloggerTranslation->name;
                        $modelBloggerTranslation->img_alt          = $bloggerTranslation->img_alt1;
                        $modelBloggerTranslation->text_for_main    = $bloggerTranslation->text;
                        $modelBloggerTranslation->text             = $bloggerTranslation->annot;
                        $modelBloggerTranslation->meta_title       = $bloggerTranslation->name;
                        $modelBloggerTranslation->meta_description = $bloggerTranslation->text;
                        $modelBloggerTranslation->save();

                        // если есть изображение
                        if ($bloggerTranslation->img_src_orig && $bloggerTranslation->lang == 'ru') {
                            $imgPath = $this->articleImgPath . $this->getImgFormat($bloggerTranslation->img_src_orig) . $bloggerTranslation->img_src_orig;
                            if (!file_exists($this->uploadPath . $bloggerTranslation->img_src_orig)) {
                                try {
                                    file_put_contents($this->uploadPath . $bloggerTranslation->img_src_orig, file_get_contents($imgPath));
                                    echo "img orig uploading" . PHP_EOL;
                                    $modelBloggers->image = '/uploads/authors/' . $bloggerTranslation->img_src_orig;
                                    $modelBloggers->save();
                                } catch (\Throwable $e) {
                                    echo $e->getMessage() . PHP_EOL;
                                }
                            } else {
                                echo "img orig exits" . PHP_EOL;
                            }
                        }

                        // если есть изображение
                        if ($bloggerTranslation->img_src2 && $bloggerTranslation->lang == 'ru') {
                            $imgPath = $this->articleImgPath . $this->getImgFormat($bloggerTranslation->img_src2) . $bloggerTranslation->img_src2;
                            if (!file_exists($this->uploadPath . $bloggerTranslation->img_src2)) {
                                try {
                                    file_put_contents($this->uploadPath . $bloggerTranslation->img_src2, file_get_contents($imgPath));
                                    echo "img thumbnail uploading" . PHP_EOL;
                                    $modelBloggers->image_thumbnail = '/uploads/authors/' . $bloggerTranslation->img_src2;
                                    $modelBloggers->save();
                                } catch (\Throwable $e) {
                                    echo $e->getMessage() . PHP_EOL;
                                }
                            } else {
                                echo "img thumbnail exits" . PHP_EOL;
                            }
                        }
                    }
                }
            }
        }

    }
}
