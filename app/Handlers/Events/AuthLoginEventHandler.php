<?php


namespace App\Handlers\Events;

use Illuminate\Auth\Events\Login;

class AuthLoginEventHandler
{
    public function handle(Login $event)
    {
        $permissions = [];

        $groups = $event->user->groups;
        if($groups){
            foreach ($groups as $group){
                $groupPermissions = $group->permissions->toArray();

                if($groupPermissions){
                    foreach ($groupPermissions as $item){
                        if($item['on']){
                            $key = $item['section_code'] . '__' . $item['block_code'] . '__' . $item['permission'];

                            if(! in_array($key,$permissions)){
                                $permissions[] = $key;
                            }
                        }
                    }
                }
            }
        }

        session()->put('user_permissions', $permissions);
    }
}
