<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::POST('settings/all', [\App\Http\Controllers\Api\SettingsController::class,'getAll']);
Route::POST('get-main-page', [\App\Http\Controllers\Api\MainController::class,'getMain']);
Route::POST('page/get-by-slug', [\App\Http\Controllers\Api\PagesController::class,'getBySlug']);
Route::POST('landing/get-by-slug', [\App\Http\Controllers\Api\LandingsController::class,'getBySlug']);
Route::POST('menu/get-by-name', [\App\Http\Controllers\Api\MenuController::class,'getByName']);
Route::POST('menu/get-by-id', [\App\Http\Controllers\Api\MenuController::class,'getById']);
Route::POST('menu/get-by-ids', [\App\Http\Controllers\Api\MenuController::class,'getByIds']);
Route::POST('request/add', [\App\Http\Controllers\Api\RequestController::class,'addRequest']);
Route::POST('request/feedback', [\App\Http\Controllers\Api\RequestController::class,'feedback']);
Route::POST('request/callback', [\App\Http\Controllers\Api\RequestController::class,'callback']);

Route::POST('news/get-by-slug', [\App\Http\Controllers\Api\BlogArticlesController::class,'getBySlug']);
Route::POST('news/get-by-slug-model', [\App\Http\Controllers\Api\BlogArticlesController::class,'getBySlugModel']);
Route::POST('news/get-by-slug-aside', [\App\Http\Controllers\Api\BlogArticlesController::class,'getBySlugAside']);
Route::POST('news/get-by-slug-multimedia', [\App\Http\Controllers\Api\BlogArticlesController::class,'getBySlugMultimedia']);
Route::POST('news/get-by-slug-news-by-theme', [\App\Http\Controllers\Api\BlogArticlesController::class,'getBySlugNewsByTheme']);


Route::POST('news/all', [\App\Http\Controllers\Api\BlogArticlesController::class,'getAll']);

Route::POST('news/category', [\App\Http\Controllers\Api\BlogArticlesController::class,'category']);
Route::POST('news/category-only-items', [\App\Http\Controllers\Api\BlogArticlesController::class,'categoryOnlyItems']);
Route::POST('news/category-only-aside-and-breadcrumbs', [\App\Http\Controllers\Api\BlogArticlesController::class,'categoryOnlyAsideAndBreadcrumbs']);


Route::POST('news/search', [\App\Http\Controllers\Api\BlogArticlesController::class,'search']);
Route::POST('news/search-items', [\App\Http\Controllers\Api\BlogArticlesController::class,'searchItems']);
Route::POST('news/search-aside', [\App\Http\Controllers\Api\BlogArticlesController::class,'searchAside']);


Route::POST('categories/get-slug-list', [\App\Http\Controllers\Api\BlogArticlesController::class,'subCategoryList']);

Route::POST('tags/get-by-tag', [\App\Http\Controllers\Api\BlogArticlesController::class,'getByTag']);
Route::POST('tags/get-by-tag-items', [\App\Http\Controllers\Api\BlogArticlesController::class,'getByTagItems']);
Route::POST('tags/get-by-tag-aside', [\App\Http\Controllers\Api\BlogArticlesController::class,'getByTagAside']);


Route::POST('news/subscribe', [\App\Http\Controllers\Api\SubscribeController::class,'subscribe']);
Route::POST('re/get-by-ids', [\App\Http\Controllers\Api\BannersController::class,'getByIds']);

Route::POST('authors/all', [\App\Http\Controllers\Api\BloggersController::class,'getAll']);
Route::POST('authors/get-by-slug', [\App\Http\Controllers\Api\BloggersController::class,'getBySlug']);
Route::POST('authors/get-by-slug-items', [\App\Http\Controllers\Api\BloggersController::class,'getBySlugItems']);
Route::POST('authors/get-by-slug-aside', [\App\Http\Controllers\Api\BloggersController::class,'getBySlugAside']);

Route::POST('re/set-views', [\App\Http\Controllers\Api\BannersController::class,'setViews']);

Route::POST('get-entity-by-url', [\App\Http\Controllers\Api\BlogArticlesController::class,'getEntityByUrl']);

Route::POST('tg', [\App\Http\Controllers\Api\TGController::class,'index']);
Route::POST('rss', [\App\Http\Controllers\Api\RssController::class,'index']);
Route::POST('sitemap', [\App\Http\Controllers\Api\SitemapController::class,'index']);
