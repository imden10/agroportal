<?php

use App\Http\Middleware\LocaleMiddleware;
use Illuminate\Support\Facades\Route;

/* @var $localeMiddleware LocaleMiddleware */
$localeMiddleware = app( LocaleMiddleware::class);

\Illuminate\Support\Facades\Auth::routes();

Route::get('/admin/cache-clear',function (){
    \Illuminate\Support\Facades\Artisan::call('cache:clear');
    \Illuminate\Support\Facades\Artisan::call('view:clear');

    return redirect()->back()->with('success','Кеш успешно очищено!');
})->name('cache-clear');

Route::get('/admin/rename-index',function (){
//   $oldTagNameUk = "Індекс АПК";
//   $oldTagNameRu = "Индекс АПК";
//   $replaceUk = "Регіони";
//   $replaceRu = "Регионы";
//
//   $modelUk = \App\Models\Translations\BlogArticleTranslation::query()
//       ->where('tags','like','%'.$oldTagNameUk.'%')
//       ->get();
//
//   foreach ($modelUk as $item){
//       $tags = $item->tags;
//       $item->tags = mb_str_replace($oldTagNameUk,$replaceUk,$tags);
//       $item->save();
//   }
//
//    $modelRu = \App\Models\Translations\BlogArticleTranslation::query()
//        ->where('tags','like','%'.$oldTagNameRu.'%')
//        ->get();
//
//    foreach ($modelRu as $item){
//        $tags = $item->tags;
//        $item->tags = mb_str_replace($oldTagNameRu,$replaceRu,$tags);
//        $item->save();
//    }
//
//    dd('Ready');

});

//Route::get('rss-generate', function (){
//    \Illuminate\Support\Facades\Artisan::call('rss:generate');
//
//    dd('RSS is generated!');
//});
//
//Route::get('sitemap-gen',function (){
//    \Illuminate\Support\Facades\Artisan::call('sitemap:generate');
//    dd('ready');
//});
//
//Route::get('article-alt-generate',function (){
//    \Illuminate\Support\Facades\Artisan::call('altEmpty:generate');
//    dd('ready');
//});
//
//Route::get('parser-meta-tags-start',function (){
//    \Illuminate\Support\Facades\Artisan::call('ParserMetaTags:start');
//    dd('ready');
//});

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any('/admin/login', [\App\Http\Controllers\Admin\HomeController::class,'login'])->name('admin.login');

/*********************************************** ADMIN ****************************************************************/
Route::group(['prefix' => 'admin', 'middleware' => ['admin']], function () {
    Route::post('/export-table', [\App\Http\Controllers\Admin\ExportTableController::class,'export'])->name('export-table');
    Route::get('/', [\App\Http\Controllers\Admin\HomeController::class,'index'])->name('admin');
    Route::resource('/users-admin', \App\Http\Controllers\Admin\UserAdminController::class);
    Route::resource('/access-groups', \App\Http\Controllers\Admin\AccessGroupsController::class);
    Route::get('/users/{id}/{tab}', [\App\Http\Controllers\Admin\UsersController::class,'getTab'])->where('tab', '[a-z0-9-]+');
    Route::post('/users-change-status', [\App\Http\Controllers\Admin\UsersController::class,'changeStatus'])->name('user.change-status');
    Route::resource('/users', \App\Http\Controllers\Admin\UsersController::class);
    Route::post('/pages/menu/add-item', [\App\Http\Controllers\Admin\PagesController::class,'addItem'])->name('page.add-item');
    Route::resource('/pages', \App\Http\Controllers\Admin\PagesController::class);
    Route::post('menu/toggle-collapse', [\App\Http\Controllers\Admin\MenuController::class,'toggleCollapse'])->name('menu.toggle-collapse');
    Route::post('menu/move', [\App\Http\Controllers\Admin\MenuController::class,'move']);
    Route::post('menu/rebuild', [\App\Http\Controllers\Admin\MenuController::class,'rebuild']);
    Route::post('menu/add-item', [\App\Http\Controllers\Admin\MenuController::class,'addItem'])->name('menu.add-item');
    Route::post('menu/delete-menu', [\App\Http\Controllers\Admin\MenuController::class,'deleteMenu'])->name('menu.delete-menu');
    Route::post('menu/add-menu', [\App\Http\Controllers\Admin\MenuController::class,'addMenu'])->name('menu.add-menu');
    Route::resource('/menu', \App\Http\Controllers\Admin\MenuController::class);
    Route::resource('/faq', \App\Http\Controllers\Admin\FaqController::class);
    Route::get('settings/{tab}', [\App\Http\Controllers\Admin\SettingsController::class,'index'])->name('add-notice')->where('tab', '[a-z0-9-]+');
    Route::post('settings/save', [\App\Http\Controllers\Admin\SettingsController::class,'save'])->name('settings.save');
    Route::post('/request/status/change', [\App\Http\Controllers\Admin\RequestController::class,'changeStatus'])->name('request.status-change');
    Route::resource('/request', \App\Http\Controllers\Admin\RequestController::class);

    Route::post('/bloggers/delete-selected', [\App\Http\Controllers\Admin\BloggersController::class,'deleteSelected'])->name('bloggers.delete-selected');
    Route::resource('/bloggers', \App\Http\Controllers\Admin\BloggersController::class);

    Route::post('/remove-item-block', [\App\Http\Controllers\Admin\BannersController::class,'removeItem'])->name('banners.remove-item');
    Route::post('/banners/add-empty-item', [\App\Http\Controllers\Admin\BannersController::class,'addEmptyItem'])->name('banner.add-empty-item');
    Route::post('/banner-stat/export', [\App\Http\Controllers\Admin\AnalyticsController::class,'export'])->name('banner-stat.export');
    Route::resource('/banner-stat', \App\Http\Controllers\Admin\AnalyticsController::class);
    Route::resource('/banners', \App\Http\Controllers\Admin\BannersController::class);

    /** BLOG **/
    Route::post('/categories/collapse', [\App\Http\Controllers\Admin\CategoriesController::class,'collapse'])->name('set-collapse');
    Route::resource('/categories', \App\Http\Controllers\Admin\CategoriesController::class);
    Route::get('/category/articles-create-with-category', [\App\Http\Controllers\Admin\CategoriesController::class,'createArticleWithCategory'])->name('articles.create-with-category');
    Route::get('/category/{path}/{sub?}', [\App\Http\Controllers\Admin\CategoriesController::class,'category'])->where('path', '[a-z0-9-]+')->where('sub', '[a-z0-9-]+');
    Route::group(['prefix' => 'blog'], function () {
        Route::resource('/tags', \App\Http\Controllers\Admin\BlogTagsController::class);
        Route::post('/articles/delete-selected', [\App\Http\Controllers\Admin\BlogArticlesController::class,'deleteSelected'])->name('articles.delete-selected');
        Route::get('/article/search',[\App\Http\Controllers\Admin\BlogArticlesController::class,'search'])->name('article.search');

        Route::post('/articles/delete-gallery', [\App\Http\Controllers\Admin\BlogArticlesController::class,'deleteFromGallery'])->name('articles.delete-gallery');
        Route::post('/articles/add-gallery', [\App\Http\Controllers\Admin\BlogArticlesController::class,'addGallery'])->name('articles.add-gallery');
        Route::resource('/articles', \App\Http\Controllers\Admin\BlogArticlesController::class);
        Route::delete('/subscribe/delete', [\App\Http\Controllers\Admin\BlogSubscribeController::class, 'delete'])->name('subscribe.delete');
        Route::get('/subscribe/list', [\App\Http\Controllers\Admin\BlogSubscribeController::class,'list'])->name('subscribe.list');
    });

    Route::get('/subscribe', [\App\Http\Controllers\Admin\BlogSubscribeController::class,'index'])->name('subscribe.index');

    Route::get('/tag/binding',[\App\Http\Controllers\Admin\TagsController::class,'binding'])->name('tag.binding');
    Route::post('/tag/tie',[\App\Http\Controllers\Admin\TagsController::class,'tie'])->name('tag.tie');
    Route::get('/tag/search',[\App\Http\Controllers\Admin\TagsController::class,'search'])->name('tags.search');
    Route::resource('/tag',\App\Http\Controllers\Admin\TagsController::class);

    Route::get('/multimedia/files',[\App\Http\Controllers\Admin\FileManagerController::class,'file'])->name('multimedia.files');
    Route::get('/multimedia/images',[\App\Http\Controllers\Admin\FileManagerController::class,'image'])->name('multimedia.images');

    Route::get('/landing/search',[\App\Http\Controllers\Admin\LandingController::class,'search'])->name('landing.search');
    Route::resource('/landing', \App\Http\Controllers\Admin\LandingController::class);
});
/************************************ END ADMIN ***********************************************************************/

Route::group(['namespace' => 'Front'], function(){
    Route::get('/rss/{id}', [\App\Http\Controllers\Front\RssController::class,'indexUk']);
    Route::get('/sitemap.xml', [\App\Http\Controllers\Front\RssController::class,'sitemap']);
    Route::get('{lang}/rss/{id}', [\App\Http\Controllers\Front\RssController::class,'index']);
    Route::any('/tg/{id}', [\App\Http\Controllers\Front\AdController::class,'index'])->where('id', '^(?!api).*$');

    Route::any('/{any}', [\App\Http\Controllers\Front\HomeController::class,'index'])->where('any', '^(?!api).*$')->name('home');
});
/*******************************************************************/
