import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

Vue.config.productionTip = false;
import {
    utils
} from '@/utils.js';
import accord from '@/textAccordion.js';

Vue.mixin(utils)
Vue.mixin(accord)

import MainLayout from '@/layout/MainLayout.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import i18n from './i18n'

import VueCookies from 'vue-cookies'
Vue.use(VueCookies)

Vue.use(require('vue-moment'))

Vue.component('main-layout', MainLayout)

router.beforeEach((to, from, next) => {
    // console.log(to.fullPath);
    console.log(to.name);
    i18n.locale = to.params.locale || 'uk';

    if (store.state.global.subRubrics == null ||
        store.state.global.rubric == null ||
        store.state.global.pageSlugs == null) {
        axios.post('/api/categories/get-slug-list').then(response => {
            store.commit('changeSubRubrics', response.data.data.list);
            store.commit('changeRubrics', response.data.data.main);
            store.commit('changePageSlugs', response.data.data.pages.splice(1, response.data.data.pages.length));
        });
    }

    // console.log(store.state.global.subRubrics);
    // console.log(store.state.global.rubric);
    // console.log(store.state.global.pageSlugs);

    if (to.name == 'RubricArticle') {
        const prevw = to.query.prevw
        if (store.state.global.subRubrics == null) {
            axios.post('/api/categories/get-slug-list').then(response => {
                store.commit('changeSubRubrics', response.data.data.list);
                let subRubrics = store.state.global.subRubrics;
                let result = subRubrics.find(item => item == to.params.subRubric);
                // console.log(result);
                if (result) next();
                else if (to.fullPath.split('/').splice(1, 1).join() == 'authors' ||
                    to.fullPath.split('/').splice(1, 2).join() == 'ru,authors') {
                    next({
                        name: 'BlogerArticle',
                        params: {
                            slug: to.params.subRubric,
                            locale: i18n.locale == 'uk' ? null : i18n.locale,
                        }
                    });
                } else if (to.fullPath.split('/').splice(1, 1).join() == 'project' ||
                    to.fullPath.split('/').splice(1, 2).join() == 'ru,project') {
                    console.log(to);
                    next({
                        name: 'Project',
                        params: {
                            projectName: to.params.subRubric,
                            locale: i18n.locale == 'uk' ? null : i18n.locale,
                        }
                    });
                } else {
                    if (to.query.prevw) {
                        next({
                            name: 'noSubrubricArticle',
                            params: {
                                rubric: to.params.rubric,
                                slug: to.params.subRubric,
                                locale: i18n.locale == 'uk' ? null : i18n.locale,
                            },
                            query: {
                                prevw: prevw ? prevw : null
                            }
                        });
                    } else {
                        next({
                            name: 'noSubrubricArticle',
                            params: {
                                rubric: to.params.rubric,
                                slug: to.params.subRubric,
                                locale: i18n.locale == 'uk' ? null : i18n.locale,
                            },
                        });
                    }
                }
            })
        } else {
            let subRubrics = store.state.global.subRubrics;
            let result = subRubrics.find(item => item == to.params.subRubric)
            if (result) next();
            else if (to.fullPath.split('/').splice(1, 1).join() == 'project' ||
                to.fullPath.split('/').splice(1, 2).join() == 'ru,project') {
                console.log(to);
                next({
                    name: 'Project',
                    params: {
                        projectName: to.params.subRubric,
                        locale: i18n.locale == 'uk' ? null : i18n.locale,
                    }
                });
            } else {
                next({
                    name: 'noSubrubricArticle',
                    params: {
                        rubric: to.params.rubric,
                        slug: to.params.subRubric,
                        locale: i18n.locale == 'uk' ? null : i18n.locale
                    },
                });
            }
        }
    } else if (to.name == 'Rubric') {
        if (store.state.global.rubric == null) {
            axios.post('/api/categories/get-slug-list').then(response => {
                store.commit('changeRubrics', response.data.data.main);
                let Rubrics = store.state.global.rubrics;
                let result = Rubrics.find(item => item == to.params.rubric);
                if (result) {
                    next();
                } else if (to.fullPath == '/authors' || to.fullPath.split('/').splice(1, 2).join() == 'ru,authors') {
                    next({
                        name: 'Autors',
                        params: {
                            locale: i18n.locale == 'uk' ? null : i18n.locale
                        }
                    });
                } else {
                    next({
                        name: 'TextPage',
                        params: {
                            textPageSlug: to.params.rubric,
                            locale: i18n.locale == 'uk' ? null : i18n.locale
                        }
                    });
                }
            })
        } else {
            let Rubrics = store.state.global.rubrics;
            let result = Rubrics.find(item => item == to.params.rubric);
            if (result) next();
            else {
                next({
                    name: 'TextPage',
                    params: {
                        textPageSlug: to.params.rubric,
                        locale: i18n.locale == 'uk' ? null : i18n.locale
                    }
                });
            }
        }
    }
    // else if ( to.name == 'Archive' ) {

    // }
    else next();
});

Vue.use(VueAxios, axios)
new Vue({
    router,
    store,
    i18n,
    render: (h) => h(App),
}).$mount("#app");
