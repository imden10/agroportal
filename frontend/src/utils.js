// const MEDIA_PATH_PREFIX = '/storage/app/public/media';
const MEDIA_PATH_PREFIX = '/storage/media';
const FILE_PATH_PREFIX = '/storage/app/public/files';

let utils = {
    props: ['compdata'],
    computed: {
        currentLang() {
            return this.$i18n.locale || 'uk'
        }
    },
    methods: {
        bannerLink(link) {
            return link
        },
        refreshSlider(refname) {
            this.$refs[refname].resize();
        },
        path(s) {
            if (process.env.NODE_ENV == 'development')
                return `https://agroportal.ua/${MEDIA_PATH_PREFIX}${s}`;
            // return `http://2124023.mf417489.web.hosting-test.net/${MEDIA_PATH_PREFIX}${s}`;
            // return `./${MEDIA_PATH_PREFIX}${s}`;
            else return MEDIA_PATH_PREFIX + s;
        },
        pathVideo(s) {
            if (process.env.NODE_ENV == 'development')
                return `https://agroportal.ua/${s}`;
                // return `http://2124023.mf417489.web.hosting-test.net/${s}`;
            // return `./${MEDIA_PATH_PREFIX}${s}`;
            else return MEDIA_PATH_PREFIX + s;
        },
        pathFile(s) {
            if (process.env.NODE_ENV == 'development')
                return `./${FILE_PATH_PREFIX}${s}`;
            else return FILE_PATH_PREFIX + s;
        },
        isExtUrl(s) {
            return s.includes('http')
        },


        // Input methods
        onFieldChange(e) {
            this[e.name] = e.value;
        },
        onPhoneChange(e) {
            this[e.name] = e.value;
            this.phoneLength = e.length;
        },
    },
}

export let rtParams = {
    ruLocaleParam() {
        let p = {};
        for (const [key, value] of Object.entries(this.$route.params)) {
            p[key] = value;
        }
        p.locale = 'ru';
        return p;
    },
    uaLocaleParam() {
        let p = {};
        for (const [key, value] of Object.entries(this.$route.params)) {
            p[key] = value;
        }
        p.locale = null;
        return p;
    },
};


export {
    utils
}
