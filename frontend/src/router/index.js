import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../pages/Home.vue";
import Main from "@/layout/MainLayout.vue";
import Project from "@/layout/ProjectLayout.vue";

Vue.use(VueRouter);

const routes = [{
        path: "/:locale(uk|ru)?",
        component: Main,
        children: [{
                path: "/",
                name: "Home",
                component: Home,
            },
            {
                path: "tags/",
                name: 'Archive',
                component: () => import( /* webpackChunkName: "archive" */ '@/pages/Archive.vue'),
                props: (route) => ({
                    query: route.query.tag
                })
            },
            {
                path: "tags/:tag",
                name: 'TagPage',
                component: () => import( /* webpackChunkName: "archive" */ '@/pages/Archive.vue'),
            },
            {
                path: "search/",
                name: 'Search',
                component: () => import( /* webpackChunkName: "search" */ '@/pages/Search.vue'),
                props: (route) => ({
                    query: route.query.q,
                    page: route.query.page,
                    t: route.query.t
                })
            },
            {
                path: ":rubric",
                name: "Rubric",
                component: () => import( /* webpackChunkName: "rubric" */ '@/pages/News.vue'),
                props: (route) => ({
                    page: route.query.page
                }),
                meta: {
                    layout: 'main'
                }
            },
            {
                path: ":rubric/:subRubric",
                name: "RubricArticle",
                component: () => import( /* webpackChunkName: "rubric" */ '@/pages/News.vue'),
                props: (route) => ({
                    page: route.query.page
                }),
                meta: {
                    layout: 'main'
                }
            },
            {
                path: ":rubric/:slug",
                name: "noSubrubricArticle",
                component: () => import( /* webpackChunkName: "rubric-article" */ '@/pages/Article.vue'),
                // props: (route) => ({
                //     prevw: route.query.prevw
                // }),
                meta: {
                    layout: 'main'
                }
            },
            {
                path: ":rubric/:subRubric/:slug",
                name: 'Article',
                component: () => import( /* webpackChunkName: "rubric-article" */ '@/pages/Article.vue'),
                params: true
            },
            {
                path: ":textPageSlug",
                name: 'TextPage',
                component: () => import( /* webpackChunkName: "textPage" */ '@/pages/TextPage.vue'),
                params: true
            },
            {
                path: "authors/",
                name: 'Autors',
                component: () => import( /* webpackChunkName: "BlogerArticle" */ '@/pages/Authors.vue'),
                params: true
            },
            {
                path: "authors/:slug",
                name: 'BlogerArticle',
                component: () => import( /* webpackChunkName: "BlogerArticle" */ '@/pages/BlogerArticle.vue'),
                params: true
            },
        ],
    },
    {
        path: "/:locale(uk|ru)?/project/:projectName",
        component: Project,
        name: 'Project',
        meta: {
            layout: 'project'
        }
    },
    {
        path: '404',
        name: 'error404',
        component: () => import( /* webpackChunkName: "errorPage" */ '@/layout/ErrorLayout.vue'),
    }
];

const router = new VueRouter({
    mode: "history",
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) return savedPosition

        if (to.hash) {
            return {selector: to.hash}
        } else {
            return { x: 0, y: 0 }
        }
    },
    routes,
});

export default router;
