export default {
    state: {
		subRubrics: null,
        rubrics: null,
        socialLinks: null,
        pageSlugs: null,
        hedaerSocialLink: null,
        footerData: null,
        pagiLoad: null
    },
    mutations: {
		changeSubRubrics(state, data) {
            state.subRubrics = data
        },
		changeRubrics(state, data) {
            state.rubrics = data
        },
        changeSocialLinks(state, data) {
            state.socialLinks = data
        },
        changePageSlugs(state, data) {
            state.pageSlugs = data
        },
        changeHedaerSocialLink(state, data) {
            state.hedaerSocialLink = data
        },
        changeFooterData(state, data) {
            state.footerData = data
        },
        changePagiLoad(state, data) {
            state.pagiLoad = data
        }
    },
    actions: {
		setSubRubrics({commit}, data){
			commit('changeSubRubrics', data)
		},
		setRubrics({commit}, data){
			commit('changeRubrics', data)
		},
        setSocialLinks({ commit }, data) {
            commit('changeSocialLinks', data)
        },
        setHedaerSocialLink({commit}, data) {
            commit('changeHedaerSocialLink', data)
        },
        setFooterData({commit}, data) {
            commit('changeFooterData', data)
        },
        setPagiLoad({commit}, data) {
            commit('changePagiLoad', data)
        }
    },
    getters: {
        getSocialLinks(state) {
            return state.socialLinks ? state.socialLinks : null
        },
        getRubrics(state) {
            return state.rubrics
        },
        getPageSlugs(state) {
            return state.pageSlugs
        },
        getHedaerSocialLink(state){
            return state.hedaerSocialLink ? state.hedaerSocialLink : null
        },
        getFooterData(state) {
            return state.footerData ? state.footerData : null
        },
            getPagiLoad(state) {
            return state.pagiLoad ? state.pagiLoad : null
        }
    },
};
