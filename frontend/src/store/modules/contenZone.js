export default {
    state: {
		block1: null,
		block2: null,
		block3: null,
		block4: null,
        blogData: null,
        articleData: null,
        newsData: null
    },
    mutations: {
        changeBlock1(state, block) {
            state.block1 = block
        },
        changeBlock2(state, block) {
            state.block2 = block
        },
        changeBlock3(state, block) {
            state.block3 = block
        },
        changeBlock4(state, block) {
            state.block4 = block
        },
        changeBlogData(state, data) {
            state.blogData = data
        },
        changeMainArticlesData(state, data) {
            state.articleData = data
        },
        changeNewsData(state, data){
            state.newsData = data
        }
	},
	actions: {
        setBlock1({commit}, block){
            commit('changeBlock1', block)
        },
        setBlock2({commit}, block){
            commit('changeBlock2', block)
        },
        setBlock3({commit}, block){
            commit('changeBlock3', block)
        },
        setBlock4({commit}, block){
            commit('changeBlock4', block)
        },
        setBlogData({commit}, data) {
            commit('changeBlogData', data)
        },
        setMainArticlesData({commit}, data) {
            commit('changeMainArticlesData', data)
        },
        setNewsData({commit}, data) {
            commit('changeNewsData', data)
        }
	},
	getters: {
        getBlock1(state) {
            return state.block1
        },
        getBlock2(state) {
            return state.block2
        },
        getBlock3(state) {
            return state.block3
        },
        getBlock4(state) {
            return state.block4
        },
        getBlogData(state) {
            return state.blogData
        },
        getMainArticlesData(state) {
            return state.articleData
        },
        getNewsData(state) {
            return state.newsData
        }
	},
}
