export default {
    state: {
      banners: {},
      nativeBanner: []
    },
    mutations: {
        changeBanners(state, data) {
            state.banners = data
        },
        changeNativeBanner(state, banner) {
            state.nativeBanner = banner
        }
    },
    actions: {
        setBanners({commit}, data) {
            commit('changeBanners', data)
        },
        setNativeBanner({commit}, banner) {
            commit('changeNativeBanner', banner)
        }
    },
    getters: {
        getBanners(state) {
            return Object.keys(state.banners).length > 0 ? state.banners : null
        },
        getNativeBanner(state){
            return state.nativeBanner
        }
    },
};
