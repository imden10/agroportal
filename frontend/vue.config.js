module.exports = {
    devServer: {
        proxy: {
            '^/api': {
                target: 'https://agroportal.ua/',
                // target: 'http://2124023.mf417489.web.hosting-test.net/',
                changeOrigin: true,
                secure: false,
                pathRewrite: {
                    '^/api': '/api'
                },
                logLevel: 'debug'
            },
        },
    },

    css: {
        loaderOptions: {
            sass: {
                prependData: "@import '~@/assets/sass/variables'",
            },
        },
    },

    // output built static files to Laravel's public dir.
    // note the "build" script in package.json needs to be modified as well.
    outputDir: '../public/assets/app',

    publicPath: process.env.NODE_ENV === 'production' ?
        '/assets/app/' : '/',

    // modify the location of the generated HTML file.
    indexPath: process.env.NODE_ENV === 'production' ?
        '../../../resources/views/front/home/frontend.blade.php' : 'index.html',

    pluginOptions: {
      i18n: {
        locale: 'uk',
        fallbackLocale: 'ru',
        localeDir: 'locales',
        enableInSFC: true,
        includeLocales: false,
        enableBridge: true
      }
    }
}
