function ReplaceButtonTagsAll(context) {
    var ui = $.summernote.ui;
    var button = ui.button({
        contents: '<i class="fa fa-code"/> Очистить все',
        tooltip: 'Очистить все форматироввание',
        click: function (e) {
            let text = $(context.$note.summernote('code')).text();

            context.$note.summernote("code", text);
        }
    });

    return button.render();
}
