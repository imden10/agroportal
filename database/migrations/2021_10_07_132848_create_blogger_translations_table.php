<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBloggerTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogger_translations', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('bloggers_id');
            $table->string('lang',10);
            $table->string('name',255)->nullable();
            $table->string('img_alt',255)->nullable();
            $table->text('text_for_main')->nullable();
            $table->text('text')->nullable();
            $table->string('meta_title',255)->nullable();
            $table->string('meta_description',255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogger_translations');
    }
}
