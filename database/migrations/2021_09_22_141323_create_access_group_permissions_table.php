<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccessGroupPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('access_group_permissions', function (Blueprint $table) {
            $table->id();
            $table->integer('group_id');
            $table->string('section_code', 255);
            $table->string('block_code', 255);
            $table->string('permission', 255);
            $table->boolean('on')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('access_group_permissions');
    }
}
