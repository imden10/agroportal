<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBloggersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bloggers', function (Blueprint $table) {
            $table->id();
            $table->integer('agro_id');
            $table->string('slug',255)->unique();
            $table->string('image',255)->nullable();
            $table->string('image_thumbnail',255)->nullable();
            $table->string('creator',255)->nullable();
            $table->string('changer',255)->nullable();
            $table->tinyInteger('status');
            $table->integer('views');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bloggers');
    }
}
