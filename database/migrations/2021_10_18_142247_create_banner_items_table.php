<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannerItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner_items', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('banners_id');
            $table->string('img_desktop',255)->nullable();
            $table->string('img2_desktop',255)->nullable();
            $table->string('img_desktop_size',255)->nullable();
            $table->string('img_desktop_alt',255)->nullable();

            $table->string('img_desktop2',255)->nullable();
            $table->string('img2_desktop2',255)->nullable();
            $table->string('img_desktop2_size',255)->nullable();
            $table->string('img_desktop2_alt',255)->nullable();

            $table->string('img_tablet',255)->nullable();
            $table->string('img_tablet_size',255)->nullable();
            $table->string('img_tablet_alt',255)->nullable();

            $table->string('img_mob',255)->nullable();
            $table->string('img_mob_size',255)->nullable();
            $table->string('img_mob_alt',255)->nullable();

            $table->string('link',255)->nullable();
            $table->text('html')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner_items');
    }
}
