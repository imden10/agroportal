<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannerItemTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner_item_translations', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('banner_items_id');
            $table->string('lang',10);
            $table->string('text',255)->nullable();
            $table->string('btn_name',255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner_item_translations');
    }
}
