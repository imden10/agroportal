@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item"><a href="{{route('request.index')}}">Онлайн записи</a></li>
            <li class="breadcrumb-item active" aria-current="page">Редактировать</li>
        </ol>
    </nav>

    <form class="form-horizontal" method="POST" action="{{route('request.update', $model->id)}}">
        @method('PUT')
        @csrf

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        @include('admin.request._form',['model' => $model])

                        <input type="submit" value="Сохранить" class="btn btn-success text-white float-right">
                    </div>
                </div>
            </div>
        </div>

    </form>
@endsection
