<?php
$items = $model->items;
$limit = 3;
?>

<ul class="nav nav-tabs" id="myTab2" role="tablist" style="margin-bottom: -10px;border-bottom: none">
    @if(count($items))
        @foreach($items as $key => $item)
            <li class="nav-item" data-item_id="{{$item->id}}">
                <a class="nav-link @if($loop->first) active @endif" id="block-{{$item->id}}-tab" data-toggle="tab" href="#block-{{$item->id}}" role="tab" aria-controls="block-{{$item->id}}">Блок {{$key + 1}}</a>
            </li>
        @endforeach
    @endif
    @if(count($items) < $limit)
        <li class="nav-item add-new-block">
            <a class="nav-link" style="cursor: pointer" title="Добавить"><i class="fa fa-plus"></i></a>
        </li>
    @endif
</ul>

<br>

<div class="tab-content" id="myTabContent2">
    @if(count($items))
        @foreach($items as $key => $item)
            <div class="tab-pane fade @if($loop->first) show active @endif" id="block-{{$item->id}}" role="tabpanel" aria-labelledby="block-{{$item->id}}-tab">
                @include('admin.banners._item_form',[
                    'model' => $model,
                    'item'  => $item
                ])
            </div>
        @endforeach
    @endif
</div>

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
@endpush

@push('scripts')
    <script src="{{asset('matrix/libs/moment/moment.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script>
        $(document).ready(function () {
            $(".add-new-block").on('click',function () {
                $("#form_add_item").trigger('click');
            });

            $(".active-datetime").each(function () {
                $(this).datetimepicker({
                    format: 'DD-MM-YYYY HH:mm',
                    useCurrent: true,
                    icons: {
                        time: "fa fa-clock",
                        date: "fa fa-calendar",
                        up: "fa fa-arrow-up",
                        down: "fa fa-arrow-down",
                        previous: "fa fa-chevron-left",
                        next: "fa fa-chevron-right",
                        today: "fa fa-clock",
                        clear: "fa fa-trash"
                    }
                });
            });
        });
    </script>
@endpush
