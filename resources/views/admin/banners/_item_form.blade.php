<div class="form-group row">
    <label class="col-md-2 text-right" for="item_name_{{$item->id}}">Название</label>
    <div class="col-md-10">
        <input type="text" name="items[{{$item->id}}][name]" value="{{ old('items['.$item->id.'][name]', $item->name ?? '') }}" id="item_name_{{$item->id}}" class="form-control">
    </div>
</div>

@include('admin.banners.forms._banner_' . $model->id,[
    'model' => $model,
    'item'  => $item
])

<div class="form-group row">
    <div class="col-md-12">
        <span class="btn btn-danger pull-right text-white delete-block" data-item_id="{{$item->id}}" title="Удалить блок"><i class="fa fa-trash"></i></span>
    </div>
</div>

