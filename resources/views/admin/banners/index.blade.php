@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item">Баннеры</li>
        </ol>
    </nav>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
{{--                <a href="{{route('banners.create')}}" class="btn btn-success float-right text-white">--}}
{{--                    <i class="fa fa-plus"></i>--}}
{{--                    Добавить--}}
{{--                </a>--}}
            </div>
            <div class="card-body">

                <form action="" method="get">
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label>Тип</label>
                            <select name="type" class="select2 form-control m-t-15">
                                <option value="">---</option>
                                @foreach(\App\Models\Banners::getTypes() as $key => $item)
                                    <option value="{{$key}}" @if(old('type', request()->input('type')) == (string)$key) selected @endif>{{$item}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-1">
                            <label for="inputPassword4">&nbsp;</label>
                            <button type="submit" class="btn btn-success form-control text-white">Фильтровать</button>
                        </div>
                        <div class="form-group col-md-1">
                            <label for="inputPassword4">&nbsp;</label>
                            <a href="{{ route('banners.index') }}" class="btn btn-danger form-control text-white">Сбросить</a>
                        </div>
                    </div>
                </form>

                <table class="table table-bordered imSortingTableLib">
                    <thead>
                    <tr>
                        <th style="min-width: 150px" class="sorting @if(request()->input('sort') == 'name' && request()->input('order') == 'asc') sorting_asc @elseif(request()->input('sort') == 'name' && request()->input('order') == 'desc') sorting_desc @endif" data-field="name">Название</th>
                        <th>Тип</th>
                        <th>Статус</th>
                        <th style="min-width: 150px" class="sorting @if(request()->input('sort') == 'created_at' && request()->input('order') == 'asc') sorting_asc @elseif(request()->input('sort') == 'created_at' && request()->input('order') == 'desc') sorting_desc @endif" data-field="created_at">Дата создания</th>
                        <th style="min-width: 150px"></th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($model as $item)
                            <tr data-id="{{$item->id}}">
                                <td>
                                    @permission('banners__banners__edit')
                                        <a href="{{ route('banners.edit', $item->id) }}">
                                            {{$item->name}}
                                        </a>
                                    @else
                                        {{$item->name}}
                                    @endpermission
                                </td>
                                <td>{{\App\Models\Banners::getTypes()[$item->type]}}</td>
                                <td>{!! $item->showStatus() !!}</td>
                                <td>{{$item->created_at->format('d-m-Y H:i')}}</td>
                                <td>
                                    <form action="{{ route('banners.destroy', $item->id) }}" method="POST">
                                        @permission('banners__banners__edit')
                                            <a href="{{ route('banners.edit', $item->id) }}" class="fa fa-edit btn btn-xs alert-primary"></a>
                                        @endpermission

                                        @csrf
                                        @method('DELETE')

                                        <a href="javascript:void(0)" title="Удалить" class="fa fa-trash btn alert-danger btn-xs delete-item-btn"></a>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $model->appends(request()->all())->links() }}
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
    <style>
        table.imSortingTableLib th.sorting {
            position: relative;
        }

        table.imSortingTableLib th.sorting::before {
            right: 1em;
            content: "\2191";
            opacity: 0.5;
            position: absolute;
        }

        table.imSortingTableLib th.sorting::after {
            right: 0.5em;
            content: "\2193";
            opacity: 0.5;
            position: absolute;
        }

        table.imSortingTableLib th.sorting.sorting_asc::before {
            opacity: 1;
        }

        table.imSortingTableLib th.sorting.sorting_desc::after {
            opacity: 1;
        }
    </style>
@endpush

@push('scripts')
    <script>
        function insertParam(key, value,url = null) {
            key = encodeURIComponent(key);
            value = encodeURIComponent(value);

            if(url){
                var kvp = url.substr(1).split('&');
            } else {
                var kvp = document.location.search.substr(1).split('&');
            }
            let i=0;

            for(; i<kvp.length; i++){
                if (kvp[i].startsWith(key + '=')) {
                    let pair = kvp[i].split('=');
                    pair[1] = value;
                    kvp[i] = pair.join('=');
                    break;
                }
            }

            if(i >= kvp.length){
                kvp[kvp.length] = [key,value].join('=');
            }

            // can return this or...
            let params = kvp.join('&');

            return params;
        }

        $(document).ready(() => {
            $('.delete-item-btn').on('click',function() {
                let _this = $(this);
                Swal.fire({
                    title: 'Вы уверенны?',
                    text: "Вы пытаетесь удалить эту запись?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Да, сделать это!',
                    cancelButtonText: 'Нет'
                }).then((result) => {
                    if (result.value) {
                        _this.closest('form').submit();
                    }
                });
            });

            $('.check-all').on('change',function () {
                if($(this).prop('checked')){
                    $('.checkbox-item').prop('checked',true);
                } else {
                    $('.checkbox-item').prop('checked',false);
                }
            });

            $('.checkbox-item').on('change',function () {
                let checked = 0;
                $('.checkbox-item').each(function () {
                    if($(this).prop('checked')){
                        checked++;
                    }
                });

                if($('.checkbox-item').length === checked){
                    $('.check-all').prop('checked',true)
                } else {
                    $('.check-all').prop('checked',false)
                }
            });

            $('.btn-delete-checked').on('click',function () {
                let checked = [];

                $('.checkbox-item').each(function () {
                    if($(this).prop('checked')){
                        checked.push($(this).val());
                    }
                });

                if(checked.length == 0){
                    Swal.fire({
                        position: 'top-end',
                        icon: 'warning',
                        title: 'Нет выбранных элементов',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    Swal.fire({
                        title: 'Вы уверенны?',
                        text: "Вы пытаетесь удалить публикации!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Да, сделать это!',
                        cancelButtonText: 'Нет'
                    }).then((result) => {
                        if (result.value) {
                            $('#delete_sel_form').find('input[name="ids"]').val(JSON.stringify(checked));
                            $('#delete_sel_form').submit();
                        }
                    });
                }
            });

            $('input[name="p"]').on('apply.daterangepicker', function(ev, picker) {
                $(".filter-btn-apply").trigger('click');
            });

            /* SORTING*/
            $(document).on('click',"table.imSortingTableLib th.sorting",function () {
                $("table.imSortingTableLib th.sorting").removeClass('sorting_asc');
                $("table.imSortingTableLib th.sorting").removeClass('sorting_desc');
                $(this).addClass('sorting_asc');

                let field = $(this).data('field');
                let url = insertParam('sort',field);
                url = insertParam('order','asc','?' + url);

                document.location.search = url;
            });

            $(document).on('click',"table.imSortingTableLib th.sorting_asc",function () {
                $("table.imSortingTableLib th.sorting").removeClass('sorting_asc');
                $("table.imSortingTableLib th.sorting").removeClass('sorting_desc');
                $(this).addClass('sorting_desc');

                let field = $(this).data('field');
                let url = insertParam('sort',field);
                url = insertParam('order','desc','?' + url);

                document.location.search = url;
            });

            $(document).on('click',"table.imSortingTableLib th.sorting_desc",function () {
                $("table.imSortingTableLib th.sorting").removeClass('sorting_asc');
                $("table.imSortingTableLib th.sorting").removeClass('sorting_desc');
                $(this).addClass('sorting_asc');

                let field = $(this).data('field');
                let url = insertParam('sort',field);
                url = insertParam('order','asc','?' + url);

                document.location.search = url;
            });
        });
    </script>
@endpush

