<div class="form-group row">
    <label class="col-md-3 text-right" for="page_name">Название</label>
    <div class="col-md-9">
        <input type="text" name="name" value="{{ old('name', $model->name ?? '') }}" id="page_name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}">

        @if ($errors->has('name'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_status">Статус</label>
    <div class="col-md-9">
        <div class="material-switch pull-left">
            <input id="someSwitchOptionSuccess" name="status" value="1" type="checkbox" {{ old('status', $model->status) ? ' checked' : '' }}/>
            <label for="someSwitchOptionSuccess" class="label-success"></label>
        </div>

        @if($errors->has('status'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_type">Тип</label>
    <div class="col-md-9">
        <?php
            $val = old('type', $model->type ?? '');
        ?>
        <strong>{{\App\Models\Banners::getTypes()[$val]}}</strong>
    </div>
</div>

{{--<div class="form-group row">--}}
{{--    <label class="col-md-3 text-right" for="page_type">Тип</label>--}}
{{--    <div class="col-md-9">--}}
{{--        <?php--}}
{{--        $val = old('type', $model->type ?? '');--}}
{{--        ?>--}}
{{--        <select name="type" class="select2">--}}
{{--            @foreach(\App\Models\Banners::getTypes() as $key => $item)--}}
{{--                <option value="{{$key}}" @if($val == $key) selected @endif>{{$item}}</option>--}}
{{--            @endforeach--}}
{{--        </select>--}}
{{--    </div>--}}
{{--</div>--}}
