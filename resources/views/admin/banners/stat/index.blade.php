@extends('layouts.admin.app')
@section('breadcrumbs')
    <li class="breadcrumb-item active" aria-current="page">{{ __('Analytics') }}</li>
@endsection
@section('content')
    <form class="form-horizontal" method="get" id="form_p">
        <div class="row" style="margin-bottom: 10px">
            <div class="col-sm-3">
                <select name="item" class="form-control">
                    @foreach(\App\Models\BannerItems::query()->orderBy('name','asc')->get() as $item)
                        <option value="{{$item->id}}" @if(request()->get('item') == $item->id) selected @endif>{{$item->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-6">
                <input type="text" name="p" class="form-control" style="width: 200px;display: inline-block;">
                <input type="submit" class="btn btn-success text-white" value="Применить" style="display: inline-block;width: 100px;">
            </div>
            <div class="col-sm-3">
                <span class="btn btn-success text-white float-right export-btn-trigger">Экспорт</span>
            </div>
        </div>
    </form>

    <form action="{{route('banner-stat.export')}}" method="post">
        @csrf
        <input type="hidden" name="dates" value="{{json_encode($dates)}}">
        <input type="hidden" name="visit" value="{{json_encode($visitData)}}">
        <input type="hidden" name="click" value="{{json_encode($clickData)}}">
        <input type="hidden" name="visit_unique" value="{{json_encode($visitUniqueData)}}">
        <input type="hidden" name="click_unique" value="{{json_encode($clickUniqueData)}}">
        <input type="hidden" name="item_id" value="{{request()->has('item') ? request()->get('item') : \App\Models\BannerItems::query()->get()[0]->id}}">

        <input type="submit" class="export-btn" style="display: none">
    </form>

    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-md-flex align-items-center">
                        <div>
                            <h4 class="card-title"></h4>
                            <h5 class="card-subtitle">{{$dateFrom}} - {{$dateTo}}</h5>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="visits-chart">
                                <canvas id="visits_chart"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endpush

@push('scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script type="text/javascript">
    function visitsChartInit(height) {
        let json_visit = @json($visitData);
        let json_click = @json($clickData);
        let json_visit_unique = @json($visitUniqueData);
        let json_click_unique = @json($clickUniqueData);

        let visits_chart = document.getElementById('visits_chart');
        visits_chart.setAttribute('height',height);

        let ctx = visits_chart.getContext('2d');
        new Chart(ctx, {
            type: 'line',
            data: {
                labels: @json($dates),
                datasets: [{
                    label: 'Показов',
                    backgroundColor: '#ff6384',
                    borderColor: '#ff6384',
                    data: json_visit,
                    fill: false,
                }, {
                    label: 'Уникальных показов',
                    fill: false,
                    backgroundColor: '#36a2eb',
                    borderColor: '#36a2eb',
                    data: json_visit_unique,
                }, {
                    label: 'Переходов',
                    fill: false,
                    backgroundColor: '#eb8814',
                    borderColor: '#eb8814',
                    data: json_click,
                }, {
                    label: 'Уникальных переходов',
                    fill: false,
                    backgroundColor: '#25ebbf',
                    borderColor: '#25ebbf',
                    data: json_click_unique,
                }
                ]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    }

    $( document ).ready(function() {
        $('input[name="p"]').daterangepicker({
            opens: 'left',
            locale: {
                format: 'YYYY-MM-DD'
            },
            startDate: "{{$dateFrom}}",
            endDate: "{{$dateTo}}",
            alwaysShowCalendars: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });

        // $('input[name="p"]').on('apply.daterangepicker', function(ev, picker) {
        //     $("#form_p").submit();
        // });

        if ($(window).width() < 768){
            visitsChartInit(300);
        } else {
            visitsChartInit(100);
        }

        $(".export-btn-trigger").on('click',function () {
            $(".export-btn").trigger('click');
        })
    });
</script>
@endpush
