<?php
$item->active_from = \Carbon\Carbon::parse($item->active_from)->format('d-m-Y H:i');
$item->active_to = \Carbon\Carbon::parse($item->active_to)->format('d-m-Y H:i');
$item->active_from__2 = \Carbon\Carbon::parse($item->active_from__2)->format('d-m-Y H:i');
$item->active_to__2 = \Carbon\Carbon::parse($item->active_to__2)->format('d-m-Y H:i');
$currentTime = \Carbon\Carbon::now()->timestamp;
?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_desktop_alt_{{$item->id}}">Дата активности</label>
            <div class="col-md-3">
                <input type="input" class="form-control active-datetime" name="items[{{$item->id}}][active_from]" value="{{ old('items['.$item->id.'][active_from]', $item->active_from ?? '') }}">
            </div>
            <div class="col-md-3">
                <input type="input" class="form-control active-datetime" name="items[{{$item->id}}][active_to]" value="{{ old('items['.$item->id.'][active_to]', $item->active_to ?? '') }}">
            </div>
            <div class="col-md-2">
                @if(\Carbon\Carbon::parse($item->active_from)->timestamp <= $currentTime && \Carbon\Carbon::parse($item->active_to)->timestamp > $currentTime)
                    <span class="badge badge-pill badge-success" title="Активный сейчас"><i class="fa fa-check" style="font-size: 30px;"></i></span>
                @else
                    <span class="badge badge-pill badge-warning" title="Не активный"><i class="fa fa-ban" style="font-size: 30px;"></i></span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_desktop_{{$item->id}}">Изображение ПК
                @if($model->img_desktop_size)
                    <span>{{$model->img_desktop_size ?? ''}}</span>
                @endif
            </label>
            <div class="col-md-8">
                {{ media_preview_box('items['.$item->id.'][img_desktop]', $item->img_desktop) }}
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_desktop_alt_{{$item->id}}">Alt текст ПК</label>
            <div class="col-md-8">
                <input type="text" name="items[{{$item->id}}][img_desktop_alt]" value="{{ old('items['.$item->id.'][img_desktop_alt]', $item->img_desktop_alt ?? '') }}" id="item_img_desktop_alt_{{$item->id}}" class="form-control">
            </div>
        </div>

        <hr>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_desktop3_{{$item->id}}">Изображение ПК
                @if($model->img_desktop3_size)
                    <span>{{$model->img_desktop3_size ?? ''}}</span>
                @endif
            </label>
            <div class="col-md-8">
                {{ media_preview_box('items['.$item->id.'][img_desktop3]', $item->img_desktop3) }}
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_desktop3_alt_{{$item->id}}">Alt текст ПК</label>
            <div class="col-md-8">
                <input type="text" name="items[{{$item->id}}][img_desktop3_alt]" value="{{ old('items['.$item->id.'][img_desktop3_alt]', $item->img_desktop3_alt ?? '') }}" id="item_img_desktop3_alt_{{$item->id}}" class="form-control">
            </div>
        </div>

        <hr>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_desktop2_{{$item->id}}">Изображение ПК
                @if($model->img_desktop2_size)
                    <span>{{$model->img_desktop2_size ?? ''}}</span>
                @endif
            </label>
            <div class="col-md-8">
                {{ media_preview_box('items['.$item->id.'][img_desktop2]', $item->img_desktop2) }}
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_desktop2_alt_{{$item->id}}">Alt текст ПК</label>
            <div class="col-md-8">
                <input type="text" name="items[{{$item->id}}][img_desktop2_alt]" value="{{ old('items['.$item->id.'][img_desktop2_alt]', $item->img_desktop2_alt ?? '') }}" id="item_img_desktop2_alt_{{$item->id}}" class="form-control">
            </div>
        </div>

        <br>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_tablet_{{$item->id}}">Изображение для планшета
                @if($model->img_tablet_size)
                    <span>{{$model->img_tablet_size ?? ''}}</span>
                @endif
            </label>
            <div class="col-md-8">
                {{ media_preview_box('items['.$item->id.'][img_tablet]', $item->img_tablet) }}
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_tablet_alt_{{$item->id}}">Alt текст для планшета</label>
            <div class="col-md-8">
                <input type="text" name="items[{{$item->id}}][img_tablet_alt]" value="{{ old('items['.$item->id.'][img_tablet_alt]', $item->img_tablet_alt ?? '') }}" id="item_img_tablet_alt_{{$item->id}}" class="form-control">
            </div>
        </div>

        <hr>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_mob_{{$item->id}}">Изображение для моб.
                @if($model->img_mob_size)
                    <span>{{$model->img_mob_size ?? ''}}</span>
                @endif
            </label>
            <div class="col-md-8">
                {{ media_preview_box('items['.$item->id.'][img_mob]', $item->img_mob) }}
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_mob_alt_{{$item->id}}">Alt текст для моб.</label>
            <div class="col-md-8">
                <input type="text" name="items[{{$item->id}}][img_mob_alt]" value="{{ old('items['.$item->id.'][img_mob_alt]', $item->img_mob_alt ?? '') }}" id="item_img_mob_alt_{{$item->id}}" class="form-control">
            </div>
        </div>

        <hr>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_link_{{$item->id}}">Ссылка</label>
            <div class="col-md-8">
                <input type="text" name="items[{{$item->id}}][link]" value="{{ old('items['.$item->id.'][link]', $item->link ?? '') }}" id="item_link_{{$item->id}}" class="form-control">
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group row">
            <label class="col-md-4 text-right">Дата активности</label>
            <div class="col-md-3">
                <input type="input" class="form-control active-datetime" name="items[{{$item->id}}][active_from__2]" value="{{ old('items['.$item->id.'][active_from__2]', $item->active_from__2 ?? '') }}">
            </div>
            <div class="col-md-3">
                <input type="input" class="form-control active-datetime" name="items[{{$item->id}}][active_to__2]" value="{{ old('items['.$item->id.'][active_to__2]', $item->active_to__2 ?? '') }}">
            </div>
            <div class="col-md-2">
                @if(\Carbon\Carbon::parse($item->active_from__2)->timestamp <= $currentTime && \Carbon\Carbon::parse($item->active_to__2)->timestamp > $currentTime)
                    <span class="badge badge-pill badge-success" title="Активный сейчас"><i class="fa fa-check" style="font-size: 30px;"></i></span>
                @else
                    <span class="badge badge-pill badge-warning" title="Не активный"><i class="fa fa-ban" style="font-size: 30px;"></i></span>
                @endif
            </div>
        </div>


        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_desktop_{{$item->id}}__2">Изображение ПК
                @if($model->img_desktop_size)
                    <span>{{$model->img_desktop_size ?? ''}}</span>
                @endif
            </label>
            <div class="col-md-8">
                {{ media_preview_box('items['.$item->id.'][img_desktop__2]', $item->img_desktop__2) }}
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_desktop_alt_{{$item->id}}__2">Alt текст ПК</label>
            <div class="col-md-8">
                <input type="text" name="items[{{$item->id}}][img_desktop_alt__2]" value="{{ old('items['.$item->id.'][img_desktop_alt__2]', $item->img_desktop_alt__2 ?? '') }}" id="item_img_desktop_alt_{{$item->id}}__2" class="form-control">
            </div>
        </div>

        <hr>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_desktop3_{{$item->id}}__2">Изображение ПК
                @if($model->img_desktop3_size)
                    <span>{{$model->img_desktop3_size ?? ''}}</span>
                @endif
            </label>
            <div class="col-md-8">
                {{ media_preview_box('items['.$item->id.'][img_desktop3__2]', $item->img_desktop3__2) }}
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_desktop3_alt_{{$item->id}}__2">Alt текст ПК</label>
            <div class="col-md-8">
                <input type="text" name="items[{{$item->id}}][img_desktop3_alt__2]" value="{{ old('items['.$item->id.'][img_desktop3_alt__2]', $item->img_desktop3_alt__2 ?? '') }}" id="item_img_desktop3_alt_{{$item->id}}__2" class="form-control">
            </div>
        </div>

        <hr>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_desktop2_{{$item->id}}__2">Изображение ПК
                @if($model->img_desktop2_size)
                    <span>{{$model->img_desktop2_size ?? ''}}</span>
                @endif
            </label>
            <div class="col-md-8">
                {{ media_preview_box('items['.$item->id.'][img_desktop2__2]', $item->img_desktop2__2) }}
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_desktop2_alt_{{$item->id}}__2">Alt текст ПК</label>
            <div class="col-md-8">
                <input type="text" name="items[{{$item->id}}][img_desktop2_alt__2]" value="{{ old('items['.$item->id.'][img_desktop2_alt__2]', $item->img_desktop2_alt__2 ?? '') }}" id="item_img_desktop2_alt_{{$item->id}}__2" class="form-control">
            </div>
        </div>

        <br>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_tablet_{{$item->id}}__2">Изображение для планшета
                @if($model->img_tablet_size)
                    <span>{{$model->img_tablet_size ?? ''}}</span>
                @endif
            </label>
            <div class="col-md-8">
                {{ media_preview_box('items['.$item->id.'][img_tablet__2]', $item->img_tablet__2) }}
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_tablet_alt_{{$item->id}}__2">Alt текст для планшета</label>
            <div class="col-md-8">
                <input type="text" name="items[{{$item->id}}][img_tablet_alt__2]" value="{{ old('items['.$item->id.'][img_tablet_alt__2]', $item->img_tablet_alt__2 ?? '') }}" id="item_img_tablet_alt_{{$item->id}}__2" class="form-control">
            </div>
        </div>

        <hr>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_mob_{{$item->id}}__2">Изображение для моб.
                @if($model->img_mob_size)
                    <span>{{$model->img_mob_size ?? ''}}</span>
                @endif
            </label>
            <div class="col-md-8">
                {{ media_preview_box('items['.$item->id.'][img_mob__2]', $item->img_mob__2) }}
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_mob_alt_{{$item->id}}__2">Alt текст для моб.</label>
            <div class="col-md-8">
                <input type="text" name="items[{{$item->id}}][img_mob_alt__2]" value="{{ old('items['.$item->id.'][img_mob_alt__2]', $item->img_mob_alt__2 ?? '') }}" id="item_img_mob_alt_{{$item->id}}__2" class="form-control">
            </div>
        </div>

        <hr>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_link_{{$item->id}}__2">Ссылка</label>
            <div class="col-md-8">
                <input type="text" name="items[{{$item->id}}][link__2]" value="{{ old('items['.$item->id.'][link__2]', $item->link__2 ?? '') }}" id="item_link_{{$item->id}}__2" class="form-control">
            </div>
        </div>
    </div>
</div>
