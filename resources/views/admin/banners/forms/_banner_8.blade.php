<?php
$data = $item->getTranslationsArray();
$item->active_from = \Carbon\Carbon::parse($item->active_from)->format('d-m-Y H:i');
$item->active_to = \Carbon\Carbon::parse($item->active_to)->format('d-m-Y H:i');
$item->active_from__2 = \Carbon\Carbon::parse($item->active_from__2)->format('d-m-Y H:i');
$item->active_to__2 = \Carbon\Carbon::parse($item->active_to__2)->format('d-m-Y H:i');
$currentTime = \Carbon\Carbon::now()->timestamp;
?>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_desktop_alt_{{$item->id}}">Дата активности</label>
            <div class="col-md-3">
                <input type="input" class="form-control active-datetime" name="items[{{$item->id}}][active_from]" value="{{ old('items['.$item->id.'][active_from]', $item->active_from ?? '') }}">
            </div>
            <div class="col-md-3">
                <input type="input" class="form-control active-datetime" name="items[{{$item->id}}][active_to]" value="{{ old('items['.$item->id.'][active_to]', $item->active_to ?? '') }}">
            </div>
            <div class="col-md-2">
                @if(\Carbon\Carbon::parse($item->active_from)->timestamp <= $currentTime && \Carbon\Carbon::parse($item->active_to)->timestamp > $currentTime)
                    <span class="badge badge-pill badge-success" title="Активный сейчас"><i class="fa fa-check" style="font-size: 30px;"></i></span>
                @else
                    <span class="badge badge-pill badge-warning" title="Не активный"><i class="fa fa-ban" style="font-size: 30px;"></i></span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_desktop_{{$item->id}}">Изображение ПК и планшет
                @if($model->img_desktop_size)
                    <span>{{$model->img_desktop_size ?? ''}}</span>
                @endif
            </label>
            <div class="col-md-8">
                {{ media_preview_box('items['.$item->id.'][img_desktop]', $item->img_desktop) }}
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_desktop_alt_{{$item->id}}">Alt текст ПК и планшет</label>
            <div class="col-md-8">
                <input type="text" name="items[{{$item->id}}][img_desktop_alt]" value="{{ old('items['.$item->id.'][img_desktop_alt]', $item->img_desktop_alt ?? '') }}" id="item_img_desktop_alt_{{$item->id}}" class="form-control">
            </div>
        </div>

        <hr>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_mob_{{$item->id}}">Изображение для моб.
                @if($model->img_mob_size)
                    <span>{{$model->img_mob_size ?? ''}}</span>
                @endif
            </label>
            <div class="col-md-8">
                {{ media_preview_box('items['.$item->id.'][img_mob]', $item->img_mob) }}
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_mob_alt_{{$item->id}}">Alt текст для моб.</label>
            <div class="col-md-8">
                <input type="text" name="items[{{$item->id}}][img_mob_alt]" value="{{ old('items['.$item->id.'][img_mob_alt]', $item->img_mob_alt ?? '') }}" id="item_img_mob_alt_{{$item->id}}" class="form-control">
            </div>
        </div>

        <hr>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_link_{{$item->id}}">Ссылка</label>
            <div class="col-md-8">
                <input type="text" name="items[{{$item->id}}][link]" value="{{ old('items['.$item->id.'][link]', $item->link ?? '') }}" id="item_link_{{$item->id}}" class="form-control">
            </div>
        </div>

        <hr>

        @foreach(\App\Models\Langs::getLangCodes() as $lang)
            <div class="form-group row">
                <label class="col-md-4 text-right" for="item_{{$lang}}_text_{{$item->id}}">Текст {{\App\Models\Langs::getLangsWithTitleShort()[$lang]}}</label>
                <div class="col-md-8">
                    <input type="text" name="items[{{$item->id}}][{{$lang}}][text]" value="{{$data[$lang]['text'] ?? ''}}" id="item_{{$lang}}_text_{{$item->id}}" class="form-control">
                </div>
            </div>
        @endforeach

        @foreach(\App\Models\Langs::getLangCodes() as $lang)
            <div class="form-group row">
                <label class="col-md-4 text-right" for="item_{{$lang}}_btn_name_{{$item->id}}">Надпись на кнопке {{\App\Models\Langs::getLangsWithTitleShort()[$lang]}}</label>
                <div class="col-md-8">
                    <input type="text" name="items[{{$item->id}}][{{$lang}}][btn_name]" value="{{$data[$lang]['btn_name'] ?? ''}}" id="item_{{$lang}}_btn_name_{{$item->id}}" class="form-control">
                </div>
            </div>
        @endforeach
    </div>
    <div class="col-sm-6">
        <div class="form-group row">
            <label class="col-md-4 text-right">Дата активности</label>
            <div class="col-md-3">
                <input type="input" class="form-control active-datetime" name="items[{{$item->id}}][active_from__2]" value="{{ old('items['.$item->id.'][active_from__2]', $item->active_from__2 ?? '') }}">
            </div>
            <div class="col-md-3">
                <input type="input" class="form-control active-datetime" name="items[{{$item->id}}][active_to__2]" value="{{ old('items['.$item->id.'][active_to__2]', $item->active_to__2 ?? '') }}">
            </div>
            <div class="col-md-2">
                @if(\Carbon\Carbon::parse($item->active_from__2)->timestamp <= $currentTime && \Carbon\Carbon::parse($item->active_to__2)->timestamp > $currentTime)
                    <span class="badge badge-pill badge-success" title="Активный сейчас"><i class="fa fa-check" style="font-size: 30px;"></i></span>
                @else
                    <span class="badge badge-pill badge-warning" title="Не активный"><i class="fa fa-ban" style="font-size: 30px;"></i></span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_desktop_{{$item->id}}__2">Изображение ПК и планшет
                @if($model->img_desktop_size)
                    <span>{{$model->img_desktop_size ?? ''}}</span>
                @endif
            </label>
            <div class="col-md-8">
                {{ media_preview_box('items['.$item->id.'][img_desktop__2]', $item->img_desktop__2) }}
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_desktop_alt_{{$item->id}}__2">Alt текст ПК и планшет</label>
            <div class="col-md-8">
                <input type="text" name="items[{{$item->id}}][img_desktop_alt__2]" value="{{ old('items['.$item->id.'][img_desktop_alt__2]', $item->img_desktop_alt__2 ?? '') }}" id="item_img_desktop_alt_{{$item->id}}__2" class="form-control">
            </div>
        </div>

        <hr>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_mob_{{$item->id}}__2">Изображение для моб.
                @if($model->img_mob_size)
                    <span>{{$model->img_mob_size ?? ''}}</span>
                @endif
            </label>
            <div class="col-md-8">
                {{ media_preview_box('items['.$item->id.'][img_mob__2]', $item->img_mob__2) }}
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_img_mob_alt_{{$item->id}}__2">Alt текст для моб.</label>
            <div class="col-md-8">
                <input type="text" name="items[{{$item->id}}][img_mob_alt__2]" value="{{ old('items['.$item->id.'][img_mob_alt__2]', $item->img_mob_alt__2 ?? '') }}" id="item_img_mob_alt_{{$item->id}}__2" class="form-control">
            </div>
        </div>

        <hr>

        <div class="form-group row">
            <label class="col-md-4 text-right" for="item_link_{{$item->id}}__2">Ссылка</label>
            <div class="col-md-8">
                <input type="text" name="items[{{$item->id}}][link__2]" value="{{ old('items['.$item->id.'][link__2]', $item->link__2 ?? '') }}" id="item_link_{{$item->id}}__2" class="form-control">
            </div>
        </div>

        <hr>

        @foreach(\App\Models\Langs::getLangCodes() as $lang)
            <div class="form-group row">
                <label class="col-md-4 text-right" for="item_{{$lang}}_text_{{$item->id}}__2">Текст {{\App\Models\Langs::getLangsWithTitleShort()[$lang]}}</label>
                <div class="col-md-8">
                    <input type="text" name="items[{{$item->id}}][{{$lang}}][text__2]" value="{{$data[$lang]['text__2'] ?? ''}}" id="item_{{$lang}}_text_{{$item->id}}__2" class="form-control">
                </div>
            </div>
        @endforeach

        @foreach(\App\Models\Langs::getLangCodes() as $lang)
            <div class="form-group row">
                <label class="col-md-4 text-right" for="item_{{$lang}}_btn_name_{{$item->id}}__2">Надпись на кнопке {{\App\Models\Langs::getLangsWithTitleShort()[$lang]}}</label>
                <div class="col-md-8">
                    <input type="text" name="items[{{$item->id}}][{{$lang}}][btn_name__2]" value="{{$data[$lang]['btn_name__2'] ?? ''}}" id="item_{{$lang}}_btn_name_{{$item->id}}__2" class="form-control">
                </div>
            </div>
        @endforeach
    </div>
</div>
