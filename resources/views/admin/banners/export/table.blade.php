<table>
    <tbody>
    <tr>
        <td colspan="5" style="background-color: #8a1cc0; color: #ffffff;text-align: center">
            Статистика по баннеру &laquo;{{$item_name}}&raquo; за период {{$dates[0]}} - {{end($dates)}}
        </td>
    </tr>
    <tr>
        <td style="background-color: #ebebeb;width: 15px">Дата</td>
        <td style="background-color: #ebebeb;width: 15px">Показов</td>
        <td style="background-color: #ebebeb;width: 25px">Уникальных показов</td>
        <td style="background-color: #ebebeb;width: 15px">Переходов</td>
        <td style="background-color: #ebebeb;width: 25px">Уникальных переходов</td>
    </tr>
    <?php
        $visitTotal = 0;
        $visitUniqueTotal = 0;
        $clickTotal = 0;
        $clickUniqueTotal = 0;
    ?>
    @foreach($dates as $key => $date)
        <?php
            $visitTotal += $visit[$key];
            $visitUniqueTotal += $visit_unique[$key];
            $clickTotal += $click[$key];
            $clickUniqueTotal += $click_unique[$key];
        ?>
        <tr>
            <td>{{ $date }}</td>
            <td>{{ $visit[$key] }}</td>
            <td>{{ $visit_unique[$key] }}</td>
            <td>{{ $click[$key] }}</td>
            <td>{{ $click_unique[$key] }}</td>
        </tr>
    @endforeach
    <tr>
        <td style="background-color: #dbdbdb"><strong>Всего</strong></td>
        <td style="background-color: #ebebeb"><strong>{{$visitTotal}}</strong></td>
        <td style="background-color: #ebebeb"><strong>{{$visitUniqueTotal}}</strong></td>
        <td style="background-color: #ebebeb"><strong>{{$clickTotal}}</strong></td>
        <td style="background-color: #ebebeb"><strong>{{$clickUniqueTotal}}</strong></td>
    </tr>
    </tbody>
</table>
