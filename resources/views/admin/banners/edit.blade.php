@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item"><a href="{{route('banners.index')}}">Баннеры</a></li>
            <li class="breadcrumb-item active" aria-current="page">Редактировать</li>
        </ol>
    </nav>

    <form action="{{route('banner.add-empty-item')}}" method="post" style="display: none">
        @csrf
        <input type="hidden" name="banner_id" value="{{$model->id}}">
        <input type="submit" id="form_add_item">
    </form>

    <form class="form-horizontal" method="POST" action="{{route('banners.update', $model->id)}}">
        @method('PUT')
        @csrf

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <ul class="nav nav-tabs" id="myTab" role="tablist" style="margin-bottom: -10px;border-bottom: none">
                            <li class="nav-item">
                                <a class="nav-link active" id="main-tab" data-toggle="tab" href="#main" role="tab" aria-controls="main" aria-selected="true">Информация</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="items-tab" data-toggle="tab" href="#items" role="tab" aria-controls="items" aria-selected="false">Блоки</a>
                            </li>
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="myTabContent">
                            {{----------------------------- MAIN TAB -----------------------------------------}}
                            <div class="tab-pane fade show active" id="main" role="tabpanel" aria-labelledby="main-tab">
                                @include('admin.banners._create_form',['model' => $model])
                            </div>

                            {{----------------------------- ITEMS TAB -----------------------------------------}}
                            <div class="tab-pane fade" id="items" role="tabpanel" aria-labelledby="items-tab">
                                @include('admin.banners._items',['model' => $model])
                            </div>
                        </div>

                        <input type="submit" value="Сохранить" class="btn btn-success text-white float-right">
                    </div>
                </div>
            </div>
        </div>

    </form>
@endsection

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
    <style>
        /*-- ==============================================================
        Switches
        ============================================================== */
        .material-switch {
            line-height: 3em;
        }
        .material-switch > input[type="checkbox"] {
            display: none;
        }

        .material-switch > label {
            cursor: pointer;
            height: 0px;
            position: relative;
            width: 40px;
        }

        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: -8px;
            position:absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 40px;
        }
        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: -4px;
            margin-top: -8px;
            position: absolute;
            top: -4px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }
        .material-switch > input[type="checkbox"]:checked + label::before {
            background: inherit;
            opacity: 0.5;
        }
        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: 20px;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #5897fb !important;
            border: 1px solid #5897fb !important;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            color: #fff !important;
        }

        .select2-container--classic .select2-selection--multiple .select2-selection__choice, .select2-container--default .select2-selection--multiple .select2-selection__choice, .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            background-color:#5897fb !important;
        }

        .select2-container--default .select2-selection--multiple {
            border: 1px solid #e9ecef;
        }

        .select2-container--default.select2-container--focus .select2-selection--multiple {
            border: 1px solid #e9ecef;
            color: #3e5569;
            background-color: #fff;
            border-color: rgba(0,0,0,0.25);
            outline: 0;
            box-shadow: 0 0 0 0.2rem rgb(0 123 255 / 25%);
        }
    </style>
@endpush

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="{{asset('matrix/libs/moment/moment.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script>
        $(document).ready(() => {
            $('.select2').select2();

            $(".delete-block").on('click', function () {
                let _this = $(this);
                Swal.fire({
                    title: 'Вы уверенны?',
                    text: "Вы пытаетесь удалить этот блок?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Да, сделать это!',
                    cancelButtonText: 'Нет'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url:"{{route('banners.remove-item')}}",
                            type:"post",
                            dataType:"json",
                            data:{
                                _token:"{{csrf_token()}}",
                                item_id:_this.data('item_id')
                            },
                            success: function(res){
                                if(res.success){
                                    _this.closest('.tab-pane').remove();
                                    $("#myTab2").find('li[data-item_id="'+_this.data('item_id')+'"]').remove();

                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: 'Блок успешно удален!',
                                        showConfirmButton: false,
                                        timer: 2000
                                    })
                                }
                            }
                        });
                    }
                });
            });
        });
    </script>
@endpush
