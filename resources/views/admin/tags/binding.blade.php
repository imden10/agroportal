@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item active" aria-current="page">Теги</li>
        </ol>
    </nav>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">

                </div>
                <form action="" method="get">
                <div class="card-body" style="flex-direction: row;display: flex; justify-content: center">

                    <div class="card border-secondary mb-3" style="width: 500px;">
                        <div class="card-header"
                             style="display: flex;justify-content: space-between;align-items: center;">
                            Рус (<span class="count_ru">{{count($data['ru'])}}</span>)
                            <input type="text" class="form-control search-ru-input" id="search_ru_input"
                                   placeholder="Быстрый поиск" style="width: 270px">
                            <span class="btn btn-danger text-white clear-btn-ru" title="Очистить"><i class="fa fa-refresh"></i></span>

                            <div>
                                <select name="a" class="form-control" style="widows: 50;" onchange="$('#submit_input').trigger('click')">
                                    <option value="">-</option>
                                    <option value="а" @if(request()->input('a') == 'а') selected @endif>А</option>
                                    <option value="б" @if(request()->input('a') == 'б') selected @endif>Б</option>
                                    <option value="в" @if(request()->input('a') == 'в') selected @endif>В</option>
                                    <option value="г" @if(request()->input('a') == 'г') selected @endif>Г</option>
                                    <option value="д" @if(request()->input('a') == 'д') selected @endif>Д</option>
                                    <option value="е" @if(request()->input('a') == 'е') selected @endif>Е</option>
                                    <option value="ё" @if(request()->input('a') == 'ё') selected @endif>Ё</option>
                                    <option value="ж" @if(request()->input('a') == 'ж') selected @endif>Ж</option>
                                    <option value="з" @if(request()->input('a') == 'з') selected @endif>З</option>
                                    <option value="и" @if(request()->input('a') == 'и') selected @endif>И</option>
                                    <option value="й" @if(request()->input('a') == 'й') selected @endif>Й</option>
                                    <option value="к" @if(request()->input('a') == 'к') selected @endif>К</option>
                                    <option value="л" @if(request()->input('a') == 'л') selected @endif>Л</option>
                                    <option value="м" @if(request()->input('a') == 'м') selected @endif>М</option>
                                    <option value="н" @if(request()->input('a') == 'н') selected @endif>Н</option>
                                    <option value="о" @if(request()->input('a') == 'о') selected @endif>О</option>
                                    <option value="п" @if(request()->input('a') == 'п') selected @endif>П</option>
                                    <option value="р" @if(request()->input('a') == 'р') selected @endif>Р</option>
                                    <option value="с" @if(request()->input('a') == 'с') selected @endif>С</option>
                                    <option value="т" @if(request()->input('a') == 'т') selected @endif>Т</option>
                                    <option value="у" @if(request()->input('a') == 'у') selected @endif>У</option>
                                    <option value="ф" @if(request()->input('a') == 'ф') selected @endif>Ф</option>
                                    <option value="х" @if(request()->input('a') == 'х') selected @endif>Х</option>
                                    <option value="ц" @if(request()->input('a') == 'ц') selected @endif>Ц</option>
                                    <option value="ч" @if(request()->input('a') == 'ч') selected @endif>Ч</option>
                                    <option value="ш" @if(request()->input('a') == 'ш') selected @endif>Ш</option>
                                    <option value="щ" @if(request()->input('a') == 'щ') selected @endif>Щ</option>
                                    <option value="э" @if(request()->input('a') == 'э') selected @endif>Э</option>
                                    <option value="ю" @if(request()->input('a') == 'ю') selected @endif>Ю</option>
                                    <option value="я" @if(request()->input('a') == 'я') selected @endif>Я</option>
                                </select>
                            </div>

                        </div>
                        <div class="card-body text-secondary" style="height: 500px;overflow-y: auto">
                            @if(count($data['ru']))
                                <ul data-lang="ru" style="list-style: none; padding: 0" class="list list-ru">
                                    @foreach($data['ru'] as $ruId => $ruName)
                                        <li data-id="{{$ruId}}" style="margin: 5px 0" data-text="{{$ruName}}">
                                            <button type="button" style="width: 100%"
                                                    class="btn btn-secondary btn-xs">{{$ruName}}</button>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>
                    <div class="card border-secondary mb-3" style="width: 500px; margin-left: 15px">
                        <div class="card-header"
                             style="display: flex;justify-content: space-between;align-items: center;">
                            Укр (<span class="count_uk">{{count($data['uk'])}}</span>)
                            <input type="text" class="form-control search-uk-input" id="search_uk_input"
                                   placeholder="Быстрый поиск" style="width: 270px">
                            <span class="btn btn-danger text-white clear-btn-uk" title="Очистить"><i class="fa fa-refresh"></i></span>

                            <div>
                                <select name="b" class="form-control" style="widows: 50;" onchange="$('#submit_input').trigger('click')">
                                    <option value="">-</option>
                                    <option value="а" @if(request()->input('b') == 'а') selected @endif>А</option>
                                    <option value="б" @if(request()->input('b') == 'б') selected @endif>Б</option>
                                    <option value="в" @if(request()->input('b') == 'в') selected @endif>В</option>
                                    <option value="г" @if(request()->input('b') == 'г') selected @endif>Г</option>
                                    <option value="д" @if(request()->input('b') == 'д') selected @endif>Д</option>
                                    <option value="е" @if(request()->input('b') == 'е') selected @endif>Е</option>
                                    <option value="є" @if(request()->input('b') == 'є') selected @endif>Є</option>
                                    <option value="ж" @if(request()->input('b') == 'ж') selected @endif>Ж</option>
                                    <option value="з" @if(request()->input('b') == 'з') selected @endif>З</option>
                                    <option value="і" @if(request()->input('b') == 'і') selected @endif>І</option>
                                    <option value="ї" @if(request()->input('b') == 'ї') selected @endif>Ї</option>
                                    <option value="й" @if(request()->input('b') == 'й') selected @endif>Й</option>
                                    <option value="к" @if(request()->input('b') == 'к') selected @endif>К</option>
                                    <option value="л" @if(request()->input('b') == 'л') selected @endif>Л</option>
                                    <option value="м" @if(request()->input('b') == 'м') selected @endif>М</option>
                                    <option value="н" @if(request()->input('b') == 'н') selected @endif>Н</option>
                                    <option value="о" @if(request()->input('b') == 'о') selected @endif>О</option>
                                    <option value="п" @if(request()->input('b') == 'п') selected @endif>П</option>
                                    <option value="р" @if(request()->input('b') == 'р') selected @endif>Р</option>
                                    <option value="с" @if(request()->input('b') == 'с') selected @endif>С</option>
                                    <option value="т" @if(request()->input('b') == 'т') selected @endif>Т</option>
                                    <option value="у" @if(request()->input('b') == 'у') selected @endif>У</option>
                                    <option value="ф" @if(request()->input('b') == 'ф') selected @endif>Ф</option>
                                    <option value="х" @if(request()->input('b') == 'х') selected @endif>Х</option>
                                    <option value="ц" @if(request()->input('b') == 'ц') selected @endif>Ц</option>
                                    <option value="ч" @if(request()->input('b') == 'ч') selected @endif>Ч</option>
                                    <option value="ш" @if(request()->input('b') == 'ш') selected @endif>Ш</option>
                                    <option value="щ" @if(request()->input('b') == 'щ') selected @endif>Щ</option>
                                    <option value="ю" @if(request()->input('b') == 'ю') selected @endif>Ю</option>
                                    <option value="я" @if(request()->input('b') == 'я') selected @endif>Я</option>
                                </select>
                            </div>
                        </div>
                        <div class="card-body text-secondary" style="height: 500px;overflow-y: auto">
                            @if(count($data['uk']))
                                <ul data-lang="uk" style="list-style: none; padding: 0" class="list list-uk">
                                    @foreach($data['uk'] as $ukId => $ukName)
                                        <li data-id="{{$ukId}}" style="margin: 5px 0" data-text="{{$ukName}}">
                                            <button type="button" style="width: 100%"
                                                    class="btn btn-secondary btn-xs">{{$ukName}}</button>
                                        </li>
                                    @endforeach
                                </ul>
                            @endif
                        </div>
                    </div>
                        <input type="submit" id="submit_input" style="display: none">
                    <span class="btn btn-success tie-btn text-white disabled"
                          style="margin-left: 15px;width: 150px; height: 34px">Связать</span>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $("#search_ru_input").on("keyup input", function () {
            if (this.value.length > 0) {
                $(".list-ru").each(function () {
                    $(this).children().hide().filter(function () {
                        return $(this).data('text').toLowerCase().lastIndexOf($("#search_ru_input").val().toLowerCase(), 0) == 0;

                    }).show();
                });

            } else {
                $(".list-ru li").show();
            }
        });

        $("#search_uk_input").on("keyup input", function () {
            if (this.value.length > 0) {
                $(".list-uk").each(function () {
                    $(this).children().hide().filter(function () {
                        return $(this).data('text').toLowerCase().lastIndexOf($("#search_uk_input").val().toLowerCase(), 0) == 0;

                    }).show();
                });

            } else {
                $(".list-uk li").show();
            }
        })

        var ruId = null;
        var ukId = null;
        $(document).ready(() => {
            $(".list li button").on('click', function () {
                $(this).closest('.list').find('li button.btn-success').removeClass('btn-success').addClass('btn-secondary');
                $(this).addClass('btn-success').removeClass('btn-secondary');
            });

            $(".list[data-lang='ru'] li button").on('click', function () {
                ruId = $(this).parent('li').data('id');

                if (ruId && ukId) {
                    $(".tie-btn").removeClass('disabled');
                } else {
                    $(".tie-btn").addClass('disabled');
                }
            });

            $(".list[data-lang='uk'] li button").on('click', function () {
                ukId = $(this).parent('li').data('id');

                if (ruId && ukId) {
                    $(".tie-btn").removeClass('disabled');
                } else {
                    $(".tie-btn").addClass('disabled');
                }
            });

            $(".tie-btn").on('click', function () {
                if ($(this).hasClass('disabled')) return false;

                $.ajax({
                    url: "{{route('tag.tie')}}",
                    type: "post",
                    dataType: "json",
                    data: {
                        _token: "{{csrf_token()}}",
                        ruId: ruId,
                        ukId: ukId
                    },
                    success: function (res) {
                        if (res.success) {
                            $(".tie-btn").addClass('disabled');

                            $(".list[data-lang='uk'] li[data-id='" + ukId + "']").remove();
                            $(".list[data-lang='ru'] li[data-id='" + ruId + "']").remove();

                            ruId = null;
                            ukId = null;

                            $(".count_ru").text($(".count_ru").text() - 1);
                            $(".count_uk").text($(".count_uk").text() - 1);
                        } else {
                            alert(res.message);
                        }
                    }
                });
            });

            $(".clear-btn-ru").on('click',function () {
                $("#search_ru_input").val('');
                $("#search_ru_input").trigger('input');
            });

            $(".clear-btn-uk").on('click',function () {
                $("#search_uk_input").val('');
                $("#search_uk_input").trigger('input');
            });
        });
    </script>
@endpush
