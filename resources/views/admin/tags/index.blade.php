@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item active" aria-current="page">Теги</li>
        </ol>
    </nav>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                @permission('tags__tags__create')
                    <a href="{{route('tag.create')}}" class="btn btn-primary float-right">
                        <i class="fa fa-plus"></i>
                        Добавить
                    </a>
                @endpermission

                <a href="{{route('tag.binding')}}" class="btn btn-info float-right" style="margin-right: 15px" title="Сопоставить теги">
                    <i class="fa fa-arrows-h"></i>
                </a>
            </div>
            <div class="card-body">
                <form action="" method="get">
                    <div class="form-row">

                        <div class="form-group col-md-2">
                            <label>Поиск</label>
                            <input type="text" class="form-control" name="name" value="{{old('name', request()->input('name'))}}">
                        </div>

                        <div class="form-group col-md-1">
                            <label for="inputPassword4">&nbsp;</label>
                            <button type="submit" class="btn btn-success form-control text-white">Фильтровать</button>
                        </div>
                        <div class="form-group col-md-1">
                            <label for="inputPassword4">&nbsp;</label>
                            <a href="{{ route('tag.index') }}" class="btn btn-danger form-control text-white">Сбросить</a>
                        </div>
                    </div>
                </form>

                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Название</th>
                            <th>Slug</th>
                            <th style="min-width: 150px">Действия</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($model as $item)
                            <tr data-id="{{$item->id}}">
                                <td>{{$item->id}}</td>
                                <td>
                                    @permission('tags__tags__edit')
                                        <a href="{{ route('tag.edit', $item->id) }}">
                                            {{$item->name}}
                                        </a>
                                    @else
                                        {{$item->name}}
                                    @endpermission
                                </td>
                                <td>{{$item->slug}}</td>
                                <td>
                                    <div style="display: flex">
                                        <div style="margin-left: 10px">
                                            <form action="{{ route('tag.destroy', $item->id) }}" method="POST">
                                                @csrf
                                                @method('DELETE')

                                                @permission('tags__tags__delete')
                                                    <a href="javascript:void(0)" title="Удалить" class="btn btn-danger btn-xs delete-item-btn text-white">
                                                        <i class="fas fa-trash"></i>
                                                    </a>
                                                @endpermission
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $model->appends(request()->all())->links() }}
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(document).ready(() => {
            $('.delete-item-btn').on('click',function() {
                if(confirm('Вы пытаетесь удалить запись?')){
                    $(this).closest('form').submit();
                }
            });
        });
    </script>
@endpush
