<div class="form-group row">
    <label class="col-md-3 text-right" for="page_name_{{ $lang }}">Название</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][name]" value="{{ old('page_data.' . $lang . '.name', $data[$lang]['name'] ?? '') }}" id="page_name_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.name') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.name'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_description_short_{{ $lang }}">Краткое описание</label>
    <div class="col-md-9">
    <textarea
        name="page_data[{{ $lang }}][description_short]"
        id="page_description_short_{{ $lang }}"
        class="summernote editor {{ $errors->has('page_data.' . $lang . '.description_short') ? ' is-invalid' : '' }}"
        cols="30"
        rows="10"
    >{{ old('page_data.' . $lang . '.description_short', $data[$lang]['description_short'] ?? '') }}</textarea>

        @if ($errors->has('page_data.' . $lang . '.description_short'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('page_data.' . $lang . '.description_short') }}</strong>
        </span>
        @endif
    </div>
</div>


<div class="form-group row">
    <label class="col-md-3 text-right" for="page_h2_{{ $lang }}">H2</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][h2]" value="{{ old('page_data.' . $lang . '.h2', $data[$lang]['h2'] ?? '') }}" id="page_h2_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.h2') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.h2'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.h2') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_description_{{ $lang }}">Описание</label>
    <div class="col-md-9">
    <textarea
        name="page_data[{{ $lang }}][description]"
        id="page_description_{{ $lang }}"
        class="summernote editor {{ $errors->has('page_data.' . $lang . '.description') ? ' is-invalid' : '' }}"
        cols="30"
        rows="10"
    >{{ old('page_data.' . $lang . '.description', $data[$lang]['description'] ?? '') }}</textarea>

        @if ($errors->has('page_data.' . $lang . '.description'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('page_data.' . $lang . '.description') }}</strong>
        </span>
        @endif
    </div>
</div>

{{--<div class="form-group row">--}}
{{--    <label class="col-md-3 text-right" for="page_description_hide_{{ $lang }}">Скрытое описание</label>--}}
{{--    <div class="col-md-9">--}}
{{--    <textarea--}}
{{--        name="page_data[{{ $lang }}][description_hide]"--}}
{{--        id="page_description_{{ $lang }}"--}}
{{--        class="summernote editor {{ $errors->has('page_data.' . $lang . '.description_hide') ? ' is-invalid' : '' }}"--}}
{{--        cols="30"--}}
{{--        rows="10"--}}
{{--    >{{ old('page_data.' . $lang . '.description_hide', $data[$lang]['description_hide'] ?? '') }}</textarea>--}}

{{--        @if ($errors->has('page_data.' . $lang . '.description_hide'))--}}
{{--            <span class="invalid-feedback" role="alert">--}}
{{--            <strong>{{ $errors->first('page_data.' . $lang . '.description_hide') }}</strong>--}}
{{--        </span>--}}
{{--        @endif--}}
{{--    </div>--}}
{{--</div>--}}
