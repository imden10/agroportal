<div class="form-group row">
    <label class="col-md-3 text-right" for="page_categories">Рубрика</label>
    <div class="col-md-9">
        @php
            if($model->id){
                $selCat = [$model->category_id];
            } else {
                $selCat = [];
                $selCat[] = $sel;
            }
        @endphp
        <select id="categories" name="categories[]" class="select2 form-control custom-select{{ $errors->has('category_id') ? ' is-invalid' : '' }}" style="width: 100%; height:36px;">
            <option value="">---</option>

            @include('admin.blog.articles.node',['sel' => $selCat ?? []])
        </select>

        @if ($errors->has('categories'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('categories') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="name">Изображение</label>
    <div class="col-md-9">
        {{ media_preview_box('image', $model->image) }}
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_slug">Slug</label>
    <div class="col-md-9">
        <input type="text" name="slug" value="{{ old('slug', $model->slug ?? '') }}" id="page_slug" class="form-control{{ $errors->has('slug') ? ' is-invalid' : '' }}">

        @if ($errors->has('slug'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('slug') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="tags">Теги</label>
    <div class="col-md-9">
        @php
            \Illuminate\Support\Facades\DB::enableQueryLog();
                $selTags = $model->newTags()->pluck('new_tags.id')->toArray();

                $allTags = \Illuminate\Support\Facades\Cache::remember('all_tags',(60*60*24),function(){
                    return \App\Models\NewTag::query()
                    ->leftJoin('new_tag_translations','new_tag_translations.new_tag_id','=','new_tags.id')
                    ->where('new_tag_translations.lang',config('translatable.locale'))
                    ->select([
                        'new_tags.id',
                        'new_tag_translations.name'
                    ])
                    ->pluck('new_tag_translations.name','new_tags.id')
                    ->toArray();
                });

                \Illuminate\Support\Facades\Log::info('allTags',[\Illuminate\Support\Facades\DB::getQueryLog()]);
        @endphp
        <select name=tags[]" id="tags" class="form-control select2-tag" multiple style="width: 100%">
            @if(count($allTags))
                @foreach($allTags as $keyTag => $item)
                    <option value="{{$keyTag}}" @if(in_array($keyTag,$selTags)) selected @endif>{{$item}}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right">Количество просмотров</label>
    <div class="col-md-9">
        <input type="text" name="views" value="{{ old('views', $model->views ?? '') }}"  class="form-control">
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="public_date">
        Дата публикации</label>
    <div class="col-md-3">
        <input type="input" class="form-control" name="public_date" value="{{$model->public_date}}" id="public_date">
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="mark">Выводить в топ</label>
    <div class="col-md-9">
        <div class="material-switch pull-left">
            <input id="someSwitchOptionSuccessmark" name="mark" value="1" type="checkbox" {{ old('mark', $model->mark) ? ' checked' : '' }}/>
            <label for="someSwitchOptionSuccessmark" class="label-success"></label>
        </div>

        @if($errors->has('mark'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('mark') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="interview">Важная новость</label>
    <div class="col-md-9">
        <div class="material-switch pull-left">
            <input id="someSwitchOptionSuccessinterview" name="interview" value="1" type="checkbox" {{ old('interview', $model->interview) ? ' checked' : '' }}/>
            <label for="someSwitchOptionSuccessinterview" class="label-success"></label>
        </div>

        @if($errors->has('interview'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('interview') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="videonews">Видео новость</label>
    <div class="col-md-9">
        <div class="material-switch pull-left">
            <input id="someSwitchOptionSuccessvideonews" name="videonews" value="1" type="checkbox" {{ old('videonews', ($isVideo ?? 0)) ? ' checked' : '' }}/>
            <label for="someSwitchOptionSuccessvideonews" class="label-success"></label>
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_status">Статус</label>
    <div class="col-md-9">
        <div class="material-switch pull-left">
            <input id="someSwitchOptionSuccess" name="status" value="1" type="checkbox" {{ old('status', $model->status) ? ' checked' : '' }}/>
            <label for="someSwitchOptionSuccess" class="label-success"></label>
        </div>

        @if($errors->has('status'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
    </div>
</div>
