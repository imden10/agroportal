<?php
    $gallery = $model->gallery;
?>

<span class="btn btn-success text-white add-images-for-gallery" data-id="{{$model->id}}" style="margin-bottom: 15px">Добавить</span>

<table class="table table-bordered imSortingTableLib">
    <thead>
    <tr>
        <th>Изображение</th>
        <th style="min-width: 150px"></th>
    </tr>
    </thead>
    <tbody>
        @if(count($gallery))
            @foreach($gallery as $item)
                @php($title = $item->title ? json_decode($item->title,true) : [])
                <tr>
                    <td>
                        <img src="/storage/media{{$item->image}}" style="width: 150px; height: auto" alt="">
                    </td>
                    <td>
                        @foreach(\App\Models\Langs::getLangCodes() as $lang)
                            <div class="row item-element" style="margin-top: 5px">
                                <div class="input-group mb-12">
                                    <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <img src="/images/langs/{{$lang}}.jpg" alt="" style="width: 20px">
                                    </span>
                                    </div>
                                    <input type="text" class="form-control" value="{{$title[$lang] ?? ''}}" name="gallery[{{$item->id}}][title][{{$lang}}]">
                                </div>
                            </div>
                        @endforeach
                    </td>
                    <td>
                        <a href="javascript:void(0)" title="Удалить" data-id="{{$item->id}}" class="fa fa-trash btn alert-danger btn-xs delete-item-btn"></a>
                    </td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>

@push('scripts')
    <script>
        $(document).ready(function () {
            $(".add-images-for-gallery").on('click', function () {
                let article_id = $(this).data('id');
                window.open('/filemanager?type=image', 'FileManager', 'width=900,height=600');
                window.SetUrl = function( url ) {
                    if(url.length){
                        let urls = [];
                        for(let i in url){
                            urls.push(url[i].url);
                        }

                        $.ajax({
                            url:"{{route('articles.add-gallery')}}",
                            type:"post",
                            dataType:"json",
                            data:{
                                _token:"{{csrf_token()}}",
                                urls:urls,
                                article_id: article_id
                            },
                            success: function (res) {
                                if(res.success){
                                    document.location.reload();
                                } else {
                                    alert(res.message);
                                }
                            }
                        });
                    }
                };
            });

            $('.delete-item-btn').on('click',function() {
                let _this = $(this);
                let id = _this.data('id');
                Swal.fire({
                    title: 'Вы уверенны?',
                    text: "Вы пытаетесь удалить эту запись?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Да, сделать это!',
                    cancelButtonText: 'Нет'
                }).then((result) => {
                    if (result.value) {
                        $.ajax({
                            url:"{{route('articles.delete-gallery')}}",
                            type:"post",
                            dataType:"json",
                            data:{
                                _token:"{{csrf_token()}}",
                                id: id
                            },
                            success: function (res) {
                                if(res.success){
                                    _this.closest('tr').remove();
                                    Swal.fire({
                                        position: 'top-end',
                                        icon: 'success',
                                        title: 'Изображение успешно удалено!',
                                        showConfirmButton: false,
                                        timer: 2000
                                    })
                                } else {
                                    alert(res.message);
                                }
                            }
                        });
                    }
                });
            });
        })
    </script>
@endpush
