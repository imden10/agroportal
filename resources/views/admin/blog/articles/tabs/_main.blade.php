<div class="form-group row">
    <label class="col-md-3 text-right" for="page_name_{{ $lang }}">Заголовок</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][name]" value="{{ old('page_data.' . $lang . '.name', $data[$lang]['name'] ?? '') }}" id="page_name_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.name') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.name'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_annot_{{ $lang }}">Аннотация</label>
    <div class="col-md-9">
        <textarea
            name="page_data[{{ $lang }}][annot]"
            id="page_annot_{{ $lang }}"
            class="summernote editor {{ $errors->has('page_data.' . $lang . '.annot') ? ' is-invalid' : '' }}"
            cols="30"
            rows="10"
        >{{ old('page_data.' . $lang . '.annot', $data[$lang]['annot'] ?? '') }}</textarea>

        @if ($errors->has('page_data.' . $lang . '.annot'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.annot') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_text_{{ $lang }}">Текст</label>
    <div class="col-md-9">
        <textarea
            name="page_data[{{ $lang }}][text]"
            id="page_text_{{ $lang }}"
            class="summernote editor {{ $errors->has('page_data.' . $lang . '.text') ? ' is-invalid' : '' }}"
            cols="30"
            rows="10"
        >{{ old('page_data.' . $lang . '.text', $data[$lang]['text'] ?? '') }}</textarea>

        @if ($errors->has('page_data.' . $lang . '.text'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.text') }}</strong>
            </span>
        @endif
    </div>
</div>
