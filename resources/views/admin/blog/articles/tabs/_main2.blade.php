<div class="form-group row">
    <label class="col-md-3 text-right" for="page_addtext_{{ $lang }}">Скрипт</label>
    <div class="col-md-9">
        <textarea
            name="page_data[{{ $lang }}][addtext]"
            id="page_addtext_{{ $lang }}"
            class="summernote editor {{ $errors->has('page_data.' . $lang . '.addtext') ? ' is-invalid' : '' }}"
            cols="30"
            rows="5"
        >{{ old('page_data.' . $lang . '.addtext', $data[$lang]['addtext'] ?? '') }}</textarea>

        @if ($errors->has('page_data.' . $lang . '.addtext'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.addtext') }}</strong>
            </span>
        @endif
    </div>
</div>


<div class="form-group row">
    <label class="col-md-3 text-right" for="page_iframe_{{ $lang }}">Iframe</label>
    <div class="col-md-9">
        <textarea
            name="page_data[{{ $lang }}][iframe]"
            id="page_iframe_{{ $lang }}"
            class="form-control {{ $errors->has('page_data.' . $lang . '.iframe') ? ' is-invalid' : '' }}"
            cols="30"
            rows="5"
        >{{ old('page_data.' . $lang . '.iframe', $data[$lang]['iframe'] ?? '') }}</textarea>

        @if ($errors->has('page_data.' . $lang . '.iframe'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.iframe') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_labels_{{ $lang }}">Метка</label>
    <div class="col-md-9">
        @php
            $selLabelsArr = $data[$lang]['labels'] ?? '';

            try {
                 $selLabels = explode(',',$selLabelsArr);
            } catch (Throwable $e){
                \Illuminate\Support\Facades\Log::info('selLabels explode error',[$selLabelsArr]);
                 $selLabels = [];
            }

            $allLabels = \Illuminate\Support\Facades\Cache::remember('all_labels_' . $lang,(60*60*24),function() use($lang){
                return \App\Models\Labels::query()->where('lang',$lang)->pluck('name')->toArray();
            });
        @endphp
        <select name=page_data[{{ $lang }}][labels][]" id="page_labels_{{ $lang }}" class="form-control select2-tag-tag" multiple style="width: 100%">
            @if(count($allLabels))
                @foreach($allLabels as $item)
                    <option value="{{$item}}" @if(in_array($item,$selLabels)) selected @endif>{{$item}}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_img_desc_{{ $lang }}">Описание изображения</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][img_desc]" value="{{ old('page_data.' . $lang . '.img_desc', $data[$lang]['img_desc'] ?? '') }}" id="page_img_desc_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.img_desc') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.img_desc'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.img_desc') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right">Ссылка на лендинг</label>
    <div class="col-md-9">
        <?php
            $wse = $data[$lang]['landing_link'] ?? '';
            $allLandings = \Illuminate\Support\Facades\Cache::remember('all_landings',(60*60*24),function(){
                return \App\Models\Landing::query()->active()->get();
            });
        ?>
        <select name="page_data[{{ $lang }}][landing_link]" class="form-control" style="width: 100%">
            <option value="">---</option>
            @foreach($allLandings as $item)
                <option value="{{$item->id}}" @if($item->id == $wse) selected @endif>{{$item->title}}</option>
            @endforeach
        </select>
    </div>
</div>
