@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item">Блог</li>
            <li class="breadcrumb-item"><a href="{{route('articles.index')}}">Публикации</a></li>
            <li class="breadcrumb-item active" aria-current="page">Редактировать</li>
        </ol>
    </nav>


    @if(isset($model->category->path))
        <div style="display: flex; justify-content: flex-end;margin-bottom: 10px;">
            <a href="{{env('APP_URL')}}/{{$model->category->path}}/{{$model->slug}}?prevw={{crc32($model->slug)}}" target="_blank" title="Посмотреть на сайте" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> Посмотреть на сайте</a>
        </div>
    @endif

    <form class="form-horizontal" method="POST" action="{{route('articles.update', $model->id)}}">
        @method('PUT')
        @csrf

        <div class="row">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-header">
                        <ul class="nav nav-tabs" id="myTab" role="tablist" style="margin-bottom: -10px;border-bottom: none">
                            <li class="nav-item">
                                <a class="nav-link active" id="main-tab" data-toggle="tab" href="#main" role="tab" aria-controls="main" aria-selected="true">Информация</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="seo-tab" data-toggle="tab" href="#seo" role="tab" aria-controls="seo" aria-selected="false">SEO</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="gallery-tab" data-toggle="tab" href="#gallery" role="tab" aria-controls="gallery" aria-selected="false">Галерея</a>
                            </li>
                        </ul>
                    </div>

                    <div class="card-body">
                        <div class="tab-content" id="myTabContent">
                            {{----------------------------- MAIN TAB -----------------------------------------}}
                            <div class="tab-pane fade show active" id="main" role="tabpanel" aria-labelledby="main-tab">
                                <ul class="nav nav-tabs" role="tablist">
                                    @foreach($localizations as $key => $lang)
                                        <li class="nav-item">
                                            <a class="nav-link @if(app()->getLocale() == $key) active @endif" data-toggle="tab" href="#main_lang_{{ $key }}" role="tab">
                                                <span class="hidden-sm-up"></span> <span class="hidden-xs-down">{{ $lang }}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>

                                <br>
                                <div class="tab-content">
                                    {{-- todo проставить категорию блоги --}}
                                    @if(isset($model->category->agro_id) && $model->category->agro_id == 444)
                                        <div class="form-group row">
                                            <label class="col-md-3 text-right" for="blogger_id">Автор блога</label>
                                            <div class="col-md-9">
                                                @php($sel_blogger = old('blogger_id', $model->blogger_id ?? null))
                                                <select name="blogger_id" class="select2">
                                                    <option value="">---</option>
                                                    @foreach(\App\Models\Bloggers::query()->active()->get() as $blogger)
                                                        <option value="{{$blogger->id}}" @if($blogger->id == $sel_blogger) selected @endif>{{$blogger->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    @endif

                                    @foreach($localizations as $key => $catLang)
                                        <div class="tab-pane p-t-20 p-b-20  @if(app()->getLocale() == $key) active @endif" id="main_lang_{{ $key }}" role="tabpanel">
                                            @include('admin.blog.articles.tabs._main',[
                                               'lang' => $key,
                                               'data' => $data,
                                               'model' => $model
                                            ])

                                            @if($model->getTranslation($key))
                                                {!! Constructor::output($model->getTranslation($key),$key) !!}
                                            @endif

                                            @include('admin.blog.articles.tabs._main2',[
                                               'lang' => $key,
                                               'data' => $data,
                                               'model' => $model
                                            ])
                                        </div>
                                    @endforeach

                                    @include('admin.blog.articles._form',['model' => $model])
                                </div>
                            </div>
                            {{----------------------------- SEO TAB -----------------------------------------}}
                            <div class="tab-pane fade" id="seo" role="tabpanel" aria-labelledby="seo-tab">
                                <ul class="nav nav-tabs" role="tablist">
                                    @foreach($localizations as $key => $lang)
                                        <li class="nav-item">
                                            <a class="nav-link @if(app()->getLocale() == $key) active @endif" data-toggle="tab" href="#seo_lang_{{ $key }}" role="tab">
                                                <span class="hidden-sm-up"></span> <span class="hidden-xs-down">{{ $lang }}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>

                                <br>
                                <div class="tab-content tabcontent-border">
                                    @foreach($localizations as $key => $catLang)
                                        <div class="tab-pane p-t-20 p-b-20  @if(app()->getLocale() == $key) active @endif" id="seo_lang_{{ $key }}" role="tabpanel">
                                            @include('admin.blog.articles.tabs._seo',[
                                               'lang' => $key,
                                               'data' => $data
                                            ])
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                            {{----------------------------- Gallery TAB -----------------------------------------}}
                            <div class="tab-pane fade" id="gallery" role="tabpanel" aria-labelledby="gallery-tab">
                                @include('admin.blog.articles.tabs._gallery',[
                                   'data' => $data,
                                   'model' => $model
                                ])
                            </div>
                        </div>

                        <input type="submit" value="Сохранить" class="btn btn-success btn-save-fixed text-white float-right">

                    </div>
                </div>
            </div>
        </div>

    </form>
@endsection

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
    <style>
        /*-- ==============================================================
        Switches
        ============================================================== */
        .material-switch {
            line-height: 3em;
        }
        .material-switch > input[type="checkbox"] {
            display: none;
        }

        .material-switch > label {
            cursor: pointer;
            height: 0px;
            position: relative;
            width: 40px;
        }

        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: -8px;
            position:absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 40px;
        }
        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: -4px;
            margin-top: -8px;
            position: absolute;
            top: -4px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }
        .material-switch > input[type="checkbox"]:checked + label::before {
            background: inherit;
            opacity: 0.5;
        }
        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: 20px;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #5897fb !important;
            border: 1px solid #5897fb !important;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            color: #fff !important;
        }

        .select2-container--classic .select2-selection--multiple .select2-selection__choice, .select2-container--default .select2-selection--multiple .select2-selection__choice, .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            background-color:#5897fb !important;
        }

        .select2-container--default .select2-selection--multiple {
            border: 1px solid #e9ecef;
        }

        .select2-container--default.select2-container--focus .select2-selection--multiple {
            border: 1px solid #e9ecef;
            color: #3e5569;
            background-color: #fff;
            border-color: rgba(0,0,0,0.25);
            outline: 0;
            box-shadow: 0 0 0 0.2rem rgb(0 123 255 / 25%);
        }

        .btn-save-fixed {
            position: fixed;
            right: 50px;
            bottom: 30px;
            z-index: 999;
        }

        .select2-container--default.select2-container--focus .select2-selection--multiple {
            height: auto;
        }

        .select2-container--default .select2-selection--multiple {
            height: auto;
        }
    </style>
@endpush

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script src="{{asset('matrix/libs/moment/moment.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
    <script>
        $(document).ready(() => {
            $('.select2').select2();

            $('.select2-tag').select2({});

            $('.select2-tag-tag').select2({
                tags: true
            });

            $('#public_date').datetimepicker({
                format: 'DD-MM-YYYY HH:mm',
                useCurrent: true,
                icons: {
                    time: "fa fa-clock",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down",
                    previous: "fa fa-chevron-left",
                    next: "fa fa-chevron-right",
                    today: "fa fa-clock",
                    clear: "fa fa-trash"
                }
            });
        });
    </script>
@endpush
