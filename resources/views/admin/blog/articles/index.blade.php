@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item">Рубрики</li>
            @if(! $fromCategory)
                <li class="breadcrumb-item active" aria-current="page">Все публикации</li>
            @else
                <li class="breadcrumb-item active" aria-current="page">{{$category->title}}</li>
                <li class="breadcrumb-item active" aria-current="page">Публикации</li>
            @endif
        </ol>
    </nav>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                @permission('categories__articles__create')
                @if(! $fromCategory)
                    <a href="{{route('articles.create')}}" class="btn btn-success float-right text-white">
                        <i class="fa fa-plus"></i>
                        Добавить публикацию
                    </a>
                @else
                    <a href="{{route('articles.create-with-category',['path' => $category->path])}}" class="btn btn-success text-white float-right">
                        <i class="fa fa-plus"></i>
                        Добавить публикацию
                    </a>

                    @permission('categories__categories__edit')
                    <a href="{{ route('categories.edit', $category->id) }}" style="margin-right: 15px" class="btn btn-primary float-right" title="Редактировать рубрику">
                        <i class="fas fa-edit fa-lg"></i>
                        Редактировать рубрику
                    </a>
                    @endpermission
                @endif
                @endpermission
            </div>
            <div class="card-body">

                <form action="" method="get">
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label>Статус</label>
                            <select name="status" class="select2 form-control m-t-15">
                                <option value="">---</option>
                                @foreach(\App\Models\BlogArticles::getStatuses() as $key => $item)
                                    <option value="{{$key}}" @if(old('status', request()->input('status')) == (string)$key) selected @endif>{{$item['title']}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-2">
                            <label>Дата публикации</label>
                            <?php
                            $dateFrom = $from;
                            $dateTo = $to;
                            $date = request()->input('p');
                            if($date) {
                                $dateFrom = substr($date,0,10);
                                $dateTo = substr($date,13,10);
                            }
                            ?>
                            <input type="text" name="p" class="form-control" style="width: 200px;">
                        </div>

                        <div class="form-group col-md-2">
                            <label>Поиск</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" name="name" value="{{old('name', request()->input('name'))}}">
                                <div class="input-group-append">
                                    <select name="search_lang" class="form-control">
                                        <option value="uk" @if(request()->input('search_lang') == 'uk') selected @endif>UK</option>
                                        <option value="ru" @if(request()->input('search_lang') == 'ru') selected @endif>RU</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-1">
                            <label for="inputPassword4">&nbsp;</label>
                            <button type="submit" class="btn btn-success form-control text-white">Фильтровать</button>
                        </div>
                        <div class="form-group col-md-1">
                            <label for="inputPassword4">&nbsp;</label>
                            <a href="{{ route('articles.index') }}" class="btn btn-danger form-control text-white">Сбросить</a>
                        </div>
                    </div>
                </form>

                <table class="table table-bordered imSortingTableLib">
                    <thead>
                    <tr>
                        <th style="width: 50px">
                            <input type="checkbox" class="check-all">
                        </th>
                        <th class="sorting @if(request()->input('sort') == 'name' && request()->input('order') == 'asc') sorting_asc @elseif(request()->input('sort') == 'name' && request()->input('order') == 'desc') sorting_desc @endif" data-field="name">Название</th>
                        <th>Рубрика</th>
                        <th class="sorting @if(request()->input('sort') == 'views' && request()->input('order') == 'asc') sorting_asc @elseif(request()->input('sort') == 'views' && request()->input('order') == 'desc') sorting_desc @endif" data-field="views" >Просмотры</th>
                        <th class="sorting @if(request()->input('sort') == 'status' && request()->input('order') == 'asc') sorting_asc @elseif(request()->input('sort') == 'status' && request()->input('order') == 'desc') sorting_desc @endif" data-field="status">Статус</th>
                        <th class="sorting @if(request()->input('sort') == 'public_date' && request()->input('order') == 'asc') sorting_asc @elseif(request()->input('sort') == 'public_date' && request()->input('order') == 'desc') sorting_desc @endif" data-field="public_date">Дата публикации</th>
                        <th style="width: 100px">Языки</th>
                        <th>Изображение</th>
                        <th style="min-width: 150px"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($model as $item)
                        <tr data-id="{{$item->id}}">
                            <td>
                                <input type="checkbox" class="checkbox-item" data-check="{{$item->id}}" name="check[]" value="{{$item->id}}">
                            </td>
                            <td>
                                @permission('categories__articles__edit')
                                <a href="{{ route('articles.edit', $item->id) }}">
                                    {{$item->name2}}
                                </a>
                                @else
                                    {{$item->name2}}
                                    @endpermission
                            </td>
                            <td>{{isset($item->category) ? $item->category->getNameWithPath() : '-'}}</td>
                            <td>{{$item->views}}</td>
                            <td>{!! $item->showStatus() !!}</td>
                            <td>{{\Carbon\Carbon::create($item->public_date)->format('d-m-Y H:i')}}</td>
                            <td>{!! $item->showAllLangsNotEmpty() !!}</td>
                            <td style="text-align: center">{!! $item->showIsImageLabel() !!}</td>
                            <td>
                                <form action="{{ route('articles.destroy', $item->id) }}" method="POST">

                                    @if(isset($item->category->path))
                                        <a href="{{env('APP_URL')}}/{{$item->category->path}}/{{$item->slug}}?prevw={{crc32($item->slug)}}" target="_blank" title="Посмотреть на сайте" class="fa fa-eye btn btn-xs alert-info"></a>
                                    @else
                                        <a href="javascript:void(0)" title="У публикаии нет категории" class="btn btn-danger text-white btn-xs"><i class="fa fa-eye"></i></a>
                                    @endif

                                    @permission('categories__articles__edit')
                                    <a href="{{ route('articles.edit', $item->id) }}" class="fa fa-edit btn btn-xs alert-primary"></a>
                                    @endpermission

                                    @csrf
                                    @method('DELETE')

                                    @permission('categories__articles__delete')
                                    <a href="javascript:void(0)" title="Удалить" class="fa fa-trash btn alert-danger btn-xs delete-item-btn"></a>
                                    @endpermission
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <form action="{{route('articles.delete-selected')}}" method="post" id="delete_sel_form">
                    @csrf
                    <input type="hidden" name="ids">
                </form>
                <div class="row">
                    <div class="col-sm-12">
                        <label style="margin-right: 15px">С отмеченными:</label>

                        <span class="fa fa-trash btn alert-danger btn-xs btn-delete-checked" title="Удалить"></span>

                    </div>
                </div>

                {{ $model->appends(request()->all())->links() }}
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <style>
        table.imSortingTableLib th.sorting {
            position: relative;
        }

        table.imSortingTableLib th.sorting::before {
            right: 1em;
            content: "\2191";
            opacity: 0.5;
            position: absolute;
        }

        table.imSortingTableLib th.sorting::after {
            right: 0.5em;
            content: "\2193";
            opacity: 0.5;
            position: absolute;
        }

        table.imSortingTableLib th.sorting.sorting_asc::before {
            opacity: 1;
        }

        table.imSortingTableLib th.sorting.sorting_desc::after {
            opacity: 1;
        }
    </style>
@endpush

@push('scripts')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script>
        function insertParam(key, value,url = null) {
            key = encodeURIComponent(key);
            value = encodeURIComponent(value);

            if(url){
                var kvp = url.substr(1).split('&');
            } else {
                var kvp = document.location.search.substr(1).split('&');
            }
            let i=0;

            for(; i<kvp.length; i++){
                if (kvp[i].startsWith(key + '=')) {
                    let pair = kvp[i].split('=');
                    pair[1] = value;
                    kvp[i] = pair.join('=');
                    break;
                }
            }

            if(i >= kvp.length){
                kvp[kvp.length] = [key,value].join('=');
            }

            // can return this or...
            let params = kvp.join('&');

            return params;
        }

        $(document).ready(() => {
            $('.delete-item-btn').on('click',function() {
                let _this = $(this);
                Swal.fire({
                    title: 'Вы уверенны?',
                    text: "Вы пытаетесь удалить эту запись?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Да, сделать это!',
                    cancelButtonText: 'Нет'
                }).then((result) => {
                    if (result.value) {
                        _this.closest('form').submit();
                    }
                });
            });

            $('.check-all').on('change',function () {
                if($(this).prop('checked')){
                    $('.checkbox-item').prop('checked',true);
                } else {
                    $('.checkbox-item').prop('checked',false);
                }
            });

            $('.checkbox-item').on('change',function () {
                let checked = 0;
                $('.checkbox-item').each(function () {
                    if($(this).prop('checked')){
                        checked++;
                    }
                });

                if($('.checkbox-item').length === checked){
                    $('.check-all').prop('checked',true)
                } else {
                    $('.check-all').prop('checked',false)
                }
            });

            $('.btn-delete-checked').on('click',function () {
                let checked = [];

                $('.checkbox-item').each(function () {
                    if($(this).prop('checked')){
                        checked.push($(this).val());
                    }
                });

                if(checked.length == 0){
                    Swal.fire({
                        position: 'top-end',
                        icon: 'warning',
                        title: 'Нет выбранных публикаций',
                        showConfirmButton: false,
                        timer: 1500
                    })
                } else {
                    Swal.fire({
                        title: 'Вы уверенны?',
                        text: "Вы пытаетесь удалить публикации!",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Да, сделать это!',
                        cancelButtonText: 'Нет'
                    }).then((result) => {
                        if (result.value) {
                            $('#delete_sel_form').find('input[name="ids"]').val(JSON.stringify(checked));
                            $('#delete_sel_form').submit();
                        }
                    });
                }
            });

            $('input[name="p"]').daterangepicker({
                opens: 'left',
                autoApply: true,
                locale: {
                    format: 'DD-MM-YYYY',
                    customRangeLabel: "Произвольный диапазон",
                    daysOfWeek: [
                        "Вс",
                        "Пн",
                        "Вт",
                        "Ср",
                        "Чт",
                        "Пт",
                        "Сб"
                    ],
                    monthNames: [
                        "Январь",
                        "Февраль",
                        "Март",
                        "Апрель",
                        "Май",
                        "Июнь",
                        "Июль",
                        "Август",
                        "Сентябрь",
                        "Октябрь",
                        "Ноябрь",
                        "Декабрь"
                    ],
                    firstDay: 1
                },
                startDate: "{{$dateFrom}}",
                endDate: "{{$dateTo}}",
                alwaysShowCalendars: true,
                ranges: {
                    'Сегодня': [moment(), moment()],
                    'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Последние 7 дней': [moment().subtract(6, 'days'), moment()],
                    'Последние 30 дней': [moment().subtract(29, 'days'), moment()],
                    'Этот месяц': [moment().startOf('month'), moment().endOf('month')],
                    'Прошлый месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            });

            $('input[name="p"]').on('apply.daterangepicker', function(ev, picker) {
                $(".filter-btn-apply").trigger('click');
            });

            /* SORTING*/
            $(document).on('click',"table.imSortingTableLib th.sorting",function () {
                $("table.imSortingTableLib th.sorting").removeClass('sorting_asc');
                $("table.imSortingTableLib th.sorting").removeClass('sorting_desc');
                $(this).addClass('sorting_asc');

                let field = $(this).data('field');
                let url = insertParam('sort',field);
                url = insertParam('order','asc','?' + url);

                document.location.search = url;
            });

            $(document).on('click',"table.imSortingTableLib th.sorting_asc",function () {
                $("table.imSortingTableLib th.sorting").removeClass('sorting_asc');
                $("table.imSortingTableLib th.sorting").removeClass('sorting_desc');
                $(this).addClass('sorting_desc');

                let field = $(this).data('field');
                let url = insertParam('sort',field);
                url = insertParam('order','desc','?' + url);

                document.location.search = url;
            });

            $(document).on('click',"table.imSortingTableLib th.sorting_desc",function () {
                $("table.imSortingTableLib th.sorting").removeClass('sorting_asc');
                $("table.imSortingTableLib th.sorting").removeClass('sorting_desc');
                $(this).addClass('sorting_asc');

                let field = $(this).data('field');
                let url = insertParam('sort',field);
                url = insertParam('order','asc','?' + url);

                document.location.search = url;
            });
        });
    </script>
@endpush

