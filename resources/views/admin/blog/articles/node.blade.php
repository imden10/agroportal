@foreach($categories as $item)

    @if(count($item['children']) > 0)
        <optgroup label="{{ $item['title'] }}">
    @else
            <option value="{{ $item['id'] }}"  @if(in_array($item['id'],$selCat)) selected @endif>{{ $item['title'] }}</option>
    @endif

    @if(!empty($item['children']))
        @php($categories = $item['children'])
        @include('admin.blog.articles.node',['sel' => $selCat ?? []])
    @endif

    @if(count($item['children']) > 0)
        </optgroup>
    @endif

@endforeach
