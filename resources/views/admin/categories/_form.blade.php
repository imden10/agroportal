<div class="form-group row">
    <label class="col-md-3 text-right" for="page_title_{{ $lang }}">Название</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][title]" value="{{ old('page_data.' . $lang . '.title', $data[$lang]['title'] ?? '') }}" id="page_title_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.title') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.title'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.title') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_h2_{{ $lang }}">H2</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][h2]" value="{{ old('page_data.' . $lang . '.h2', $data[$lang]['h2'] ?? '') }}" id="page_h2_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.h2') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.h2'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.h2') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_description_{{ $lang }}">Описание</label>
    <div class="col-md-9">
        <textarea
            name="page_data[{{ $lang }}][description]"
            id="page_description_{{ $lang }}"
            class="summernote editor {{ $errors->has('page_data.' . $lang . '.description') ? ' is-invalid' : '' }}"
            cols="30"
            rows="10"
        >{{ old('page_data.' . $lang . '.description', $data[$lang]['description'] ?? '') }}</textarea>

        @if ($errors->has('page_data.' . $lang . '.description'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.description') }}</strong>
            </span>
        @endif
    </div>
</div>

{{--<div class="form-group row">--}}
{{--    <label class="col-md-3 text-right" for="page_excerpt_{{ $lang }}">Скрытое описание</label>--}}
{{--    <div class="col-md-9">--}}
{{--        <textarea--}}
{{--            name="page_data[{{ $lang }}][excerpt]"--}}
{{--            id="page_excerpt_{{ $lang }}"--}}
{{--            class="summernote editor {{ $errors->has('page_data.' . $lang . '.excerpt') ? ' is-invalid' : '' }}"--}}
{{--            cols="30"--}}
{{--            rows="10"--}}
{{--        >{{ old('page_data.' . $lang . '.excerpt', $data[$lang]['excerpt'] ?? '') }}</textarea>--}}

{{--        @if ($errors->has('page_data.' . $lang . '.excerpt'))--}}
{{--            <span class="invalid-feedback" role="alert">--}}
{{--                <strong>{{ $errors->first('page_data.' . $lang . '.excerpt') }}</strong>--}}
{{--            </span>--}}
{{--        @endif--}}
{{--    </div>--}}
{{--</div>--}}

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_meta_title_{{ $lang }}">Meta title</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][meta_title]" value="{{ old('page_data.' . $lang . '.meta_title', $data[$lang]['meta_title'] ?? '') }}" id="page_meta_title_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.meta_title') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.meta_title'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.meta_title') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_meta_description_{{ $lang }}">Meta description</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][meta_description]" value="{{ old('page_data.' . $lang . '.meta_description', $data[$lang]['meta_description'] ?? '') }}" id="page_meta_description_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.meta_description') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.meta_description'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.meta_description') }}</strong>
            </span>
        @endif
    </div>
</div>
