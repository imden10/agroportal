@php($settingData = $model->setting ? json_decode($model->setting->setting,true) : [] )

<ul class="nav nav-tabs" id="myTabSetting" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="setting_cat1-tab" data-toggle="tab" href="#setting_cat1" role="tab" aria-controls="setting_cat1" aria-selected="true">Блок &laquo;Категория 1&raquo;</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="setting_news-tab" data-toggle="tab" href="#setting_news" role="tab" aria-controls="setting_news" aria-selected="false">Блок &laquo;Новости&raquo;</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="setting_cat2-tab" data-toggle="tab" href="#setting_cat2" role="tab" aria-controls="setting_cat2" aria-selected="true">Блок &laquo;Категория 2&raquo;</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="setting_blog-tab" data-toggle="tab" href="#setting_blog" role="tab" aria-controls="setting_blog" aria-selected="true">Блок &laquo;Блоги&raquo;</a>
    </li>
</ul>

<br>

<div class="tab-content" id="myTabSettingContent">
    <div class="tab-pane fade show active" id="setting_cat1" role="tabpanel" aria-labelledby="setting_cat1-tab">
        <div class="form-group row">
            <label class="col-md-3 text-right">Статус</label>
            <div class="col-md-9">
                <div class="material-switch pull-left">
                    <input id="setting_cat1_status" name="aside_setting[cat1][status]" value="1"
                           type="checkbox" {{ (isset($settingData['cat1']['status'])) ? ' checked' : '' }}/>
                    <label for="setting_cat1_status" class="label-success"></label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-3 text-right" for="setting_cat1_category">Выберите рубрику</label>
            <div class="col-md-9">
                <?php
                    $categories = app(\App\Models\Category::class)->treeStructure();
                    $selCat1 = $settingData['cat1']['category_id'] ?? null;
                ?>
                <select name="aside_setting[cat1][category_id]" id="setting_cat1_category" class="select2-field" style="width: 100%">
                    <option value="">---</option>
                    @include('admin.categories.menu_node',['sel' => $selCat1])
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-3 text-right" for="setting_cat1_count">Количество элементов</label>
            <div class="col-md-9">
                <input type="number" name="aside_setting[cat1][count]" value="{{$settingData['cat1']['count'] ?? ''}}"
                       id="setting_cat1_count"
                       class="form-control">
            </div>
        </div>
    </div>

    <div class="tab-pane fade" id="setting_news" role="tabpanel" aria-labelledby="setting_news-tab">
        <div class="form-group row">
            <label class="col-md-3 text-right">Статус</label>
            <div class="col-md-9">
                <div class="material-switch pull-left">
                    <input id="setting_news_status" name="aside_setting[news][status]" value="1"
                           type="checkbox" {{ (isset($settingData['news']['status'])) ? ' checked' : '' }}/>
                    <label for="setting_news_status" class="label-success"></label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-3 text-right" for="setting_news_count">Количество элементов</label>
            <div class="col-md-9">
                <input type="number" name="aside_setting[news][count]" value="{{$settingData['news']['count'] ?? ''}}"
                       id="setting_news_count"
                       class="form-control">
            </div>
        </div>
    </div>

    <div class="tab-pane fade" id="setting_cat2" role="tabpanel" aria-labelledby="setting_cat2-tab">
        <div class="form-group row">
            <label class="col-md-3 text-right">Статус</label>
            <div class="col-md-9">
                <div class="material-switch pull-left">
                    <input id="setting_cat2_status" name="aside_setting[cat2][status]" value="1"
                           type="checkbox" {{ (isset($settingData['cat2']['status'])) ? ' checked' : '' }}/>
                    <label for="setting_cat2_status" class="label-success"></label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-3 text-right" for="setting_cat2_category">Выберите рубрику</label>
            <div class="col-md-9">
                <?php
                    $categories = app(\App\Models\Category::class)->treeStructure();
                    $selCat2 = $settingData['cat2']['category_id'] ?? null;
                ?>
                <select name="aside_setting[cat2][category_id]" id="setting_cat2_category" class="select2-field" style="width: 100%">
                    <option value="">---</option>
                    @include('admin.categories.menu_node',['sel' => $selCat2])
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-3 text-right" for="setting_cat2_count">Количество элементов</label>
            <div class="col-md-9">
                <input type="number" name="aside_setting[cat2][count]" value="{{$settingData['cat2']['count'] ?? ''}}"
                       id="setting_cat2_count"
                       class="form-control">
            </div>
        </div>
    </div>

    <div class="tab-pane fade" id="setting_blog" role="tabpanel" aria-labelledby="setting_blog-tab">
        <div class="form-group row">
            <label class="col-md-3 text-right">Статус</label>
            <div class="col-md-9">
                <div class="material-switch pull-left">
                    <input id="setting_blog_status" name="aside_setting[blog][status]" value="1"
                           type="checkbox" {{ (isset($settingData['blog']['status'])) ? ' checked' : '' }}/>
                    <label for="setting_blog_status" class="label-success"></label>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-3 text-right" for="setting_blog_count">Количество элементов</label>
            <div class="col-md-9">
                <input type="number" name="aside_setting[blog][count]" value="{{$settingData['blog']['count'] ?? ''}}"
                       id="setting_blog_count"
                       class="form-control">
            </div>
        </div>
    </div>
</div>
