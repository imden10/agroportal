@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item">Рубрики</li>
            <li class="breadcrumb-item active" aria-current="page">Редактировать</li>
        </ol>
    </nav>
    <form class="form-horizontal" method="POST" action="{{route('categories.update', $model->id)}}">
        @csrf
        @method('PUT')

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <ul class="nav nav-tabs" id="myTab" role="tablist" style="margin-bottom: -10px;border-bottom: none">
                            <li class="nav-item">
                                <a class="nav-link active" id="main-tab" data-toggle="tab" href="#main" role="tab" aria-controls="main" aria-selected="true">Инфо</a>
                            </li>
                            @if($model->parent_id == 0)
                                <li class="nav-item">
                                    <a class="nav-link" id="setting-tab" data-toggle="tab" href="#setting" role="tab" aria-controls="setting" aria-selected="false">Настройки боковой колонки</a>
                                </li>
                            @endif
                        </ul>
                    </div>
                    <div class="card-body">
                        <div class="tab-content" id="myTabContent">
                            {{----------------------------- MAIN TAB -----------------------------------------}}
                            <div class="tab-pane fade show active" id="main" role="tabpanel" aria-labelledby="main-tab">
                                <ul class="nav nav-tabs" role="tablist">
                                    @foreach($localizations as $key => $lang)
                                        <li class="nav-item">
                                            <a class="nav-link @if(app()->getLocale() == $key) active @endif"
                                               data-toggle="tab" href="#main_lang_{{ $key }}" role="tab">
                                                <span class="hidden-sm-up"></span> <span
                                                    class="hidden-xs-down">{{ $lang }}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>

                                <br>
                                <div class="tab-content">
                                    @foreach($localizations as $key => $catLang)
                                        <div class="tab-pane p-t-20 p-b-20  @if(app()->getLocale() == $key) active @endif"
                                             id="main_lang_{{ $key }}" role="tabpanel">
                                            @include('admin.categories._form',[
                                               'lang'  => $key,
                                               'model' => $model,
                                               'data'  => $data
                                            ])
                                        </div>
                                    @endforeach

                                    <div class="form-group row">
                                        <label class="col-md-3 text-right" for="page_parent_id">Родительская категория</label>
                                        <div class="col-md-9">
                                            <select id="parent_id" name="parent_id" class="select2 form-control custom-select{{ $errors->has('parent_id') ? ' is-invalid' : '' }}" style="width: 100%; height:36px;">
                                                <option value="0" @if($model->parent_id == 0) selected @endif>Главная</option>

                                                @include('admin.categories.node')
                                            </select>

                                            @if ($errors->has('parent_id'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('parent_id') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 text-right">RSS категория</label>
                                        <div class="col-md-9">
                                            <select name="rss_category_id" class="form-control rss_category_id">
                                                <?php $sel = old('rss_category_id', $model->rss_category_id ?? ''); ?>
                                                <option value="">---</option>
                                                @foreach(\App\Models\RssCategory::getList() as $id => $item)
                                                    <option value="{{$id}}" @if($id == $sel || ($sel > 17 && $id == 17)) selected @endif>{{$item}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group row rss-sub-category" @if($sel < 17) style="display: none" @endif>
                                        <label class="col-md-3 text-right"></label>
                                        <div class="col-md-9">
                                            <select name="rss_sub_category_id" class="form-control rss_sub_category_id">
                                                <?php $sel = old('rss_category_id', $model->rss_category_id ?? ''); ?>
                                                <option value="">---</option>
                                                @foreach(\App\Models\RssCategory::getSubList(17) as $id => $item)
                                                    <option value="{{$id}}" @if($id == $sel) selected @endif>{{$item}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>


                                        <div class="form-group row">
                                        <label class="col-md-3 text-right" for="page_order">Сортировка</label>
                                        <div class="col-md-9">
                                            <input type="text" name="order" value="{{ old('order', $model->order ?? '') }}"
                                                   id="page_order"
                                                   class="form-control{{ $errors->has('order') ? ' is-invalid' : '' }}">

                                            @if ($errors->has('order'))
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('order') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 text-right" for="page_slug">Slug</label>
                                        <div class="col-md-9">
                                            <input type="text" name="slug" value="{{ old('slug', $model->slug ?? '') }}"
                                                   id="page_slug"
                                                   class="form-control{{ $errors->has('slug') ? ' is-invalid' : '' }}">

                                            @if ($errors->has('slug'))
                                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('slug') }}</strong>
                                        </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 text-right" for="page_active">Активная</label>
                                        <div class="col-md-9">
                                            <div class="material-switch pull-left">
                                                <input id="someSwitchOptionSuccess_active" name="active" value="1"
                                                       type="checkbox" {{ old('active', $model->active) ? ' checked' : '' }}/>
                                                <label for="someSwitchOptionSuccess_active" class="label-success"></label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="col-md-3 text-right" for="page_status">Статус</label>
                                        <div class="col-md-9">
                                            <div class="material-switch pull-left">
                                                <input id="someSwitchOptionSuccess" name="status" value="1"
                                                       type="checkbox" {{ old('status', $model->status) ? ' checked' : '' }}/>
                                                <label for="someSwitchOptionSuccess" class="label-success"></label>
                                            </div>

                                            @if ($errors->has('status'))
                                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('status') }}</strong>
                                        </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            {{----------------------------- ASIDE SETTINGS TAB -----------------------------------------}}
                            @if($model->parent_id == 0)
                            <div class="tab-pane fade" id="setting" role="tabpanel" aria-labelledby="setting-tab">
                                @include('admin.categories._aside_setting',['model' => $model])
                            </div>
                            @endif
                        </div>

                        <input type="submit" value="Сохранить" class="btn btn-success text-white float-right">

                    </div>
                </div>
            </div>
        </div>

    </form>

    @permission('categories__categories__delete')
        <form action="{{ route('categories.destroy', $model->id) }}" method="POST">
            @csrf
            @method('DELETE')

            <a href="javascript:void(0)" title="Удалить" style="position: absolute;right: 150px;bottom: 110px;" class="btn btn-danger delete-item-btn text-white float-right">
                <i class="fas fa-trash"></i>
            </a>
        </form>
    @endpermission
@endsection

@push('styles')
    <link rel="stylesheet" href="{{asset('assets/plugins/select2/css/select2.min.css')}}">
    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #5897fb !important;
            border: 1px solid #5897fb !important;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            color: #fff !important;
        }

        .select2-container--classic .select2-selection--multiple .select2-selection__choice, .select2-container--default .select2-selection--multiple .select2-selection__choice, .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            background-color:#5897fb !important;
        }

        .select2-container--default .select2-selection--multiple {
            border: 1px solid #e9ecef;
        }

        .select2-container--default.select2-container--focus .select2-selection--multiple {
            border: 1px solid #e9ecef;
            color: #3e5569;
            background-color: #fff;
            border-color: rgba(0,0,0,0.25);
            outline: 0;
            box-shadow: 0 0 0 0.2rem rgb(0 123 255 / 25%);
        }
        /*-- ==============================================================
        Switches
        ============================================================== */
        .material-switch {
            line-height: 3em;
        }
        .material-switch > input[type="checkbox"] {
            display: none;
        }

        .material-switch > label {
            cursor: pointer;
            height: 0px;
            position: relative;
            width: 40px;
        }

        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: -8px;
            position:absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 40px;
        }
        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: -4px;
            margin-top: -8px;
            position: absolute;
            top: -4px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }
        .material-switch > input[type="checkbox"]:checked + label::before {
            background: inherit;
            opacity: 0.5;
        }
        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: 20px;
        }

        .select2-container--classic .select2-selection--single, .select2-container--default .select2-selection--multiple, .select2-container--default .select2-selection--single, .select2-container--default .select2-selection--single .select2-selection__arrow, .select2-container--default .select2-selection--single .select2-selection__rendered {
            height: inherit;
        }
    </style>
@endpush

@push('scripts')
    <script src="{{asset('assets/plugins/select2/js/select2.min.js')}}"></script>
    <script>
        $(document).ready(function(){
            $(".select2-field").select2();

            $(".select2-field-tagable").select2({
                tags: true
            });

            $('.delete-item-btn').on('click',function() {
                let _this = $(this);
                Swal.fire({
                    title: 'Вы уверенны?',
                    text: "Вы пытаетесь удалить эту запись?",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Да, сделать это!',
                    cancelButtonText: 'Нет'
                }).then((result) => {
                    if (result.value) {
                        _this.closest('form').submit();
                    }
                });
            });

            $(".rss_category_id").on('change',function () {
                let val = $(this).val();

                if(val == 17){
                    $(".rss-sub-category").css('display','flex');
                } else {
                    $(".rss-sub-category").css('display','none');
                    $(".rss-sub-category").find('.rss_sub_category_id').val(null);
                }
            })
        });
    </script>
@endpush
