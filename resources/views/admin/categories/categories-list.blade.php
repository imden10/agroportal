@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb" class="breadcrumb-nav">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item active" aria-current="page">Рубрики</li>
            <li class="breadcrumb-item active" aria-current="page">{{$category->title}}</li>
        </ol>
    </nav>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                @permission('categories__categories__edit')
                <a href="{{ route('categories.edit', $category->id) }}" class="btn btn-primary float-right" title="Редактировать рубрику">
                    <i class="fas fa-edit fa-lg"></i>
                    Редактировать рубрику
                </a>
                @endpermission
            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Название</th>
                            <th>Сортировка</th>
                            <th>Статус</th>
                            <th>Дата создания</th>
                            <th style="min-width: 150px"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($subCategories as $item)
                            <tr data-id="{{$item->id}}">
                                <td>{{$item->id}}</td>
                                <td>
                                    <a href="/admin/category/{{$item->path}}">
                                        {{$item->title}}
                                    </a>
                                </td>
                                <td>{{$item->order}}</td>
                                <td>{!! $item->showStatus() !!}</td>
                                <td>{{$item->created_at->format('d.m.Y')}}</td>
                                <td>
                                    <div style="display: flex">
                                        <div style="margin-left: 10px">
                                            <form action="{{ route('categories.destroy', $item->id) }}" method="POST">

{{--                                                <a href="/catalog/{{$item->path}}" target="_blank" class="btn btn-info btn-xs" title="Посмотреть на сайте"><i class="fa fa-eye"></i></a>--}}

                                                @permission('categories__categories__edit')
                                                <a href="{{ route('categories.edit', $item->id) }}" class="btn btn-xs btn-primary" title="Редактировать рубрику">
                                                    <i class="fas fa-edit fa-lg"></i>
                                                </a>
                                                @endpermission

                                                @csrf
                                                @method('DELETE')
                                                @permission('categories__categories__delete')
                                                <a href="javascript:void(0)" title="Удалить" class="btn btn-danger btn-xs delete-item-btn text-white">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                                @endpermission
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $subCategories->appends(request()->all())->links() }}
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(document).ready(() => {
            $('.delete-item-btn').on('click',function() {
                if(confirm('Вы пытаетесь удалить запись?')){
                    $(this).closest('form').submit();
                }
            });
        });
    </script>
@endpush
