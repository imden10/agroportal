@foreach($categories as $item)

    @if(count($item['children']) > 0)
        <optgroup label="{{ $item['title'] }}">
            <option value="{{ $item['id'] }}" @if(isset($sel) && $sel == $item['id']) selected @endif>{{ $item['title'] }}</option>
    @else
            <option value="{{ $item['id'] }}" @if(isset($sel) && $sel == $item['id']) selected @endif>{{ $item['title'] }}</option>
    @endif

    @if(!empty($item['children']))
        @php($categories = $item['children'])
        @include('admin.categories.menu_node')
    @endif

    @if(count($item['children']) > 0)
        </optgroup>
    @endif

@endforeach
