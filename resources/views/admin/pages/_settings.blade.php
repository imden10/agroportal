<?php
    $defaultLangCode = \App\Models\Langs::getDefaultLangCode();
    $categories = app(\App\Models\Category::class)->treeStructure();
?>
<ul class="nav nav-tabs" id="myTabB" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="news-tab" data-toggle="tab" href="#news" role="tab" aria-controls="news" aria-selected="true">Новости</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="blog-tab" data-toggle="tab" href="#blog" role="tab" aria-controls="blog" aria-selected="false">Блоги</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="block1-tab" data-toggle="tab" href="#block1" role="tab" aria-controls="block1" aria-selected="false">Блок 1</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="block2-tab" data-toggle="tab" href="#block2" role="tab" aria-controls="block2" aria-selected="false">Блок 2</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="block3-tab" data-toggle="tab" href="#block3" role="tab" aria-controls="block3" aria-selected="false">Блок 3</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="block4-tab" data-toggle="tab" href="#block4" role="tab" aria-controls="block4" aria-selected="false">Блок 4</a>
    </li>
</ul>

<br>

<div class="tab-content" id="myTabContentB">
    {{------------------------------------------ NEWS ----------------------------------------------------------------}}
    <div class="tab-pane fade show active" id="news" role="tabpanel" aria-labelledby="news-tab">
        <div class="form-group row">
            <label class="col-md-3 text-right" for="setting_data_main_page_setting_news_count_{{ $defaultLangCode }}">Количество элементов</label>
            <div class="col-md-9">
                <?php
                    $v = isset($data[$defaultLangCode]['main_page_setting'][0]['value']) ? json_decode($data[$defaultLangCode]['main_page_setting'][0]['value'],true) : null;
                ?>
                <input type="text" name="setting_data[{{ $defaultLangCode }}][main_page_setting][news_count]" value="{{ old('setting_data.' . $defaultLangCode . '.main_page_setting.news_count', $v['news_count'] ?? '') }}" id="setting_data_main_page_setting_news_count_{{ $defaultLangCode }}" class="form-control">
            </div>
        </div>
    </div>
    {{----------------------------------------------------------------------------------------------------------------}}

    {{------------------------------------------ BLOGS ---------------------------------------------------------------}}
    <div class="tab-pane fade" id="blog" role="tabpanel" aria-labelledby="blog-tab">
        <div class="form-group row">
            <label class="col-md-3 text-right" for="setting_data_main_page_setting_blog_count_{{ $defaultLangCode }}">Количество элементов</label>
            <div class="col-md-9">
                <?php
                $v = isset($data[$defaultLangCode]['main_page_setting'][0]['value']) ? json_decode($data[$defaultLangCode]['main_page_setting'][0]['value'],true) : null;
                ?>
                <input type="text" name="setting_data[{{ $defaultLangCode }}][main_page_setting][blog_count]" value="{{ old('setting_data.' . $defaultLangCode . '.main_page_setting.blog_count', $v['blog_count'] ?? '') }}" id="setting_data_main_page_setting_blog_count_{{ $defaultLangCode }}" class="form-control">
            </div>
        </div>
    </div>
    {{----------------------------------------------------------------------------------------------------------------}}

    {{------------------------------------------ BLOCK 1 -------------------------------------------------------------}}
    <div class="tab-pane fade" id="block1" role="tabpanel" aria-labelledby="block1-tab">
        <div class="form-group row">
            <label class="col-md-3 text-right">Рубрика/Тег</label>
            <div class="col-md-9">
                <?php
                    $v = isset($data[$defaultLangCode]['main_page_setting'][0]['value']) ? json_decode($data[$defaultLangCode]['main_page_setting'][0]['value'],true) : null;
                    $val = old('setting_data.' . $defaultLangCode . '.main_page_setting.block1_type', $v['block1_type'] ?? '');
                ?>
                <select name="setting_data[{{ $defaultLangCode }}][main_page_setting][block1_type]" class="form-control elem-type">
                    <option value="">---</option>
                    <option value="category" @if($val === 'category') selected @endif>Рубрика</option>
                    <option value="tag" @if($val === 'tag') selected @endif>Тег</option>
                </select>
            </div>
        </div>

        <div class="form-group row block-type block-category" @if(isset($v['block1_type']) && $v['block1_type'] == 'category') style="display: flex" @else style="display: none" @endif >
            <label class="col-md-3 text-right">Рубрика</label>
            <div class="col-md-9">
                <?php
                    $v = isset($data[$defaultLangCode]['main_page_setting'][0]['value']) ? json_decode($data[$defaultLangCode]['main_page_setting'][0]['value'],true) : null;
                    $val = old('setting_data.' . $defaultLangCode . '.main_page_setting.block1_category', $v['block1_category'] ?? '');
                ?>
                <select name="setting_data[{{ $defaultLangCode }}][main_page_setting][block1_category]" class="select2-field" style="width: 100%">
                    <option value="">---</option>
                    @include('admin.categories.menu_node',['sel' => $val])
                </select>
            </div>
        </div>

        <div class="form-group row block-type block-tag" @if(isset($v['block1_type']) && $v['block1_type'] == 'tag') style="display: flex" @else style="display: none" @endif >
            <label class="col-md-3 text-right">Тег</label>
            <div class="col-md-9">
                <?php
                    $v = isset($data[$defaultLangCode]['main_page_setting'][0]['value']) ? json_decode($data[$defaultLangCode]['main_page_setting'][0]['value'],true) : null;
                    $val = old('setting_data.' . $defaultLangCode . '.main_page_setting.block1_tag', $v['block1_tag'] ?? '');
                    $selTag = \App\Models\NewTag::query()
                        ->leftJoin('new_tag_translations','new_tag_translations.new_tag_id','=','new_tags.id')
                        ->where('new_tag_translations.lang',$defaultLangCode)
                        ->select([
                            'new_tags.id',
                            'new_tag_translations.name'
                        ])
                        ->where('new_tags.id',$val)
                        ->first();
                ?>
                <select name="setting_data[{{ $defaultLangCode }}][main_page_setting][block1_tag]" class="select2-elem-tag" style="width: 100%">
                    @if($selTag)
                        <option value="{{$val}}">{{$selTag->name}}</option>
                    @endif
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-3 text-right" for="setting_data_main_page_setting_block1_count_{{ $defaultLangCode }}">Количество элементов</label>
            <div class="col-md-9">
                <?php
                $v = isset($data[$defaultLangCode]['main_page_setting'][0]['value']) ? json_decode($data[$defaultLangCode]['main_page_setting'][0]['value'],true) : null;
                ?>
                <input type="text" name="setting_data[{{ $defaultLangCode }}][main_page_setting][block1_count]" value="{{ old('setting_data.' . $defaultLangCode . '.main_page_setting.block1_count', $v['block1_count'] ?? '') }}" id="setting_data_main_page_setting_block1_count_{{ $defaultLangCode }}" class="form-control">
            </div>
        </div>
    </div>
    {{----------------------------------------------------------------------------------------------------------------}}

    {{------------------------------------------ BLOCK 2 -------------------------------------------------------------}}
    <div class="tab-pane fade" id="block2" role="tabpanel" aria-labelledby="block2-tab">
        <div class="form-group row">
            <label class="col-md-3 text-right">Рубрика/Тег</label>
            <div class="col-md-9">
                <?php
                    $v = isset($data[$defaultLangCode]['main_page_setting'][0]['value']) ? json_decode($data[$defaultLangCode]['main_page_setting'][0]['value'],true) : null;
                    $val = old('setting_data.' . $defaultLangCode . '.main_page_setting.block2_type', $v['block2_type'] ?? '');
                ?>
                <select name="setting_data[{{ $defaultLangCode }}][main_page_setting][block2_type]" class="form-control elem-type">
                    <option value="">---</option>
                    <option value="category" @if($val === 'category') selected @endif>Рубрика</option>
                    <option value="tag" @if($val === 'tag') selected @endif>Тег</option>
                </select>
            </div>
        </div>

        <div class="form-group row block-type block-category" @if(isset($v['block2_type']) && $v['block2_type'] == 'category') style="display: flex" @else style="display: none" @endif >
            <label class="col-md-3 text-right">Рубрика</label>
            <div class="col-md-9">
                <?php
                $v = isset($data[$defaultLangCode]['main_page_setting'][0]['value']) ? json_decode($data[$defaultLangCode]['main_page_setting'][0]['value'],true) : null;
                $val = old('setting_data.' . $defaultLangCode . '.main_page_setting.block2_category', $v['block2_category'] ?? '');
                ?>
                <select name="setting_data[{{ $defaultLangCode }}][main_page_setting][block2_category]" class="select2-field" style="width: 100%">
                    <option value="">---</option>
                    @include('admin.categories.menu_node',['sel' => $val])
                </select>
            </div>
        </div>

        <div class="form-group row block-type block-tag" @if(isset($v['block2_type']) && $v['block2_type'] == 'tag') style="display: flex" @else style="display: none" @endif>
            <label class="col-md-3 text-right">Тег</label>
            <div class="col-md-9">
                <?php
                $v = isset($data[$defaultLangCode]['main_page_setting'][0]['value']) ? json_decode($data[$defaultLangCode]['main_page_setting'][0]['value'],true) : null;
                $val = old('setting_data.' . $defaultLangCode . '.main_page_setting.block2_tag', $v['block2_tag'] ?? '');
                $selTag =\App\Models\NewTag::query()
                    ->leftJoin('new_tag_translations','new_tag_translations.new_tag_id','=','new_tags.id')
                    ->where('new_tag_translations.lang',$defaultLangCode)
                    ->select([
                        'new_tags.id',
                        'new_tag_translations.name'
                    ])
                    ->where('new_tags.id',$val)
                    ->first();
                ?>
                <select name="setting_data[{{ $defaultLangCode }}][main_page_setting][block2_tag]" class="select2-elem-tag" style="width: 100%">
                    @if($selTag)
                        <option value="{{$val}}">{{$selTag->name}}</option>
                    @endif
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-3 text-right" for="setting_data_main_page_setting_block2_count_{{ $defaultLangCode }}">Количество элементов</label>
            <div class="col-md-9">
                <?php
                $v = isset($data[$defaultLangCode]['main_page_setting'][0]['value']) ? json_decode($data[$defaultLangCode]['main_page_setting'][0]['value'],true) : null;
                ?>
                <input type="text" name="setting_data[{{ $defaultLangCode }}][main_page_setting][block2_count]" value="{{ old('setting_data.' . $defaultLangCode . '.main_page_setting.block2_count', $v['block2_count'] ?? '') }}" id="setting_data_main_page_setting_block2_count_{{ $defaultLangCode }}" class="form-control">
            </div>
        </div>
    </div>
    {{----------------------------------------------------------------------------------------------------------------}}

    {{------------------------------------------ BLOCK 3 -------------------------------------------------------------}}
    <div class="tab-pane fade" id="block3" role="tabpanel" aria-labelledby="block3-tab">
        <div class="form-group row">
            <label class="col-md-3 text-right">Рубрика/Тег</label>
            <div class="col-md-9">
                <?php
                $v = isset($data[$defaultLangCode]['main_page_setting'][0]['value']) ? json_decode($data[$defaultLangCode]['main_page_setting'][0]['value'],true) : null;
                $val = old('setting_data.' . $defaultLangCode . '.main_page_setting.block3_type', $v['block3_type'] ?? '');
                ?>
                <select name="setting_data[{{ $defaultLangCode }}][main_page_setting][block3_type]" class="form-control elem-type">
                    <option value="">---</option>
                    <option value="category" @if($val === 'category') selected @endif>Рубрика</option>
                    <option value="tag" @if($val === 'tag') selected @endif>Тег</option>
                </select>
            </div>
        </div>

        <div class="form-group row block-type block-category" @if(isset($v['block3_type']) && $v['block3_type'] == 'category') style="display: flex" @else style="display: none" @endif>
            <label class="col-md-3 text-right">Рубрика</label>
            <div class="col-md-9">
                <?php
                $v = isset($data[$defaultLangCode]['main_page_setting'][0]['value']) ? json_decode($data[$defaultLangCode]['main_page_setting'][0]['value'],true) : null;
                $val = old('setting_data.' . $defaultLangCode . '.main_page_setting.block3_category', $v['block3_category'] ?? '');
                ?>
                <select name="setting_data[{{ $defaultLangCode }}][main_page_setting][block3_category]" class="select2-field" style="width: 100%">
                    <option value="">---</option>
                    @include('admin.categories.menu_node',['sel' => $val])
                </select>
            </div>
        </div>

        <div class="form-group row block-type block-tag" @if(isset($v['block3_type']) && $v['block3_type'] == 'tag') style="display: flex" @else style="display: none" @endif>
            <label class="col-md-3 text-right">Тег</label>
            <div class="col-md-9">
                <?php
                $v = isset($data[$defaultLangCode]['main_page_setting'][0]['value']) ? json_decode($data[$defaultLangCode]['main_page_setting'][0]['value'],true) : null;
                $val = old('setting_data.' . $defaultLangCode . '.main_page_setting.block3_tag', $v['block3_tag'] ?? '');
                $selTag = \App\Models\NewTag::query()
                    ->leftJoin('new_tag_translations','new_tag_translations.new_tag_id','=','new_tags.id')
                    ->where('new_tag_translations.lang',$defaultLangCode)
                    ->select([
                        'new_tags.id',
                        'new_tag_translations.name'
                    ])
                    ->where('new_tags.id',$val)
                    ->first();
                ?>
                <select name="setting_data[{{ $defaultLangCode }}][main_page_setting][block3_tag]" class="select2-elem-tag" style="width: 100%">
                    @if($selTag)
                        <option value="{{$val}}">{{$selTag->name}}</option>
                    @endif
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-3 text-right" for="setting_data_main_page_setting_block3_count_{{ $defaultLangCode }}">Количество элементов</label>
            <div class="col-md-9">
                <?php
                $v = isset($data[$defaultLangCode]['main_page_setting'][0]['value']) ? json_decode($data[$defaultLangCode]['main_page_setting'][0]['value'],true) : null;
                ?>
                <input type="text" name="setting_data[{{ $defaultLangCode }}][main_page_setting][block3_count]" value="{{ old('setting_data.' . $defaultLangCode . '.main_page_setting.block3_count', $v['block3_count'] ?? '') }}" id="setting_data_main_page_setting_block3_count_{{ $defaultLangCode }}" class="form-control">
            </div>
        </div>
    </div>
    {{----------------------------------------------------------------------------------------------------------------}}

    {{------------------------------------------ BLOCK 4 -------------------------------------------------------------}}
    <div class="tab-pane fade" id="block4" role="tabpanel" aria-labelledby="block4-tab">
        <div class="form-group row">
            <label class="col-md-3 text-right">Рубрика/Тег</label>
            <div class="col-md-9">
                <?php
                $v = isset($data[$defaultLangCode]['main_page_setting'][0]['value']) ? json_decode($data[$defaultLangCode]['main_page_setting'][0]['value'],true) : null;
                $val = old('setting_data.' . $defaultLangCode . '.main_page_setting.block4_type', $v['block4_type'] ?? '');
                ?>
                <select name="setting_data[{{ $defaultLangCode }}][main_page_setting][block4_type]" class="form-control elem-type">
                    <option value="">---</option>
                    <option value="category" @if($val === 'category') selected @endif>Рубрика</option>
                    <option value="tag" @if($val === 'tag') selected @endif>Тег</option>
                </select>
            </div>
        </div>

        <div class="form-group row block-type block-category" @if(isset($v['block4_type']) && $v['block4_type'] == 'category') style="display: flex" @else style="display: none" @endif>
            <label class="col-md-3 text-right">Рубрика</label>
            <div class="col-md-9">
                <?php
                $v = isset($data[$defaultLangCode]['main_page_setting'][0]['value']) ? json_decode($data[$defaultLangCode]['main_page_setting'][0]['value'],true) : null;
                $val = old('setting_data.' . $defaultLangCode . '.main_page_setting.block4_category', $v['block4_category'] ?? '');
                ?>
                <select name="setting_data[{{ $defaultLangCode }}][main_page_setting][block4_category]" class="select2-field" style="width: 100%">
                    <option value="">---</option>
                    @include('admin.categories.menu_node',['sel' => $val])
                </select>
            </div>
        </div>

        <div class="form-group row block-type block-tag" @if(isset($v['block4_type']) && $v['block4_type'] == 'tag') style="display: flex" @else style="display: none" @endif>
            <label class="col-md-3 text-right">Тег</label>
            <div class="col-md-9">
                <?php
                $v = isset($data[$defaultLangCode]['main_page_setting'][0]['value']) ? json_decode($data[$defaultLangCode]['main_page_setting'][0]['value'],true) : null;
                $val = old('setting_data.' . $defaultLangCode . '.main_page_setting.block4_tag', $v['block4_tag'] ?? '');
                $selTag = \App\Models\NewTag::query()
                    ->leftJoin('new_tag_translations','new_tag_translations.new_tag_id','=','new_tags.id')
                    ->where('new_tag_translations.lang',$defaultLangCode)
                    ->select([
                        'new_tags.id',
                        'new_tag_translations.name'
                    ])
                    ->where('new_tags.id',$val)
                    ->first();
                ?>
                <select name="setting_data[{{ $defaultLangCode }}][main_page_setting][block4_tag]" class="select2-elem-tag" style="width: 100%">
                    @if($selTag)
                        <option value="{{$val}}">{{$selTag->name}}</option>
                    @endif
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-3 text-right" for="setting_data_main_page_setting_block4_count_{{ $defaultLangCode }}">Количество элементов</label>
            <div class="col-md-9">
                <?php
                $v = isset($data[$defaultLangCode]['main_page_setting'][0]['value']) ? json_decode($data[$defaultLangCode]['main_page_setting'][0]['value'],true) : null;
                ?>
                <input type="text" name="setting_data[{{ $defaultLangCode }}][main_page_setting][block4_count]" value="{{ old('setting_data.' . $defaultLangCode . '.main_page_setting.block4_count', $v['block4_count'] ?? '') }}" id="setting_data_main_page_setting_block4_count_{{ $defaultLangCode }}" class="form-control">
            </div>
        </div>
    </div>
    {{----------------------------------------------------------------------------------------------------------------}}

</div>

@push('scripts')
    <script>
        $(document).ready(function () {
            $(".elem-type").on('change',function () {
                let id = $(this).val();

                $(this).closest('.tab-pane').find('.block-type').hide();

                $(this).closest('.tab-pane').find('.block-type.block-' + id).show();
            });

            $('.select2-elem-tag').select2({
                minimumInputLength: 1,
                placeholder: 'Начните вводить название тега',
                "language": {
                    "noResults": function(){
                        return "Нет результатов!";
                    },
                    "searching": function(){
                        return "поиск";
                    },
                    "inputTooShort": function(){
                        return "Пожалуйста, введите 1 или более символов";
                    },
                },
                ajax: {
                    url: "{{route('tags.search')}}",
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 50,
                    data: function (term) {
                        return {
                            term: term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    }
                }
            }).on('change', function (e) {
                $(this).closest('form').find('input[name="name"]').val($(this).find(':selected').text());
            });
        });
    </script>
@endpush
