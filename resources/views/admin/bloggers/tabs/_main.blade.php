<div class="form-group row">
    <label class="col-md-3 text-right" for="page_name_{{ $lang }}">ФИО автора</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][name]" value="{{ old('page_data.' . $lang . '.name', $data[$lang]['name'] ?? '') }}" id="page_name_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.name') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.name'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_text_for_main_{{ $lang }}">Титулатура на главную</label>
    <div class="col-md-9">
        <input type="text" name="page_data[{{ $lang }}][text_for_main]" value="{{ old('page_data.' . $lang . '.text_for_main', $data[$lang]['text_for_main'] ?? '') }}" id="page_text_for_main_{{ $lang }}" class="form-control{{ $errors->has('page_data.' . $lang . '.text_for_main') ? ' is-invalid' : '' }}">

        @if ($errors->has('page_data.' . $lang . '.text_for_main'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.text_for_main') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_text_{{ $lang }}">Титулатура</label>
    <div class="col-md-9">
        <textarea
            name="page_data[{{ $lang }}][text]"
            id="page_text_{{ $lang }}"
            class="summernote editor {{ $errors->has('page_data.' . $lang . '.text') ? ' is-invalid' : '' }}"
            cols="30"
            rows="10"
        >{{ old('page_data.' . $lang . '.text', $data[$lang]['text'] ?? '') }}</textarea>

        @if ($errors->has('page_data.' . $lang . '.text'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('page_data.' . $lang . '.text') }}</strong>
            </span>
        @endif
    </div>
</div>
