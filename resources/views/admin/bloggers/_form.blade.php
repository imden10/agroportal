<div class="form-group row">
    <label class="col-md-3 text-right" for="name">Изображение</label>
    <div class="col-md-9">
        {{ media_preview_box('image', $model->image) }}
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="name">Миниатюра изображение</label>
    <div class="col-md-9">
        {{ media_preview_box('image_thumbnail', $model->image_thumbnail) }}
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_slug">Slug</label>
    <div class="col-md-9">
        <input type="text" name="slug" value="{{ old('slug', $model->slug ?? '') }}" id="page_slug" class="form-control{{ $errors->has('slug') ? ' is-invalid' : '' }}">

        @if ($errors->has('slug'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('slug') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_status">Статус</label>
    <div class="col-md-9">
        <div class="material-switch pull-left">
            <input id="someSwitchOptionSuccess" name="status" value="1" type="checkbox" {{ old('status', $model->status) ? ' checked' : '' }}/>
            <label for="someSwitchOptionSuccess" class="label-success"></label>
        </div>

        @if($errors->has('status'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
    </div>
</div>

<h4>Соц. сети</h4>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_email">E-mail</label>
    <div class="col-md-9">
        <input type="text" name="email" value="{{ old('email', $model->email ?? '') }}" id="page_email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}">

        @if ($errors->has('email'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_facebook">Facebook</label>
    <div class="col-md-9">
        <input type="text" name="facebook" value="{{ old('facebook', $model->facebook ?? '') }}" id="page_facebook" class="form-control{{ $errors->has('facebook') ? ' is-invalid' : '' }}">

        @if ($errors->has('facebook'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('facebook') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_instagram">Instagram</label>
    <div class="col-md-9">
        <input type="text" name="instagram" value="{{ old('instagram', $model->instagram ?? '') }}" id="page_instagram" class="form-control{{ $errors->has('instagram') ? ' is-invalid' : '' }}">

        @if ($errors->has('instagram'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('instagram') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_telegram">Telegram</label>
    <div class="col-md-9">
        <input type="text" name="telegram" value="{{ old('telegram', $model->telegram ?? '') }}" id="page_telegram" class="form-control{{ $errors->has('telegram') ? ' is-invalid' : '' }}">

        @if ($errors->has('telegram'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('telegram') }}</strong>
            </span>
        @endif
    </div>
</div>

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
    <style>
        /*-- ==============================================================
        Switches
        ============================================================== */
        .material-switch {
            line-height: 3em;
        }
        .material-switch > input[type="checkbox"] {
            display: none;
        }

        .material-switch > label {
            cursor: pointer;
            height: 0px;
            position: relative;
            width: 40px;
        }

        .material-switch > label::before {
            background: rgb(0, 0, 0);
            box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
            border-radius: 8px;
            content: '';
            height: 16px;
            margin-top: -8px;
            position:absolute;
            opacity: 0.3;
            transition: all 0.4s ease-in-out;
            width: 40px;
        }
        .material-switch > label::after {
            background: rgb(255, 255, 255);
            border-radius: 16px;
            box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
            content: '';
            height: 24px;
            left: -4px;
            margin-top: -8px;
            position: absolute;
            top: -4px;
            transition: all 0.3s ease-in-out;
            width: 24px;
        }
        .material-switch > input[type="checkbox"]:checked + label::before {
            background: inherit;
            opacity: 0.5;
        }
        .material-switch > input[type="checkbox"]:checked + label::after {
            background: inherit;
            left: 20px;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #5897fb !important;
            border: 1px solid #5897fb !important;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            color: #fff !important;
        }

        .select2-container--classic .select2-selection--multiple .select2-selection__choice, .select2-container--default .select2-selection--multiple .select2-selection__choice, .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            background-color:#5897fb !important;
        }

        .select2-container--default .select2-selection--multiple {
            border: 1px solid #e9ecef;
        }

        .select2-container--default.select2-container--focus .select2-selection--multiple {
            border: 1px solid #e9ecef;
            color: #3e5569;
            background-color: #fff;
            border-color: rgba(0,0,0,0.25);
            outline: 0;
            box-shadow: 0 0 0 0.2rem rgb(0 123 255 / 25%);
        }
    </style>
@endpush

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(() => {
            $('.select2').select2();

            $('.select2-tag').select2({
                tags:true
            });
        });
    </script>
@endpush
