<input type="hidden" name="_tab" value="{{$tab}}">


<?php $defaultLang = \App\Models\Langs::getDefaultLangCode();?>

<div class="form-group row">
    <label class="col-md-3 text-right" for="setting_default_og_image_{{ $defaultLang }}">Логотип сайта</label>
    <div class="col-md-9">
        {{ media_preview_box("setting_data[".$defaultLang."][default_og_image]",old('setting_data.' . $defaultLang . '.default_og_image', $data[$defaultLang]['default_og_image'][0]['value'] ?? '')) }}
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="setting_head_code_{{ $defaultLang }}">Код в HEAD</label>
    <div class="col-md-9">
        <textarea name="setting_data[{{ $defaultLang }}][head_code]" id="setting_head_code_{{ $defaultLang }}" cols="30" rows="10" class="form-control">{{old('setting_data.' . $defaultLang . '.head_code', $data[$defaultLang]['head_code'][0]['value'] ?? '')}}</textarea>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="setting_body_code_{{ $defaultLang }}">Код в конец BODY</label>
    <div class="col-md-9">
        <textarea name="setting_data[{{ $defaultLang }}][body_code]" id="setting_body_code_{{ $defaultLang }}" cols="30" rows="10" class="form-control">{{old('setting_data.' . $defaultLang . '.body_code', $data[$defaultLang]['body_code'][0]['value'] ?? '')}}</textarea>
    </div>
</div>

<h4>Соц. сети в header</h4>

<div class="form-group row">
    <label class="col-md-3 text-right" for="setting_header_facebook_{{ $defaultLang }}">Facebook</label>
    <div class="col-md-9">
        <input type="text" value="{{old('setting_data.' . $defaultLang . '.header_facebook', $data[$defaultLang]['header_facebook'][0]['value'] ?? '')}}" class="form-control" name="setting_data[{{ $defaultLang }}][header_facebook]" id="setting_header_facebook_{{ $defaultLang }}">
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="setting_header_telegram_{{ $defaultLang }}">Telegram</label>
    <div class="col-md-9">
        <input type="text" value="{{old('setting_data.' . $defaultLang . '.header_telegram', $data[$defaultLang]['header_telegram'][0]['value'] ?? '')}}" class="form-control" name="setting_data[{{ $defaultLang }}][header_telegram]" id="setting_header_telegram_{{ $defaultLang }}">
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="setting_header_instagram_{{ $defaultLang }}">Instagram</label>
    <div class="col-md-9">
        <input type="text" value="{{old('setting_data.' . $defaultLang . '.header_instagram', $data[$defaultLang]['header_instagram'][0]['value'] ?? '')}}" class="form-control" name="setting_data[{{ $defaultLang }}][header_instagram]" id="setting_header_instagram_{{ $defaultLang }}">
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="setting_header_youtube_{{ $defaultLang }}">Youtube</label>
    <div class="col-md-9">
        <input type="text" value="{{old('setting_data.' . $defaultLang . '.header_youtube', $data[$defaultLang]['header_youtube'][0]['value'] ?? '')}}" class="form-control" name="setting_data[{{ $defaultLang }}][header_youtube]" id="setting_header_youtube_{{ $defaultLang }}">
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="setting_header_tiktok_{{ $defaultLang }}">Tiktok</label>
    <div class="col-md-9">
        <input type="text" value="{{old('setting_data.' . $defaultLang . '.header_tiktok', $data[$defaultLang]['header_tiktok'][0]['value'] ?? '')}}" class="form-control" name="setting_data[{{ $defaultLang }}][header_tiktok]" id="setting_header_tiktok_{{ $defaultLang }}">
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="setting_header_linkedin_{{ $defaultLang }}">LinkedIn</label>
    <div class="col-md-9">
        <input type="text" value="{{old('setting_data.' . $defaultLang . '.header_linkedin', $data[$defaultLang]['header_linkedin'][0]['value'] ?? '')}}" class="form-control" name="setting_data[{{ $defaultLang }}][header_linkedin]" id="setting_header_linkedin_{{ $defaultLang }}">
    </div>
</div>



<ul class="nav nav-tabs" role="tablist">
    @foreach($localizations as $key => $lang)
        <li class="nav-item">
            <a class="nav-link @if(app()->getLocale() == $key) active @endif"
               data-toggle="tab" href="#main_lang_{{ $key }}" role="tab">
                <span class="hidden-sm-up"></span> <span
                    class="hidden-xs-down">{{ $lang }}</span>
            </a>
        </li>
    @endforeach
</ul>

<br>

<div class="tab-content">
    @foreach($localizations as $key => $catLang)
        <div class="tab-pane p-t-20 p-b-20  @if(app()->getLocale() == $key) active @endif"
             id="main_lang_{{ $key }}" role="tabpanel"
        >
            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_header_rss_{{ $key }}">Rss</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][header_rss]" value="{{ old('setting_data.' . $key . '.header_rss', $data[$key]['header_rss'][0]['value'] ?? '') }}" id="setting_header_rss_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.header_rss') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_header_subscribe_link_{{ $key }}">Подписаться ссылка</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][subscribe_link]" value="{{ old('setting_data.' . $key . '.subscribe_link', $data[$key]['subscribe_link'][0]['value'] ?? '') }}" id="setting_subscribe_link_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.subscribe_link') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_header_subscribe_text_{{ $key }}">Подписаться текст</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][subscribe_text]" value="{{ old('setting_data.' . $key . '.subscribe_text', $data[$key]['subscribe_text'][0]['value'] ?? '') }}" id="setting_subscribe_text_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.subscribe_text') ? ' is-invalid' : '' }}">
                </div>
            </div>
        </div>
    @endforeach
</div>





