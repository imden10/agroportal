<ul class="nav nav-tabs" role="tablist">
    @foreach($localizations as $key => $lang)
        <li class="nav-item">
            <a class="nav-link @if(app()->getLocale() == $key) active @endif"
               data-toggle="tab" href="#main_lang_{{ $key }}" role="tab">
                <span class="hidden-sm-up"></span> <span
                    class="hidden-xs-down">{{ $lang }}</span>
            </a>
        </li>
    @endforeach
</ul>

<br>

<div class="tab-content">
    @foreach($localizations as $key => $catLang)
        <div class="tab-pane p-t-20 p-b-20  @if(app()->getLocale() == $key) active @endif"
             id="main_lang_{{ $key }}" role="tabpanel"
        >

            <h4>Meta данные</h4>

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_faq_title_{{ $key }}">Заголовок</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][faq_title]" value="{{ old('setting_data.' . $key . '.faq_title', $data[$key]['faq_title'][0]['value'] ?? '') }}" id="setting_faq_title_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.faq_title') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_faq_description_{{ $key }}">Описание</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][faq_description]" value="{{ old('setting_data.' . $key . '.faq_description', $data[$key]['faq_description'][0]['value'] ?? '') }}" id="setting_faq_description_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.faq_description') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <hr>
        </div>
    @endforeach
</div>
