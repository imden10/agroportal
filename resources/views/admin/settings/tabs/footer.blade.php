<input type="hidden" name="_tab" value="{{$tab}}">

<ul class="nav nav-tabs" role="tablist">
    @foreach($localizations as $key => $lang)
        <li class="nav-item">
            <a class="nav-link @if(app()->getLocale() == $key) active @endif"
               data-toggle="tab" href="#main_lang_{{ $key }}" role="tab">
                <span class="hidden-sm-up"></span> <span
                    class="hidden-xs-down">{{ $lang }}</span>
            </a>
        </li>
    @endforeach
</ul>

<br>

<div class="tab-content">
    @foreach($localizations as $key => $catLang)
        <div class="tab-pane p-t-20 p-b-20  @if(app()->getLocale() == $key) active @endif"
             id="main_lang_{{ $key }}" role="tabpanel"
        >

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_footer_copyright_{{ $key }}">Копирайт</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][footer_copyright]" value="{{ old('setting_data.' . $key . '.footer_copyright', $data[$key]['footer_copyright'][0]['value'] ?? '') }}" id="setting_footer_copyright_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.footer_copyright') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <h4>Форма 1</h4>

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_footer_form1_title_{{ $key }}">Заголовок</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][footer_form1_title]" value="{{ old('setting_data.' . $key . '.footer_form1_title', $data[$key]['footer_form1_title'][0]['value'] ?? '') }}" id="setting_footer_form1_title_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.footer_form1_title') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_footer_form1_placeholder_{{ $key }}">Подсказка на поле ввода</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][footer_form1_placeholder]" value="{{ old('setting_data.' . $key . '.footer_form1_placeholder', $data[$key]['footer_form1_placeholder'][0]['value'] ?? '') }}" id="setting_footer_form1_placeholder_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.footer_form1_placeholder') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <h4>Форма 2</h4>

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_footer_form2_title_{{ $key }}">Заголовок</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][footer_form2_title]" value="{{ old('setting_data.' . $key . '.footer_form2_title', $data[$key]['footer_form2_title'][0]['value'] ?? '') }}" id="setting_footer_form2_title_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.footer_form2_title') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_footer_form2_placeholder_{{ $key }}">Подсказка на поле ввода</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][footer_form2_placeholder]" value="{{ old('setting_data.' . $key . '.footer_form2_placeholder', $data[$key]['footer_form2_placeholder'][0]['value'] ?? '') }}" id="setting_footer_form2_placeholder_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.footer_form2_placeholder') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <h4>Соц. сети</h4>

            <div class="form-group row">
                <label class="col-md-3 text-right"></label>
                <div class="col-md-9">
                    <div class="input-group mb-1">
                        <div style="display: none;">
                            <div data-item-id="#dynamicListPlaceholder" class="item-link-template-none item-group input-group mb-1">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="text" placeholder="Название" name="setting_data[{{ $key }}][links][#dynamicListPlaceholder][name]" class="form-control" disabled="">
                                        <input type="text" placeholder="Ссылка" name="setting_data[{{ $key }}][links][#dynamicListPlaceholder][link]" class="form-control" disabled="">
                                        <select name="setting_data[{{ $key }}][links][#dynamicListPlaceholder][icon]" style="width: 100%" class="form-control">
                                            <option value="">---</option>
                                            <option value="ic-fb">Facebook</option>
                                            <option value="ic-tl">Telegram</option>
                                            <option value="ic-inst">Instagram</option>
                                            <option value="ic-youtube">Youtube</option>
                                            <option value="ic-tik-tok">Tik-Tok</option>
                                            <option value="ic-linkedin2">LinkedIn</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="input-group-append">
                                            <button type="button" class="btn btn-danger remove-item text-white">Удалить</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="setting_data[{{ $key }}][links]" value="">

                        <?php $dataLinks = isset($data[$key]['links'][0]['value']) ? json_decode($data[$key]['links'][0]['value'],true) : [];?>
                        <div class="items-link-container w-100">
                            @foreach($dataLinks as $k => $value)
                                <div data-item-id="{{$k}}" class="item-template item-group input-group mb-1">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <input type="text" placeholder="Название" name="setting_data[{{ $key }}][links][{{$k}}][name]" value="{{$value['name']}}" class="form-control">
                                            <input type="text" placeholder="Ссылка" name="setting_data[{{ $key }}][links][{{$k}}][link]" value="{{$value['link']}}" class="form-control">
                                            <select name="setting_data[{{ $key }}][links][{{$k}}][icon]" style="width: 100%" class="form-control">
                                                <option value="">---</option>
                                                <option value="ic-fb" @if($value['icon'] == 'ic-fb') selected @endif >Facebook</option>
                                                <option value="ic-tl" @if($value['icon'] == 'ic-tl') selected @endif >Telegram</option>
                                                <option value="ic-inst" @if($value['icon'] == 'ic-inst') selected @endif >Instagram</option>
                                                <option value="ic-youtube" @if($value['icon'] == 'ic-youtube') selected @endif >Youtube</option>
                                                <option value="ic-tik-tok" @if($value['icon'] == 'ic-tik-tok') selected @endif >Tik-Tok</option>
                                                <option value="ic-linkedin2" @if($value['icon'] == 'ic-linkedin2') selected @endif >LinkedIn</option>
                                            </select>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="input-group-append">
                                                <button type="button" class="btn btn-danger remove-item text-white">Удалить</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                    </div>

                    <button type="button" class="btn btn-info btn-sm add-item-link">Добавить</button>
                </div>
            </div>


            <h4>Публикации</h4>

            <?php  $categories = app(\App\Models\Category::class)->treeStructure(); ?>

            {{------------------------------------------ BLOCK 1 -------------------------------------------------------------}}
            <div id="block1" class="block">
                <div class="form-group row">
                    <label class="col-md-3 text-right">Рубрика/Тег</label>
                    <div class="col-md-9">
                        <?php
                        $v = isset($data[$key]['footer_cat'][0]['value']) ? json_decode($data[$key]['footer_cat'][0]['value'],true) : null;
                        $val = old('setting_data.' . $key . '.footer_cat.block1_type', $v['block1_type'] ?? '');
                        ?>
                        <select name="setting_data[{{ $key }}][footer_cat][block1_type]" class="form-control elem-type">
                            <option value="">---</option>
                            <option value="category" @if($val === 'category') selected @endif>Рубрика</option>
                            <option value="tag" @if($val === 'tag') selected @endif>Тег</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row block-type block-category" @if(isset($v['block1_type']) && $v['block1_type'] == 'category') style="display: flex" @else style="display: none" @endif >
                    <label class="col-md-3 text-right">Рубрика</label>
                    <div class="col-md-9">
                        <?php
                        $v = isset($data[$key]['footer_cat'][0]['value']) ? json_decode($data[$key]['footer_cat'][0]['value'],true) : null;
                        $val = old('setting_data.' . $key . '.footer_cat.block1_category', $v['block1_category'] ?? '');
                        ?>
                        <select name="setting_data[{{ $key }}][footer_cat][block1_category]" class="select2-field" style="width: 100%">
                            <option value="">---</option>
                            @include('admin.categories.menu_node',['sel' => $val])
                        </select>
                    </div>
                </div>

                <div class="form-group row block-type block-tag" @if(isset($v['block1_type']) && $v['block1_type'] == 'tag') style="display: flex" @else style="display: none" @endif >
                    <label class="col-md-3 text-right">Тег</label>
                    <div class="col-md-9">
                        <?php
                        $v = isset($data[$key]['footer_cat'][0]['value']) ? json_decode($data[$key]['footer_cat'][0]['value'],true) : null;
                        $val = old('setting_data.' . $key . '.footer_cat.block1_tag', $v['block1_tag'] ?? '');
                        $selTag = \App\Models\NewTag::query()
                            ->leftJoin('new_tag_translations','new_tag_translations.new_tag_id','=','new_tags.id')
                            ->where('new_tag_translations.lang',$key)
                            ->select([
                                'new_tags.id',
                                'new_tag_translations.name'
                            ])
                            ->where('new_tags.id',$val)
                            ->first();
                        ?>
                        <select name="setting_data[{{ $key }}][footer_cat][block1_tag]" class="select2-elem-tag" style="width: 100%">
                            @if($selTag)
                                <option value="{{$val}}">{{$selTag->name}}</option>
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            {{----------------------------------------------------------------------------------------------------------------}}
            <hr>
            {{------------------------------------------ BLOCK 2 -------------------------------------------------------------}}
            <div id="block2" class="block">
                <div class="form-group row">
                    <label class="col-md-3 text-right">Рубрика/Тег</label>
                    <div class="col-md-9">
                        <?php
                        $v = isset($data[$key]['footer_cat2'][0]['value']) ? json_decode($data[$key]['footer_cat2'][0]['value'],true) : null;
                        $val = old('setting_data.' . $key . '.footer_cat2.block1_type', $v['block1_type'] ?? '');
                        ?>
                        <select name="setting_data[{{ $key }}][footer_cat2][block1_type]" class="form-control elem-type">
                            <option value="">---</option>
                            <option value="category" @if($val === 'category') selected @endif>Рубрика</option>
                            <option value="tag" @if($val === 'tag') selected @endif>Тег</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row block-type block-category" @if(isset($v['block1_type']) && $v['block1_type'] == 'category') style="display: flex" @else style="display: none" @endif >
                    <label class="col-md-3 text-right">Рубрика</label>
                    <div class="col-md-9">
                        <?php
                        $v = isset($data[$key]['footer_cat2'][0]['value']) ? json_decode($data[$key]['footer_cat2'][0]['value'],true) : null;
                        $val = old('setting_data.' . $key . '.footer_cat2.block1_category', $v['block1_category'] ?? '');
                        ?>
                        <select name="setting_data[{{ $key }}][footer_cat2][block1_category]" class="select2-field" style="width: 100%">
                            <option value="">---</option>
                            @include('admin.categories.menu_node',['sel' => $val])
                        </select>
                    </div>
                </div>

                <div class="form-group row block-type block-tag" @if(isset($v['block1_type']) && $v['block1_type'] == 'tag') style="display: flex" @else style="display: none" @endif >
                    <label class="col-md-3 text-right">Тег</label>
                    <div class="col-md-9">
                        <?php
                        $v = isset($data[$key]['footer_cat2'][0]['value']) ? json_decode($data[$key]['footer_cat2'][0]['value'],true) : null;
                        $val = old('setting_data.' . $key . '.footer_cat2.block1_tag', $v['block1_tag'] ?? '');
                        $selTag = \App\Models\NewTag::query()
                            ->leftJoin('new_tag_translations','new_tag_translations.new_tag_id','=','new_tags.id')
                            ->where('new_tag_translations.lang',$key)
                            ->select([
                                'new_tags.id',
                                'new_tag_translations.name'
                            ])
                            ->where('new_tags.id',$val)
                            ->first();
                        ?>
                        <select name="setting_data[{{ $key }}][footer_cat2][block1_tag]" class="select2-elem-tag" style="width: 100%">
                            @if($selTag)
                                <option value="{{$val}}">{{$selTag->name}}</option>
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            {{----------------------------------------------------------------------------------------------------------------}}
            <hr>
            {{------------------------------------------ BLOCK 3 -------------------------------------------------------------}}
            <div id="block3" class="block">
                <div class="form-group row">
                    <label class="col-md-3 text-right">Рубрика/Тег</label>
                    <div class="col-md-9">
                        <?php
                        $v = isset($data[$key]['footer_cat3'][0]['value']) ? json_decode($data[$key]['footer_cat3'][0]['value'],true) : null;
                        $val = old('setting_data.' . $key . '.footer_cat3.block1_type', $v['block1_type'] ?? '');
                        ?>
                        <select name="setting_data[{{ $key }}][footer_cat3][block1_type]" class="form-control elem-type">
                            <option value="">---</option>
                            <option value="category" @if($val === 'category') selected @endif>Рубрика</option>
                            <option value="tag" @if($val === 'tag') selected @endif>Тег</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row block-type block-category" @if(isset($v['block1_type']) && $v['block1_type'] == 'category') style="display: flex" @else style="display: none" @endif >
                    <label class="col-md-3 text-right">Рубрика</label>
                    <div class="col-md-9">
                        <?php
                        $v = isset($data[$key]['footer_cat3'][0]['value']) ? json_decode($data[$key]['footer_cat3'][0]['value'],true) : null;
                        $val = old('setting_data.' . $key . '.footer_cat3.block1_category', $v['block1_category'] ?? '');
                        ?>
                        <select name="setting_data[{{ $key }}][footer_cat3][block1_category]" class="select2-field" style="width: 100%">
                            <option value="">---</option>
                            @include('admin.categories.menu_node',['sel' => $val])
                        </select>
                    </div>
                </div>

                <div class="form-group row block-type block-tag" @if(isset($v['block1_type']) && $v['block1_type'] == 'tag') style="display: flex" @else style="display: none" @endif >
                    <label class="col-md-3 text-right">Тег</label>
                    <div class="col-md-9">
                        <?php
                        $v = isset($data[$key]['footer_cat3'][0]['value']) ? json_decode($data[$key]['footer_cat3'][0]['value'],true) : null;
                        $val = old('setting_data.' . $key . '.footer_cat3.block1_tag', $v['block1_tag'] ?? '');
                        $selTag = \App\Models\NewTag::query()
                            ->leftJoin('new_tag_translations','new_tag_translations.new_tag_id','=','new_tags.id')
                            ->where('new_tag_translations.lang',$key)
                            ->select([
                                'new_tags.id',
                                'new_tag_translations.name'
                            ])
                            ->where('new_tags.id',$val)
                            ->first();
                        ?>
                        <select name="setting_data[{{ $key }}][footer_cat3][block1_tag]" class="select2-elem-tag" style="width: 100%">
                            @if($selTag)
                                <option value="{{$val}}">{{$selTag->name}}</option>
                            @endif
                        </select>
                    </div>
                </div>
            </div>
            {{----------------------------------------------------------------------------------------------------------------}}



        </div>
    @endforeach
</div>


<?php
    $defaultLang = \App\Models\Langs::getDefaultLangCode();
?>

<h4>Меню</h4>

<div class="form-group row">
    <label class="col-md-3 text-right" for="setting_footer_menu_{{ $defaultLang }}">Меню в подвале</label>
    <div class="col-md-9">
        <select name="setting_data[{{ $defaultLang }}][footer_menu]" class="select2-field" id="setting_footer_menu_{{ $defaultLang }}">
            <option value="">---</option>
            @foreach(\App\Models\Menu::getTagsWithId() as $menuId => $item)
                <option value="{{$menuId}}" @if(old('setting_data.' . $defaultLang . '.footer_menu', $data[$defaultLang]['footer_menu'][0]['value'] ?? '') == $menuId) selected @endif>{{$item}}</option>
            @endforeach
        </select>
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="setting_footer_menu2_{{ $defaultLang }}">Меню в подвале 2</label>
    <div class="col-md-9">
        <select name="setting_data[{{ $defaultLang }}][footer_menu2]" class="select2-field" id="setting_footer_menu2_{{ $defaultLang }}">
            <option value="">---</option>
            @foreach(\App\Models\Menu::getTagsWithId() as $menuId => $item)
                <option value="{{$menuId}}" @if(old('setting_data.' . $defaultLang . '.footer_menu2', $data[$defaultLang]['footer_menu2'][0]['value'] ?? '') == $menuId) selected @endif>{{$item}}</option>
            @endforeach
        </select>
    </div>
</div>


<hr>
@push('scripts')
    <script>
        $(document).ready(function () {
            $(document).on('click','.add-item-link',function () {
                const template = $(this).parent().find('.item-link-template-none');
                const container = $(this).parent().find('.items-link-container');

                create_item(template, container, '#dynamicListPlaceholder');

                container.find('input, textarea').prop('disabled', false);
            });

            $(document).on('click', '.remove-item', function () {
                $(this).parents('.item-group').remove();
            });

            $(".elem-type").on('change',function () {
                let id = $(this).val();

                $(this).closest('.block').find('.block-type').hide();

                $(this).closest('.block').find('.block-type.block-' + id).show();
            });

            $('.select2-elem-tag').select2({
                minimumInputLength: 1,
                placeholder: 'Начните вводить название тега',
                "language": {
                    "noResults": function(){
                        return "Нет результатов!";
                    },
                    "searching": function(){
                        return "поиск";
                    },
                    "inputTooShort": function(){
                        return "Пожалуйста, введите 1 или более символов";
                    },
                },
                ajax: {
                    url: "{{route('tags.search')}}",
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 50,
                    data: function (term) {
                        return {
                            term: term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    }
                }
            }).on('change', function (e) {
                $(this).closest('form').find('input[name="name"]').val($(this).find(':selected').text());
            });
        });
    </script>
@endpush


