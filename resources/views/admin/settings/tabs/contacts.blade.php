<input type="hidden" name="_tab" value="{{$tab}}">

<ul class="nav nav-tabs" role="tablist">
    @foreach($localizations as $key => $lang)
        <li class="nav-item">
            <a class="nav-link @if(app()->getLocale() == $key) active @endif"
               data-toggle="tab" href="#main_lang_{{ $key }}" role="tab">
                <span class="hidden-sm-up"></span> <span
                    class="hidden-xs-down">{{ $lang }}</span>
            </a>
        </li>
    @endforeach
</ul>

<br>

<div class="tab-content">
    @foreach($localizations as $key => $catLang)
        <div class="tab-pane p-t-20 p-b-20  @if(app()->getLocale() == $key) active @endif"
             id="main_lang_{{ $key }}" role="tabpanel"
        >

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_address_{{ $key }}">Адрес</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][address]" value="{{ old('setting_data.' . $key . '.address', $data[$key]['address'][0]['value'] ?? '') }}" id="setting_address_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.address') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_email_{{ $key }}">Почта</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][email]" value="{{ old('setting_data.' . $key . '.email', $data[$key]['email'][0]['value'] ?? '') }}" id="setting_email_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.email') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 text-right">Телефони</label>
                <div class="col-md-9">
                    <div class="input-group mb-1">
                        <div style="display: none;">
                            <div data-item-id="#dynamicListPlaceholder" class="item-template-none item-group input-group mb-1">
                                <input type="text" placeholder="Заголовок" name="setting_data[{{ $key }}][phones][#dynamicListPlaceholder][label]" class="form-control mr-1" disabled="">
                                <input type="text" placeholder="Номер" name="setting_data[{{ $key }}][phones][#dynamicListPlaceholder][number]" class="form-control" disabled="">
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-danger remove-item text-white">Удалить</button>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="phones" value="">

                        <?php $dataPhones = isset($data[$key]['phones'][0]['value']) ? json_decode($data[$key]['phones'][0]['value'],true) : [];?>
                        <div class="items-container w-100">
                            @foreach($dataPhones as $k => $value)
                                <div data-item-id="{{$k}}" class="item-template item-group input-group mb-1">
                                    <input type="text" placeholder="Заголовок" name="setting_data[{{ $key }}][phones][{{$k}}][label]" value="{{$value['label']}}" class="form-control mr-1">
                                    <input type="text" placeholder="Номер" name="setting_data[{{ $key }}][phones][{{$k}}][number]" value="{{$value['number']}}" class="form-control">
                                    <div class="input-group-append">
                                        <button type="button" class="btn btn-danger remove-item text-white">Удалить</button>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                    </div>

                    <button type="button" class="btn btn-info btn-sm add-item-phone">Добавить</button>
                </div>
            </div>


            <h4>График работы</h4>

            <div class="form-group row">
                <label class="col-md-3 text-right">Рабочий график</label>
                <div class="col-md-9">
                    <div class="input-group mb-1">
                        <div style="display: none;">
                            <div data-item-id="#dynamicListPlaceholder" class="item-schedule-template-none item-group input-group mb-1">
                                <input type="text" placeholder="Заголовок" name="setting_data[{{ $key }}][schedules][#dynamicListPlaceholder][label]" class="form-control mr-1" disabled="">
                                <input type="text" placeholder="Время" name="setting_data[{{ $key }}][schedules][#dynamicListPlaceholder][time]" class="form-control" disabled="">
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-danger remove-item text-white">Удалить</button>
                                </div>
                            </div>
                        </div>

                        <input type="hidden" name="schedules" value="">

                        <?php $dataSchedules = isset($data[$key]['schedules'][0]['value']) ? json_decode($data[$key]['schedules'][0]['value'],true) : [];?>
                        <div class="items-schedule-container w-100">
                            @foreach($dataSchedules as $k => $value)
                                <div data-item-id="{{$k}}" class="item-template item-group input-group mb-1">
                                    <input type="text" placeholder="Заголовок" name="setting_data[{{ $key }}][schedules][{{$k}}][label]" value="{{$value['label']}}" class="form-control mr-1">
                                    <input type="text" placeholder="Время" name="setting_data[{{ $key }}][schedules][{{$k}}][time]" value="{{$value['time']}}" class="form-control">
                                    <div class="input-group-append">
                                        <button type="button" class="btn btn-danger remove-item text-white">Удалить</button>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                    </div>

                    <button type="button" class="btn btn-info btn-sm add-item-schedule">Добавить</button>
                </div>
            </div>

        </div>
    @endforeach
</div>


@push('scripts')
    <script>
        $(document).ready(function () {
            $(document).on('click','.add-item-schedule',function () {
                const template = $(this).parent().find('.item-schedule-template-none');
                const container = $(this).parent().find('.items-schedule-container');

                create_item(template, container, '#dynamicListPlaceholder');

                container.find('input, textarea').prop('disabled', false);
            });

            $(document).on('click','.add-item-phone',function () {
                const template = $(this).parent().find('.item-template-none');
                const container = $(this).parent().find('.items-container');

                create_item(template, container, '#dynamicListPlaceholder');

                container.find('input, textarea').prop('disabled', false);
            });

            $(document).on('click','.add-item-link',function () {
                const template = $(this).parent().find('.item-link-template-none');
                const container = $(this).parent().find('.items-link-container');

                create_item(template, container, '#dynamicListPlaceholder');

                container.find('input, textarea').prop('disabled', false);
            });

            $(document).on('click', '.remove-item', function () {
                $(this).parents('.item-group').remove();
            });
        });
    </script>
@endpush
