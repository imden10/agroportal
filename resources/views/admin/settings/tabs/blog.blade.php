<input type="hidden" name="_tab" value="{{$tab}}">

<ul class="nav nav-tabs" role="tablist">
    @foreach($localizations as $key => $lang)
        <li class="nav-item">
            <a class="nav-link @if(app()->getLocale() == $key) active @endif"
               data-toggle="tab" href="#main_lang_{{ $key }}" role="tab">
                <span class="hidden-sm-up"></span> <span
                    class="hidden-xs-down">{{ $lang }}</span>
            </a>
        </li>
    @endforeach
</ul>

<br>

<div class="tab-content">
    @foreach($localizations as $key => $catLang)
        <div class="tab-pane p-t-20 p-b-20  @if(app()->getLocale() == $key) active @endif"
             id="main_lang_{{ $key }}" role="tabpanel"
        >

            <h4>Meta данные</h4>

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_news_title_{{ $key }}">Заголовок</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][news_title]" value="{{ old('setting_data.' . $key . '.news_title', $data[$key]['news_title'][0]['value'] ?? '') }}" id="setting_news_title_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.news_title') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <div class="form-group row">
                <label class="col-md-3 text-right" for="setting_news_description_{{ $key }}">Описание</label>
                <div class="col-md-9">
                    <input type="text" name="setting_data[{{ $key }}][news_description]" value="{{ old('setting_data.' . $key . '.news_description', $data[$key]['news_description'][0]['value'] ?? '') }}" id="setting_news_description_{{ $key }}" class="form-control{{ $errors->has('setting_data.' . $key . '.news_description') ? ' is-invalid' : '' }}">
                </div>
            </div>

            <hr>
        </div>
    @endforeach
</div>



<?php
$defaultLang = \App\Models\Langs::getDefaultLangCode();
$categories = app(\App\Models\Category::class)->treeStructure();
?>

<div class="form-group row">
    <label class="col-md-3 text-right" for="setting_news_per_page_{{ $defaultLang }}">Количество новостей на странице</label>
    <div class="col-md-9">
        <input type="text" value="{{old('setting_data.' . $defaultLang . '.news_per_page', $data[$defaultLang]['news_per_page'][0]['value'] ?? '')}}" class="form-control" name="setting_data[{{ $defaultLang }}][news_per_page]" id="setting_news_per_page_{{ $defaultLang }}">
    </div>
</div>

<h4>Блок мультимедиа</h4>

{{------------------------------------------ BLOCK 1 -------------------------------------------------------------}}
<div id="block1" class="block">
    <div class="form-group row">
        <label class="col-md-3 text-right">Рубрика/Тег</label>
        <div class="col-md-9">
            <?php
            $v = isset($data[$defaultLang]['article_multimedia'][0]['value']) ? json_decode($data[$defaultLang]['article_multimedia'][0]['value'],true) : null;
            $val = old('setting_data.' . $defaultLang . '.article_multimedia.type', $v['type'] ?? '');
            ?>
            <select name="setting_data[{{ $defaultLang }}][article_multimedia][type]" class="form-control elem-type">
                <option value="">---</option>
                <option value="category" @if($val === 'category') selected @endif>Рубрика</option>
                <option value="tag" @if($val === 'tag') selected @endif>Тег</option>
            </select>
        </div>
    </div>

    <div class="form-group row block-type block-category" @if(isset($v['type']) && $v['type'] == 'category') style="display: flex" @else style="display: none" @endif >
        <label class="col-md-3 text-right">Рубрика</label>
        <div class="col-md-9">
            <?php
            $v = isset($data[$defaultLang]['article_multimedia'][0]['value']) ? json_decode($data[$defaultLang]['article_multimedia'][0]['value'],true) : null;
            $val = old('setting_data.' . $defaultLang . '.article_multimedia.category', $v['category'] ?? '');
            ?>
            <select name="setting_data[{{ $defaultLang }}][article_multimedia][category]" class="select2-field" style="width: 100%">
                <option value="">---</option>
                @include('admin.categories.menu_node',['sel' => $val])
            </select>
        </div>
    </div>

    <div class="form-group row block-type block-tag" @if(isset($v['type']) && $v['type'] == 'tag') style="display: flex" @else style="display: none" @endif >
        <label class="col-md-3 text-right">Тег</label>
        <div class="col-md-9">
            <?php
            $v = isset($data[$defaultLang]['article_multimedia'][0]['value']) ? json_decode($data[$defaultLang]['article_multimedia'][0]['value'],true) : null;
            $val = old('setting_data.' . $defaultLang . '.article_multimedia.tag', $v['tag'] ?? '');
            $selTag = \App\Models\NewTag::query()
                ->leftJoin('new_tag_translations','new_tag_translations.new_tag_id','=','new_tags.id')
                ->where('new_tag_translations.lang',$defaultLang)
                ->select([
                    'new_tags.id',
                    'new_tag_translations.name'
                ])
                ->where('new_tags.id',$val)
                ->first();
            ?>
            <select name="setting_data[{{ $defaultLang }}][article_multimedia][tag]" class="select2-elem-tag" style="width: 100%">
                @if($selTag)
                    <option value="{{$val}}">{{$selTag->name}}</option>
                @endif
            </select>
        </div>
    </div>
</div>
{{----------------------------------------------------------------------------------------------------------------}}


@push('scripts')
    <script>
        $(document).ready(function () {
            $(".elem-type").on('change',function () {
                let id = $(this).val();

                $(this).closest('.block').find('.block-type').hide();

                $(this).closest('.block').find('.block-type.block-' + id).show();
            });

            $('.select2-elem-tag').select2({
                minimumInputLength: 1,
                placeholder: 'Начните вводить название тега',
                "language": {
                    "noResults": function(){
                        return "Нет результатов!";
                    },
                    "searching": function(){
                        return "поиск";
                    },
                    "inputTooShort": function(){
                        return "Пожалуйста, введите 1 или более символов";
                    },
                },
                ajax: {
                    url: "{{route('tags.search')}}",
                    dataType: 'json',
                    type: "GET",
                    quietMillis: 50,
                    data: function (term) {
                        return {
                            term: term
                        };
                    },
                    processResults: function (data) {
                        return {
                            results: $.map(data.items, function (item) {
                                return {
                                    text: item.name,
                                    id: item.id
                                }
                            })
                        };
                    }
                }
            }).on('change', function (e) {
                $(this).closest('form').find('input[name="name"]').val($(this).find(':selected').text());
            });
        });
    </script>
@endpush
