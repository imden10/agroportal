{{--<div class="form-group row">--}}
{{--    <label class="col-md-3 text-right" for="template">Шаблон</label>--}}
{{--    <div class="col-md-9">--}}
{{--        <select name="template" id="template" class="form-control">--}}
{{--            @foreach(config('themes.items') as $item)--}}
{{--                <option value="{{$item['path']}}" @if(old('template', $model->template ?? '') === $item['path']) selected @endif>{{$item['name']}}</option>--}}
{{--            @endforeach--}}
{{--        </select>--}}
{{--    </div>--}}
{{--</div>--}}

<div class="form-group row">
    <label class="col-md-3 text-right" for="name">Изображение</label>
    <div class="col-md-9">
        {{ media_preview_box('image',$model->image) }}
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="color_theme">Цвет темы</label>
    <div class="col-md-9">
        <input type="text" name="color_theme" value="{{ old('color_theme', $model->color_theme ?? '') }}" id="color_theme" class="form-control{{ $errors->has('color_theme') ? ' is-invalid' : '' }}">

        @if ($errors->has('color_theme'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('color_theme') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_slug">Slug</label>
    <div class="col-md-9">
        <input type="text" name="slug" value="{{ old('slug', $model->slug ?? '') }}" id="page_slug" class="form-control{{ $errors->has('slug') ? ' is-invalid' : '' }}">

        @if ($errors->has('slug'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('slug') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="page_status">Статус</label>
    <div class="col-md-9">
        <div class="material-switch pull-left">
            <input id="someSwitchOptionSuccess" name="status" value="1" type="checkbox" {{ old('status', $model->status) ? ' checked' : '' }}/>
            <label for="someSwitchOptionSuccess" class="label-success"></label>
        </div>

        @if ($errors->has('status'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('status') }}</strong>
            </span>
        @endif
    </div>
</div>

