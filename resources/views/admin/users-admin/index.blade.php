@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item active" aria-current="page">Пользователи</li>
        </ol>
    </nav>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                @permission('admins__users__create')
                <a href="{{route('users-admin.create')}}" class="btn btn-primary float-right">
                    <i class="fa fa-plus"></i>
                    Добавить
                </a>
                @endpermission
            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Имя</th>
                            <th>Email</th>
                            <th>Телефон</th>
                            <th>Группа пользователей</th>
                            <th style="min-width: 150px"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr data-id="{{$user->id}}">
                                <td>
                                    @permission('admins__users__edit')
                                        <a href="{{ route('users-admin.edit', $user->id) }}" title="Редактировать">
                                            {{$user->name}}
                                        </a>
                                    @else
                                        {{$user->name}}
                                    @endpermission
                                </td>
                                <td>{{$user->email}}</td>
                                <td>{{$user->phone}}</td>
                                <td>
                                    @foreach($user->groups as $group)
                                        <span class="badge" style="color: white; background-color: #009688">{{$group->name}}</span>
                                    @endforeach
                                </td>
                                <td>
                                    <div style="display: flex">
                                        <div style="margin-left: 10px">
                                            <form action="{{ route('users-admin.destroy', $user->id) }}" method="POST">

                                                @permission('admins__users__edit')
                                                <a href="{{ route('users-admin.edit', $user->id) }}" class="btn btn-primary text-white btn-xs" title="Редактировать">
                                                    <i class="fas fa-edit fa-lg"></i>
                                                </a>
                                                @endpermission&nbsp;

                                                @csrf
                                                @method('DELETE')

                                                @permission('admins__users__delete')
                                                @if($user->id != \Illuminate\Support\Facades\Auth::user()->id)
                                                <a href="javascript:void(0)" title="Удалить" class="btn btn-danger delete-item-btn text-white btn-xs">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                                @endif
                                                @endpermission
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $users->appends(request()->all())->links() }}
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(document).ready(() => {
            $('.delete-item-btn').on('click',function() {
                if(confirm('You definitely want to delete the user?')){
                    $(this).closest('form').submit();
                }
            });
        });
    </script>
@endpush
