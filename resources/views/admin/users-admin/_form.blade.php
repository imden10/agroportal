@if($action === 'create')
    <form class="form-horizontal" method="POST" action="{{ route('users-admin.store') }}">
@elseif($action === 'edit')
    <form action="{{route('users-admin.update', $model->id)}}" class="form-horizontal" method="post">
        @method('PUT')
@endif
    @csrf

        <div class="form-group row">
            <label class="col-md-3 text-right" for="name"><span style="color:red">*</span>Имя</label>
            <div class="col-md-9">
                <input type="text" name="name" id="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name', $model->name ?? '') }}">

                @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-3 text-right" for="group_ids"><span style="color:red">*</span>Группы пользователя</label>
            <div class="col-md-9">
                <?php
                    $sel = old('group_ids', \App\Models\AccessUserGroups::query()->where('user_id',$model->id)->pluck('group_id')->toArray() ?? []);
                ?>
                <select name="group_ids[]" id="group_ids" multiple class="select2-field {{ $errors->has('group_ids') ? ' is-invalid' : '' }}" style="width: 100%">
                    @foreach(\App\Models\AccessGroups::query()->get() as $item)
                        <option value="{{$item->id}}" @if(in_array($item->id,$sel)) selected @endif>{{$item->name}}</option>
                    @endforeach
                </select>

                @if ($errors->has('group_ids'))
                    <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('group_ids') }}</strong>
                </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-3 text-right" for="email"><span style="color:red">*</span> E-mail</label>
            <div class="col-md-9">
                <input type="text" name="email" id="email" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email', $model->email ?? '') }}">

                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-3 text-right" for="phone">Телефон</label>
            <div class="col-md-9">
                <input type="text" name="phone" id="phone" class="form-control" value="{{$model->phone}}">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-md-3 text-right" for="password">
                @if(! $model->id)
                <span style="color:red">*</span>
                @endif
                Пароль</label>
            <div class="col-md-9">
                <input type="password" name="password" id="password" class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}">

                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <input type="submit" class="btn btn-success text-white" value="Сохранить">
</form>

@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <style>
        .select2-container--default .select2-selection--multiple .select2-selection__choice {
            background-color: #009688 !important;
            border: 1px solid #009688 !important;
        }

        .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            color: #fff !important;
        }

        .select2-container--classic .select2-selection--multiple .select2-selection__choice, .select2-container--default .select2-selection--multiple .select2-selection__choice, .select2-container--default .select2-selection--multiple .select2-selection__choice__remove {
            background-color:#009688 !important;
        }

        .select2-container--default .select2-selection--multiple {
            border: 1px solid #e9ecef;
        }

        .select2-container--default.select2-container--focus .select2-selection--multiple {
            border: 1px solid #e9ecef;
            color: #3e5569;
            background-color: #fff;
            border-color: rgba(0,0,0,0.25);
            outline: 0;
            box-shadow: 0 0 0 0.2rem rgb(0 123 255 / 25%);
        }
    </style>
@endpush

@push('scripts')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>
        $(document).ready(() => {
            $('.select2-field').select2();
        });
    </script>
@endpush
