@foreach($categories as $item)

    @if(count($item['children']) > 0)
        <optgroup label="{{ $item['title'] }}">
    @else
            <option value="{{ $item['id'] }}" >{{ $item['title'] }}</option>
    @endif

    @if(!empty($item['children']))
        @php($categories = $item['children'])
        @include('admin.menu.node')
    @endif

    @if(count($item['children']) > 0)
        </optgroup>
    @endif

@endforeach
