<?php
    $allPermissions = \App\Models\AccessPermissions::DATA;
    $names = \App\Models\AccessPermissions::CODE_NAMES;
    $selectedPermissions = isset($model->tree_permission) ? json_decode($model->tree_permission,true) : [];
?>
<div style="display: flex;justify-content: space-between;">
    <h4>Выберите привилегии для группы</h4>
    <div>
        <span class="btn btn-success text-white selected-all-btn">Выделить все</span>
        <span class="btn btn-danger text-white unselected-all-btn">Снять выделения</span>
    </div>
</div>

<div class="tree-checkbox">
    <ul>
        @foreach($allPermissions as $item)
            <li data-section_code="{{$item['section_code']}}" class="open">
                <span class="fa fa-angle-right arrow-element"></span>
                <input type="checkbox" id="{{$item['section_code']}}" class="checkbox-section">
                <label for="{{$item['section_code']}}">{{$item['section_name']}}</label>

                <ul>
                    @foreach($item['blocks'] as $block)
                        <li data-block_code="{{$block['code']}}" class="open">
                            <span class="fa fa-angle-right arrow-element"></span>
                            <input type="checkbox" id="{{$item['section_code']}}__{{$block['code']}}" class="checkbox-block">
                            <label for="{{$item['section_code']}}__{{$block['code']}}">{{$block['name']}}</label>

                            <ul>
                                @foreach($block['permissions'] as $permission)
                                    <li data-permission="{{$permission}}">
                                        <input
                                            type="checkbox"
                                            value="1"
                                            class="checkbox-element"
                                            id="{{$item['section_code']}}__{{$block['code']}}__{{$permission}}"
                                            name="permissions[{{$item['section_code']}}][{{$block['code']}}][{{$permission}}][on]"
                                            @if(isset($selectedPermissions[$item['section_code']][$block['code']][$permission]) && $selectedPermissions[$item['section_code']][$block['code']][$permission]) checked @endif
                                        >
                                        <label for="{{$item['section_code']}}__{{$block['code']}}__{{$permission}}">{{$names[$permission]}}</label>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                    @endforeach
                </ul>
            </li>
        @endforeach
    </ul>
</div>

@push('scripts')
    <style>
        .tree-checkbox ul {
            list-style: none;
        }

        .tree-checkbox ul li input[type="checkbox"] {
            cursor: pointer;
        }

        .tree-checkbox label {
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            cursor: pointer;
        }

        .tree-checkbox ul li .arrow-element {
            color: #999999;
            display: inline-flex;
            width: 20px;
            height: 20px;
            align-items: center;
            justify-content: center;
            cursor: pointer;
            transition: all .3s;
        }

        .tree-checkbox ul li.open > .arrow-element {
            transform: rotate(90deg);
            transition: all .3s;
        }
    </style>
@endpush

@push('scripts')
    <script>
        function selected(flag){
            $(".tree-checkbox").find("input[type='checkbox']").prop('checked',flag);
        }

        function checkAll(){
            $(".tree-checkbox ul li").each(function(){
                let e1 = $(this).find('.checkbox-section');
                let group1 = [];
                e1.siblings('ul').children('li').each(function(){
                  let e2 = $(this).find('.checkbox-block');
                  group1.push(e2);
                  let group2 = [];
                    e2.siblings('ul').children('li').each(function(){
                      let e3 = $(this).find('.checkbox-element');
                        group2.push(e3);
                    });

                    if(group2.length){
                        let counterTrue2 = 0;
                        for(let i= 0; i<group2.length; i++){
                            if(group2[i].prop('checked')){
                                counterTrue2++;
                            }
                        }

                        if(group2.length === counterTrue2){
                            e2.prop('checked',true);
                        } else {
                            e2.prop('checked',false);
                        }
                    }
                });

                if(group1.length){
                    let counterTrue1 = 0;
                    for(let i= 0; i<group1.length; i++){
                        if(group1[i].prop('checked')){
                            counterTrue1++;
                        }
                    }

                    if(group1.length === counterTrue1){
                        e1.prop('checked',true);
                    } else {
                        e1.prop('checked',false);
                    }
                }
            });
        }

        $(document).ready(function () {
            checkAll();

            $(".checkbox-block").on('change',function () {
                if($(this).prop('checked')){
                    $(this).siblings('ul').find('.checkbox-element').prop('checked',true);
                } else {
                    $(this).siblings('ul').find('.checkbox-element').prop('checked',false);
                }
            });

            $(".checkbox-section").on('change',function () {
                if($(this).prop('checked')){
                    $(this).siblings('ul').find('.checkbox-block').prop('checked',true);
                    $(this).siblings('ul').find('.checkbox-block').siblings('ul').find('.checkbox-element').prop('checked',true);
                } else {
                    $(this).siblings('ul').find('.checkbox-block').prop('checked',false);
                    $(this).siblings('ul').find('.checkbox-block').siblings('ul').find('.checkbox-element').prop('checked',false);
                }
            });

            $('.tree-checkbox input[type="checkbox"]').on('change',function () {
                checkAll();
            });

            $(".selected-all-btn").on('click',function () {
                selected(true);
            });

            $(".unselected-all-btn").on('click',function () {
                selected(false);
            });

            $(".arrow-element").on("click",function () {
                if($(this).parent('li').hasClass('open')){
                    $(this).parent('li').removeClass('open');
                    $(this).parent('li').children('ul').slideUp();
                } else {
                    $(this).parent('li').addClass('open');
                    $(this).parent('li').children('ul').slideDown();
                }
            });
        });
    </script>
@endpush    
