<div class="form-group row">
    <label class="col-md-3 text-right" for="name"><span style="color:red">*</span>Название</label>
    <div class="col-md-9">
        <input type="text" name="name" id="name" class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name', $model->name ?? '') }}">

        @if ($errors->has('name'))
            <span class="invalid-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="code">Код</label>
    <div class="col-md-9">
        <input type="text" name="code" id="code" class="form-control" value="{{$model->code}}">
    </div>
</div>

<div class="form-group row">
    <label class="col-md-3 text-right" for="description">Описание</label>
    <div class="col-md-9">
        <textarea
            name="description"
            id="description"
            class="summernote editor {{ $errors->has('description') ? ' is-invalid' : '' }}"
            cols="30"
            rows="10"
        >{{ old('description', $model->description) }}</textarea>
    </div>
</div>
