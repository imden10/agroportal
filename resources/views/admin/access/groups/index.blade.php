@extends('layouts.admin.app')

@section('content')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route('admin')}}">Панель управления</a></li>
            <li class="breadcrumb-item active" aria-current="page">Группы пользователей</li>
        </ol>
    </nav>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                @permission('admins__user_groups__create')
                    <a href="{{route('access-groups.create')}}" class="btn btn-primary float-right">
                        <i class="fa fa-plus"></i>
                        Добавить
                    </a>
                @endpermission
            </div>
            <div class="card-body">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Название</th>
                            <th>Код</th>
                            <th style="width: 150px"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($model as $item)
                            <tr data-id="{{$item->id}}">
                                <td>
                                    @permission('admins__user_groups__edit')
                                    <a href="{{ route('access-groups.edit', $item->id) }}" title="Редактировать">
                                        {{$item->name}}
                                    </a>
                                    @else
                                        {{$item->name}}
                                    @endpermission
                                </td>
                                <td>{{$item->code}}</td>
                                <td>
                                    <div style="display: flex">
                                        <div style="margin-left: 10px">
                                            <form action="{{ route('access-groups.destroy', $item->id) }}" method="POST">

                                                @permission('admins__user_groups__edit')
                                                <a href="{{ route('access-groups.edit', $item->id) }}" class="btn btn-primary text-white btn-xs" title="Редактировать">
                                                    <i class="fas fa-edit  fa-lg"></i>
                                                </a>
                                                @endpermission&nbsp;

                                                @csrf
                                                @method('DELETE')

                                                @permission('admins__user_groups__delete')
                                                <a href="javascript:void(0)" title="Удалить" class="btn btn-danger delete-item-btn text-white btn-xs">
                                                    <i class="fas fa-trash"></i>
                                                </a>
                                                @endpermission&nbsp;
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                {{ $model->appends(request()->all())->links() }}
            </div>
        </div>
    </div>
</div>
@endsection

@push('styles')
    <style>
        nav.breadcrumb-nav {
            position: relative;
        }
        nav.breadcrumb-nav a.btn {
            position: absolute;
            right: 15px;
            top: 4px;
        }
    </style>
@endpush

@push('scripts')
    <script>
        $(document).ready(() => {
            $('.delete-item-btn').on('click',function() {
                if(confirm('You definitely want to delete the user?')){
                    $(this).closest('form').submit();
                }
            });
        });
    </script>
@endpush
