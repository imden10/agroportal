@if($action === 'create')
    <form class="form-horizontal" method="POST" action="{{ route('access-groups.store') }}">
@elseif($action === 'edit')
    <form action="{{route('access-groups.update', $model->id)}}" class="form-horizontal" method="post">
        @method('PUT')
@endif
    @csrf
        <div class="card">

            <div class="card-header">
                <ul class="nav nav-tabs" id="myTab" role="tablist" style="margin-bottom: -10px;border-bottom: none">
                    <li class="nav-item">
                        <a class="nav-link active" id="main-tab" data-toggle="tab" href="#main" role="tab" aria-controls="main" aria-selected="true">Информация</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="permission-tab" data-toggle="tab" href="#permission" role="tab" aria-controls="permission" aria-selected="false">Привилегии</a>
                    </li>
                </ul>
            </div>

            <div class="card-body">
                <div class="tab-content" id="myTabContent">
                    {{----------------------------- MAIN TAB -----------------------------------------}}
                    <div class="tab-pane fade show active" id="main" role="tabpanel" aria-labelledby="main-tab">
                        @include('admin.access.groups.tabs._main',[
                            'model' => $model, 'action' => $action
                        ])
                    </div>

                    {{----------------------------- SEO TAB -----------------------------------------}}
                    <div class="tab-pane fade" id="permission" role="tabpanel" aria-labelledby="permission-tab">
                        @include('admin.access.groups.tabs._pemission',[
                            'model' => $model, 'action' => $action
                        ])
                    </div>
                </div>

                <input type="submit" value="Сохранить" class="btn btn-success text-white float-right">

            </div>
        </div>


{{--        <div class="form-group row">--}}
{{--            <label class="col-md-3 text-right" for="name"><span style="color:red">*</span>Ім'я</label>--}}
{{--            <div class="col-md-9">--}}
{{--                <input type="text" name="name" id="name" class="form-control" value="{{$model->name}}">--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="form-group row">--}}
{{--            <label class="col-md-3 text-right" for="email"><span style="color:red">*</span> E-mail</label>--}}
{{--            <div class="col-md-9">--}}
{{--                <input type="text" name="email" id="email" class="form-control" value="{{$model->email}}">--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="form-group row">--}}
{{--            <label class="col-md-3 text-right" for="phone">Телефон</label>--}}
{{--            <div class="col-md-9">--}}
{{--                <input type="text" name="phone" id="phone" class="form-control" value="{{$model->phone}}">--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <div class="form-group row">--}}
{{--            <label class="col-md-3 text-right" for="password">--}}
{{--                @if(! $model->id)--}}
{{--                <span style="color:red">*</span>--}}
{{--                @endif--}}
{{--                Пароль</label>--}}
{{--            <div class="col-md-9">--}}
{{--                <input type="password" name="password" id="password" class="form-control">--}}
{{--            </div>--}}
{{--        </div>--}}

{{--        <input type="submit" class="btn btn-success text-white" value="Зберегти">--}}
</form>

@push('scripts')
    <script>
        $(document).ready(() => {

        });
    </script>
@endpush
