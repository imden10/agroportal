<?php

use Illuminate\Support\Facades\Request;

$subCatId = 0;

if(request()->has('category')){
    $cat = \App\Models\Category::query()->where('id',request()->get('category'))->first();
    if($cat){
        $subCatId = $cat->parent ? $cat->parent->id : 0;
    }
}
?>

<aside class="left-sidebar" data-sidebarbg="skin5">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav" class="pt-4">

                @permission('dashboard__dashboard__view')
                <li class="sidebar-item @if(in_array(Request::segment(1),['admin']) && Request::segment(2) == '') active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('admin')}}" aria-expanded="false">
                        <i class="mdi mdi-blur-linear"></i>
                        <span class="hide-menu">Панель управления</span>
                    </a>
                </li>
                @endpermission

                @permission(['admins__users__view','admins__user_groups__view'])
                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-account"></i>
                        <span class="hide-menu">Пользователи</span>
                    </a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        @permission('admins__users__view')
                        <li class="sidebar-item @if(in_array(Request::segment(2),['users-admin'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('users-admin.index')}}" aria-expanded="false">
                                <i class="mdi mdi-account"></i>
                                <span class="hide-menu">Пользователи</span>
                            </a>
                        </li>
                        @endpermission

                        @permission('admins__user_groups__view')
                        <li class="sidebar-item @if(in_array(Request::segment(2),['access-groups'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('access-groups.index')}}" aria-expanded="false">
                                <i class="mdi mdi-group"></i>
                                <span class="hide-menu">Группы пользователей</span>
                            </a>
                        </li>
                        @endpermission
                    </ul>
                </li>
                @endpermission

                @permission('pages__pages__view')
                <li class="sidebar-item @if(in_array(Request::segment(2),['pages'])) active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('pages.index')}}" aria-expanded="false">
                        <i class="mdi mdi-book-open-page-variant"></i>
                        <span class="hide-menu">Страницы</span>
                    </a>
                </li>
                @endpermission

                @permission('landings__landings__view')
                <li class="sidebar-item @if(in_array(Request::segment(2),['landing'])) active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('landing.index')}}" aria-expanded="false">
                        <i class="mdi mdi-book-open"></i>
                        <span class="hide-menu">Лендинги</span>
                    </a>
                </li>
                @endpermission

                @permission('tags__tags__view')
                <li class="sidebar-item @if(in_array(Request::segment(2),['tag'])) active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('tag.index')}}" aria-expanded="false">
                        <i class="mdi mdi-tag"></i>
                        <span class="hide-menu">Теги</span>
                    </a>
                </li>
                @endpermission


                @permission(['categories__categories__view','categories__articles__view','categories__categories__create'])
                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark @if(Request::segment(3) === 'articles') active @endif" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-newspaper"></i>
                        <span class="hide-menu">Рубрики</span>
                    </a>

                    <?php
                        $tree = app(\App\Models\Category::class)->treeStructure();
                    ?>

                    <ul aria-expanded="false" class="collapse  first-level categories-ul @if(Request::segment(3) === 'articles') in @endif">
                        @permission('categories__articles__view')
                        <li @if((Request::segment(3) === 'articles' && ! request()->has('category')) || (request()->has('category') && (request()->get('category') == '-1') || request()->get('category') == ''))) class="active2" @endif>
                            <a href="{{route('articles.index')}}"><i class="mdi mdi-message-outline"></i>Все публикации</a>
                        </li>
                        @endpermission

                        @permission('categories__categories__create')
                        <li @if(Request::segment(2) === 'categories' && Request::segment(3) === 'create') class="active2" @endif >
                            <a href="{{route('categories.create')}}"><i class="fa fa-plus"></i>Добавить рубрику</a>
                        </li>
                        @endpermission

                        @permission('categories__categories__view')
                        @if(count($tree))
                            @foreach($tree as $item)
                                <li @if(Request::segment(3) === 'articles' && request()->get('category') == $item['id']) class="active" @endif>
                                    <a href="/admin/category/{{$item['path']}}" class="active__{{$item['active']}}">
                                        <i
                                            data-id="{{$item['id']}}"
                                            class="fa @if(count($item['children'])) @if($item['collapse'] || $subCatId === $item['id']) fa-minus-square-o has-sub @else fa-plus-square-o has-sub @endif @else fa-square-o @endif"></i>
                                        {{$item['title']}}
                                    </a>

                                    @if(count($item['children']))
                                        <ul class="sub" @if(! $item['collapse'] && $subCatId !== $item['id']) style="display: none" @endif>
                                            @foreach($item['children'] as $child)
                                                <li @if(Request::segment(3) === 'articles' && request()->get('category') == $child['id']) class="active" @endif>
                                                    <a href="/admin/category/{{$child['path']}}" class="active__{{$child['active']}}">
                                                        <i class="fa fa-square-o"></i>{{$child['title']}}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                            @endforeach
                        @endif
                        @endpermission
                    </ul>
                </li>
                @endpermission

                @permission('categories__subscribe__view')
                <li class="sidebar-item @if(in_array(Request::segment(2),['subscribe'])) active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('subscribe.index')}}" aria-expanded="false">
                        <i class="mdi mdi-email"></i>
                        <span class="hide-menu">Подписка</span>
                    </a>
                </li>
                @endpermission

                @permission('bloggers__bloggers__view')
                <li class="sidebar-item @if(in_array(Request::segment(2),['bloggers'])) active @endif">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('bloggers.index')}}" aria-expanded="false">
                        <i class="mdi mdi-account-box"></i>
                        <span class="hide-menu">Страница автора</span>
                    </a>
                </li>
                @endpermission

                @permission(['banners__banners__view','banners__stat__view'])
                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-image-filter-tilt-shift"></i>
                        <span class="hide-menu">Баннеры</span>
                    </a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        @permission('banners__banners__view')
                        <li class="sidebar-item @if(in_array(Request::segment(2),['banners'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('banners.index')}}" aria-expanded="false">
                                <i class="mdi mdi-image-filter-tilt-shift"></i>
                                <span class="hide-menu">Баннеры</span>
                            </a>
                        </li>
                        @endpermission

                        @permission('banners__stat__view')
                        <li class="sidebar-item @if(in_array(Request::segment(2),['banner-stat'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('banner-stat.index')}}" aria-expanded="false">
                                <i class="mdi mdi-chart-line"></i>
                                <span class="hide-menu">Статистика</span>
                            </a>
                        </li>
                        @endpermission
                    </ul>
                </li>
                @endpermission

                @permission('fm__fm__view')
                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-image-album"></i>
                        <span class="hide-menu">Мультимедиа</span>
                    </a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        <li class="sidebar-item @if(in_array(Request::segment(3),['images'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('multimedia.images')}}" aria-expanded="false">
                                <i class="mdi mdi-checkbox-blank-circle-outline"></i>
                                <span class="hide-menu">Изображения</span>
                            </a>
                        </li>
                        <li class="sidebar-item @if(in_array(Request::segment(3),['files'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('multimedia.files')}}" aria-expanded="false">
                                <i class="mdi mdi-checkbox-blank-circle-outline"></i>
                                <span class="hide-menu">Файлы</span>
                            </a>
                        </li>
                    </ul>
                </li>
                @endpermission

                @permission(['appearance__widgets__view','appearance__menu__view'])
                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-format-paint"></i>
                        <span class="hide-menu">Внешний вид</span>
                    </a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        @permission('appearance__widgets__view')
                        <li class="sidebar-item @if(in_array(Request::segment(2),['widgets'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/admin/widgets" aria-expanded="false">
                                <i class="mdi mdi-widgets"></i>
                                <span class="hide-menu">Виджеты</span>
                            </a>
                        </li>
                        @endpermission

                        @permission('appearance__menu__view')
                        <li class="sidebar-item @if(in_array(Request::segment(2),['menu'])) active @endif">
                            <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{route('menu.index')}}" aria-expanded="false">
                                <i class="mdi mdi-menu"></i>
                                <span class="hide-menu">Меню</span>
                            </a>
                        </li>
                        @endpermission
                    </ul>
                </li>
                @endpermission

                @permission(['settings__main__view','settings__contacts__view','settings__blog__view','settings__pages__view'])
                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                        <i class="mdi mdi-settings"></i>
                        <span class="hide-menu">Настройки</span>
                    </a>
                    <ul aria-expanded="false" class="collapse  first-level">
                        @foreach(\App\Models\Settings::getTabNames() as $tabCode => $tabName)
                            @permission('settings__'.$tabCode.'__view')
                            <li class="sidebar-item @if(in_array(Request::segment(4),[$tabCode])) active @endif">
                                <a class="sidebar-link waves-effect waves-dark sidebar-link" href="/admin/settings/{{$tabCode}}" aria-expanded="false">
                                    <i class="mdi mdi-checkbox-blank-circle-outline"></i>
                                    <span class="hide-menu">{{$tabName}}</span>
                                </a>
                            </li>
                            @endpermission
                        @endforeach
                    </ul>
                </li>
                @endpermission
            </ul>
        </nav>

    </div>
</aside>

<style>
    @media (max-width: 768px) {
        .page-wrapper .card {
            min-width: 100%;
            overflow: scroll;
        }
    }

    /* for widgets index */
    @media (max-width: 576px) {
        .widgets-card-header button[type="submit"]{
            margin-top: 10px;
            margin-bottom: 10px;
            margin-left: 1px !important;
        }

        .widgets-card-header .input-search-widget-container {
            display: initial !important;
            float: initial !important;
        }

        .note-btn-group .note-btn {
            font-size: 10px !important;
        }

        .tree-node-inner .btns {
            margin-right: 0px;
        }

        .tree-node-inner .btns>span {
            display: none;
        }

        .tree-node-inner .btns .fa-chevron-up {
            display: none;
        }

        .tree-node-inner .btns .fa-chevron-down {
            display: none;
        }

        .card-body-menu-mob {
            padding: 5px !important;
        }
    }

    @media (min-width: 768px) {
        #main-wrapper.mini-sidebar ul.categories-ul {
            display: none;
        }

        #main-wrapper[data-sidebartype="mini-sidebar"] ul.categories-ul {
            display: none;
        }
    }

    ul.categories-ul {
        margin-left: 25px;
    }

    .categories-ul li {
        margin: 5px 0;
    }

    .categories-ul li.active a {
        opacity: 1;
        transition: all 0.1s ease-out;
        text-decoration: underline;
    }

    .categories-ul li ul li.active a {
        opacity: 1;
        transition: all 0.1s ease-out;
        text-decoration: underline;
    }

    .categories-ul li.active2 a {
        text-decoration: underline;
        opacity: 1;
    }

    .categories-ul > li a {
        color: #141b1e;
        opacity: 0.6;
        transition: all 0.1s ease-out;
    }

    .categories-ul > li a:hover {
        opacity: 1;
        transition: all 0.1s ease-out;
        text-decoration: underline;
    }

    .categories-ul > li a i {
        margin-right: 5px;
        opacity: 0.9;
        font-size: 18px;
        display: inline-block;
        vertical-align: middle;
        margin-top: -2px;
    }

    .categories-ul > li .sub {
        margin-left: 15px;
    }

    .categories-ul > li .sub li a {
        color: #141b1e;
        opacity: 0.6;
        transition: all 0.1s ease-out;
    }

    .categories-ul > li .sub li a:hover {
        opacity: 1;
        transition: all 0.1s ease-out;
        text-decoration: underline;
    }

    .categories-ul > li .sub li a i {
        margin-right: 5px;
        opacity: 0.9;
        font-size: 18px;
        display: inline-block;
        vertical-align: middle;
        margin-top: -2px;
    }

    .categories-ul li a.active__,
    .categories-ul li ul li a.active__
    {
        color: #f44336;
    }
</style>
