<div class="quoteWrp">
    @if (!empty($content['title']))
        <h3>{{ $content['title'] }}</h3>
    @endif
    <div class="quote">
        {!! $content['text'] !!}
        <img src="{{get_image_uri($content['image'], 'original')}}" alt="{{$content['title']}}">
    </div>
</div>
