<div class="gallery">
    @if(! empty($content['title']))
        <h2>{!! $content['title'] !!}</h2>
    @endif
    @if(! empty($content['list']) && count($content['list']))
    <div class="masonryWrap">
        @foreach($content['list'] as $item)
            <div class="itemWraper">
                <img src="{{get_image_uri($item['image'],'original')}}" alt="">
                <span>{{$item['description']}}</span>
            </div>
        @endforeach
    </div>
    @endif
</div>
