<div class="images">
    @if(! empty($content['title']))
        <h2>{!! $content['title'] !!}</h2>
    @endif
    @if(! empty($content['list']) && count($content['list']))
    <div class="masonryWrap">
        @foreach($content['list'] as $item)
            <a href="{{$item['link']}}" class="itemWraper">
                <img src="{{get_image_uri($item['image'],'original')}}" alt="">
                <span>{{$item['description']}}</span>
            </a>
        @endforeach
    </div>
    @endif
</div>
