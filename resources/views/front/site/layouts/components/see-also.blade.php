@foreach($content['list'] as $item)
<div data-name="see-also">
    <a href="{{ $item['url'] }}">
        <img src="{{$item['image_full']}}" alt="{{$item['title']}}">
        <span>{{$item['title']}}</span>
    </a>
</div>
@endforeach

