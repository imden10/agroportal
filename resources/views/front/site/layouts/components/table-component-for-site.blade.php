@if(! empty($content['title']))
    <h3>{!! $content['title'] !!}</h3>
@endif

@if(isset($content['rows']) && is_array($content['rows']) && count($content['rows']))
    <table>
        <tbody>
        @foreach($content['rows'] as $row)
            <tr>
                @foreach($row as $item)
                    <td>{!! $item['column_text'] !!}</td>
                @endforeach
            </tr>
        @endforeach
        </tbody>
    </table>
@endif
