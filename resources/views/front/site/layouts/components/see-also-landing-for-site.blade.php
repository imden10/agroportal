@isset($content['title'])
<span>{{$content['title']}}</span>
@endisset
@foreach($content['list'] as $item)
<div>
    <a href="{{ $item['link'] ?? $item['url'] }}">
        <img src="{{get_image_uri($item['image'])}}" alt="">
        {!! $item['text'] !!}
    </a>
</div>
@endforeach

