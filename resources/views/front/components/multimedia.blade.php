@if(isset($data['article_multimedia']['block1']))
    <a href="{{$data['article_multimedia']['block1']['category']['url']}}">
        <h2>{{$data['article_multimedia']['block1']['category']['name']}}</h2>
    </a>
@endif

@isset($data['article_multimedia']['block1']['items'])
    @if(count($data['article_multimedia']['block1']['items']))
        <ul>
            @foreach($data['article_multimedia']['block1']['items'] as $item)
                <a href="{{$item['url']}}">
                    <img src="{{get_image_uri($item['image'])}}" alt="">
                    <span>{{$item['public_date']}}</span>
                    <span>{{$item['views']}}</span>
                    <p>{{$item['name']}}</p>
                </a>
            @endforeach
        </ul>
    @endif
@endisset
