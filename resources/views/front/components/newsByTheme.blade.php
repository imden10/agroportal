@isset($data['news_by_theme'])
    @if(count($data['news_by_theme']))
        <ul>
            @foreach($data['news_by_theme'] as $item)
                <a href="{{$item['model']['url']}}">
                    <img src="{{get_image_uri($item['model']['image'])}}" alt="">
                    <span>{{$item['model']['public_date']}}</span>
                    <span>{{$item['model']['views']}}</span>
                    <p>{{$item['translate']['name']}}</p>
                    <p>{!! $item['translate']['annot'] !!}</p>
                </a>

                @if(isset($item['translate']['tags_obj']) && count($item['translate']['tags_obj']))
                    <ul>
                        @foreach($item['translate']['tags_obj'] as $tag)
                            <li>
                                <a href="{{$tag['url']}}">{{$tag['name']}}</a>
                            </li>
                        @endforeach
                    </ul>
                @endif
            @endforeach
        </ul>
    @endif
@endisset
