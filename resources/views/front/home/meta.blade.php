<?php
$segment = 1;
$allLangs = ['ru', 'uk'];
$route = request()->segment($segment);

if (in_array($route, $allLangs)) {
    $segment = 2;
}

$route = request()->segment($segment);
$slug = request()->segment($segment + 1);

$data = [];

$url = $_SERVER['REQUEST_URI'];
$pathInfo = pathinfo($url);
if(isset($pathInfo['extension'])){
    return;
}

$defaultImage = cache()->remember('get_default_image',(60 * 60 * 24), function (){
   return get_image_uri(app(Setting::class)->get('default_og_image', \App\Models\Langs::getDefaultLangCode()));
});

switch ($route) {
    case null:
        /* главная страница */
        $model = \App\Models\Pages::query()->where('id', 1)->first();

        if (!$model)
            break;

        $title = $model->getTranslation(app()->getLocale())->meta_title ?? '';

        $mr = '<script type="application/ld+json">
        {
          "@context": "http://schema.org/",
          "@type": "Organization",
          "name": "'.$title.'",
          "logo": "'.$defaultImage.'",
          "url": "https://agroportal.ua/",
          "address": {
            "@type": "PostalAddress",
            "addressCountry": "Ukraine",
            "email": "npomianska@gmail.com"
          },
          "sameAs": ["https://www.facebook.com/AgroPortalUa", "https://www.instagram.com/agroportal.ua/", "https://www.youtube.com/channel/UCnuqno49s9C8SJTZAUzTDFQ"]
        }
        </script>';

        $data = [
            'title'       => $title,
            'description' => $model->getTranslation(app()->getLocale())->meta_description ?? '',
            'image'       => null,
            'mr'          => $mr
        ];

        $constructor = $model->getTranslation(app()->getLocale())->constructor->data;

        $text = '';

        if (count($constructor)) {
            $text .= $constructor[1]['content']['list'][1]['item'] ?? '';
            $text .= $constructor[1]['content']['list'][2]['item'] ?? '';
        }

        $body = [
            'type'  => 'main_page',
            'title' => $model->getTranslation(app()->getLocale())->title ?? '',
            'description'  => $model->getTranslation(app()->getLocale())->description ?? '',
            'text'  => $text,
        ];

        request()->session()->flash('body_data', json_encode($body,JSON_UNESCAPED_UNICODE));

        break;
    case 'authors':
        $model = \App\Models\Bloggers::query()->where('slug', $slug)->first();

        if (!$model)
            break;

        $data = [
            'title'       => $model->getTranslation(app()->getLocale())->meta_title??'',
            'description' => $model->getTranslation(app()->getLocale())->meta_description??'',
            'image'       => get_image_uri($model->image)
        ];
        break;
    case 'project':
        $model = \App\Models\Landing::query()->where('slug', $slug)->first();

        if (!$model)
            break;

        $data = [
            'title'       => $model->getTranslation(app()->getLocale())->meta_title??'',
            'description' => $model->getTranslation(app()->getLocale())->meta_description??'',
            'image'       => get_image_uri($model->image)
        ];
        break;
    case 'views':
        if ($slug === 'blogs') {
            $model = \App\Models\Category::query()->where('slug', $slug)->first();

            if (!$model)
                break;

            $data = [
                'title'       => $model->getTranslation(app()->getLocale())->meta_title??'',
                'description' => $model->getTranslation(app()->getLocale())->meta_description??'',
                'image'       => get_image_uri($model->image)
            ];
        }
        break;
    case 'search':
        $urlCurrent = url()->current();
        $q = request()->input('q',"");

        $q = str_replace(['?',' '],['','-'],$q);

        $lng = request()->segment(1);

        if(in_array($lng,$allLangs) && $lng !== \App\Models\Langs::getDefaultLangCode()){
            $ruUrl = $urlCurrent;
            $defaultUrl = str_replace('/' . $lng,'',$ruUrl);
        } else {
            $defaultUrl = $urlCurrent;
            $urlAfterDomain = request()->path();

            $r = '/ru/' . $urlAfterDomain;
            $r = str_replace('//','',$r);
            $ruUrl = env('APP_URL') . $r;
        }

        $defaultUrl .= '?q=' . $q;
        $ruUrl .= '?q=' . $q;

        $page = request()->input('page',"1");

        $afterTitle = '';

        switch ($page){
            case "1":
                $canonical = PHP_EOL . "<link rel='canonical' href='".$urlCurrent."?q=".$q."'>" . PHP_EOL;
                $canonical .= "<link rel='alternate' hreflang='uk' href='".$defaultUrl."'>" . PHP_EOL;
                $canonical .= "<link rel='alternate' hreflang='ru' href='".$ruUrl."'>" . PHP_EOL;
                $canonical .= "<link rel='next' href='".$urlCurrent."?q=".$q."&page=2'>";
                break;
            case "2":
                $canonical = PHP_EOL . "<link rel='canonical' href='".$urlCurrent."?q=".$q."'>" . PHP_EOL;
                $canonical .= "<link rel='alternate' hreflang='uk' href='".$defaultUrl."&page=2'>" . PHP_EOL;
                $canonical .= "<link rel='alternate' hreflang='ru' href='".$ruUrl."&page=2'>" . PHP_EOL;
                $canonical .= "<link rel='prev' href='".$urlCurrent."?q=".$q."'>" . PHP_EOL;
                $canonical .= "<link rel='next' href='".$urlCurrent."?q=".$q."&page=3'>";
                break;
            default:
                $canonical = PHP_EOL . "<link rel='canonical' href='".$urlCurrent."?q=".$q."'>" . PHP_EOL;
                $canonical .= "<link rel='alternate' hreflang='uk' href='".$defaultUrl."&page=".$page."'>" . PHP_EOL;
                $canonical .= "<link rel='alternate' hreflang='ru' href='".$ruUrl."&page=".$page."'>" . PHP_EOL;
                $canonical .= "<link rel='prev' href='".$urlCurrent."?q=".$q."&page=".($page - 1)."'>" . PHP_EOL;
                $canonical .= "<link rel='next' href='".$urlCurrent."?q=".$q."&page=".($page + 1)."'>";
                break;
        }
        break;
    case 'tags':
        $tagSlug =$slug;

        $model = \App\Models\NewTag::query()->where('slug', $tagSlug)->first();

        if (!$model)
            break;

        $urlCurrent = url()->current();

        $lng = request()->segment(1);

        if(in_array($lng,$allLangs) && $lng !== \App\Models\Langs::getDefaultLangCode()){
            $ruUrl = $urlCurrent;
            $defaultUrl = str_replace('/' . $lng,'',$ruUrl);
        } else {
            $defaultUrl = $urlCurrent;
            $urlAfterDomain = request()->path();

            $r = '/ru/' . $urlAfterDomain;
            $r = str_replace('//','',$r);
            $ruUrl = env('APP_URL') . $r;
        }

        $page = request()->input('page',"1");

        $afterTitle = '';

        switch ($page){
            case "1":
                $canonical = PHP_EOL . "<link rel='canonical' href='".$urlCurrent."'>" . PHP_EOL;
                $canonical .= "<link rel='alternate' hreflang='uk' href='".$defaultUrl."'>" . PHP_EOL;
                $canonical .= "<link rel='alternate' hreflang='ru' href='".$ruUrl."'>" . PHP_EOL;
                $canonical .= "<link rel='next' href='".$urlCurrent."&page=2'>";
                break;
            case "2":
                $canonical = PHP_EOL . "<link rel='canonical' href='".$urlCurrent."'>" . PHP_EOL;
                $canonical .= "<link rel='alternate' hreflang='uk' href='".$defaultUrl."&page=2'>" . PHP_EOL;
                $canonical .= "<link rel='alternate' hreflang='ru' href='".$ruUrl."&page=2'>" . PHP_EOL;
                $canonical .= "<link rel='prev' href='".$urlCurrent."'>" . PHP_EOL;
                $canonical .= "<link rel='next' href='".$urlCurrent."&page=3'>";
                break;
            default:
                $canonical = PHP_EOL . "<link rel='canonical' href='".$urlCurrent."'>" . PHP_EOL;
                $canonical .= "<link rel='alternate' hreflang='uk' href='".$defaultUrl."&page=".$page."'>" . PHP_EOL;
                $canonical .= "<link rel='alternate' hreflang='ru' href='".$ruUrl."&page=".$page."'>" . PHP_EOL;
                $canonical .= "<link rel='prev' href='".$urlCurrent."&page=".($page - 1)."'>" . PHP_EOL;
                $canonical .= "<link rel='next' href='".$urlCurrent."&page=".($page + 1)."'>";
                break;
        }

        $data = [
            'title'       => $model->getTranslation(app()->getLocale())->meta_title ?? env('APP_NAME'),
            'description' => $model->getTranslation(app()->getLocale())->meta_description??'',
            'image'       => null
        ];

        if($data['title'] !== '' && $page > 1){
            $data['title'] = $data['title'] . ' - ' . __('page') . ' ' . $page;
            $data['description'] = $data['description'] . ' - ' . __('page') . ' ' . $page;
        }

        break;
    default:
        $url = url()->current();
        $parts = explode('/',$url);
        $slug = end($parts);

        $model = \App\Models\BlogArticles::query()
            ->leftJoin('blog_article_translations', 'blog_article_translations.blog_articles_id', '=', 'blog_articles.id')
            ->where('blog_article_translations.lang', app()->getLocale())
            ->select([
                'blog_articles.*',
                'blog_article_translations.name AS articleName',
                'blog_article_translations.annot AS articleAnnot',
                'blog_article_translations.text AS articleText',
                'blog_article_translations.constructor_html AS articleConstructorHtml',
                'blog_article_translations.meta_title AS articleMetaTitle',
                'blog_article_translations.meta_description AS articleMetaDescription',
            ])
            ->where('blog_articles.slug', $slug)
            ->with('gallery')
            ->first();

        if($model){
            $h1 = $model->articleName;

            $mr = '<script type="application/ld+json">
                {
                  "@context": "http://schema.org/",
                  "@type": "Article",
                  "mainEntityOfPage": {
                    "@type": "WebPage",
                    "@id": "'.env('APP_URL').$_SERVER['REQUEST_URI'].'"
                  },
                  "author": {
                    "@type": "Organization",
                    "name": "AgroPortal"
                  },
                  "publisher": {
                    "@type": "Organization",
                    "name": "AgroPortal.ua",
                    "logo": {
                      "@type": "ImageObject",
                      "url": "'.$defaultImage.'"
                    }
                  },
                  "headline": "'.$h1.'",
                  "image": "'.get_image_uri($model->image).'",
                  "datePublished": "'.\Carbon\Carbon::parse($model->public_date)->format('Y-m-d').'"
                }
                </script>';

            $data = [
                'title'       => $model->articleMetaTitle ?? '',
                'description' => $model->articleMetaDescription ?? '',
                'image'       => get_image_uri($model->image),
                'mr'          => $mr
            ];

            /** multimedia block **/

            $multimediaView = '';

            if(cache()->has('getBySlugMultimedia_78_' . app()->getLocale())){
                $multimediaData =  cache('getBySlugMultimedia_78_' . app()->getLocale());
                $multimediaView = view('front.components.multimedia',['data' => $multimediaData])->render();
            }

            /** *******************************************************************************************************/

            /** News By Theme Block **/

            $newsByThemeView = '';

            if(cache()->has('getBySlugNewsByTheme_' . $model->id . '_' . app()->getLocale())){
                $newsByThemeData =  cache('getBySlugNewsByTheme_' . $model->id . '_' . app()->getLocale());
                $newsByThemeView = view('front.components.newsByTheme',['data' => $newsByThemeData])->render();
            }

            /** *******************************************************************************************************/

            $body = [
                'type'  => 'article',
                'name'  => $h1,
                'annot' => $model->articleAnnot,
                'text'  => $model->articleText,
                'constructor_html'  => $model->articleConstructorHtml,
                'multimedia'  => $multimediaView,
                'news_by_theme' => $newsByThemeView,
            ];

            request()->session()->flash('body_data', json_encode($body,JSON_UNESCAPED_UNICODE));

            break;
        }

        $model = \App\Models\Category::query()->where('slug', $slug)->first();

        if($model){
            $urlCurrent = url()->current();

            $lng = request()->segment(1);

            if(in_array($lng,$allLangs) && $lng !== \App\Models\Langs::getDefaultLangCode()){
                $ruUrl = $urlCurrent;
                $defaultUrl = str_replace('/' . $lng,'',$ruUrl);
            } else {
                $defaultUrl = $urlCurrent;
                $urlAfterDomain = request()->path();

                $r = '/ru/' . $urlAfterDomain;
                $r = str_replace('//','',$r);
                $ruUrl = env('APP_URL') . $r;
            }

            $page = request()->input('page',"1");

            $afterTitle = '';

            switch ($page){
                case "1":
                    $canonical = PHP_EOL . "<link rel='canonical' href='".$urlCurrent."'>" . PHP_EOL;
                    $canonical .= "<link rel='alternate' hreflang='uk' href='".$defaultUrl."'>" . PHP_EOL;
                    $canonical .= "<link rel='alternate' hreflang='ru' href='".$ruUrl."'>" . PHP_EOL;
                    $canonical .= "<link rel='next' href='".$urlCurrent."?page=2'>";
                break;
                case "2":
                    $canonical = PHP_EOL . "<link rel='canonical' href='".$urlCurrent."'>" . PHP_EOL;
                    $canonical .= "<link rel='alternate' hreflang='uk' href='".$defaultUrl."?page=2'>" . PHP_EOL;
                    $canonical .= "<link rel='alternate' hreflang='ru' href='".$ruUrl."?page=2'>" . PHP_EOL;
                    $canonical .= "<link rel='prev' href='".$urlCurrent."'>" . PHP_EOL;
                    $canonical .= "<link rel='next' href='".$urlCurrent."?page=3'>";
                break;
                default:
                    $canonical = PHP_EOL . "<link rel='canonical' href='".$urlCurrent."'>" . PHP_EOL;
                    $canonical .= "<link rel='alternate' hreflang='uk' href='".$defaultUrl."?page=".$page."'>" . PHP_EOL;
                    $canonical .= "<link rel='alternate' hreflang='ru' href='".$ruUrl."?page=".$page."'>" . PHP_EOL;
                    $canonical .= "<link rel='prev' href='".$urlCurrent."?page=".($page - 1)."'>" . PHP_EOL;
                    $canonical .= "<link rel='next' href='".$urlCurrent."?page=".($page + 1)."'>";
                break;
            }

            $data = [
                'title'       => $model->getTranslation(app()->getLocale())->meta_title??'',
                'description' => $model->getTranslation(app()->getLocale())->meta_description??'',
                'image'       => get_image_uri($model->image)
            ];

            if($data['title'] !== '' && $page > 1){
                $data['title'] = $data['title'] . ' - ' . __('page') . ' ' . $page;
                $data['description'] = $data['description'] . ' - ' . __('page') . ' ' . $page;
            }

            $body = [
                'type'  => 'category',
                'description' => $model->getTranslation(app()->getLocale())->description ?? '',
                'description_hide'  => $model->getTranslation(app()->getLocale())->description_hide ?? '',
            ];

            request()->session()->flash('body_data', json_encode($body,JSON_UNESCAPED_UNICODE));

            break;
        }

        $model = \App\Models\Pages::query()->where('slug', $slug)->first();

        if($model){
            $data = [
                'title'       => $model->getTranslation(app()->getLocale())->meta_title??'',
                'description' => $model->getTranslation(app()->getLocale())->meta_description??'',
                'image'       => get_image_uri($model->image)
            ];
            break;
        }
        break;
}
?>

@isset($data['title'])
    <title>{{$data['title']}}</title>
@else
    <title>{{env('APP_NAME')}}</title>
@endisset

@if(count($data))
    @if((! isset($data['image'])) || (isset($data['image']) && $data['image'] === env('APP_URL') . '/media/images/original/images/no-image.png'))
        <?php $data['image'] = $defaultImage; ?>
    @endif

    @isset($data['description'])
        <meta name="description" content="{{$data['description']}}">
    @endisset

    @isset($data['title'])
        <meta property="og:title" content="{{$data['title']}}"/>
    @else
        <meta property="og:title" content="{{env('APP_NAME')}}"/>
    @endisset

    @isset($data['description'])
        <meta property="og:description" content="{{$data['description']}}"/>
    @endisset

    @if($route !== 'tags')
        <meta property="og:url" content="{{url()->current()}}"/>
    @else
        <meta property="og:url" content="{{url()->current()}}"/>
    @endif
    <meta property="og:type" content="website"/>
    <meta property="og:locale" content="{{app()->getLocale()}}"/>
    <meta property="og:site_name" content="{{env('APP_NAME')}}"/>
    @isset($data['image'])
        <meta property="og:image" content="{{$data['image']}}"/>
    @endisset
    @if(isset($data['mr']) && $data['mr'] != '')
        {!! $data['mr'] !!}
    @endif
@endif

<?php

if(! isset($canonical)){
    $urlCurrent = url()->current();

    $lng = request()->segment(1);

    if(in_array($lng,$allLangs) && $lng !== \App\Models\Langs::getDefaultLangCode()){
        $ruUrl = $urlCurrent;
        $defaultUrl = str_replace('/' . $lng,'',$ruUrl);
    } else {
        $defaultUrl = $urlCurrent;
        $urlAfterDomain = request()->path();

        $r = '/ru/' . $urlAfterDomain;
        $r = str_replace('//','',$r);
        $ruUrl = env('APP_URL') . $r;
    }

    $canonical = "<link rel='canonical' href='".$urlCurrent."'>" . PHP_EOL;
    $canonical .= "<link rel='alternate' hreflang='uk' href='".$defaultUrl."'>" . PHP_EOL;
    $canonical .= "<link rel='alternate' hreflang='ru' href='".$ruUrl."'>";
}

echo $canonical;
?>

