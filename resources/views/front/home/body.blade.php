@if(\Illuminate\Support\Facades\Session::has('body_data'))
<div style="position: absolute; left: -100000px;top: -100000px">
    <?php
        $body = json_decode(request()->session()->get('body_data'),true);
        $menuData = null;

        if(cache()->has('menu_14_142' . app()->getLocale())){
            $menuData = cache('menu_14_142' . app()->getLocale());

            if(is_array($menuData) && count($menuData)){
                foreach ($menuData as $menu){
                    $mStr = '<ul>';
                    foreach ($menu['items'] as $item){
                        $mStr .= '<li><a href="'.$item['url'].'">'.$item['name'].'</a>';

                        if(isset($item['children']) && count($item['children'])){
                            $mStr .= '<ul>';
                            foreach ($item['children'] as $child){
                                $mStr .= '<li><a href="'.$child['url'].'">'.$child['name'].'</a>';
                            }
                            $mStr .= '</ul>';
                        }

                        $mStr .= '</li>';
                    }

                    $mStr .= '</ul>';
                    echo $mStr;
                }
            }
        }

        if(is_array($body) && array_key_exists('type',$body)){
            switch($body['type']){
                case 'article':
                    echo $body['annot'] ?? '';
                    echo $body['text'] ?? '';
                    echo $body['constructor_html'] ?? '';
                    echo $body['multimedia'] ?? '';
                    echo $body['news_by_theme'] ?? '';

                    break;
                case 'category':
                    echo isset($body['description']) ? mb_str_replace('скрыть весь текст внизу','',$body['description']) : '';
                    echo isset($body['description_hide']) ? mb_str_replace('скрыть весь текст внизу','',$body['description_hide']) : '';

                    break;
                case 'main_page':
                    echo $body['title'] ?? '';
                    echo $body['description'] ?? '';
                    echo $body['text'] ?? '';

                    break;
            }
        }
    ?>
</div>
@endif
